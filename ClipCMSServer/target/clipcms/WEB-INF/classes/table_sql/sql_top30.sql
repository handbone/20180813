-- Table: point.tbl_stat_top_user

-- DROP TABLE point.tbl_stat_top_user;

CREATE TABLE point.tbl_stat_top_user
(
  media character varying(255) NOT NULL,
  stat_date character(8) NOT NULL,
  point_type character(1) NOT NULL,
  point numeric NOT NULL,
  rank integer NOT NULL,
  user_ci character varying(256) NOT NULL,
  cust_id character varying(256),
  ctn character varying(256),
  ga_id character varying(256),
  CONSTRAINT pk_tbl_stat_top_user PRIMARY KEY (media, stat_date, point_type, rank)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE point.tbl_stat_top_user
  OWNER TO point;

CREATE OR REPLACE FUNCTION point.fn_make_top_stat(curDate character, limitCount integer)
RETURNS BOOLEAN AS $$
	BEGIN
		-- 적립처리
		INSERT INTO point.tbl_stat_top_user (media, stat_date, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
		SELECT
			a.reg_source as media, 
			curDate, 
			'I' as point_type, 
			(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
			a.point,
			ui.user_ci, 
			ui.cust_id, 
			ui.ctn, 
			ui.ga_id 
		FROM
			(
				SELECT
					reg_source, 
					user_ci, 
					SUM(point_value) as point 
				FROM
					point."POINT_INFO_TBL"
				WHERE
					reg_date = curDate and 
					point_type = 'I' 
				GROUP BY reg_source, user_ci 
				ORDER BY point DESC
				LIMIT limitCount
			) as a,
			point."USER_INFO_TBL" as ui
		WHERE
			a.user_ci = ui.user_ci;

		-- 차감처리
		INSERT INTO point.tbl_stat_top_user (media, stat_date, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
		SELECT
			a.reg_source as media, 
			curDate, 
			'O' as point_type, 
			(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
			a.point,
			ui.user_ci, 
			ui.cust_id, 
			ui.ctn, 
			ui.ga_id 
		FROM
			(
				SELECT
					reg_source, 
					user_ci, 
					SUM(point_value) as point 
				FROM
					point."POINT_INFO_TBL"
				WHERE
					reg_date = curDate and 
					point_type = 'O' 
				GROUP BY reg_source, user_ci 
				ORDER BY point DESC
				LIMIT limitCount
			) as a,
			point."USER_INFO_TBL" as ui
		WHERE
			a.user_ci = ui.user_ci;

		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
$$
LANGUAGE PLPGSQL;

-- 일 단위 실행
select point.fn_make_top_stat('일자', 소팅갯수);
select point.fn_make_top_stat('20161030', 30);
-- 조회
select media, stat_date, point_type, rank, point, user_ci, cust_id, ctn, ga_id from point.tbl_stat_top_user where media = '' and stat_date = '';
