CREATE INDEX point_hist_tbl_01 ON "POINT_HIST_TBL" (reg_date, reg_source );
CREATE INDEX point_info_tbl_01 ON "POINT_INFO_TBL" (reg_date, reg_source );


CREATE TABLE cms_common_code
(
  code_idx serial NOT NULL,
  code_name character varying(256) NOT NULL,
  code_value character varying(256) NOT NULL,
  CONSTRAINT "PK_cms_common_code" PRIMARY KEY (code_idx)
)
WITH (
  OIDS=FALSE
);