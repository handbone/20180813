-- CREATE INDEX point_roulette_conn_log_index_sdate 
-- ON point_roulette_conn_log 
-- USING btree(sdate)

-- SELECT * FROM pg_indexes WHERE tablename = 'your_table'; 





-- Function: make_daily_stat(text)
-- DROP FUNCTION make_daily_stat(text);

CREATE OR REPLACE FUNCTION make_daily_stat(curdate text, nextdate text)
  RETURNS boolean AS
$BODY$
	DECLARE
		_balance record;
		_remove record;
		_input record;
		_output record;
		_pre record;
		today date;
	BEGIN
		select to_date(curDate,'YYYYMMDD') into today;
		select a_user,t_balance into _balance from make_daily_balance(nextdate) ;
		--RAISE NOTICE 'result = %',_balance.a_user;
		--RAISE NOTICE 'result = %',_balance.t_balance;
		select r_user,r_point into _remove from make_daily_remove(curDate);
		select i_count,i_point into _input from make_daily_input(curDate);
		select o_count,o_point into _output from make_daily_output(curDate);
		select p_count,p_point into _pre from make_daily_pre(curDate);




		insert into tbl_stat_daily_point(year,month,day,d_day,total_balance,input_point,output_point,remove_point,pre_point,input_count,output_count,remove_count,pre_count,active_user)
		values (substr(curDate,1,4),substr(curDate,5,2),substr(curDate,7,2),date_part('dow', today),_balance.t_balance,_input.i_point,_output.o_point,_remove.r_point,_pre.p_point,_input.i_count,_output.o_count,_remove.r_user,_pre.p_count,_balance.a_user);
		
		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION make_daily_stat(text, text)
  OWNER TO admin;


































-- Function: make_daily_balance()
-- DROP FUNCTION make_daily_balance();
CREATE OR REPLACE FUNCTION make_daily_balance(IN nextdate text)
  RETURNS TABLE(a_user bigint, t_balance numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		Select count(*),sum(balance) 
   		from "POINT_INFO_TBL" 
   		where point_hist_idx in (
			select max(p.point_hist_idx) 
			from 	"POINT_INFO_TBL" p,
				"USER_INFO_TBL" u
			where
				p.user_ci = u.user_ci 
				and u.status = 'A'
AND p.reg_date != nextdate 
			group by p.user_ci);
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_balance()
  OWNER TO admin;





  
  
  
  
  
  
  
  
  
  
  -- Function: make_monthly_stat(text)

-- DROP FUNCTION make_monthly_stat(text);

CREATE OR REPLACE FUNCTION make_monthly_stat(curdate text, nextdate text)
  RETURNS boolean AS
$BODY$
	DECLARE
		_balance record;
		_remove record;
		_input record;
		_output record;
		_pre record;
	BEGIN
		select a_user,t_balance into _balance from make_daily_balance(nextdate) ;
		--RAISE NOTICE 'result = %',_balance.a_user;
		--RAISE NOTICE 'result = %',_balance.t_balance;
		select r_user,r_point into _remove from make_daily_remove(curDate);
		select i_count,i_point into _input from make_daily_input(curDate);
		select o_count,o_point into _output from make_daily_output(curDate);
		select p_count,p_point into _pre from make_daily_pre(curDate);

		insert into tbl_stat_monthly_point(year,month,total_balance,input_point,output_point,remove_point,pre_point,input_count,output_count,remove_count,pre_count,active_user)
		values (substr(curDate,1,4),substr(curDate,5,2),_balance.t_balance,_input.i_point,_output.o_point,_remove.r_point,_pre.p_point,_input.i_count,_output.o_count,_remove.r_user,_pre.p_count,_balance.a_user);
		
		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION make_monthly_stat(text, text)
  OWNER TO admin;













-- Function: make_monthly_balance()

-- DROP FUNCTION make_monthly_balance();

CREATE OR REPLACE FUNCTION make_monthly_balance(IN nextdate text)
  RETURNS TABLE(a_user bigint, t_balance numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		Select count(*),sum(balance) 
   		from "POINT_INFO_TBL" 
   		where point_hist_idx in (
			select max(p.point_hist_idx) 
			from 	point."POINT_INFO_TBL" p,
				point."USER_INFO_TBL" u
			where
				p.user_ci = u.user_ci 
				and u.status = 'A'
				AND substr(p.reg_date, 1, 6) != nextdate 
			group by p.user_ci);
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_monthly_balance(text)
  OWNER TO admin;



  
  
  UPDATE tbl_stat_daily_point SET year = '2017' WHERE year = '2016'
  UPDATE tbl_stat_monthly_point SET year = '2017' WHERE year = '2016'
  
  http://localhost:8080/clipcms/makeDailyStat.do?curDate=20161114&nextDate=20161115
  http://localhost:8080/clipcms/makeMonthlyStat.do?curDate=201610&nextDate=201611
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  CREATE OR REPLACE FUNCTION fn_make_top_stat2(
    curdate character,
    limitcount integer)
  RETURNS boolean AS
$BODY$
	DECLARE _reg_sources text[];
	BEGIN

	
		_reg_sources := array(SELECT requester_code FROM "ACL_INFO_TBL" WHERE use_status = 'Y');


		FOR _i IN 1 .. array_upper(_reg_sources, 1) LOOP

			
			-- 적립처리
			INSERT INTO tbl_stat_top_user (media, stat_date, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
			SELECT
				(select requester_name from "ACL_INFO_TBL" where requester_code = a.reg_source ) as media,
				curDate, 
				'I' as point_type, 
				(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
				a.point,
				ui.user_ci, 
				ui.cust_id, 
				ui.ctn, 
				ui.ga_id 
			FROM
				( 
					SELECT
						reg_source, 
						user_ci, 
						SUM(point_value) as point 
					FROM
						"POINT_INFO_TBL"
					WHERE
						reg_source = _reg_sources[_i] and
						reg_date = curDate and 
						point_type = 'I' 
					GROUP BY reg_source, user_ci 
					ORDER BY point DESC
					LIMIT limitCount
					
				) as a,
				"USER_INFO_TBL" as ui
			WHERE
				a.user_ci = ui.user_ci;

			-- 차감처리
			INSERT INTO tbl_stat_top_user (media, stat_date, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
			SELECT
				(select requester_name from "ACL_INFO_TBL" where requester_code = a.reg_source ) as media,
				curDate, 
				'O' as point_type, 
				(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
				a.point,
				ui.user_ci, 
				ui.cust_id, 
				ui.ctn, 
				ui.ga_id 
			FROM
				(
					SELECT
						reg_source, 
						user_ci, 
						SUM(point_value) as point 
					FROM
						"POINT_INFO_TBL"
					WHERE
						reg_source = _reg_sources[_i] and
						reg_date = curDate and 
						point_type = 'O' 
					GROUP BY reg_source, user_ci 
					ORDER BY point DESC
					LIMIT limitCount
					
				) as a,
				"USER_INFO_TBL" as ui
			WHERE
				a.user_ci = ui.user_ci;

		END LOOP;

		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_make_top_stat2(character, integer)
  OWNER TO point;
  
  
  
  
  DELETE FROM tbl_stat_top_user WHERE stat_date = ? and media != 'unUsed'
  select fn_make_top_stat2(?, ?);
  /clipcms/makeTop30_2.do?curDate=20161102&limitCount=30
  
  
  
  
  
  
  

  
  ========== 이건 해야되는것인지 아닌지 알 수 없음
  ALTER TABLE "ACL_INFO_TBL" ADD COLUMN regdate timestamp with time zone
UPDATE "ACL_INFO_TBL"  SET regdate = NOW()
이미 있음
CREATE TABLE "BZSCREEN_POST_TBL"
(
  transaction_id character varying(256) NOT NULL,
  user_id character varying(256) NOT NULL,
  campaign_id bigint,
  campaign_name character varying(256),
  event_at bigint,
  is_media integer,
  extra character varying(512),
  action_type character(1),
  point bigint,
  base_point bigint,
  data character varying(256),
  reg_date character varying(8),
  reg_time character varying(6),
  CONSTRAINT "PK_BZSCREEN_POST_TBL" PRIMARY KEY (transaction_id, user_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "BZSCREEN_POST_TBL"
  OWNER TO point;


  