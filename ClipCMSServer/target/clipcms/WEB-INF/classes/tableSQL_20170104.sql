-- Table: "ACL_INFO_TBL"

-- DROP TABLE "ACL_INFO_TBL";

CREATE TABLE "ACL_INFO_TBL"
(
  requester_code character varying(255) NOT NULL,
  requester_name character varying(255) NOT NULL,
  ip_addr character varying(2000), -- 다중 아이피의 경우 ',' 로 구분/대역 아이피의 경우 '-' 로 대역 표시
  use_status character(1),
  description character varying(2000),
  regdate timestamp with time zone,
  CONSTRAINT "ACL_INFO_TBL_pkey" PRIMARY KEY (requester_code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "ACL_INFO_TBL"
  OWNER TO point;
COMMENT ON COLUMN "ACL_INFO_TBL".ip_addr IS '다중 아이피의 경우 '','' 로 구분/대역 아이피의 경우 ''-'' 로 대역 표시';









-- Table: "BZAD_POST_TBL"

-- DROP TABLE "BZAD_POST_TBL";

CREATE TABLE "BZAD_POST_TBL"
(
  app_key character varying(256) NOT NULL,
  campaign_id bigint,
  title character varying(256),
  user_id character varying(256) NOT NULL,
  point bigint,
  transaction_id character varying(256) NOT NULL,
  event_at bigint,
  extra character varying(512),
  is_media integer,
  action_type character(1),
  reg_date character varying(8),
  reg_time character varying(6),
  CONSTRAINT "PK_BZAD_POST_TBL" PRIMARY KEY (app_key, transaction_id, user_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "BZAD_POST_TBL"
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  -- Table: "BZSCREEN_POST_TBL"

-- DROP TABLE "BZSCREEN_POST_TBL";

CREATE TABLE "BZSCREEN_POST_TBL"
(
  transaction_id character varying(256) NOT NULL,
  user_id character varying(256) NOT NULL,
  campaign_id bigint,
  campaign_name character varying(256),
  event_at bigint,
  is_media integer,
  extra character varying(512),
  action_type character(1),
  point bigint,
  base_point bigint,
  data character varying(256),
  reg_date character varying(8),
  reg_time character varying(6),
  CONSTRAINT "PK_BZSCREEN_POST_TBL" PRIMARY KEY (transaction_id, user_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "BZSCREEN_POST_TBL"
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: "POINT_HIST_TBL"

-- DROP TABLE "POINT_HIST_TBL";

CREATE TABLE "POINT_HIST_TBL"
(
  point_hist_idx serial NOT NULL,
  user_ci character varying(256),
  cust_id character varying(256),
  ctn character varying(256),
  ga_id character varying(256),
  transaction_id character varying(256) NOT NULL,
  point_value bigint,
  description character varying(1024),
  status character(1),
  reg_source character varying(30) NOT NULL,
  reg_date character varying(8),
  reg_time character varying(6),
  chg_date character varying(8),
  chg_time character varying(6),
  point_type character(1),
  CONSTRAINT "PK_POINT_HIST_TBL" PRIMARY KEY (point_hist_idx),
  CONSTRAINT "POINT_HIST_TBL_transaction_id_key" UNIQUE (transaction_id, reg_source)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "POINT_HIST_TBL"
  OWNER TO point;

-- Index: "IDX_POINT_HIST_TBL_01"

-- DROP INDEX "IDX_POINT_HIST_TBL_01";

CREATE INDEX "IDX_POINT_HIST_TBL_01"
  ON "POINT_HIST_TBL"
  USING btree
  (user_ci COLLATE pg_catalog."default", cust_id COLLATE pg_catalog."default", ctn COLLATE pg_catalog."default", ga_id COLLATE pg_catalog."default", status COLLATE pg_catalog."default");

-- Index: point_hist_tbl_01

-- DROP INDEX point_hist_tbl_01;

CREATE INDEX point_hist_tbl_01
  ON "POINT_HIST_TBL"
  USING btree
  (reg_date COLLATE pg_catalog."default", reg_source COLLATE pg_catalog."default");


  
  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: "POINT_INFO_TBL"

-- DROP TABLE "POINT_INFO_TBL";

CREATE TABLE "POINT_INFO_TBL"
(
  user_ci character varying(256) NOT NULL,
  point_hist_idx serial NOT NULL,
  point_type character(1),
  point_value bigint,
  balance bigint,
  description character varying(1024),
  reg_date character varying(8),
  reg_time character varying(6),
  point_info_idx serial NOT NULL,
  reg_source character varying(30) NOT NULL,
  reg_date_timestamp timestamp without time zone,
  exit_date timestamp without time zone,
  exit_gbn character(1),
  CONSTRAINT "PK_POINT_INFO_TBL" PRIMARY KEY (user_ci, point_hist_idx)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "POINT_INFO_TBL"
  OWNER TO point;

-- Index: "IDX_POINT_INFO_TBL_01"

-- DROP INDEX "IDX_POINT_INFO_TBL_01";

CREATE INDEX "IDX_POINT_INFO_TBL_01"
  ON "POINT_INFO_TBL"
  USING btree
  (user_ci COLLATE pg_catalog."default", reg_date COLLATE pg_catalog."default", reg_time COLLATE pg_catalog."default");

-- Index: point_info_tbl_01

-- DROP INDEX point_info_tbl_01;

CREATE INDEX point_info_tbl_01
  ON "POINT_INFO_TBL"
  USING btree
  (reg_date COLLATE pg_catalog."default", reg_source COLLATE pg_catalog."default");

-- Index: point_info_tbl_02

-- DROP INDEX point_info_tbl_02;

CREATE INDEX point_info_tbl_02
  ON "POINT_INFO_TBL"
  USING btree
  (reg_date COLLATE pg_catalog."default", "substring"(reg_time::text, 1, 2) COLLATE pg_catalog."default", point_type COLLATE pg_catalog."default");

-- Index: point_info_tbl_03

-- DROP INDEX point_info_tbl_03;

CREATE INDEX point_info_tbl_03
  ON "POINT_INFO_TBL"
  USING btree
  (user_ci COLLATE pg_catalog."default", point_info_idx);


  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: "USER_INFO_TBL"

-- DROP TABLE "USER_INFO_TBL";

CREATE TABLE "USER_INFO_TBL"
(
  user_idx integer NOT NULL DEFAULT nextval('user_info_tbl_user_idx_seq'::regclass),
  user_ci character varying(256) NOT NULL,
  cust_id character varying(256),
  ctn character varying(256),
  ga_id character varying(256),
  status character(1),
  reg_date character varying(8),
  reg_time character varying(6),
  exit_ci character varying(256),
  exit_date character varying(8),
  exit_time character varying(6),
  CONSTRAINT pk_user_info_tbl PRIMARY KEY (user_idx),
  CONSTRAINT unq_user_info_tbl_01 UNIQUE (user_ci)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "USER_INFO_TBL"
  OWNER TO point;

-- Index: "IDX_USER_INFO_TBL_01"

-- DROP INDEX "IDX_USER_INFO_TBL_01";

CREATE INDEX "IDX_USER_INFO_TBL_01"
  ON "USER_INFO_TBL"
  USING btree
  (user_ci COLLATE pg_catalog."default", cust_id COLLATE pg_catalog."default", ctn COLLATE pg_catalog."default", ga_id COLLATE pg_catalog."default", status COLLATE pg_catalog."default");


  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: "USER_INFO_TBL_LOG"

-- DROP TABLE "USER_INFO_TBL_LOG";

CREATE TABLE "USER_INFO_TBL_LOG"
(
  user_log_idx serial NOT NULL,
  user_idx integer,
  user_ci character varying(256),
  cust_id character varying(256),
  ctn character varying(256),
  ga_id character varying(256),
  status character(1),
  exit_ci character varying(256),
  update_gbn character(1), -- I:Insert...
  regdate timestamp without time zone,
  CONSTRAINT "USER_INFO_TBL_LOG_pkey" PRIMARY KEY (user_log_idx)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "USER_INFO_TBL_LOG"
  OWNER TO point;
COMMENT ON COLUMN "USER_INFO_TBL_LOG".update_gbn IS 'I:Insert
U:Update
D:Delete(Exit)
';

















-- Table: cms_common_code

-- DROP TABLE cms_common_code;

CREATE TABLE cms_common_code
(
  code_idx serial NOT NULL,
  code_name character varying(256) NOT NULL,
  code_value character varying(256) NOT NULL,
  CONSTRAINT "PK_cms_common_code" PRIMARY KEY (code_idx)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cms_common_code
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  -- Table: cms_login_log

-- DROP TABLE cms_login_log;

CREATE TABLE cms_login_log
(
  idx integer NOT NULL DEFAULT nextval('"CMS_LOGIN_LOG_IDX_seq"'::regclass),
  sessionid character varying(40) NOT NULL,
  logintime timestamp without time zone NOT NULL,
  logouttime timestamp without time zone,
  userid character varying(20) NOT NULL,
  ipaddr character varying(16) NOT NULL,
  CONSTRAINT "CMS_LOGIN_LOG_pkey" PRIMARY KEY (idx)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cms_login_log
  OWNER TO admin;

  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: cms_point_modify

-- DROP TABLE cms_point_modify;

CREATE TABLE cms_point_modify
(
  point_modify_idx serial NOT NULL,
  ctn character varying(256),
  user_ci character varying(256),
  cust_id character varying(256),
  ga_id character varying(256),
  point_type character(1), -- I:PlusPoint O:MinusPoint
  sdate character varying(8),
  stime character varying(6),
  point_value bigint,
  description character varying(1024),
  send_userid character varying(128) NOT NULL, -- 조정자 아이디
  balance bigint, -- 잔액
  regdate timestamp without time zone,
  message character varying(2056),
  CONSTRAINT cms_point_modify_pkey PRIMARY KEY (point_modify_idx)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cms_point_modify
  OWNER TO point;
COMMENT ON COLUMN cms_point_modify.point_type IS 'I:PlusPoint O:MinusPoint';
COMMENT ON COLUMN cms_point_modify.send_userid IS '조정자 아이디';
COMMENT ON COLUMN cms_point_modify.balance IS '잔액';













-- Table: cms_sms_log

-- DROP TABLE cms_sms_log;

CREATE TABLE cms_sms_log
(
  sms_idx serial NOT NULL,
  send_userid character varying(128) NOT NULL,
  username character varying(32) NOT NULL,
  msg_content character varying(90),
  call_ctn character varying(20),
  rcv_ctn character varying(20),
  success_yn character varying(1),
  response character varying(2048),
  ipaddr character varying(16) NOT NULL,
  regdate timestamp without time zone NOT NULL,
  CONSTRAINT "CMS_SMS_LOG_pkey" PRIMARY KEY (sms_idx)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cms_sms_log
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: cms_use_log

-- DROP TABLE cms_use_log;

CREATE TABLE cms_use_log
(
  idx integer NOT NULL DEFAULT nextval('"CMS_USE_LOG_IDX_seq"'::regclass),
  userid character varying(128) NOT NULL,
  menu character varying(20),
  action character varying(200),
  ipaddr character varying(16) NOT NULL,
  regdate timestamp without time zone NOT NULL,
  CONSTRAINT "CMS_USE_LOG_pkey" PRIMARY KEY (idx)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cms_use_log
  OWNER TO admin;

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: cms_user_info

-- DROP TABLE cms_user_info;

CREATE TABLE cms_user_info
(
  userid character varying(128) NOT NULL,
  passwd character varying(100) NOT NULL,
  username character varying(32) NOT NULL,
  tel character varying(20) NOT NULL,
  email character varying(62),
  state character(1),
  arrowip character varying(300),
  userauth character(1),
  loginfailcnt numeric DEFAULT 0,
  passdate character varying(10),
  ipaddr character varying(16) NOT NULL,
  regdate timestamp without time zone NOT NULL,
  modiipaddr character varying(16),
  modidate timestamp without time zone,
  CONSTRAINT "CMS_USER_INFO_pkey" PRIMARY KEY (userid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cms_user_info
  OWNER TO admin;

  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: point_apicall_log

-- DROP TABLE point_apicall_log;

CREATE TABLE point_apicall_log
(
  idx integer NOT NULL DEFAULT nextval('"POINT_APICALL_LOG_IDX_seq"'::regclass),
  cust_id character varying(256),
  ga_id character varying(256),
  ctn character varying(256),
  user_ci character varying(256),
  apiurl character varying(1024),
  tr_id character varying(256),
  tr_response character varying(1000),
  ipaddr character varying(16) NOT NULL,
  regdate timestamp without time zone NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE point_apicall_log
  OWNER TO admin;

  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: point_banner

-- DROP TABLE point_banner;

CREATE TABLE point_banner
(
  idx integer NOT NULL DEFAULT nextval('"POINT_BANNER_IDX_seq"'::regclass),
  bannername character varying(100) NOT NULL,
  bannerimage character varying(150) NOT NULL,
  startdate character varying(10) NOT NULL,
  enddate character varying(10) NOT NULL,
  status character(1),
  linktype character varying(10),
  linkurl character varying(1024),
  userid character varying(100) NOT NULL,
  ipaddr character varying(16) NOT NULL,
  regdate timestamp without time zone NOT NULL,
  modiipaddr character varying(16),
  modidate timestamp without time zone,
  androidgbn character varying(5),
  iosgbn character varying(5),
  CONSTRAINT "POINT_BANNER_pkey" PRIMARY KEY (idx)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE point_banner
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: point_roulette

-- DROP TABLE point_roulette;

CREATE TABLE point_roulette
(
  roulette_id character varying(16) NOT NULL,
  channelname character varying(150) NOT NULL,
  routype character(1) NOT NULL,
  area01 character varying(100) NOT NULL,
  area02 character varying(100) NOT NULL,
  area03 character varying(100) NOT NULL,
  area04 character varying(100) NOT NULL,
  area05 character varying(100) NOT NULL,
  area06 character varying(100) NOT NULL,
  point01 numeric,
  point02 numeric,
  point03 numeric,
  point04 numeric,
  point05 numeric,
  point06 numeric,
  max01 numeric,
  max02 numeric,
  max03 numeric,
  max04 numeric,
  max05 numeric,
  max06 numeric,
  totmax numeric,
  timegbn character(1),
  starttime character(2),
  endtime character(2),
  timenum numeric,
  startdate character varying(10) NOT NULL,
  enddate character varying(10) NOT NULL,
  status character(1),
  userid character varying(100),
  ipaddr character varying(16) NOT NULL,
  regdate timestamp without time zone NOT NULL,
  modiipaddr character varying(16),
  modidate timestamp without time zone,
  imagepath character varying(20),
  desc01 text,
  desc02 text,
  requester_code character varying(100),
  requester_description character varying(100),
  point_limit_yn character varying(1),
  point_limit_cnt integer,
  CONSTRAINT "POINT_ROULLETE_pkey" PRIMARY KEY (roulette_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE point_roulette
  OWNER TO admin;
GRANT ALL ON TABLE point_roulette TO admin;














-- Table: point_roulette_auth

-- DROP TABLE point_roulette_auth;

CREATE TABLE point_roulette_auth
(
  idx serial NOT NULL,
  roulette_id character varying(16) NOT NULL, -- 룰렛아이디
  ctn character varying(256), -- 전화번호
  smscode character varying(10) NOT NULL, -- 인증번호
  ipaddr character varying(16), -- 아이피
  regdate timestamp with time zone, -- 등록일자
  CONSTRAINT foreign_key01 FOREIGN KEY (roulette_id)
      REFERENCES point_roulette (roulette_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE point_roulette_auth
  OWNER TO admin;
COMMENT ON TABLE point_roulette_auth
  IS '전화번호 인증';
COMMENT ON COLUMN point_roulette_auth.roulette_id IS '룰렛아이디';
COMMENT ON COLUMN point_roulette_auth.ctn IS '전화번호';
COMMENT ON COLUMN point_roulette_auth.smscode IS '인증번호';
COMMENT ON COLUMN point_roulette_auth.ipaddr IS '아이피';
COMMENT ON COLUMN point_roulette_auth.regdate IS '등록일자';













-- Table: point_roulette_conn_log

-- DROP TABLE point_roulette_conn_log;

CREATE TABLE point_roulette_conn_log
(
  idx integer NOT NULL DEFAULT nextval('"POINT_ROULLETE_CONN_LOG_IDX_seq"'::regclass),
  roulette_id character varying(16) NOT NULL,
  cust_id character varying(256),
  ga_id character varying(256),
  ctn character varying(256),
  sdate character varying(8) NOT NULL,
  ipaddr character varying(16) NOT NULL,
  regdate timestamp without time zone NOT NULL,
  CONSTRAINT "POINT_ROULLETE_CONN_LOG_pkey" PRIMARY KEY (idx)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE point_roulette_conn_log
  OWNER TO admin;

  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: point_roulette_conn_log_sum

-- DROP TABLE point_roulette_conn_log_sum;

CREATE TABLE point_roulette_conn_log_sum
(
  idx integer NOT NULL DEFAULT nextval('"POINT_ROULLETE_CONN_LOG_SUM_IDX_seq"'::regclass),
  roulette_id character varying(16) NOT NULL,
  regdate timestamp without time zone NOT NULL,
  sdate character varying(8) NOT NULL,
  conn_cnt numeric DEFAULT 0,
  CONSTRAINT "POINT_ROULLETE_CONN_LOG_SUM_pkey" PRIMARY KEY (idx)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE point_roulette_conn_log_sum
  OWNER TO admin;

-- Index: idx_point_roulette_conn_log_sum

-- DROP INDEX idx_point_roulette_conn_log_sum;

CREATE INDEX idx_point_roulette_conn_log_sum
  ON point_roulette_conn_log_sum
  USING btree
  (roulette_id COLLATE pg_catalog."default", sdate COLLATE pg_catalog."default");


  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: point_roulette_join

-- DROP TABLE point_roulette_join;

CREATE TABLE point_roulette_join
(
  idx integer NOT NULL DEFAULT nextval('"POINT_ROULLETE_JOIN_IDX_seq"'::regclass),
  roulette_id character varying(16) NOT NULL,
  cust_id character varying(256),
  ga_id character varying(256),
  ctn character varying(256),
  user_ci character varying(256),
  result character(1),
  sdate character varying(8) NOT NULL,
  ipaddr character varying(16) NOT NULL,
  regdate timestamp without time zone NOT NULL,
  status character(1) NOT NULL,
  tr_id character varying(256),
  save_point numeric,
  save_result character varying(8),
  modiipaddr character varying(16),
  modidate timestamp without time zone,
  CONSTRAINT "POINT_ROULLETE_JOIN_pkey" PRIMARY KEY (idx)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE point_roulette_join
  OWNER TO admin;

-- Index: point_roulette_join_01

-- DROP INDEX point_roulette_join_01;

CREATE INDEX point_roulette_join_01
  ON point_roulette_join
  USING btree
  (roulette_id COLLATE pg_catalog."default");

-- Index: point_roulette_join_02

-- DROP INDEX point_roulette_join_02;

CREATE INDEX point_roulette_join_02
  ON point_roulette_join
  USING btree
  (roulette_id COLLATE pg_catalog."default", sdate COLLATE pg_catalog."default");


  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: point_roulette_join_sum

-- DROP TABLE point_roulette_join_sum;

CREATE TABLE point_roulette_join_sum
(
  idx integer NOT NULL DEFAULT nextval('"POINT_ROULLETE_JOIN_SUM_IDX_seq"'::regclass),
  roulette_id character varying(16) NOT NULL,
  sdate character varying(8) NOT NULL,
  area01 numeric DEFAULT 0,
  area02 numeric DEFAULT 0,
  area03 numeric DEFAULT 0,
  area04 numeric DEFAULT 0,
  area05 numeric DEFAULT 0,
  area06 numeric DEFAULT 0,
  CONSTRAINT "POINT_ROULLETE_JOIN_SUM_pkey" PRIMARY KEY (idx)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE point_roulette_join_sum
  OWNER TO admin;

-- Index: idx_point_roulette_join_sum_01

-- DROP INDEX idx_point_roulette_join_sum_01;

CREATE INDEX idx_point_roulette_join_sum_01
  ON point_roulette_join_sum
  USING btree
  (roulette_id COLLATE pg_catalog."default", sdate COLLATE pg_catalog."default");


  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: point_roulette_optin

-- DROP TABLE point_roulette_optin;

CREATE TABLE point_roulette_optin
(
  idx serial NOT NULL,
  cust_id character varying(256), -- cust id
  ga_id character varying(256), -- 구글id
  ctn character varying(256), -- 전화번호
  roulette_id character varying(16), -- 룰렛아이디
  sdate character varying(8), -- 날짜
  status character varying(10), -- 상태값
  ipaddr character varying(16), -- 아이피
  regdate timestamp with time zone NOT NULL -- 등록일자
)
WITH (
  OIDS=FALSE
);
ALTER TABLE point_roulette_optin
  OWNER TO admin;
COMMENT ON TABLE point_roulette_optin
  IS '룰렛 동의';
COMMENT ON COLUMN point_roulette_optin.cust_id IS 'cust id';
COMMENT ON COLUMN point_roulette_optin.ga_id IS '구글id';
COMMENT ON COLUMN point_roulette_optin.ctn IS '전화번호';
COMMENT ON COLUMN point_roulette_optin.roulette_id IS '룰렛아이디';
COMMENT ON COLUMN point_roulette_optin.sdate IS '날짜';
COMMENT ON COLUMN point_roulette_optin.status IS '상태값';
COMMENT ON COLUMN point_roulette_optin.ipaddr IS '아이피';
COMMENT ON COLUMN point_roulette_optin.regdate IS '등록일자';














-- Table: point_roulette_set_log

-- DROP TABLE point_roulette_set_log;

CREATE TABLE point_roulette_set_log
(
  idx serial NOT NULL,
  roulette_id character varying(16),
  settime character varying(10),
  setnum character varying(5),
  ipaddr character varying(16),
  regdate timestamp without time zone NOT NULL,
  sdate character varying(10) NOT NULL,
  CONSTRAINT point_roulette_set_log_pkey PRIMARY KEY (idx)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE point_roulette_set_log
  OWNER TO admin;

  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: tbl_hourly_stat

-- DROP TABLE tbl_hourly_stat;

CREATE TABLE tbl_hourly_stat
(
  p_media character varying(255) NOT NULL,
  p_user_ci character varying(256) NOT NULL,
  p_point_type character(1) NOT NULL,
  p_point_value numeric NOT NULL,
  CONSTRAINT pk_tbl_hourly_stat PRIMARY KEY (p_media, p_user_ci, p_point_type)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tbl_hourly_stat
  OWNER TO point;

-- Index: tbl_hourly_stat_01

-- DROP INDEX tbl_hourly_stat_01;

CREATE INDEX tbl_hourly_stat_01
  ON tbl_hourly_stat
  USING btree
  (p_media COLLATE pg_catalog."default", p_point_type COLLATE pg_catalog."default", p_point_value);


  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: tbl_stat_anomaly_detection

-- DROP TABLE tbl_stat_anomaly_detection;

CREATE TABLE tbl_stat_anomaly_detection
(
  media character varying(255) NOT NULL,
  stat_date character(8) NOT NULL,
  stat_hour character(2) NOT NULL,
  limit_point_value numeric NOT NULL,
  limit_input_count integer NOT NULL,
  limit_output_count integer NOT NULL,
  input_point numeric NOT NULL,
  output_point numeric NOT NULL,
  CONSTRAINT pk_tbl_stat_anomaly_detection PRIMARY KEY (media, stat_date, stat_hour)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tbl_stat_anomaly_detection
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  -- Table: tbl_stat_anomaly_detection_2

-- DROP TABLE tbl_stat_anomaly_detection_2;

CREATE TABLE tbl_stat_anomaly_detection_2
(
  stat_date character(8) NOT NULL,
  stat_hour character(2) NOT NULL,
  point_type character(1) NOT NULL,
  point numeric NOT NULL,
  rank integer NOT NULL,
  user_ci character varying(256) NOT NULL,
  cust_id character varying(256),
  ctn character varying(256),
  ga_id character varying(256),
  media character varying(256),
  CONSTRAINT pk_tbl_stat_anomaly_detection_2 PRIMARY KEY (stat_date, stat_hour, point_type, rank)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tbl_stat_anomaly_detection_2
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: tbl_stat_connect

-- DROP TABLE tbl_stat_connect;

CREATE TABLE tbl_stat_connect
(
  year character(4) NOT NULL,
  month character(2) NOT NULL,
  day character(2) NOT NULL,
  hour character(2) NOT NULL,
  stat_type character varying(20) NOT NULL,
  stat_code character varying(100) NOT NULL,
  count bigint DEFAULT 0,
  regdate timestamp without time zone,
  CONSTRAINT pk_connect_stat PRIMARY KEY (year, month, day, hour, stat_type, stat_code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tbl_stat_connect
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: tbl_stat_daily_point

-- DROP TABLE tbl_stat_daily_point;

CREATE TABLE tbl_stat_daily_point
(
  year character(4) NOT NULL,
  month character(2) NOT NULL,
  day character(2) NOT NULL,
  d_day character(1) NOT NULL, -- 0 : sun...
  total_balance bigint,
  input_point bigint,
  output_point bigint,
  remove_point bigint,
  pre_point bigint,
  input_count bigint,
  output_count bigint,
  remove_count bigint,
  pre_count bigint,
  active_user bigint,
  CONSTRAINT pk_daily_stat PRIMARY KEY (year, month, day, d_day)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tbl_stat_daily_point
  OWNER TO point;
COMMENT ON COLUMN tbl_stat_daily_point.d_day IS '0 : sun
1 : mon
2 : tues
3: wed
4 : thur
5 : fri
6 : sat';
















-- Table: tbl_stat_daily_point2

-- DROP TABLE tbl_stat_daily_point2;

CREATE TABLE tbl_stat_daily_point2
(
  year character(4) NOT NULL,
  month character(2) NOT NULL,
  day character(2) NOT NULL,
  d_day character(1) NOT NULL, -- 0 : sun...
  total_balance bigint,
  active_user bigint,
  input_unique_ci bigint,
  input_point bigint,
  input_count bigint,
  output_unique_ci bigint,
  output_point bigint,
  output_count bigint,
  remove_unique_ci bigint,
  remove_point bigint,
  remove_count bigint,
  pre_point bigint,
  pre_count bigint,
  modify_unique_ci bigint,
  modify_point bigint,
  modify_count bigint,
  io_sum_count bigint,
  io_sum_unique_ci bigint,
  CONSTRAINT pk_daily_stat2 PRIMARY KEY (year, month, day, d_day)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tbl_stat_daily_point2
  OWNER TO point;
COMMENT ON COLUMN tbl_stat_daily_point2.d_day IS '0 : sun
1 : mon
2 : tues
3: wed
4 : thur
5 : fri
6 : sat';














-- Table: tbl_stat_media_point

-- DROP TABLE tbl_stat_media_point;

CREATE TABLE tbl_stat_media_point
(
  media character varying(255) NOT NULL,
  year character(4) NOT NULL,
  month character(2) NOT NULL,
  day character(2) NOT NULL,
  d_day integer NOT NULL, -- 0:sun...
  point_type character(1) NOT NULL,
  point numeric,
  count bigint,
  CONSTRAINT pk_media_stat PRIMARY KEY (media, year, month, day, d_day, point_type)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tbl_stat_media_point
  OWNER TO point;
COMMENT ON COLUMN tbl_stat_media_point.d_day IS '0:sun
1:mon
2:tues
3:wed
4:thur
5:fri
6:sat';
















-- Table: tbl_stat_monthly_point

-- DROP TABLE tbl_stat_monthly_point;

CREATE TABLE tbl_stat_monthly_point
(
  year character(4) NOT NULL,
  month character(2) NOT NULL,
  total_balance bigint,
  input_point bigint,
  output_point bigint,
  remove_point bigint,
  pre_point bigint,
  input_count bigint,
  output_count bigint,
  remove_count bigint,
  pre_count bigint,
  active_user bigint,
  CONSTRAINT pk_monthly_stat PRIMARY KEY (year, month)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tbl_stat_monthly_point
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: tbl_stat_receiver_info

-- DROP TABLE tbl_stat_receiver_info;

CREATE TABLE tbl_stat_receiver_info
(
  no integer NOT NULL,
  receiver_tel_no character varying(255) NOT NULL,
  user_info character varying(255) NOT NULL,
  use_flag character(1) NOT NULL DEFAULT 'Y'::bpchar,
  CONSTRAINT pk_tbl_stat_receiver_info PRIMARY KEY (no)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tbl_stat_receiver_info
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  
  
  -- Table: tbl_stat_top_user

-- DROP TABLE tbl_stat_top_user;

CREATE TABLE tbl_stat_top_user
(
  media character varying(255) NOT NULL,
  stat_date character(8) NOT NULL,
  point_type character(1) NOT NULL,
  point numeric NOT NULL,
  rank integer NOT NULL,
  user_ci character varying(256) NOT NULL,
  cust_id character varying(256),
  ctn character varying(256),
  ga_id character varying(256),
  CONSTRAINT pk_tbl_stat_top_user PRIMARY KEY (media, stat_date, point_type, rank)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tbl_stat_top_user
  OWNER TO point;

  
  
  
  
  
  
  
  
  
  
  
  
  