





CREATE TABLE tbl_stat_anomaly_detection_2
(
  media character varying(255) NOT NULL,
  stat_date character(8) NOT NULL,
  stat_hour character(2) NOT NULL,
  point_type character(1) NOT NULL,
  point numeric NOT NULL,
  rank integer NOT NULL,
  user_ci character varying(256) NOT NULL,
  cust_id character varying(256),
  ctn character varying(256),
  ga_id character varying(256),
  CONSTRAINT pk_tbl_stat_anomaly_detection_2 PRIMARY KEY (media, stat_date, stat_hour, point_type, rank)
)
WITH (
  OIDS=FALSE
);






-- Function: fn_make_anomaly_stat2(character, character, numeric)

-- DROP FUNCTION fn_make_anomaly_stat2(character, character, numeric);

CREATE OR REPLACE FUNCTION fn_make_anomaly_stat2(
    curdate character,
    curtime character,
    limitpoint numeric)
  RETURNS boolean AS
$BODY$


	BEGIN
		-- 利赋贸府
		INSERT INTO tbl_stat_anomaly_detection_2 ( stat_date, stat_hour, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
		SELECT
			curdate,
			curtime,
			'I' as point_type, 
			(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
			a.point,
			ui.user_ci, 
			ui.cust_id, 
			ui.ctn, 
			ui.ga_id 
		FROM
			(


				SELECT
					user_ci,
					SUM(point_value) as point
				FROM
					"POINT_INFO_TBL"
				WHERE
					reg_date = curdate and
					SUBSTRING(reg_time, 1, 2) = curtime and
					point_type = 'I' 
				GROUP BY
					user_ci,
					point_type

			) as a,
			"USER_INFO_TBL" as ui
		WHERE
			a.user_ci = ui.user_ci
			AND point > limitpoint;

		-- 瞒皑贸府
		INSERT INTO tbl_stat_anomaly_detection_2 ( stat_date, stat_hour, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
		SELECT
			curdate,
			curtime,
			'O' as point_type, 
			(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
			a.point,
			ui.user_ci, 
			ui.cust_id, 
			ui.ctn, 
			ui.ga_id 
		FROM
			(


				SELECT
					user_ci,
					SUM(point_value) as point
				FROM
					"POINT_INFO_TBL"
				WHERE
					reg_date = curdate and
					SUBSTRING(reg_time, 1, 2) = curtime and
					point_type = 'O' 
				GROUP BY
					user_ci,
					point_type

			) as a,
			"USER_INFO_TBL" as ui
		WHERE
			a.user_ci = ui.user_ci
			AND point > limitpoint;

		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_make_anomaly_stat2(character, character, numeric)
  OWNER TO point;
















