CMS > make_monthly_stat 를 모두 삭제하고, 해당 페이지는 일별통계에서 가져올 수 있도록 view를 수정
DROP FUNCTION make_monthly_stat(text, text)
DROP FUNCTION make_monthly_remove(text)
DROP FUNCTION make_monthly_pre(text)
DROP FUNCTION make_monthly_output(text)
DROP FUNCTION make_monthly_input(text)
DROP FUNCTION make_monthly_balance(text)
===========================================================================================
CREATE INDEX point_info_tbl_02 ON "POINT_INFO_TBL" (reg_date, SUBSTRING(reg_time, 1, 2), point_type )
DROP INDEX point_info_tbl_02


============================================================================================


fn_make_anomaly_stat2 이상탐지 합계.. 그냥 빠름. 서버에서도 빠른지 확인하자.
EXPLAIN					
SELECT	
			'unUsed',
			'20161111',
			'01',
			'I' as point_type, 
			(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
			a.point,
			ui.user_ci, 
			ui.cust_id, 
			ui.ctn, 
			ui.ga_id 
		FROM
			(	SELECT
					user_ci,
					SUM(point_value) as point
				FROM
					"POINT_INFO_TBL"
				WHERE
					reg_date = '20161111' and
					SUBSTRING(reg_time, 1, 2) = '01' and
					point_type = 'I' 
				GROUP BY
					user_ci,
					point_type
			) as a,
			"USER_INFO_TBL" as ui
		WHERE
			a.user_ci = ui.user_ci
			AND point >= 1000;
			
"WindowAgg  (cost=2.63..2.65 rows=1 width=2096)"
"  ->  Sort  (cost=2.63..2.64 rows=1 width=2096)"
"        Sort Key: a.point"
"        ->  Hash Join  (cost=1.36..2.62 rows=1 width=2096)"
"              Hash Cond: ((ui.user_ci)::text = (a.user_ci)::text)"
"              ->  Seq Scan on "USER_INFO_TBL" ui  (cost=0.00..1.18 rows=18 width=2064)"
"              ->  Hash  (cost=1.35..1.35 rows=1 width=59)"
"                    ->  Subquery Scan on a  (cost=1.33..1.35 rows=1 width=59)"
"                          ->  HashAggregate  (cost=1.33..1.34 rows=1 width=37)"
"                                Filter: (sum("POINT_INFO_TBL".point_value) >= 1000000::numeric)"
"                                ->  Seq Scan on "POINT_INFO_TBL"  (cost=0.00..1.32 rows=1 width=37)"
"                                      Filter: ((point_type = 'I'::bpchar) AND ((reg_date)::text = '20161111'::text) AND ("substring"((reg_time)::text, 1, 2) = '01'::text))"


만약 서버에서는 느려서 인덱스를 추가한다면
DROP INDEX point_info_tbl_02;
CREATE INDEX point_info_tbl_02 ON "USER_INFO_TBL" USING btree(reg_date, SUBSTRING(reg_time, 1, 2), point_type)




===================================================================================				
이상탐지 매체별 - 걸면 빨라짐					
fn_make_anomaly_stat

SELECT
				COALESCE(COUNT(p_user_ci),0) into _limit_input_count
			FROM
				tbl_hourly_stat
			WHERE
				p_media = '0000' and
				p_point_type = 'I' and
				p_point_value >= 10000;

"Aggregate  (cost=42.38..42.39 rows=1 width=2)"
"  ->  Seq Scan on tbl_hourly_stat  (cost=0.00..42.38 rows=1 width=2)"
"        Filter: ((p_point_value >= 10000::numeric) AND ((p_media)::text = '0000'::text) AND (p_point_type = 'I'::bpchar))"

CREATE INDEX tbl_hourly_stat_01 ON tbl_hourly_stat (p_media, p_point_type, p_point_value )
DROP INDEX tbl_hourly_stat_01

"Aggregate  (cost=8.28..8.29 rows=1 width=2)"
"  ->  Index Scan using tbl_hourly_stat_01 on tbl_hourly_stat  (cost=0.00..8.27 rows=1 width=2)"
"        Index Cond: (((p_media)::text = '0000'::text) AND (p_point_type = 'I'::bpchar) AND (p_point_value >= 10000::numeric))"





이 부분은 안해도 됨. - 안빨라짐. 인덱스 안탐.
SELECT
				COALESCE(SUM(p_point_value),0) into _input_point
			FROM
				tbl_hourly_stat
			WHERE
				p_media = '0000' and
				p_point_type = 'I'
				
"Aggregate  (cost=40.83..40.84 rows=1 width=5)"
"  ->  Seq Scan on tbl_hourly_stat  (cost=0.00..37.75 rows=1233 width=5)"
"        Filter: (((p_media)::text = '0000'::text) AND (p_point_type = 'I'::bpchar))"

CREATE INDEX tbl_hourly_stat_02 ON tbl_hourly_stat (p_media, p_point_type )
DROP INDEX tbl_hourly_stat_02
옵티마이저가 인텍스를 안타게 선택하여 삭제함
"Aggregate  (cost=40.83..40.84 rows=1 width=5)"
"  ->  Seq Scan on tbl_hourly_stat  (cost=0.00..37.75 rows=1233 width=5)"
"        Filter: (((p_media)::text = '0000'::text) AND (p_point_type = 'I'::bpchar))"









