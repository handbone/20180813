--1=============================================================================
-- Sequence: "BZSCREEN_POST_TBL_EVENT_idx_seq1"

-- DROP SEQUENCE "BZSCREEN_POST_TBL_EVENT_idx_seq1";

CREATE SEQUENCE "BZSCREEN_POST_TBL_EVENT_idx_seq1"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "BZSCREEN_POST_TBL_EVENT_idx_seq1"
  OWNER TO point;

--2=============================================================================
-- Sequence: "CMS_LOGIN_LOG_IDX_seq"

-- DROP SEQUENCE "CMS_LOGIN_LOG_IDX_seq";

CREATE SEQUENCE "CMS_LOGIN_LOG_IDX_seq"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "CMS_LOGIN_LOG_IDX_seq"
  OWNER TO admin;

--3=============================================================================
-- Sequence: "CMS_USE_LOG_IDX_seq"

-- DROP SEQUENCE "CMS_USE_LOG_IDX_seq";

CREATE SEQUENCE "CMS_USE_LOG_IDX_seq"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "CMS_USE_LOG_IDX_seq"
  OWNER TO admin;

--4=============================================================================
-- Sequence: "POINT_APICALL_LOG_IDX_seq"

-- DROP SEQUENCE "POINT_APICALL_LOG_IDX_seq";

CREATE SEQUENCE "POINT_APICALL_LOG_IDX_seq"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "POINT_APICALL_LOG_IDX_seq"
  OWNER TO admin;

--5=============================================================================
-- Sequence: "POINT_BANNER_IDX_seq"

-- DROP SEQUENCE "POINT_BANNER_IDX_seq";

CREATE SEQUENCE "POINT_BANNER_IDX_seq"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "POINT_BANNER_IDX_seq"
  OWNER TO point;

--6=============================================================================
-- Sequence: "POINT_HIST_TBL_COUNT_point_hist_count_idx_seq"

-- DROP SEQUENCE "POINT_HIST_TBL_COUNT_point_hist_count_idx_seq";

CREATE SEQUENCE "POINT_HIST_TBL_COUNT_point_hist_count_idx_seq"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "POINT_HIST_TBL_COUNT_point_hist_count_idx_seq"
  OWNER TO point;

  
-- Sequence: "POINT_HIST_TBL_COUNT_point_hist_count_idx_seq1"

-- DROP SEQUENCE "POINT_HIST_TBL_COUNT_point_hist_count_idx_seq1";

CREATE SEQUENCE "POINT_HIST_TBL_COUNT_point_hist_count_idx_seq1"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "POINT_HIST_TBL_COUNT_point_hist_count_idx_seq1"
  OWNER TO point;

--7=============================================================================
-- Sequence: "POINT_HIST_TBL_point_hist_idx_seq"

-- DROP SEQUENCE "POINT_HIST_TBL_point_hist_idx_seq";

CREATE SEQUENCE "POINT_HIST_TBL_point_hist_idx_seq"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "POINT_HIST_TBL_point_hist_idx_seq"
  OWNER TO point;

-- Sequence: "POINT_HIST_TBL_point_hist_idx_seq1"

-- DROP SEQUENCE "POINT_HIST_TBL_point_hist_idx_seq1";

CREATE SEQUENCE "POINT_HIST_TBL_point_hist_idx_seq1"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "POINT_HIST_TBL_point_hist_idx_seq1"
  OWNER TO point;

  
  -- Sequence: "POINT_INFO_TBL_point_hist_idx_seq"

-- DROP SEQUENCE "POINT_INFO_TBL_point_hist_idx_seq";

CREATE SEQUENCE "POINT_INFO_TBL_point_hist_idx_seq"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "POINT_INFO_TBL_point_hist_idx_seq"
  OWNER TO point;
  
  -- Sequence: "POINT_INFO_TBL_point_info_idx_seq"

-- DROP SEQUENCE "POINT_INFO_TBL_point_info_idx_seq";

CREATE SEQUENCE "POINT_INFO_TBL_point_info_idx_seq"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "POINT_INFO_TBL_point_info_idx_seq"
  OWNER TO point;

  
--8=============================================================================
-- Sequence: "POINT_ROULLETE_CONN_LOG_IDX_seq"

-- DROP SEQUENCE "POINT_ROULLETE_CONN_LOG_IDX_seq";

CREATE SEQUENCE "POINT_ROULLETE_CONN_LOG_IDX_seq"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "POINT_ROULLETE_CONN_LOG_IDX_seq"
  OWNER TO admin;

--9=============================================================================
-- Sequence: "POINT_ROULLETE_CONN_LOG_SUM_IDX_seq"

-- DROP SEQUENCE "POINT_ROULLETE_CONN_LOG_SUM_IDX_seq";

CREATE SEQUENCE "POINT_ROULLETE_CONN_LOG_SUM_IDX_seq"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "POINT_ROULLETE_CONN_LOG_SUM_IDX_seq"
  OWNER TO admin;

--10=============================================================================
-- Sequence: "POINT_ROULLETE_JOIN_IDX_seq"

-- DROP SEQUENCE "POINT_ROULLETE_JOIN_IDX_seq";

CREATE SEQUENCE "POINT_ROULLETE_JOIN_IDX_seq"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "POINT_ROULLETE_JOIN_IDX_seq"
  OWNER TO admin;

--11=============================================================================
-- Sequence: "POINT_ROULLETE_JOIN_SUM_IDX_seq"

-- DROP SEQUENCE "POINT_ROULLETE_JOIN_SUM_IDX_seq";

CREATE SEQUENCE "POINT_ROULLETE_JOIN_SUM_IDX_seq"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "POINT_ROULLETE_JOIN_SUM_IDX_seq"
  OWNER TO admin;

--12=============================================================================
-- Sequence: "USER_INFO_TBL_LOG_user_log_idx_seq"

-- DROP SEQUENCE "USER_INFO_TBL_LOG_user_log_idx_seq";

CREATE SEQUENCE "USER_INFO_TBL_LOG_user_log_idx_seq"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "USER_INFO_TBL_LOG_user_log_idx_seq"
  OWNER TO point;

  
  -- Sequence: "USER_INFO_TBL_LOG_user_log_idx_seq1"

-- DROP SEQUENCE "USER_INFO_TBL_LOG_user_log_idx_seq1";

CREATE SEQUENCE "USER_INFO_TBL_LOG_user_log_idx_seq1"
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE "USER_INFO_TBL_LOG_user_log_idx_seq1"
  OWNER TO point;

--13=============================================================================
-- Sequence: cms_common_code_code_idx_seq

-- DROP SEQUENCE cms_common_code_code_idx_seq;

CREATE SEQUENCE cms_common_code_code_idx_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE cms_common_code_code_idx_seq
  OWNER TO point;

  
--14=============================================================================
-- Sequence: cms_point_modify_point_modify_idx_seq

-- DROP SEQUENCE cms_point_modify_point_modify_idx_seq;

CREATE SEQUENCE cms_point_modify_point_modify_idx_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE cms_point_modify_point_modify_idx_seq
  OWNER TO point;

--15=============================================================================
-- Sequence: cms_sms_log_sms_idx_seq

-- DROP SEQUENCE cms_sms_log_sms_idx_seq;

CREATE SEQUENCE cms_sms_log_sms_idx_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE cms_sms_log_sms_idx_seq
  OWNER TO point;

--16=============================================================================
-- Sequence: point_info_tbl_point_hist_idx_seq

-- DROP SEQUENCE point_info_tbl_point_hist_idx_seq;

CREATE SEQUENCE point_info_tbl_point_hist_idx_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE point_info_tbl_point_hist_idx_seq
  OWNER TO point;

--17=============================================================================
-- Sequence: point_info_tbl_point_info_idx_seq

-- DROP SEQUENCE point_info_tbl_point_info_idx_seq;

CREATE SEQUENCE point_info_tbl_point_info_idx_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE point_info_tbl_point_info_idx_seq
  OWNER TO point;

--18=============================================================================
-- Sequence: point_roulette_auth_idx_seq

-- DROP SEQUENCE point_roulette_auth_idx_seq;

CREATE SEQUENCE point_roulette_auth_idx_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE point_roulette_auth_idx_seq
  OWNER TO admin;

--19=============================================================================
-- Sequence: point_roulette_optin_idx_seq

-- DROP SEQUENCE point_roulette_optin_idx_seq;

CREATE SEQUENCE point_roulette_optin_idx_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE point_roulette_optin_idx_seq
  OWNER TO admin;

--20=============================================================================
-- Sequence: point_roulette_set_log_idx_seq

-- DROP SEQUENCE point_roulette_set_log_idx_seq;

CREATE SEQUENCE point_roulette_set_log_idx_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE point_roulette_set_log_idx_seq
  OWNER TO admin;

--21=============================================================================
-- Sequence: tbl_stat_receiver_info_no_seq

-- DROP SEQUENCE tbl_stat_receiver_info_no_seq;

CREATE SEQUENCE tbl_stat_receiver_info_no_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE tbl_stat_receiver_info_no_seq
  OWNER TO point;

--22=============================================================================
-- Sequence: user_info_tbl_user_idx_seq

-- DROP SEQUENCE user_info_tbl_user_idx_seq;

CREATE SEQUENCE user_info_tbl_user_idx_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE user_info_tbl_user_idx_seq
  OWNER TO point;
