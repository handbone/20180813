<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!doctype html>
<html lang="ko">

<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <title>CLIP | CMS</title>

    <script lang="javascript" src="<c:url value='/resources/js/jquery-3.1.0.js'/>"></script>
</head>
<body>

<div class="content-error error-404">
	<a href="#" class="logo"></a>

    <div class="description">
        <h2>ERROR</h2>
        <p>요청하신 페이지를 찾을 수 없습니다.</p>
        <a href="javascript:window.history.back();" class="button-default match-width">go back</a>
    </div>
</div>
</body>
</html>