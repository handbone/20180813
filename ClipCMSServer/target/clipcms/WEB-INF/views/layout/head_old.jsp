<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn" %>     
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="google-site-verification" 
             content="zArvCH1j3LugduMHKE-ozRvlSmmoysUi3eDB8zosvb8" />

  
</html>

<title>Clip CMS</title>

<link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/resources/css/common.css"/>
<link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/resources/css/style.css"/>
<link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/resources/css/ui/jquery-ui.min.css"/>
<!-- JQuery -->
<script lang="javascript" src="<c:out value='${pageContext.request.contextPath}'/>/resources/js/jquery-1.11.3.min.js"></script>
<script>
//백스페이스 방지코드
$(document).keydown(function(e) {
    var nodeName = e.target.nodeName.toLowerCase();

    if (e.which === 8) {
        if ((nodeName === 'input' && (e.target.type === 'text' || e.target.type === 'password' )) ||
            nodeName === 'textarea') {
        	//do nothing
        } else {
            e.preventDefault();
        }
    }
    $(".objbox").click(function(){
  	  zindex = zindex+1;
  	 var p =  $(this).parentsUntil(".pop-layer ui-draggable ui-draggable-handle");
  	 p.css("z-index",zindex);
    });
   //드레그,마우스우측키 방지 코드 (요청사항) 
    var omitformtags=["input", "textarea", "select"]
    omitformtags=omitformtags.join("|")
    function disableselect(e){
    	if (omitformtags.indexOf(e.target.tagName.toLowerCase())==-1)
    		return false
    }
    function reEnable(){
    	return true
    }
    if (typeof document.onselectstart!="undefined")
    document.onselectstart=new Function ("return false")
    else{
    	document.onmousedown=disableselect
    	document.onmouseup=reEnable
    }

});


//home
function home(){
	location.href="<c:url value='/main.do' />";
}
//logout
function logout(){
	location.href="<c:url value='/signout.do' />";
}
</script>
<script type="text/javascript">
$( document ).ready(function() {
	//lnb 상단 날짜표기
	var d = new Date();
	var week = new Array('일', '월', '화', '수', '목', '금', '토');
	var dd = d.getFullYear() + '년' + (d.getMonth() + 1) + '월' + d.getDate() + '일&nbsp;'+ week[d.getDay()] + '요일';
	$(".title").html("<img src='<c:out value='${pageContext.request.contextPath}'/>/resources/img/icon_monitor.png'>&nbsp"+ dd);
	
	$("span[class=menuNm]:contains('${menu1}')").parents('li').attr('class','active');
});

</script>

</head>
<%-- <body style="height:100%;background-image:url(<c:out value='${pageContext.request.contextPath}'/>/resources/img/body_bg.png);background-repeat: repeat-y;"  oncontextmenu="return false" onselectstart="return false" ondragstart="return false"> 
 --%>
 <body style="height:100%;background-image:url(<c:out value='${pageContext.request.contextPath}'/>/resources/img/body_bg.png);background-repeat: repeat-y;" >

<!-- Head Area -->
<div id="head_area">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/resources/img/logo.png" onclick="home()" style="cursor:pointer" class="logo">
<%--     <ul><img src="<c:out value='${pageContext.request.contextPath}'/>/resources/img/btn_home.png" onclick="home()" style="cursor:pointer"></ul>
 --%>    <ul><img src="<c:out value='${pageContext.request.contextPath}'/>/resources/img/btn_logout.png" onclick="logout()" style="cursor:pointer"></ul>
    <ul class="theme_blue">[업체명:${sessionScope.AgentCoName},  마지막접속: ${sessionScope.LastLoginTime}]</ul>
    <ul class="theme_bold">${sessionScope.AgentUserId}님 좋은 하루 되세요</ul>
</div>
<div id="head_sub_title">
	<ul class="title"> </ul>
    <ul class="sub_title">
    	<span><img src="<c:out value='${pageContext.request.contextPath}'/>/resources/img/icon_title.png"> 
    		<c:if test="${menu1 ne null}">
	    	<li>${menu1}</li>
	    	</c:if>
	    	<c:if test="${menu2 ne null}">
	    	<li>> ${menu2}</li>
	    	</c:if>
	    	<c:if test="${menu3 ne null}">
	    	<li>> ${menu3}</li>
	    	</c:if>
	    </span>
    </ul>
    <ul class="sub_nav">
      <!--   <li>대메뉴</li>
        <li>서브메뉴</li>
        <li>3차메뉴</li> -->
    </ul>
</div>

<div style="clear:both"></div>
<div id="wrapper">
	<!-- 좌측 메뉴 -->
	<div class="sub_menu_container">
		<ul class="sub_menu">
		
			<!-- UI Object -->
			<div id="menu_v" class="menu_v">
				<ul>
				<!--개인정보 취급동의 대상자 아닐경우  -->
				<c:if test="${sessionScope.RoleName ne 'ROLE_GUEST' }">
					<!--KT임직원 관리  1039 아닐경우-->
					<c:if test="${sessionScope.AgentCode ne '1039'}">
					
						<li><a href="<c:url value='/notice/noticeList.do' />"><span class="menuNm">공지dsf사항</span><span class="i"></span></a></li>
						
						<!-- rbmarket, rbm -->
						<c:if test="${sessionScope.AgentCode eq '1021' or sessionScope.AgentCode eq '1028' }">
						<li><a href="<c:url value='/serviceinfo/serviceinfoList.do' />"><span class="menuNm">서비스신청확인(SHOW)</span><span class="i"></span></a></li>
						</c:if>
						
						<c:if test="${sessionScope.AgentLevel eq '3' }">
							<li><a href="<c:url value='/agency/agencyView.do?code=${sessionScope.AgentCode }' />"><span class="menuNm">영업대행사 정보</span><span class="i"></span></a></li>
						</c:if>
						<c:if test="${sessionScope.AgentLevel ne '3' }">
						<li><a href="<c:url value='/agency/agencyList.do' />"><span class="menuNm">영업대행사 관리</span><span class="i"></span></a></li>
						</c:if>
						<c:if test="${sessionScope.AgentLevel eq '3' }">
							<li><a href="<c:url value='/agencyuser/agencyUserView.do?code=${sessionScope.AgentUserCode }' />"><span class="menuNm">영업대행사 사용자 정보</span><span class="i"></span></a></li>
						</c:if>
						<c:if test="${sessionScope.AgentLevel ne '3' }">
							<li><a href="<c:url value='/agencyuser/agencyUserList.do' />"><span class="menuNm">영업대행사 사용자 관리</span><span class="i"></span></a></li>
						</c:if>
						
						<li><a href="<c:url value='/pay/payList.do' />"><span class="menuNm">과금번호 관리</span><span class="i"></span></a></li>
						<li><a href="<c:url value='/company/companyList.do' />"><span class="menuNm">기업정보 관리</span><span class="i"></span></a></li>
						<li><a href="<c:url value='/sound/soundList.do' />"><span class="menuNm">음원정보 관리</span><span class="i"></span></a></li>
						<li><a href="<c:url value='/reserved/reservedList.do' />"><span class="menuNm">비즈링 예약가입</span><span class="i"></span></a></li>
						<li><a href="<c:url value='/reserved/reservedCancelList.do' />"><span class="menuNm">비즈링 예약해지</span><span class="i"></span></a></li>
						<li><a href="<c:url value='/userinfo/userInfoList.do' />"><span class="menuNm">고객정보 조회</span><span class="i"></span></a></li>
						<li><a href="<c:url value='/ring/ringList.do' />"><span class="menuNm">링투유가입</span><span class="i"></span></a></li>
						<li><a href="<c:url value='/clip/bizringList.do' />"><span class="menuNm">비즈링설정</span><span class="i"></span></a></li>
						<li><a href="<c:url value='/bizusersearch/bizusersearch.do' />"><span class="menuNm">비즈링 고객정보조회</span><span class="i"></span></a></li>
						<li><a href="<c:url value='/comsoundbatch/comSoundBatchFormAction.do'/>"><span class="menuNm">기업별 일괄 음원설정</span><span class="i"></span></a></li>
						
						<c:if test="${sessionScope.AgentCode eq '1007' or sessionScope.AgentCode eq '1012' or sessionScope.AgentLevel eq '1'}">
							<li><a href="<c:url value='/cancelcompany/cancelForm.do' />"><span class="menuNm">이벤트&링투유기업해지</span><span class="i"></span></a></li>
							<li><a href="<c:url value='/cancelpart/cancelPartForm.do' />"><span class="menuNm">이벤트&링투유부분해지</span><span class="i"></span></a></li>
						</c:if>
						
						<li><a href="<c:url value='/bizcancelcompany/bizCancelForm.do' />"><span class="menuNm">비즈링&링투유기업해지</span><span class="i"></span></a></li>
						<li><a href="<c:url value='/bizcancelpart/bizCancelPartForm.do' />"><span class="menuNm">비즈링&링투유부분해지</span><span class="i"></span></a></li>
						<li><a href="<c:url value='/statistics/dayStatisticsList.do' />"><span class="menuNm">통계 > 기업별 설정/해지 내역</span><span class="i"></span></a></li>
						<li><a href="<c:url value='/statistics/billingTermStatisticsList.do' />"><span class="menuNm">통계 > 기간별 과금내역</span><span class="i"></span></a></li>
						
						<c:if test="${sessionScope.AgentLevel ne '3' }">
							<li><a href="<c:url value='/statistics/billingTermAgentStatisticsList.do' />"><span class="menuNm">통계 > 기간별 상세과금내역</span><span class="i"></span></a></li>
						</c:if>
						
						<c:if test="${sessionScope.AgentLevel eq '3' }">
							<li><a href="<c:url value='/statistics/billingTermCompanyStatisticsList.do' />"><span class="menuNm">통계 > 기간별 상세과금내역</span><span class="i"></span></a></li>
						</c:if>
						
						<li><a href="<c:url value='/statistics/billingStatisticsList.do' />"><span class="menuNm">통계 > 과금통계</span><span class="i"></span></a></li>
						<li><a href="<c:url value='/statistics/dayBillingStatisticsList.do' />"><span class="menuNm">통계 > 일별과금내역</span><span class="i"></span></a></li> 
						
						<c:if test="${sessionScope.AgentCode eq '1007' or sessionScope.AgentCode eq '1012' or sessionScope.AgentCode eq '1004' or sessionScope.AgentLevel eq '1'}">
							<c:if test="${sessionScope.AgentCode ne '1004'}"><!--1004 케이티 엠하우스  -->
								<li><a href="<c:url value='/statistics/ponyBillingStatisticsList.do' />"><span class="menuNm">통계 > 월정산</span><span class="i"></span></a></li>
							</c:if>
							<li><a href="<c:url value='/statistics/bizGaipStatisticsList.do' />"><span class="menuNm">통계 > 비즈링가입내역</span><span class="i"></span></a></li>
							<li><a href="<c:url value='/statistics/bizHejiStatisticsList.do' />"><span class="menuNm">통계 > 비즈링해지내역</span><span class="i"></span></a></li>
							<li><a href="<c:url value='/statistics/agentDayResultStatisticsList.do' />"><span class="menuNm">통계 > 대행사 일별실적</span><span class="i"></span></a></li>
						</c:if>
				
					</c:if>
					<!--KT임직원 관리  1039 경우-->
					<c:if test="${sessionScope.AgentCode eq '1039'}">
						<li><a href="<c:url value='/reserved/reservedList.do' />"><span class="menuNm">비즈링 예약가입</span><span class="i"></span></a></li>
						<li><a href="<c:url value='/reserved/reservedCancelList.do' />"><span class="menuNm">비즈링 예약해지</span><span class="i"></span></a></li>
						<li><a href="<c:url value='/bizcancelcompany/bizCancelForm.do' />"><span class="menuNm">비즈링&링투유기업해지</span><span class="i"></span></a></li>
						<li><a href="<c:url value='/bizcancelpart/bizCancelPartForm.do' />"><span class="menuNm">비즈링&링투유부분해지</span><span class="i"></span></a></li>
						<li><a href="<c:url value='/statistics/dayStatisticsList.do' />"><span class="menuNm">통계 > 기업별 설정/해지 내역</span><span class="i"></span></a></li>
					</c:if>
					<li><a href="<c:url value='/allcompanydata/allCompanyDataList.do' />"><span class="menuNm">현행화 데이터	</span><span class="i"></span></a></li>
					<li><a href="<c:url value='/voc/vocList.do' />"><span class="menuNm">가입해지오류 확인요청</span><span class="i"></span></a></li>
					<c:if test="${sessionScope.AgentLevel eq '1'}">
						<li><a href="<c:url value='/system/systemMain.do' />"><span class="menuNm">*시스템 관리(관리자,로그)</span><span class="i"></span></a></li>
					</c:if>
				</c:if>
				<!--개인정보 취급동의 대상자 화면  -->	
				<c:if test="${sessionScope.RoleName eq 'ROLE_GUEST' }">
					<li><a href="<c:url value='/privateCert.do' />"><span class="menuNm">개인정보 취급동의</span><span class="i"></span></a></li>
				</c:if>
				</ul>
			</div><!-- //UI Object -->
		</ul>
    </div><!-- sub_menu_container -->

	<div class="container">
		<!-- 컨텐츠 시작 -->
		<div id="content">

