<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn" %>     
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="google-site-verification" 
             content="zArvCH1j3LugduMHKE-ozRvlSmmoysUi3eDB8zosvb8" />

  
</html>

<title>Clip CMS</title>

<link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/resources/css/common.css"/>
<link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/resources/css/style.css?20171127"/>
<link rel="stylesheet" type="text/css" href="<c:out value='${pageContext.request.contextPath}'/>/resources/css/ui/jquery-ui.min.css"/>
<!-- JQuery -->
<script lang="javascript" src="<c:out value='${pageContext.request.contextPath}'/>/resources/js/jquery-3.1.0.js"></script>
<script>
//백스페이스 방지코드
$(document).keydown(function(e) {
    var nodeName = e.target.nodeName.toLowerCase();

    if (e.which === 8) {
        if ((nodeName === 'input' && (e.target.type === 'text' || e.target.type === 'password' )) ||
            nodeName === 'textarea') {
        	//do nothing
        } else {
            e.preventDefault();
        }
    }
    $(".objbox").click(function(){
  	  zindex = zindex+1;
  	 var p =  $(this).parentsUntil(".pop-layer ui-draggable ui-draggable-handle");
  	 p.css("z-index",zindex);
    });
   //드레그,마우스우측키 방지 코드 (요청사항) 
    var omitformtags=["input", "textarea", "select"]
    omitformtags=omitformtags.join("|")
    function disableselect(e){
    	if (omitformtags.indexOf(e.target.tagName.toLowerCase())==-1)
    		return false
    }
    function reEnable(){
    	return true
    }
    if (typeof document.onselectstart!="undefined")
    document.onselectstart=new Function ("return false")
    else{
    	document.onmousedown=disableselect
    	document.onmouseup=reEnable
    }

});


//home
function home(){
	location.href="<c:url value='/main.do' />";
}
//logout
function logout(){
	location.href="<c:url value='/signout.do' />";
}

function goPage(url){
	$('#f').attr('action', url);
	f.submit();
}
</script>
<script type="text/javascript">
$( document ).ready(function() {
	//lnb 상단 날짜표기
	var d = new Date();
	var week = new Array('일', '월', '화', '수', '목', '금', '토');
	var dd = d.getFullYear() + '년' + (d.getMonth() + 1) + '월' + d.getDate() + '일&nbsp;'+ week[d.getDay()] + '요일';
	$(".title").html("<img src='<c:out value='${pageContext.request.contextPath}'/>/resources/img/icon_monitor.png'>&nbsp"+ dd);
	
	$("span[class=menuNm]:contains('${menu1}')").parents('li').attr('class','active');
	$("span[class=menuNm]:contains('${menu2}')").parents('li').attr('class','active');
});

</script>

</head>
<%-- <body style="height:100%;background-image:url(<c:out value='${pageContext.request.contextPath}'/>/resources/img/body_bg.png);background-repeat: repeat-y;"  oncontextmenu="return false" onselectstart="return false" ondragstart="return false"> 
 --%>
 <body style="height:100%;background-image:url(<c:out value='${pageContext.request.contextPath}'/>/resources/img/body_bg.png);background-repeat: repeat-y;" >

<!-- Head Area -->
<div id="head_area">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/resources/img/logo.png" onclick="home()" style="cursor:pointer" class="logo">
    <ul><img src="<c:out value='${pageContext.request.contextPath}'/>/resources/img/btn_logout.png" onclick="logout()" style="cursor:pointer"></ul>
    <ul class="theme_blue">[마지막접속: ${sessionScope.LastLoginTime}]</ul>
    <ul class="theme_bold">${sessionScope.AgentUserId}님 좋은 하루 되세요</ul>
</div>
<div id="head_sub_title">
	<ul class="title"> </ul>
    <ul class="sub_title">
    	<span><img src="<c:out value='${pageContext.request.contextPath}'/>/resources/img/icon_title.png"> 
    		<c:if test="${menu1 ne null}">
	    	<li>${menu1}</li>
	    	</c:if>
	    	<c:if test="${menu2 ne null}">
	    	<li>> ${menu2}</li>
	    	</c:if>
	    	<c:if test="${menu3 ne null}">
	    	<li>> ${menu3}</li>
	    	</c:if>
	    </span>
    </ul>
    <ul class="sub_nav">
      <!--   <li>대메뉴</li>
        <li>서브메뉴</li>
        <li>3차메뉴</li> -->
    </ul>
</div>

<div style="clear:both"></div>
<form id="f" method="POST"></form>
<div id="wrapper">
	<!-- 좌측 메뉴 -->
	<div class="sub_menu_container">
		<ul class="sub_menu">
			
			<!-- UI Object -->
			<div id="menu_v" class="menu_v">
				<ul>
				<c:if test="${sessionScope.RoleName eq 'ROLE_ADMIN'}">
						<li><a href="<c:url value='/user/userList.do' />"><span class="menuNm">사용자 관리</span></a></li>
						<li><ul><li><a href="<c:url value='/user/userList.do' />"><span class="menuNm">ID 관리</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/user/viewLogLoginOut.do' />"><span class="menuNm">로그인/아웃 내역확인</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/user/viewLogMenu.do' />"><span class="menuNm">메뉴 접속로그</span></a></li></ul></li>
				</c:if>
						<li><a href="<c:url value='/manage/acl/aclList.do' />"><span class="menuNm">포인트관리</span></a></li>
						<li><ul><li><a href="<c:url value='/manage/acl/aclList.do' />"><span class="menuNm">매체관리</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/manage/stat/statLog.do' />"><span class="menuNm">통계관리</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/manage/stat/statLog2.do' />"><span class="menuNm">통계확장</span></a></li></ul></li>
						<%-- <li><ul><li><a href="<c:url value='/manage/manageLog/manageLog.do' />"><span class="menuNm">로그관리</span></a></li></ul></li> --%>
						<li><ul><li><a href="<c:url value='/manage/manageLog/manageLog.do' />"><span class="menuNm">간편조회</span></a></li></ul></li>
						<li><ul><li><a href="#" onclick="goPage('<c:url value="/clip3_0/KTCPMW01.do" />')"><span class="menuNm">카드포인트 전환 조회</span></a></li></ul></li>
						<li><ul><li><a href="#" onclick="goPage('<c:url value="/clip3_0/KTCPMW02.do" />')"><span class="menuNm">쇼핑적립 적립예정 내역 조회</span></a></li></ul></li>
			 			<li><ul><li><a href="<c:url value='/clip3_0/KTCPMW03.do' />"><span class="menuNm">쇼핑적립 적립 내역 조회</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/clip3_0/KTCPMW04.do' />"><span class="menuNm">포인트 쿠폰 내역 조회</span></a></li></ul></li>
<%-- 						<li><ul><li><a href="<c:url value='/clip3_0/KTCPMW05.do' />"><span class="menuNm">매장 바로 쓰기 내역 조회</span></a></li></ul></li> --%>
						<li><ul><li><a href="<c:url value='/clip3_0/KTCPMW06.do' />"><span class="menuNm">포인트 폭탄 내역 조회</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/manage/userInfo/userInfo.do' />"><span class="menuNm">사용자 조회</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/manage/userInfo/userLog.do' />"><span class="menuNm">사용자 변경기록</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/manage/userInfo/userHistory.do' />"><span class="menuNm">사용자 기록조회</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/manage/pointChk/anomalyList2.do' />"><span class="menuNm">이상탐지(합계)</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/manage/pointChk/anomalyList.do' />"><span class="menuNm">이상탐지(매체별)</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/manage/pointChk/pointChk2.do' />"><span class="menuNm">TOP 30(합계)</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/manage/pointChk/pointChk.do' />"><span class="menuNm">TOP 30(매체별)</span></a></li></ul></li>
						<!--  -->
						<li><ul><li><a href="<c:url value='/manage/prod_test.do' />"><span class="menuNm">상용 테스터 등록</span></a></li></ul></li>
						<!--  -->
						<c:if test="${sessionScope.RoleName eq 'ROLE_ADMIN' || sessionScope.RoleName eq 'ROLE_USER_2' }">
						<li><ul><li><a href="<c:url value='/manage/pointChk/pointUpdateHistory.do' />"><span class="menuNm">포인트 조정</span></a></li></ul></li>
						</c:if>
						<li><a href="<c:url value='/roulette/channel/channelList.do' />"><span class="menuNm">룰렛관리</span></a></li>
						<li><ul><li><a href="<c:url value='/roulette/channel/channelList.do' />"><span class="menuNm">채널관리</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/roulette/viewLog/joinByChannel.do' />"><span class="menuNm">[통계] 채널별 참여자</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/roulette/viewLog/detailByChannel.do' />"><span class="menuNm">[통계] 채널별 당첨내역</span></a></li></ul></li>
						<!-- 현재 사용하고 있지 않은 페이지 -->
						<%-- <li><ul><li><a href="<c:url value='/roulette/viewLog/pageView.do' />"><span class="menuNm">[통계] 페이지뷰</span></a></li></ul></li> --%>
						<li><ul><li><a href="<c:url value='/roulette/viewLog/join.do' />"><span class="menuNm">[로그] 참여자 로그</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/roulette/viewLog/connectAPI.do' />"><span class="menuNm">[로그] API연동 로그</span></a></li></ul></li>
						<!-- 현재 사용하고 있지 않은 페이지 -->
						<%-- <li><ul><li><a href="<c:url value='/roulette/viewLog/joinEvent.do' />"><span class="menuNm">[로그] 페이지뷰 로그</span></a></li></ul></li> --%>
						<li><ul><li><a href="<c:url value='/roulette/viewLog/dailyJoinSum.do' />"><span class="menuNm">[로그] 일자별 당첨자합계 로그</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/roulette/viewLog/timeGbn.do' />"><span class="menuNm">[로그] 시간당첨값 로그</span></a></li></ul></li>
						<%-- <li><a href="<c:url value='' />"><span class="menuNm">MY포인트관리</span></a></li> --%>
						<li><a href="<c:url value='' />"><span class="menuNm">형상관리</span></a></li>
						<li><ul><li><a href="<c:url value='/mypoint/banner/bannerList.do' />"><span class="menuNm">배너관리</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/clip3_0/KTCPMW07.do' />"><span class="menuNm">잠금화면 일차 관리</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/clip3_0/KTCPMW08.do' />"><span class="menuNm">잠금화면 랜덤포인트 관리</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/clip3_0/KTCPMW09.do' />"><span class="menuNm">쇼핑적립 관리</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/clip3_0/KTCPMW10.do' />"><span class="menuNm">CLIP3.0 메인 배너 관리</span></a></li></ul></li>
<%-- 						<li><ul><li><a href="<c:url value='/clip3_0/KTCPMW11.do' />"><span class="menuNm">룰렛 배너 구좌 관리</span></a></li></ul></li> --%>
				<c:if test="${sessionScope.RoleName eq 'ROLE_ADMIN'}">		
						<li><a href="<c:url value='/onm/bzadPostLog.do' />"><span class="menuNm">ONM</span></a></li>
						<li><ul><li><a href="<c:url value='/onm/bzadPostLog.do' />"><span class="menuNm">[로그] BZAD</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/onm/bzScreenPostLog.do' />"><span class="menuNm">[로그] BZSCREEN</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/onm/smsSend.do' />"><span class="menuNm">[로그] SMS 발송기록</span></a></li></ul></li>
						<li><ul><li><a href="<c:url value='/onm/statConnect.do' />"><span class="menuNm">엑세스로그</span></a></li></ul></li>
				</c:if>
				
				</ul>
			</div><!-- //UI Object -->
		</ul>
    </div><!-- sub_menu_container -->

	<div class="container">
		<!-- 컨텐츠 시작 -->
		<div id="content">

