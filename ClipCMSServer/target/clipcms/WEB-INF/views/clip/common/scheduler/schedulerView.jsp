<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>

<script>

 $(document).ready(function(){ 
	 
	
}); 

</script>

	<div class="top_title"><h2> schedulerView</h2></div>
        
   <br>

   <table class="table_style1" cellpadding="0" cellspacing="0">
        <tr>
        	<td > 
			<textarea id="desc01" name="desc01" rows="50" cols="150">  
					
			
프로그램명	작동시간	메소드명	비고
make_daily_stat 프로시져 호출 	매일 4시 0분에 실행	makeDailyStat	
make_daily_stat2 프로시져 호출 	매일 4시 30분에 실행	makeDailyStat2	
make_media_stat 프로시져 호출 	매일 3시 30분에 실행	makeMediaStat	
당첨자 합계테이블 생성	매일 1시 0분에 실행	makePointRouletteJoinSum	
페이지뷰 통계 생성	매일 3시 0분에 실행	makePointRouletteConnLogSum	
부정정립 체크	10분마다 실행 실행	falsePointChk	
TOP 30 통계 생성	매일 4시 15분에 실행	makeTop30	어제 날짜의 통계를 생성한다
TOP 30 (매체별 )통계 생성	매일 5시 0분에 실행	makeTop30_2	어제 날짜의 통계를 생성한다
이상탐지 통계 생성	매시간 10분에 실행	makeAnomaly	2시 10분에 실행된다면 1시~2시에 해당하는 결과값을 조회
이상탐지 통계 생성2	매시간 5분에 실행	makeAnomaly2	2시 5분에 실행된다면 1시~2시에 해당하는 결과값을 조회
			
			
프로그램명	URL		
make_daily_stat 프로시져 호출 즉시실행	/clipcms/makeDailyStat.do?preDate=20160921	makeDailyStat	어제의 통계를 뽑고 싶다면 preDate가 어제 날짜이다.
make_daily_stat2 프로시져 호출 즉시실행	/clipcms/makeDailyStat2.do?preDate=20160921	makeDailyStat2	어제의 통계를 뽑고 싶다면 preDate가 어제 날짜이다.
make_media_stat 프로시져 호출  즉시실행	/clipcms/makeMediaStat.do?preDate=20160921	makeMediaStat	어제의 통계를 뽑고 싶다면 preDate가 어제 날짜이다.
당첨자 합계테이블 생성 즉시실행	/clipcms/makePointRouletteJoinSum.do?curDate=20160926	makePointRouletteJoinSum	
페이지뷰 통계 생성 즉시실행	/clipcms/makePointRouletteConnLogSum.do?curDate=20160925	makePointRouletteConnLogSum	어제의 통계를 뽑고 싶다면 curDate는 오늘 날짜이다.
부정정립 체크 즉시실행	/clipcms/falsePointChkCurr.do	falsePointChkCurr	
TOP 30 통계 생성  즉시 실행 	/clipcms/makeTop30.do?curDate=20161102&limitCount=30		해당날짜 기준으로 동작 - 오늘 날짜를 넣으면 오늘 통계가 쌓인다
TOP 30 (매체별) 통계 생성  즉시 실행 	/clipcms/makeTop30_2.do?curDate=20161102&limitCount=30		해당날짜 기준으로 동작 - 오늘 날짜를 넣으면 오늘 통계가 쌓인다
이상탐지 통계 생성  즉시 실행	/clipcms/makeAnomaly.do?curDate=20161109&curTime=18&limitPoint=1	makeAnomaly	14시를 조회한다면 14시~15시에 해당하는 결과값을 조회
이상탐지 통계 생성  즉시 실행2	/clipcms/makeAnomaly2.do?curDate=20161109&curTime=18&limitPoint=1	makeAnomaly2	14시를 조회한다면 14시~15시에 해당하는 결과값을 조회
			
			
암호화 테스트	/crypto.do?stringData=testStering		
	http://localhost:8080/clipcms/decrypto.do?stringData=njzWiYc4cX9LdrtDn3rMuRnwa/JnNuXt3dvSwYitbgE0YGf8xFu9WMxIqCxmeG+1		
			
			
			
	/roulette/test01.do		
웹상에서 쿼리 테스트 할 수 있는 페이지	http://localhost:8080/clipcms/sendQuery.do		
	http://event.smarthubs.co.kr/clipcms/sendQuery.do		
	http://localhost:8080/clipcms/schedulerView.do		
	http://localhost:8080/clipcms/sendExitUser.do?user_ci=ibxAdsL7f3IiFIWVAG2T4JA/1qrRGnv+ThMOkSjJNddGnWgqgO5GE7jt688O3Moxz4AT1LzdogDyEv7Bfv+8Cw==		
			
인증 페이지 테스트	http://localhost:8080/clip/roulette/agreeForm.do?roulette_id=2016001003259&keytype=3&keyid=gaidgaidgaidgaid&returnPage=rouletteMain2&status=test		
			
수동실행	SELECT make_media_stat( '20161111' ) -- 그냥 select만 해주면 된다. 기존 ROW는 업데이트하거나 삭제해주고 작업한다.		
			
IF 회원탈퇴시 사용하는 암호화	http://localhost:8080/sendDecrypt.do?id=lSTrHwxwCpQ/VADiXiLr7E9+EIqafzCSbRPKFA5W3Fo=		
	http://localhost:8080/sendIncrypt.do?id=01012345678		
			
http://event111.smarthubs.co.kr/clipcms/sendMinusPoint.do?ctn=01054441324&point_value=62066			
http://even111t.smarthubs.co.kr/clipcms/sendMinusPoint.do?ctn=01035086882&point_value=97300			
http://even111t.smarthubs.co.kr/clipcms/sendMinusPoint.do?ctn=0166948803&point_value=2645000			
			
http://event.smarthubs.co.kr/clipcms/sendPlusPoint.do?ctn=01090035760&point_value=44922			

			
			
			
			
			</textarea> 
			</td>
        </tr>
    </table>
    