<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>

<script>


$( function() {
    $( "#datepicker1, #datepicker2" ).datepicker({
    autoSize: true,
    showOn: 'button', 
    buttonImage: '<c:url value='/resources/css/ui/img/calendar.gif'/>',   
    buttonImageOnly: true,
    dateFormat: 'yymmdd'});
} );
$.datepicker.setDefaults( $.datepicker.regional[ "ko" ] );

 $(document).ready(function(){ 
	  
	 $('img.ui-datepicker-trigger').attr('style', 'margin:0px 7px 0px 7px');
	 
	// 검색
	 $("#goSearch").bind("click",function() {
		 $("#excelDownload").val("");
		 formSubmit();
	 });
	 
	 // 엑셀 다운로드
	 $("#goExcel").bind("click",function() {
		 formSubmit("goExcel");
	 });
	 
	 function formSubmit(goExcelString){
		 
		 if( goExcelString == "goExcel"){
			 $("#excelDownload").val("Y");
		 }
		 
			$("#startdate").val($("#datepicker1").val());
			$("#enddate").val($("#datepicker2").val());
			
			 if($("#startdate").val() == ""){
				 	alert("시작일을 입력해주세요.");
					return;
				 }
			 if($("#enddate").val() == ""){
				 	alert("종료일을 입력해주세요.");
					return;
				 }
		 $('#mainForm').attr('action','<c:url value="/manage/pointChk/anomalyList2.do" />').attr('method', 'post').submit();
	 }
	 
	 // 검색값 설정
	 if( '' != '${searchBean.point_type}'){
			$("input[name=point_type][value=" + "${searchBean.point_type}" + "]").attr("checked", true);
		}
    if('${searchBean.media}'!=''){
  		$("#media").val('${searchBean.media}').attr("selected", "selected");
    }
	/*  if('${searchBean.limit}' == ''){
		 $("#limit").val('100');
	 }else{
		$("#limit").val('${searchBean.limit}');
	 } */
    $("#datepicker1").val('${searchBean.startdate}');
	 $("#datepicker2").val('${searchBean.enddate}');
	 
	var preDate = new Date();
	preDate.setDate(preDate.getDate() );
	if( '' == $("#datepicker1").val()){
		$("#datepicker1").val($.datepicker.formatDate('yymmdd', preDate));
	}
	if( '' == $("#datepicker2").val()){
		$("#datepicker2").val($.datepicker.formatDate('yymmdd', preDate));
	}
	
	
	
	
	
	
	$(".table_style1 #viewDetail").bind("click",function() {
		 
		 	$("#selectdate").val($(this).parent().parent().find("td:eq(1)").text().split(" : ")[0]);
			$("#user_ci").val($(this).parent().parent().find("td:eq(3)").text());
			$("#stat_hour").val($(this).parent().parent().find("td:eq(1)").text().split(" : ")[1]);
			$("#statusNum").val($(this).parent().parent().find("td:eq(3)").attr("id"));
			 
			if( 'Y' == '${WINDOW_OPEN_YN}' ){
				//$("#WINDOW_OPEN_YN").val('Y');
				window.open("", "popup", "width=700px, height=800px, left=100px, top=100px");
				document.mainForm.target = "popup";
				document.mainForm.method = "POST";
				document.mainForm.action = "<c:url value='/manage/pointChk/viewDetailAnomalyPopup.do'/>";
				document.mainForm.submit();
				document.mainForm.target = "";
				//$("#WINDOW_OPEN_YN").val('N');
			}else{

				 formData = $("#mainForm").serialize();
			 	 $.ajax({	
			            type: "POST",
			            url: "<c:url value='/manage/pointChk/viewDetailAnomaly.do' />",
			            data: formData,
			            dataType: "Json",
			            success: function (resData) {
			                  
			              $.each(resData, function(index, item){
			            	  
			            	  var inserthtml = ""; 
			            	  
			            	  inserthtml += "<tr style=\"background:#e3e9f1;\">";
			            	  inserthtml += "<td align=\"center\">";
			                 
			            	  inserthtml += item.media + "  </td>";
			            	  /*  inserthtml += "<td align=\"center\" >"+item.reg_time.substring(0,2)+":"+item.reg_time.substring(2,4)+":"+item.reg_time.substring(4,6)+"</td>"; */
			            	  inserthtml += "<td colspan=\"4\" align=\"center\" >"+item.description+"</td>";
			            	  inserthtml += "<td align=\"center\" style=\"word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;\" ><span title=\""+item.ctn+"\"><img id=\"convertCtn\" src=\"<c:out value='${pageContext.request.contextPath}'/>/resources/img/icon_title.png\"> "+item.ctn;
			            	  inserthtml += "</td>";
			            	  inserthtml += "<td align=\"center\" style=\"word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;\" ></td>";
			            	  inserthtml += "<td align=\"right\"> "+item.point+" P</td>";
			            	  inserthtml += "<td align=\"center\">";
			                
			            	  inserthtml += 		    			"</td>";
			                
			            	  inserthtml += "</tr>";
			            	  
			            	  $('#'+$("#statusNum").val()).parent().after(inserthtml);
			            	  $('#'+$("#statusNum").val()).parent().find("td:eq(8)").text("");
			              
			              });
			              
			              $(".table_style1 #convertCtn").unbind().bind("click",function() {
			          		
			          		$("#stringData").val($(this).parent().text().trim());
			          		 formData = $("#mainForm").serialize();
			          	 	 $.ajax({	
			          	            type: "POST",
			          	            url: "<c:url value='/decrypto.do'/>",
			          	            data: formData,
			          	            dataType: "Json",
			          	            success: function (resData) {
			          	                  alert(resData.ctn);
			          	            },
			          	            error: function (e) {
			          	            	alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
			          	            }
			          	            
			          	        }); 
			          	 });
			             
			              
			            },
			            error: function (e) {
			            	alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
			            }
			        });
			 	  
			 	 
			}
			
		 	 
		 	 
	 });
	
	
	
	
	$(".table_style1 #convertCtn").unbind().bind("click",function() {
		
		$("#stringData").val($(this).parent().text().trim());
		 formData = $("#mainForm").serialize();
	 	 $.ajax({	
	            type: "POST",
	            url: "<c:url value='/decrypto.do'/>",
	            data: formData,
	            dataType: "Json",
	            success: function (resData) {
	                  alert(resData.ctn);
	            },
	            error: function (e) {
	            	alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
	            }
	            
	        }); 
	 });
	
}); 

</script>

	<div class="top_title"><h2> 이상탐지(합계) </h2></div>
    <!-- 검색조건 -->
    <form id="mainForm" name="mainForm" >
    <table width="100%" class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="100">
            <col width="">
       
        </colgroup>
		<tr>
        	<th>검색조건</th>
        	<td>
            	<select class="form_sel_style1" name="status" id="status">
            		<!-- <option value="" selected>전체</option>
            		<option value="cust_id" >cust_id</option>
                	<option value="ga_id" >ga_id</option> -->
                	<option value="ctn" selected>ctn</option>
            	</select>
            	<input class="form_input_style2" id="keywd" name="keywd" style="width:200px" value="${searchBean.keywd}">
            </td>
        </tr>
		<tr>
       	<th>검색기간</th>
       	<td>
           	<input name="datepicker1"  type="text"  id="datepicker1"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
		 ~ <input name="datepicker2" type="text" id="datepicker2"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
		  <br>* 이상탐지 기준값은 '이상탐지(시간별)' 메뉴에서 설정 할 수 있습니다. 한시간에 한번씩 자동으로 '기준값 이상 적립한 사용자'를 찾습니다. 
		  <br>해당 사용자가 있을때만 리스트에 보여집니다. </td>
       </tr>
       <tr>
       	<th>포인트 타입</th>
       	<td>
           		<input id="point_type" name="point_type" value="I" checked="CHECKED" type="radio" /> 적립
          		 <input id="point_type" name="point_type" value="O" type="radio"/> 차감
           </td>
       </tr>
    </table>
    <div id="btn_area">
    <input type="button" id="goSearch" name="goSearch" class="btn_area_a" value="검색" >
    <input type="button" id="goExcel" name="goExcel" class="btn_area_a" value="엑셀 다운로드" >
    </div>
    <input type="hidden" id="startdate" name="startdate" value="" >
    <input type="hidden" id="enddate" name="enddate" value="" >
    <input type="hidden" id="excelDownload" name="excelDownload" value="" >
    
    <input type="hidden" id="user_ci" name="user_ci" value="" >
    <input type="hidden" id="stringData" name="stringData" value="" >
     <input type="hidden" id="stat_hour" name="stat_hour" value="" >
     <input type="hidden" id="statusNum" name="statusNum" value="" >
	<input type="hidden" id="selectdate" name="selectdate" value="" >
	
	
	<input type="hidden" id="WINDOW_OPEN_YN" name="WINDOW_OPEN_YN" value="" >
	<input type="hidden" id="pg" name="pg" value="" >
	
    </form>
    
    
   <br>

   <table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
    	<col width="">
    	<col width="">
    	<col width="">
    	<col width="">
    	<col width="">
    		<col width="">
            <col width="">
            <col width="">
            <col width="">
        </colgroup>
        <tr>
        	<th>매체</th>
        	<th>날짜 : 시간</th>
        	<th>사용/적립</th>
        	<th>user_ci</th>
        	<th>cust_id</th>
        	<th>ctn</th>
        	<th>ga_id</th>
        	<th>포인트</th>
        	<th>상세</th>
        </tr>
        
        <c:forEach var="list" items="${list}" varStatus="status">
        <tr>
            <td align="center">합계</td>
            <td align="center">${list.stat_date} : ${list.stat_hour}</td>
            <td align="center">
             <c:choose>
				 <c:when test="${list.point_type == 'I'  }">
				  적립
				 </c:when>
				 <c:when test="${list.point_type == 'O' }">
				  사용
				 </c:when>
			</c:choose>  
			</td>
            <td id="${status.count}" align="center" style="word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;" ><span title="${list.user_ci}">${list.user_ci}</span></td>
            <td align="center" style="word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;" ><span title="${list.cust_id}">${list.cust_id}</span></td>
            <td align="center" style="word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;" ><span title="${list.ctn}"><img id="convertCtn" src="<c:out value='${pageContext.request.contextPath}'/>/resources/img/icon_title.png"> ${list.ctn}
            
            </span></td>
            <td align="center" style="word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;" ><span title="${list.ga_id}">${list.ga_id}</span></td>
            <td align="right"><fmt:formatNumber value="${list.point}" pattern="#,###" /> P</td>
            <td align="center">
				  	<input type="button" id="viewDetail" name="viewDetail" class="btn_area_a" value="상세" >
			</td>
            
        </tr>
        </c:forEach>
        <c:if test="${fn:length(list) == 0}" >
        <tr>
        	<td colspan="8" > 조회값이 없습니다. </td>
        </tr>
        </c:if>
    </table>
    
  
    <table width="100%"><tr height="45px"><td align="center">${boardPaging.pagingHTML}</td></tr></table>
    <script>
    //페이징
    function boardPaging(pg){
    	
    	 $("#pg").val(pg);
    	 $("#excelDownload").val("");
    	 
    	$("#startdate").val($("#datepicker1").val());
		$("#enddate").val($("#datepicker2").val());
			
			 if($("#startdate").val() == ""){
				 	alert("시작일을 입력해주세요.");
					return;
				 }
			 if($("#enddate").val() == ""){
				 	alert("종료일을 입력해주세요.");
					return;
				 }
		 $('#mainForm').attr('action','<c:url value="/manage/pointChk/anomalyList2.do" />').attr('method', 'post').submit();

    }
    </script>
    
    
    
    
    