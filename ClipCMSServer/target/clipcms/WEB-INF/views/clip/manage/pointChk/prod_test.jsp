<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>

<script>

	$(document).ready(function(){ 
		
		$('#show_add_page').click(function(){
			
			var user_ci = $('#user_ci').val();
			var ctn = $('#ctn').val();
			
			if (!user_ci && user_ci.trim().length == 0 ){
				alert('user_ci를 입력하세요.');
				return;
			}
			
			if (!ctn && ctn.trim().length == 0){
				alert('ctn를 입력하세요.');
				return;
			}
			
			if (confirm('등록하시겠습니까?')) {				
				var formData = $('#fm').serialize();
				 $.ajax({	
			            type: "POST",
			            url: "<c:url value='/manage/prod_add_tester.do'/>",
			            data: formData,
			            dataType: "Json",
			            success: function (resData) {
			            		if (resData.result == 'success') {
			            			location.reload();
							}
			            },
			            error: function (e) {
			            	alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
			            }
				});
			}
		});
		
		$('button[name=action_del]').click(function(){
			if (confirm('삭제하시겠습니까?')) {
				 $.ajax({	
			            type: "POST",
			            url: "<c:url value='/manage/prod_del_tester.do' />",
			            data: {key : $(this).val()},
			            dataType: "Json",
			            success: function (resData) {
				            	if (resData.result == 'success') {
				            		$('#row'+resData.id).remove();
							}else{
								alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
							}
			            },
			            error: function (e) {
			            		alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
			            }
				});
			}
		});
		
		$('button[name=active_button]').click(function(){
			var obj = $(this);
			$.ajax({	
	            type: "POST",
	            url: "<c:url value='/manage/prod_active_tester.do' />",
	            data: {key : obj.attr('id'), state : obj.val()},
	            dataType: "Json",
	            success: function (resData) {	            	
	            		if (resData.result == 'success') {
	            			obj.val(resData.state)
	    	    				obj.html(resData.msg);
					}else{
						alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
					}
	            },
	            error: function (e) {
	           		alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
	            }
			});
		});
		
	});
	
</script>

<div class="top_title"><h2> 상용 테스터 등록 </h2></div>

<form id="fm">
	<table width="80%" class="table_style1" cellpadding="0" cellspacing="0">
		<colgroup>
			<col width="10%">
			<col width="">
		</colgroup>
		<tr>
	       	<th>user_ci</th>
	       	<td>
	       		<input id="user_ci" name="user_ci" type="text" style="width: 50%">
	       	</td>
	    </tr>
		<tr>
	       	<th>ctn</th>
	       	<td>
	       		<input id="ctn" name="ctn" type="text" style="width: 50%">
	       	</td>
	    </tr>
		<tr>
	       	<th>활성화</th>
	       	<td>
	       		<input type="radio" name="active" value="1" checked>&nbsp;&nbsp;활성&nbsp;&nbsp;<input name="active" type="radio" value="0">&nbsp;&nbsp;비활성
	       	</td>
	    </tr>
	</table>
</form>

<input type="button" id="show_add_page" class="btn_area_a" value="신규등록" style="float: right;margin-bottom: 15px">

<div style="width: 100%;height: 500px;overflow:scroll;">
	<form>
		<table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
	  	 	<colgroup>
	    		<col width="5%">
	    		<col width="15%">
	    		<col width="45%">
	    		<col width="20%">
	    		<col width="15%">
	       </colgroup>
	       <tr>
	       	<th>구분</th>
	       	<th>등록일시</th>
	       	<th>user_ci</th>
	       	<th>ctn</th>
	       	<th>비고</th>
	       </tr>	       
	       <c:forEach var="list" items="${list}" varStatus="status">
	       <tr id="row${status.count}">
	           <td align="center">${status.count}</td>
	           <td align="center">${list.reg_date}</td>
	           <td align="center"><div style="overflow:hidden;text-overflow:ellipsis;white-space: nowrap;width: 300px">${list.user_ci}</div></td>
	           <td align="center">${list.ctn}</td>
	           <td align="center">
				<c:choose>
					 <c:when test="${list.active == '1'  }">
					  	<button id="${status.count}" name="active_button" type="button" class="btn_area_a" value="1">활성화</button><br>
					 </c:when>
					 <c:otherwise>
					  	<button id="${status.count}" name="active_button" type="button" class="btn_area_a" value="0">비활성화</button><br>
					 </c:otherwise>
				</c:choose>
				<button name="action_del" type="button" class="btn_area_x" value="${status.count}" >삭&nbsp;&nbsp;제</button>
	           </td>
	       </tr>
	       </c:forEach>
	       <c:if test="${fn:length(list) == 0}" >
	       <tr>
	       	<td colspan="5" > 조회값이 없습니다. </td>
	       </tr>
	       </c:if>
	   </table>
   </form>
</div>