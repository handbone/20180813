<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script src="<c:url value='/resources/js/jquery.validate.min.js' />"></script>
<script>
	
	 $(document).ready(function() {
		 $("input[name=use_status][value=" + "${aclInfoTbl.use_status}" + "]").attr("checked", true);
		 $("input[name=acl_limit_yn][value=" + "${aclInfoTbl.acl_limit_yn}" + "]").attr("checked", true);
	});
	 </script>
<!-- [D] add -->
<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>

<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>
<script>
	$(function() {
		$("#datepicker1, #datepicker2")
				.datepicker(
						{
							autoSize : true,
							showOn : 'button',
							buttonImage : '<c:url value="/resources/css/ui/img/calendar.gif"/>',
							buttonImageOnly : true,
							dateFormat : 'yymmdd'
						});
		$('#datepicker1').change(function(){
        	var date1 = $('#datepicker1').val();
        	var date2 = $('#datepicker2').val();
        	
        	if(date2 == '' || date2 == null){
        		return;
        	}
        	
        	var startDate = new Date(date1.slice(0, 4), date1.slice(4, 6), date1.slice(6, 8));
        	var endDate = new Date(date2.slice(0, 4), date2.slice(4, 6), date2.slice(6, 8));
        	
        	if(startDate.getTime() > endDate.getTime()){
        		alert('사용 기간을 확인해주세요.');
        		$('#datepicker1').val('');
        		return;
        	}
        });
        $('#datepicker2').change(function(){
        	var date1 = $('#datepicker1').val();
        	var date2 = $('#datepicker2').val();
        	
        	var startDate = new Date(date1.slice(0, 4), date1.slice(4, 6), date1.slice(6, 8));
        	var endDate = new Date(date2.slice(0, 4), date2.slice(4, 6), date2.slice(6, 8));

        	if(startDate.getTime() > endDate.getTime()){
        		alert('사용 기간을 확인해주세요.');
        		$('#datepicker2').val('');
        		return;
        	}
        });
	});
	$.datepicker.setDefaults($.datepicker.regional["ko"]);

	$(document).ready(function() {

		$('img.ui-datepicker-trigger').attr('style', 'margin:0px 7px 0px 7px');

		$("#datepicker3, #datepicker4").next().click(function() {

			$("#ui-datepicker-div > .ui-datepicker-calendar").hide();

			$("#ui-datepicker-div").position({
				my : "center top",
				at : "center bottom",
				of : $(this)
			});
		});

	});
</script>
<!--// [D] add -->

<div class="top_title"><h2>매체관리 - 매체수정</h2></div>
<form method="post" name="mainForm" id="mainForm" action="<c:url value='/manage/acl/aclUpdate.do' />" >
<table width="100%" class="table_style8" cellpadding="0" cellspacing="0" dwcopytype="CopyTableCell">
  
   	<colgroup>
       		<col width="200">
            <col width="">
       </colgroup>
   	   <tr>
       	<th>매체명</th>
           <td>
           		${aclInfoTbl.requester_name} ( * 매체명은 수정하실 수 없습니다.)
           </td>
       </tr>
       <tr>
       	<th>매체코드</th>
           <td>
           		${aclInfoTbl.requester_code} ( * 매체코드는 수정하실 수 없습니다.)
           </td>
       </tr>
       <tr>
       	<th>서비스 상태</th>
           <td>
       			<input id="use_status" name="use_status" value="Y" type="radio" /> 사용중
          		<input id="use_status" name="use_status" value="N" type="radio"/> 중지
			</td>
       </tr> 
			    
       <tr>
       	<th>IP</th>
           <td>
           		<input id="ip_addr" name="ip_addr" size="120" maxlength="2000" type="text" value="${aclInfoTbl.ip_addr}"> (2000byte 이내) 
           		<br>
           		다중 아이피의 경우 ',' 로 구분/대역 아이피의 경우 '-' 로 대역 표시
           </td>
       </tr>
       <tr>
       	<th>적립 제한 설정</th>
           <td>
       			<input id="acl_limit_yn" name="acl_limit_yn" value="Y"  type="radio" /> 사용중
          		<input id="acl_limit_yn" name="acl_limit_yn" value="N" checked="CHECKED" type="radio"/> 중지
          		<br>
          		* 적립 제한 설정을 하면 특정한 횟수 이상 적립 후에는 적립으로 전환되지 않는다..
			</td>
       </tr> 
        <tr>
       	<th>적립 허용 횟수</th>
           <td>
           		<input id="acl_limit_cnt" name="acl_limit_cnt" style="text-align: right;" size="10" maxlength="10" type="text" value="${aclInfoTbl.acl_limit_cnt}"> 
           		* 정수로 입력한다. ( ex: 4회까지 허용 할 경우 숫자 4 를 입력한다. )
           </td>
       </tr>
       <tr>
       <tr>
       	<th>사용 기간</th>
       	<td>
       		<div id="datepickerDiv1">
				<fmt:parseDate var="date1" value="${aclInfoTbl.datepicker1 }" pattern="yyyyMMdd"/>
   				<input name="datepicker1" type="text" id="datepicker1"  value="<fmt:formatDate value="${date1 }" pattern="yyyyMMdd"/>" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
    			~ 
    			<fmt:parseDate var="date2" value="${aclInfoTbl.datepicker2 }" pattern="yyyyMMdd"/>
   				<input name="datepicker2" type="text" id="datepicker2"  value="<fmt:formatDate value="${date2 }" pattern="yyyyMMdd"/>" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
           </div>
       	</td>
       </tr> 
       <tr>
       	<th>비고</th>
           <td>
	           	<textarea id="description" name="description" rows="10" cols="100">${aclInfoTbl.description }</textarea> 
           </td>
       </tr>
</table>

<div id="btn_area">
 	<input type="button" class="btn_area_a" value="취소" onclick="javascript:history.go(-1)">
 	<input type="button" id="goUpdate" name="goUpdate" class="btn_area_a" value="확인" >
 	
 	<input type="hidden" id="requester_code" name="requester_code" value="${aclInfoTbl.requester_code}" >
	<input type="hidden" id="page" name="page" value="${pg}" >
 	
 	
</div>  
</form>

<style type="text/css">
input.error, textarea.error{
  border:1px dashed red;
}
label.error{
  margin-left:10px;
  color:red;
}
</style>

<script>
	
	 $(document).ready(function() {
		 $("#goUpdate").bind("click",function() {
			 $("#mainForm").submit();
		});
	});
 	

	
	$("#mainForm").validate({
		rules: {
			ip_addr: { required: true,	maxlength: 2000},
			description: { required: true,	maxlength: 2000},
			acl_limit_cnt: { digits : true }
		},
		messages: {
			
		},submitHandler: function(form) {
			
			//alert($("input:checkbox[name=timegbn]:checked").val());
		    form.submit();
		}
	});

</script>