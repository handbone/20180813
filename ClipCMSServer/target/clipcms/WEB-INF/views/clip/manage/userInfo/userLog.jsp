<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>

<script>


/* $( function() {
    $( "#datepicker1, #datepicker2" ).datepicker({
    autoSize: true,
    showOn: 'button', 
    buttonImage: '<c:url value='/resources/css/ui/img/calendar.gif'/>',   
    buttonImageOnly: true,
    dateFormat: 'yymmdd'});
} );
$.datepicker.setDefaults( $.datepicker.regional[ "ko" ] );
 */
 $(document).ready(function(){ 
	  
	 $('img.ui-datepicker-trigger').attr('style', 'margin:0px 7px 0px 7px');
	 
	// 검색
	 $("#goSearch").bind("click",function() {
		 $("#excelDownload").val("");
		 formSubmit();
	 });
	 
	 // 엑셀 다운로드
	 $("#goExcel").bind("click",function() {
		 formSubmit("goExcel");
	 });
	 
	 function formSubmit(goExcelString){
		 
		 if( goExcelString == "goExcel"){
			 $("#excelDownload").val("Y");
		 }
		 
		 
		 	$("#search_yn").val("Y");
			/* $("#startdate").val($("#datepicker1").val());
			$("#enddate").val($("#datepicker2").val()); */
			 
			 if($("#keywd").val() == ""){
			 	alert("검색조건을 입력해주세요.");
				return;
			 }
			 /* if($("#startdate").val() == ""){
				 	alert("시작일을 입력해주세요.");
					return;
				 }
			 if($("#enddate").val() == ""){
			 	alert("종료일을 입력해주세요.");
				return;
			 }
			 
			 if($("#enddate").val() < $("#startdate").val()){
			 	alert("시작일이 종료일보다 빠를수 없습니다.");
				return;
			 } */
				
		 $('#mainForm').attr('action','<c:url value="/manage/userInfo/userLog.do" />').attr('method', 'post').submit();
	 }
	 
	 
	// 검색값 설정
    if('${searchBean.status}'!=''){
  		$("#status").val('${searchBean.status}').attr("selected", "selected");
    }
	$("#keywd").val('${searchBean.keywd}')
    /* $("#datepicker1").val('${searchBean.startdate}');
	$("#datepicker2").val('${searchBean.enddate}'); */

	/* if( '' == $("#datepicker1").val()){
		$("#datepicker1").val($.datepicker.formatDate('yymmdd', new Date()));
	}
	if( '' == $("#datepicker2").val()){
		$("#datepicker2").val($.datepicker.formatDate('yymmdd', new Date()));
	} */
	
	
}); 

</script>

	<div class="top_title"><h2> 사용자 변경기록 </h2></div>
    <!-- 검색조건 -->
    <form id="mainForm" name="mainForm" >
    <table width="100%" class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="100">
            <col width="">
       
        </colgroup>
        <tr>
        	<th>검색조건</th>
        	<td>
            	<select class="form_sel_style1" name="status" id="status">
            		<option value="" selected>전체</option>
            		<option value="user_ci">user_ci</option>
            		<option value="cust_id">cust_id</option>
                	<option value="ga_id" >ga_id</option>
                	<option value="ctn" >ctn</option>
            	</select>
            	<input type="text" id="enterKeyfalse" name="enterKeyfalse" style="display:none" readOnly="readonly">
            	<input class="form_input_style2" id="keywd" name="keywd" style="width:200px" value="${searchBean.keywd}">
            	<br> 
            	* 암호화되지 않은 전화번호를 입력시에는 검색조건을 '전체'가 아닌 'ctn'으로 변경한다.
            </td>
        </tr>
		<!-- <tr>
       	<th>검색기간</th>
       	<td>
           	<input name="datepicker1"  type="text"  id="datepicker1"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
			~ <input name="datepicker2" type="text" id="datepicker2"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">		            
           </td>
       </tr> -->
       <tr></tr>
    </table>
    <div id="btn_area">
    <input type="button" id="goSearch" name="goSearch" class="btn_area_a" value="검색" >
    <input type="button" id="goExcel" name="goExcel" class="btn_area_a" value="엑셀 다운로드" >
    </div>
    <input type="hidden" id="startdate" name="startdate" value="" >
    <input type="hidden" id="enddate" name="enddate" value="" >
    <input type="hidden" id="excelDownload" name="excelDownload" value="" >
    
    <input type="hidden" id="pg" name="pg" value="" >		
    
    <input type="hidden" id="search_yn" name="search_yn" value="" >
    
    </form>
    
    
   <br>

   <table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
    		<col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="70">
            <col width="">
            <col width="70">
            <col width="150">
        </colgroup>
        <tr>
            <th>user_ci</th>
        	<th>cust_id</th>
        	<th>ctn</th>
        	<th>ga_id</th>
        	<th>상태</th>
        	<th>exit_ci</th>
            <th>구분값</th>
            <th>변경일자</th>
        </tr>
        
        <c:forEach var="list" items="${list}">
        <tr>
            <td align="center" style="word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;"><span title="${list.user_ci}">${list.user_ci}</span></td>
        	<td align="center" style="word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;"><span title="${list.cust_id}">${list.cust_id}</span></td>
        	<td align="center" style="word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;"><span title="${list.ctn}">${list.ctn}</span></td>
        	<td align="center" style="word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;"><span title="${list.ga_id}">${list.ga_id}</span></td>
            <td align="center">${list.status}</td>
            <td align="center" style="word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;"><span title="${list.exit_ci}">${list.exit_ci}</span></td>
            <td align="center">${list.update_gbn}</td>
            <td align="center">${list.regdate}</td>
        </tr>
        </c:forEach>
        <c:if test="${fn:length(list) == 0}" >
        <tr>
        	<td colspan="8" > 조회값이 없습니다. </td>
        </tr>
        </c:if>
    </table>
    
    
    







<table width="100%"><tr height="45px"><td align="center">${boardPaging.pagingHTML}</td></tr></table>
    <script>
    //페이징
    function boardPaging(pg){
    	
    	 $("#pg").val(pg);
    	 $("#excelDownload").val("");
    	 
    	 $("#search_yn").val("Y");
    	 /* $("#startdate").val($("#datepicker1").val());
			$("#enddate").val($("#datepicker2").val()); */
			 
		 if($("#keywd").val() == ""){
		 	alert("검색조건을 입력해주세요.");
			return;
		 }
		 /* if($("#startdate").val() == ""){
			 	alert("시작일을 입력해주세요.");
				return;
			 }
		 if($("#enddate").val() == ""){
		 	alert("종료일을 입력해주세요.");
			return;
		 }
		 
		 if($("#enddate").val() < $("#startdate").val()){
		 	alert("시작일이 종료일보다 빠를수 없습니다.");
			return;
		 } */
				
		 $('#mainForm').attr('action','<c:url value="/manage/userInfo/userLog.do" />').attr('method', 'post').submit();

    }
    </script>