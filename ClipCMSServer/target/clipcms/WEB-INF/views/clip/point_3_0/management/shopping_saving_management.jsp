<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>   
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 

	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/common.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/style.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/ui/jquery-ui.min.css'/>">
	<!-- JQuery -->
	<script lang="javascript" src="<c:url value='/resources/js/jquery-3.1.0.js'/>"></script>

	<style>
	.dot1 {
	    width: 160px;
	    height: 20px;
	 /* border: 1px solid red;  */
	}
	.dot2 {
	    width: 315px;
	    height: 20px;
	   /*   border: 1px solid red;  */
	}​
	</style>
	<script src="<c:url value='/resources/js/jquery.dotdotdot.min.js'/>" type="text/javascript"></script>
	<script>

		

        $(document).ready(function(){ 
        	$('#goInsert').click(function(){
            	location.href='<c:url value="/clip3_0/shopping_saving_new.do?mode=0"/>';
            });
            $('#goRank').click(function(){
           		location.href='<c:url value="/clip3_0/shopping_saving_new.do?mode=3"/>';
            });
            $('.dot1').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,
               
            }); 
            $('.dot2').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,  
               
            });
        }); 

	</script>


	<div class="top_title"><h2> 쇼핑적립 관리 - 목록</h2></div>
	
	<div id="btn_area">
       <input id="goRank" name="goRank" type="button" class="btn_area_a" value="노출 순서 설정">
       <input id="goInsert" name="goInsert" type="button" class="btn_area_a" value="신규등록">
    </div>  
    <br>
    <table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
        	<col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
        </colgroup>
        <tbody>
            <tr>
            	<th>상점 ID</th>
                <th>상점명</th>
                <th>링크프라이스 적립율</th>
                <th>KT 적립율</th>
                <th>사용여부</th>
                <th>노출 순서</th>
                <th>최종 등록일</th>
            </tr>            
            <c:choose>
				<c:when test="${not empty list }">
					<c:forEach var="item" items="${list}">
						<tr>
							<td align="center"><span class="dot2">${item.mall_id }</span></td>
							<td align="center">
								<a href="<c:url value='/clip3_0/shopping_saving_new.do?mode=1&mall_id=${item.mall_id }'/>">${item.mall_name }</a>
							</td>
							<td align="center">${item.reward }</td>
							<td align="center">${item.reward_kt }</td>
							<td align="center">${item.use_yn }</td>
							<td align="center">${item.ord }</td>
							<td align="center">
								<fmt:parseDate var="date1" value="${item.regdate}" pattern="yyyy-MM-dd kk:mm:ss"/>
								<fmt:formatDate value="${date1 }" pattern="yyyy.MM.dd"/>
								<br><fmt:formatDate value="${date1 }" pattern="kk:mm:ss"/>
							</td>
						</tr>					
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr>
						<td colspan="7"> 조회값이 없습니다. </td>
					</tr>
				</c:otherwise>
			</c:choose>
        </tbody>
    </table>
    
