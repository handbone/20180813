<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/common.css'/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/style.css'/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/ui/jquery-ui.min.css'/>">
<!-- JQuery -->
<script lang="javascript"
	src="<c:url value='/resources/js/jquery-3.1.0.js'/>"></script>
<script src="<c:url value="/resources/js/jquery.fileDownload.js"/>"></script>

<style>
.dot1 {
	width: 160px;
	height: 20px;
	/* border: 1px solid red;  */
}

.dot2 {
	width: 315px;
	height: 20px;
	/*   border: 1px solid red;  */
}
​
</style>
<script src="<c:url value='/resources/js/jquery.dotdotdot.min.js'/>"
	type="text/javascript"></script>
<script>
	$(document)
		.ready(
			function() {
				var status = '<c:out value="${status}"/>';
				var keywd = '<c:out value="${keywd}"/>';
				var card_com = '<c:out value="${card_com}"/>';
				var use_status = '<c:out value="${use_status}"/>';
				var value = '<c:out value="${value}"/>';
// 				var reqCode = '<c:out value="${reqCode}"/>';
// 				console.log(reqCode);
// 				if (reqCode == 'PE') {
// 					alert('잘못된 검색어입니다.');
// 				}

				$('#status').val(status);
				$('#keywd').val(keywd);
				console.log(card_com);
				$('#card_com').val(card_com);
				$('input[name=use_status][value=' + use_status + ']')
						.prop("checked", true);
				$('#value').val(value);

				$('.dot1').dotdotdot({
					ellipsis : '... ',
					wrap : 'word',
					after : null,
					watch : true,
					height : 20,

				});
				$('.dot2').dotdotdot({
					ellipsis : '... ',
					wrap : 'word',
					after : null,
					watch : true,
					height : 20,

				});
				$('#goSearch').click(function() {
					if(valuecheck()){
						alert('잘못된 검색조건 값이 잘못되었습니다.');
					}else{
						$('#mainForm').submit();
					}
				});
// 				$('#goExcel')
// 					.click(
// 						function() {
// 							var form = document
// 									.createElement("form");
// 							form.method = "POST";
// 							form.action = "<c:url value='/clip3_0/KTCPMW01/excelDownload.do'/>";
// 							document.body.appendChild(form);
// 							form.submit();
// 						});
				$('#datepicker1').change(function(){
	            	var date1 = $('#datepicker1').val();
	            	var date2 = $('#datepicker2').val();
	            	
	            	if(date2 == '' || date2 == null){
	            		return;
	            	}
	            	
	            	var startDate = new Date(date1.slice(0, 4), date1.slice(4, 6), date1.slice(6, 8));
	            	var endDate = new Date(date2.slice(0, 4), date2.slice(4, 6), date2.slice(6, 8));
	            	
	            	if(startDate.getTime() > endDate.getTime()){
	            		alert('사용 기간을 확인해주세요.');
	            		$('#datepicker1').val('');
	            		return;
	            	}
	            });
	            $('#datepicker2').change(function(){
	            	var date1 = $('#datepicker1').val();
	            	var date2 = $('#datepicker2').val();
	            	
	            	var startDate = new Date(date1.slice(0, 4), date1.slice(4, 6), date1.slice(6, 8));
	            	var endDate = new Date(date2.slice(0, 4), date2.slice(4, 6), date2.slice(6, 8));
	            	
	            	if(startDate.getTime() > endDate.getTime()){
	            		alert('사용 기간을 확인해주세요.');
	            		$('#datepicker2').val('');
	            		return;
	            	}
	            });
			});

	function valuecheck(){
		var status = $('#status').val();
		var keywd = $('#keywd').val();
		if(status == 'ctn'){
			return !$.isNumeric(keywd);
		}
		
		return false;
	}
	
	function onlyNumber(event) {
		event = event || window.event;
		var keyID = (event.which) ? event.which : event.keyCode;
		if ((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105)
				|| keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
			return;
		else
			return false;
	}
	function removeChar(event) {
		event = event || window.event;
		var keyID = (event.which) ? event.which : event.keyCode;
		if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
			return;
		else
			event.target.value = event.target.value.replace(/[^0-9]/g, "");
	}
</script>

<!--Content layout -->
<!-- [D] add -->
<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>

<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>
<script>
	$( function() {
	    $( "#datepicker1, #datepicker2" ).datepicker({
	    autoSize: true,
	    showOn: 'button', 
	    buttonImage: '<c:url value="/resources/css/ui/img/calendar.gif"/>',   
	    buttonImageOnly: true,
	    dateFormat: 'yymmdd'});
	} );
	$.datepicker.setDefaults( $.datepicker.regional[ "ko" ] );
	
	$(document).ready(function() {

		$('img.ui-datepicker-trigger').attr('style', 'margin:0px 7px 0px 7px');

		$("#datepicker3, #datepicker4").next().click(function() {

			$("#ui-datepicker-div > .ui-datepicker-calendar").hide();

			$("#ui-datepicker-div").position({
				my : "center top",
				at : "center bottom",
				of : $(this)
			});
		});

	});
</script>
<!--// [D] add -->

<div class="top_title">
	<h2>카드포인트 전환 조회</h2>
</div>

<script>
	function CancelDealProcess(regSource, userCi, transactionId, point,
			description, custIdOrg, approveNo) {
		console.log('regSource:' + regSource,
				'/userCi+' + userCi,
				'/transactionId+' + transactionId,
				'/point+' + point,
				'/description+' + description,
				'/custIdOrg+' + custIdOrg)
		$.ajax({
			type : "POST",
			url : "<c:url value='/clip3_0/cancelPointSwap.do'/>",
			data : {
				'regSource' : regSource,
				'userCi' : userCi,
				'transactionId' : transactionId,
				'point' : point,
				'description' : description,
				'custIdOrg' : custIdOrg,
				'approveNo' : approveNo
			},
			dataType : "Json",
			success : function(resData) {
				if (resData.result == 'F') {
					alert(resData.resultMsg);
				} else if (resData.result == 'S') {
					alert("정상처리되었습니다.");
				} else {
					alert("시스템 오류가 발생하였습니다. 관리자에게 문의하세요.");
				}
			},
			error : function(e) {
				alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
			}
		});
	}
</script>

<form id="mainForm" name="mainForm"
	action="<c:url value='/clip3_0/KTCPMW01.do'/>" method="POST">
	<table width="100%" class="table_style1" cellpadding="0"
		cellspacing="0">
		<colgroup>
			<col width="">
			<col width="">

		</colgroup>
		<tbody>
			<tr>
				<th>검색조건</th>
				<td><select class="form_sel_style1" name="status" id="status">
						<option value="" selected>전체</option>
						<option value="user_ci">user_ci</option>
						<option value="cust_id">cust_id</option>
						<option value="ga_id">ga_id</option>
						<option value="ctn">ctn</option>
				</select> <input class="form_input_style2" id="keywd" name="keywd"
					style="width: 200px" value=""></td>
			</tr>
			<tr>
				<th>겸색기간</th>
				<td>
					<div id="datepickerDiv1">
						<%
							Calendar cal = Calendar.getInstance();
							cal.add(Calendar.MONTH, -1);
							Date date = cal.getTime();
							String past = new SimpleDateFormat("yyyyMMdd").format(date);
						%>
						<c:choose>
							<c:when test="${datepicker1 ne null}">
								<input name="datepicker1" type="text" id="datepicker1"
									value="${datepicker1}" class="form_input_style2" maxlength="12"
									readonly style="text-align: center" style="width:140px">
							</c:when>
							<c:otherwise>
								<input name="datepicker1" type="text" id="datepicker1"
									value="<%=past%>" class="form_input_style2" maxlength="12"
									readonly style="text-align: center" style="width:140px">
							</c:otherwise>
						</c:choose>

						~

						<c:choose>
							<c:when test="${datepicker2 ne null}">
								<input name="datepicker2" type="text" id="datepicker2"
									value="${datepicker2}" class="form_input_style2" maxlength="12"
									readonly style="text-align: center" style="width:140px">
							</c:when>
							<c:otherwise>
								<input name="datepicker2" type="text" id="datepicker2"
									value="<fmt:formatDate value="<%=new java.util.Date()%>" pattern="yyyyMMdd"/>"
									class="form_input_style2" maxlength="12" readonly
									style="text-align: center" style="width:140px">
							</c:otherwise>
						</c:choose>


					</div>
				</td>
			</tr>
			<tr>
				<th>카드사 구분</th>
				<td><select class="form_sel_style1" name="card_com"
					id="card_com">
						<option value="0" selected>전체</option>
						<option value="shinhan">신한카드</option>
						<option value="hanamembers">하나카드</option>
						<option value="bccard">비씨카드</option>
						<option value="kbpointree">국민카드</option>
				</select></td>
			</tr>
			<tr>
				<th>거래 결과</th>
				<td>
					<input name="use_status" value="0" checked="CHECKED" type="radio"> 전체 
					<input name="use_status" value="S" type="radio"> 성공 
					<input name="use_status" value="F" type="radio"> 실패
				</td>
			</tr>
			<tr>
				<th>전환액 기준</th>
				<td><input class="form_input_style2" id="" name="value"
					style="width: 200px" value="0" onkeydown='return onlyNumber(event)'
					onkeyup='removeChar(event)' style='ime-mode:disabled;'
					maxlength="10"> (*전환액기준은 해당 금액 이상의 이력만 노출합니다.)</td>
			</tr>
		</tbody>
	</table>
	<div id="btn_area">
		<input type="button" id="goSearch" name="goSearch" class="btn_area_a"
			value="검색"> <input type="button" id="goExcel" name="goExcel"
			class="btn_area_a" value="엑셀 다운로드">
	</div>

</form>

<br>

조회 목록 전체 : 총
<font color="red">${totalcount}</font>
건 &nbsp; &nbsp; 정산금액 합계 :
<font color="red"><fmt:formatNumber value="${totalvalue}"
		pattern="#,###" /></font>
		&nbsp; &nbsp; 전환 포인트 합계 :
<font color="red"><fmt:formatNumber value="${totalpoint}"
		pattern="#,###" /></font>
<br>
<br>

<table class="table_style1" cellpadding="0" cellspacing="0"
	style="table-layout: fixed;word-wrap:break-word;">
	<colgroup>
		<col width="">
		<col width="">
		<col width="100px">
		<col width="200px">
		<col width="200px">
		<col width="">
		<col width="">
		<col width="">
		<col width="">
		<col width="">
		<col width="">
	</colgroup>
	<tbody>
		<tr>
			<th>거래 요청일</th>
			<th>거래 응답일</th>
			<th>카드사</th>
			<th>원 거래번호</th>
			<th>참조 거래번호</th>
			<th>요청포인트</th>
			<th>응답 포인트</th>
			<th>거래결과</th>
			<th>응답코드</th>
			<th>거래구분</th>
			<th>망취소</th>
			<th>정산금액</th>
		</tr>



		<c:choose>
			<c:when test="${fn:length(list) > 0 }">
				<c:forEach items="${list}" var="obj" end="${endNum}">
					<tr>
						<td align="center">${obj.trans_req_date}</td>
						<td align="center">${obj.trans_res_date}</td>
						<td align="center">${obj.reg_source}</td>
						<td align="center">${obj.trans_id}</td>
						<td align="center">${obj.ref_trans_id}</td>
						<td align="center"><fmt:formatNumber value="${obj.req_point}"
								pattern="#,###" /></td>
						<td align="center"><fmt:formatNumber value="${obj.res_point}"
								pattern="#,###" /></td>
						<td align="center"><c:choose>
								<c:when test="${obj.result_code eq 'S'}">
										성공
									</c:when>
									<c:otherwise>
										실패
									</c:otherwise>
							</c:choose></td>
						<td align="center">(${obj.res_code})<br>${obj.res_message}</td>
						<td align="center"><c:choose>
								<c:when test="${obj.trans_div eq 'U'}">
									사용
								</c:when>
								<c:when test="${obj.trans_div eq 'N'}">
									망취소
								</c:when>
								<c:otherwise>
									trans_div누락
								</c:otherwise>
							</c:choose></td>
						<td align="center"><input type="button" value="거래취소"
							onclick="CancelDealProcess('${obj.reg_source}','${obj.user_ci}','${obj.trans_id}','${obj.req_point}','description','${obj.cust_id_org}', '${obj.approve_no }');">
						</td>
						<td align="center">
							${obj.price_point}
						</td>
					</tr>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<tr>
					<td colspan="9">조회값이 없습니다.</td>
				</tr>
			</c:otherwise>
		</c:choose>

	</tbody>
</table>

<table width="100%">
	<tbody>
		<tr height="45px">
			<td align="center"><c:out value="${boardPaging.pagingHTML}"
					escapeXml="false"></c:out></td>
		</tr>
	</tbody>
</table>

<!-- 파일 생성중 보여질 진행막대를 포함하고 있는 다이얼로그 입니다. -->
<div title="Data Download" id="preparing-file-modal"
	style="display: none;">
	<div id="progressbar"
		style="width: 100%; height: 22px; margin-top: 20px;"></div>
</div>
<!-- 에러발생시 보여질 메세지 다이얼로그 입니다. -->
<div title="Error" id="error-modal" style="display: none;">
	<p>생성실패.</p>
</div>


<script>
	//페이징
	function boardPaging(pg) {
		var strHtml = '<input type="hidden" name="pg" value="' + pg + '"/>';
		$('#mainForm').append(strHtml);
		$('#mainForm').submit();
	}
</script>

<script type="text/javascript">
	$(function() {
		$("#goExcel").on("click", function() {
			var $preparingFileModal = $("#preparing-file-modal");
			$preparingFileModal.dialog({
				modal : true
			});
			$("#progressbar").progressbar({
				value : false
			});
			var url = '<c:url value="/clip3_0/KTCPMW01/excelDownload.do"/>';
			$.fileDownload(url, {
				successCallback : function(url) {
					$preparingFileModal.dialog('close');
				},
				failCallback : function(responseHtml, url) {
					$preparingFileModal.dialog('close');
					$("#error-modal").dialog({
						modal : true
					});
				}
			}); // 버튼의 원래 클릭 이벤트를 중지 시키기 위해 필요합니다. 
			return false;
		});
	});
</script>

