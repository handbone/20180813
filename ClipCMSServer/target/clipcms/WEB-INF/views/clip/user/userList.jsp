<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<style>
.dot1 {
    width: 160px;
    height: 20px;
 /* border: 1px solid red;  */
}
.dot2 {
    width: 315px;
    height: 20px;
   /*   border: 1px solid red;  */
}​
</style>
<script src="<c:out value='${pageContext.request.contextPath}'/>/resources/js/jquery.dotdotdot.min.js" type="text/javascript"></script>
<script>

 $(document).ready(function(){ 
    $('.dot1').dotdotdot({ 
        ellipsis: '... ', 
        wrap: 'word', 
        after: null, 
        watch: true, 
        height: 20,
       
    }); 
    $('.dot2').dotdotdot({ 
        ellipsis: '... ', 
        wrap: 'word', 
        after: null, 
        watch: true, 
        height: 20,  
       
    }); 
    if('${status}'!='')
    $("#status").val('${status}').attr("selected", "selected");
  
}); 

</script>

	<div class="top_title"><h2> 사용자 ID 목록</h2></div>
    
    <table width="100%" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="100">
            <col width="">
    	   
        </colgroup>
        <tr>
        	<th></th>
            <td>
	            <a href="<c:url value='/user/userInsertForm.do' />" class="btn_area_b">신규등록</a>
           </td>
            
           
        </tr>

    </table>
    
   <br>
   <!-- 유저 목록 -->
   <table class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="150px">
            <col width="180px">
            <col width="100px">
            <col width="200px">
            <col width="100px">
            <col style="">
        </colgroup>
        <tr>
        	<th>아이디</th>
            <th>성명</th>
            <th>연락처</th>
            <th>이메일</th>
            <th>상태<!-- <br> 1:정상<br> 2:패스워드 변경 대상자<br> 3:패스워드 실패 잠금상태 --></th>
            <th>권한</th>
        </tr>
        <c:forEach var="list" items="${list}">
        <tr>
        	<td>${list.userid }</td>
            <td align="center"><a href="<c:url value='/user/userView.do?userid=${list.userid }&page=${pg}' />"><span class="dot2" >${list.username }</span></a></td>
            <td>${list.tel }</td>
             <td>${list.email }</td>
            <td align="center">
            <c:choose>
				 <c:when test="${list.state == 1 }">
				  사용중
				 </c:when>
				 <c:when test="${list.state == 2 }">
				  사용중 : 패스워드 변경 대상자
				 </c:when>
				 <c:when test="${list.state == 3 }">
				  잠금 : 패스워드 실패 잠금상태
				 </c:when>
			</c:choose>  
            </td>
            <td align="center">
            <c:choose>
				 <c:when test="${list.userauth == 0 }">
				  슈퍼관리자
				 </c:when>
				 <c:when test="${list.userauth == 2 }">
				  포인트 조정 관리자
				 </c:when>
				 <c:when test="${list.userauth == 1 }">
				  일반관리자
				 </c:when>
			</c:choose>  
            </td>
        </tr>
        </c:forEach>
        

    </table>
    
    <table width="100%"><tr height="45px"><td align="center">${boardPaging.pagingHTML }</td></tr></table>
   
    
    
    <script>
    //페이징
    function boardPaging(pg){
    	location.href="<c:url value='/user/userList.do?pg="+pg+"&keywd=${keywd}&status=${status}' />";
    }
    </script>