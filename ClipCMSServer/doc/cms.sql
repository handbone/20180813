--///////////////////////////////////////////////////////////////////////////////////////////////
--CMS 상용테스터 등록 테이블
 
CREATE TABLE point_prod_tester
(
	user_ci character varying(256) NOT NULL,
	reg_date character varying(14) NOT NULL,
	ctn character varying(256) NOT NULL,
	active character varying(1) DEFAULT 1 NOT NULL,
	CONSTRAINT point_prod_tester_pkey PRIMARY KEY (user_ci)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE point_prod_tester
  OWNER TO point;
  
insert into point_prod_tester(reg_date,user_ci,ctn) values('20171112','kL+YpLW1l9x0nC8vPB1D9inVPEbL82uAr5e7/tvjCd+VsbD78QkGjsmQ/FRMTJAhcYe2dg/Vx0BWSAkQGKA5qA==','Z+l3CxB37aywhvK1BevFow==');

--///////////////////////////////////////////////////////////////////////////////////////////////

CREATE TABLE point_bomb_info_default
(
	randompoint_totalvalue numeric DEFAULT 30 NOT NULL,
	offerwall_addpointrate numeric DEFAULT 80 NOT NULL
)
WITH (
  OIDS=FALSE
);

ALTER TABLE point_bomb_info_default
  OWNER TO point;
  
insert into point_bomb_info_default(randompoint_totalvalue,offerwall_addpointrate) values(30, 80);

  
--///////////////////////////////////////////////////////////////////////////////////////////////

CREATE TABLE mainbanner_info_tbl
(
  mb_id character varying(16) NOT NULL,
  mb_name character varying(256) NOT NULL,
  link_url character varying(256) NOT NULL,
  start_date timestamp without time zone NOT NULL,
  end_date timestamp without time zone NOT NULL,
  reg_date timestamp without time zone NOT NULL,
  service_status character varying(1) NOT NULL,
  rank character varying(10) NOT NULL,
  mbimg_url character varying(256),
  description character varying(256) NOT NULL,
  os_type character varying(1) NOT NULL DEFAULT 'D'::character varying,
  url_type character varying(1) NOT NULL DEFAULT 'D'::character varying,
  CONSTRAINT mainbanner_info_tbl_pkey PRIMARY KEY (mb_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE mainbanner_info_tbl
  OWNER TO point;
GRANT ALL ON TABLE mainbanner_info_tbl TO point_admin;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE mainbanner_info_tbl TO point_dml;
GRANT SELECT ON TABLE mainbanner_info_tbl TO point_select;
GRANT ALL ON TABLE mainbanner_info_tbl TO point;

-- Index: idx_mainbanner_info_tbl_01

-- DROP INDEX idx_mainbanner_info_tbl_01;

CREATE INDEX idx_mainbanner_info_tbl_01
  ON mainbanner_info_tbl
  USING btree
  (mb_id COLLATE pg_catalog."default", reg_date, rank COLLATE pg_catalog."default");


Alter table mainbanner_info_tbl add os_type character varying(1) default 'D' not null;
Alter table mainbanner_info_tbl add url_type character varying(1) default 'D' not null;
Alter table mainbanner_info_tbl ALTER COLUMN mb_id type character varying(8);

--///////////////////////////////////////////////////////////////////////////////////////////////
Alter table "ACL_INFO_TBL" ALTER COLUMN start_date type character varying(8);
Alter table "ACL_INFO_TBL" ALTER COLUMN end_date type character varying(8);