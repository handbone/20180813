<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/common.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/style.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/ui/jquery-ui.min.css'/>">
	<!-- JQuery -->
	<script lang="javascript" src="<c:url value='/resources/js/jquery-3.1.0.js'/>"></script>

	<style>
	.dot1 {
	    width: 160px;
	    height: 20px;
	 /* border: 1px solid red;  */
	}
	.dot2 {
	    width: 315px;
	    height: 20px;
	   /*   border: 1px solid red;  */
	}​
	</style>
	<script src="<c:url value='/resources/js/jquery.dotdotdot.min.js'/>" type="text/javascript"></script>
	<script>

		

        $(document).ready(function(){ 
        	$('#goSearch').on('click', goSearch);
        	$('#goExcel').on('click', goExcel);
        	
        	var status = '<c:out value="${searchBean.status}"/>';
        	var point_type = '<c:out value="${searchBean.point_type}"/>';
        	var reqCode = '<c:out value="${reqCode}"/>';
        	
        	if(reqCode == "PE"){
        		alert('WARNING');
        	}
        	
        	$('#status').val(status);
        	$('input[name=point_type][value=' + point_type + ']').prop("checked", true);

        	$('.dot1').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,
               
            }); 
            $('.dot2').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,  
               
            }); 
        }); 

	</script>


	<div class="top_title"><h2> 적립 예정 포인트 조회</h2></div>

    
    <form id="mainForm" name="mainForm" method="POST">
    <table width="100%" class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="">
            <col width="">
       
        </colgroup>
        <tbody>
            <tr>
                <th>검색조건</th>
                <td>
                    <select class="form_sel_style1" name="status" id="status">
                        <option value="" selected="">전체</option>
                        <option value="user_ci">user_ci</option>
                        <option value="cust_id">cust_id</option>
                        <option value="ga_id">ga_id</option>
                        <option value="ctn">ctn</option>
                    </select>
                    <input class="form_input_style2" id="keywd" name="keywd" style="width:200px" value="${searchBean.keywd }">
                </td>
            </tr>
            <tr>
                <th>조회구분</th>
                <td>
					<input name="point_type" value="N" checked type="radio"> 당월
                    <input name="point_type" value="I" type="radio"> 익월
                    <input name="point_type" value="O" type="radio"> 익익월 (* 포인트 전환이력은 간편조회에서 조회 가능)
                </td>
            </tr>
            <tr>
                <th>쇼핑몰 구분</th>
                <td>
                    <select class="form_sel_style1" name="requester_code" id="requester_code">
                        <option value="">전체</option>
                          <c:forEach items="${shoppinMallList}" var="list" >
		            		<option value="${list.mall_id}">${list.mall_name}</option>
		            		 
		           			</c:forEach>
                    </select>
                </td>
            </tr>
        </tbody>
    </table>
    <div id="btn_area">
        <input type="button" id="goSearch" name="goSearch" class="btn_area_a" value="검색">
        <input type="button" id="goExcel" name="goExcel" class="btn_area_a" value="엑셀 다운로드">
    </div>
    
    </form>
    
   <br>
   
   <c:choose>
		<c:when test="${(not empty list) and ('0' ne totalcount) }">
			조회 목록 전체 : 총 <font color="red">${totalcount} </font>건 &nbsp; &nbsp; 
			적립 예정 포인트 합계 : <font color="red"><fmt:formatNumber value="${order_sum }"/></font>
		</c:when>
		<c:otherwise>
			조회 목록 전체 : 총 <font color="red">0</font>건 &nbsp; &nbsp; 적립 예정 포인트 합계 : <font color="red">0</font>
		</c:otherwise>
   </c:choose>
 
   <br>
   <br>
   <!-- 매체 목록 -->
   <table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
        	<col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
        </colgroup>
        <tbody>
            <tr>
            	<th>주문일자</th>
                <th>쇼핑몰</th>
                <th>주문번호</th>
                <th>상품명</th>
                <th>주문수량</th>
                <th>판매금액 합계</th>
                <th>적립예정 포인트</th>
            </tr>            
            
<!--             <tr> -->
<!--             	<td align="center">2017.08.07 <br> 23:59:59</td> -->
<!--                 <td align="center">11번가</td> -->
<!--                 <td align="center">847910137</td> -->
<!--                 <td align="center"><a href="#"><span class="dot2">OUTLET BEST 헤지스남성 OUTS6BH53GE 로고장식 단가라 티셔츠</span></a></td> -->
<!--                 <td align="center">1</td> -->
<!--                 <td align="center">39,000원</td> -->
<!--                 <td align="center">3P</td> -->
<!--             </tr> -->
			<c:choose>
				<c:when test="${(not empty list) and ('0' ne totalcount) }">
					<c:forEach var="item" items="${list}">
						<tr>
							<td align="center">
								<fmt:parseDate var="date1" value="${item.yyyymmdd}" pattern="yyyyMMdd"/>
								<fmt:parseDate var="date2" value="${item.hhmiss }" pattern="HHmmss"/>
								<fmt:formatDate value="${date1 }" pattern="yyyy.MM.dd"/>
								<br><fmt:formatDate value="${date2 }" pattern="HH:mm:ss"/>
							</td>
							<td align="center">${item.merchant_name }</td>
							<td align="center">${item.order_code }</td>
							<td align="center"><span class="dot2">${item.product_name }</span></td>
							<td align="center">${item.itemCount }</td>
							<td align="center"><fmt:formatNumber value="${item.sales }"/></td>
							<td align="center"><fmt:formatNumber value="${item.commission}"/>P</td>
						</tr>					
					</c:forEach>
				</c:when>
				<c:otherwise>
					 <tr>
                		<td colspan="7"> 조회값이 없습니다. </td>
		            </tr>
				</c:otherwise>
			</c:choose>
        </tbody>
    </table>
    
    <c:choose>
    	<c:when test="${not empty boardPaging}">
    		<table width="100%">
	    		<tbody>
		    		<tr height="45px">
			    		<td align="center">
			    			<c:out value="${boardPaging.pagingHTML}" escapeXml="false"></c:out>
			    		</td>
		    		</tr>
	    		</tbody>
   			</table>
    	</c:when>
    	<c:otherwise>
    		<table width="100%"><tbody><tr height="45px"><td align="center"><span id="currentPageSpan" class="page_btn2" onclick="boardPaging(1)">1</span></td></tr></tbody></table>
    	</c:otherwise>
    </c:choose>
    
    <script>
    //페이징
    function boardPaging(pg){
       	var strHtml = '<input type="hidden" name="pg" value="' + pg + '"/>';
   		$('#mainForm').append(strHtml);
   		$('#mainForm').submit();
    }
    
    function goSearch(){
    	$('#mainForm').attr('action', '<c:url value="/clip3_0/KTCPMW02.do"/>');
   		$('#mainForm').submit();
	}
    
    function goExcel(){
    	var form = document.createElement("form");
		 form.method = "POST";
		 form.action = "<c:url value='/clip3_0/preSavingCheckList/excelDownload.do'/>";  
		 document.body.appendChild(form);
		 form.submit();
    }
    
    </script>

