<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    

	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/common.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/style.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/ui/jquery-ui.min.css'/>">
	<!-- JQuery -->
	<script lang="javascript" src="<c:url value='/resources/js/jquery-3.1.0.js'/>"></script>

	<style>
	.dot1 {
	    width: 160px;
	    height: 20px;
	 /* border: 1px solid red;  */
	}
	.dot2 {
	    width: 315px;
	    height: 20px;
	   /*   border: 1px solid red;  */
	}​
	</style>
	<script src="<c:url value='/resources/js/jquery.dotdotdot.min.js'/>" type="text/javascript"></script>
	<script>

        $(document).ready(function(){ 
            $('.dot1').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,
               
            }); 
            $('.dot2').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,  
               
            }); 
            
        }); 
        
        function reset() {
	    		var randompoint_totalvalue = "<c:out value='${screenlockrandompointm.randompoint_totalvalue}'/>";
	    		var offerwall_addpointrate = "<c:out value='${screenlockrandompointm.offerwall_addpointrate}'/>";
	    		$('#randompoint_totalvalue').val(randompoint_totalvalue);
	    		$('#offerwall_addpointrate').val(offerwall_addpointrate);
	    }
        
        function update() {
       	 	var 	formData = $("#mainForm").serialize();
      		 $.ajax({
      	            type: "POST",
      	            url: "<c:url value='/clip3_0/KTCPMI02.do'/>",
      	            data: formData,
      	            dataType: "Json",
      	            success: function (resData) {
      	                console.log(resData.result);  
      	                if (resData.result == "success") {
      	                		location.href='<c:url value="/clip3_0/KTCPMW08.do"/>';
      	                }
      	            },
      	            error: function (e) {
      	            	alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
      	            }
      	        });
        }

	</script>

    <!--Content layout -->

	<div class="top_title"><h2> 랜덤 포인트 관리</h2></div>
	<form method="post" name="mainForm" id="mainForm" action="">
    <table width="100%" class="table_style8" cellpadding="0" cellspacing="0">
        <colgroup>
            <col width="200">
            <col width="">
        </colgroup>
        <tbody>
            <tr>
                <th>랜덤포인트 평균치</th>
                <td>
                    <input id="randompoint_totalvalue" class="form_input_style2" name="randompoint_totalvalue" size="10" maxlength="2000" type="text" value="<c:out value='${screenlockrandompointm.randompoint_totalvalue}'/>"> p * 최소 10P에서 최대 100P사이의 값을 입력해주세요.
                </td>
            </tr>
            <tr>
                <th>오퍼월 수행시 합산 포인트 율</th>
                <td>
                    <input id="offerwall_addpointrate" class="form_input_style2" name="offerwall_addpointrate" size="10" maxlength="2000" type="text" value="<c:out value='${screenlockrandompointm.offerwall_addpointrate}'/>"> % * 오퍼월 광고의 지급 포인트 비율을 입력해주세요.
                </td>
            </tr>
        </tbody>
    </table>
    </form>
    
    <div id="btn_area">
        <input type="button" class="btn_area_a" value="초기화" onclick="reset();">
        <input type="button" id="goUpdate" name="goUpdate" class="btn_area_a" value="확인" onclick="update();">
    </div>  

    <style type="text/css">
    input.error, textarea.error{
      border:1px dashed red;
    }
    label.error{
      margin-left:10px;
      color:red;
    }
    </style>
