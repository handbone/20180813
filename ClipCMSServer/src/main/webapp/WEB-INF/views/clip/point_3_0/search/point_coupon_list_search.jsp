<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/common.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/style.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/ui/jquery-ui.min.css'/>">
	<!-- JQuery -->
	<script lang="javascript" src="<c:url value='/resources/js/jquery-3.1.0.js'/>"></script>

	<style>
	.dot1 {
	    width: 160px;
	    height: 20px;
	 /* border: 1px solid red;  */
	}
	.dot2 {
	    width: 315px;
	    height: 20px;
	   /*   border: 1px solid red;  */
	}​
	</style>
	<script src="<c:url value='/resources/js/jquery.dotdotdot.min.js'/>" type="text/javascript"></script>
	<script>

        $(document).ready(function(){ 
        	var status = '<c:out value="${status}"/>';
        	var keywd = '<c:out value="${keywd}"/>';
        	var couponNumber = '<c:out value="${couponNumber}"/>';
        	
        	$('#status').val(status);
        	$('#keywd').val(keywd);
        	$('#couponNumber').val(couponNumber);
        	
            $('.dot1').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,
               
            }); 
            $('.dot2').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,  
               
            });
            $('#goSearch').click(function(){
            	var keywd = $('#keywd').val();
//             	if(keywd == ''){
//             		alert('검색조건을 입력해주세요.');
//             		return;
//             	}
           		$('#mainForm').submit();
            });
			$('#goExcel').click(function(){
				 var form = document.createElement("form");
				 form.method = "POST";
				 form.action = "<c:url value='/clip3_0/KTCPMW04/excelDownload.do'/>";  
				 document.body.appendChild(form);
				 form.submit();
            });
        }); 

	</script>
<!--Content layout -->
<!-- [D] add -->
<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>
<script>
	$( function() {
	    $( "#datepicker1, #datepicker2" ).datepicker({
	    autoSize: true,
	    showOn: 'button', 
	    buttonImage: '<c:url value="/resources/css/ui/img/calendar.gif"/>',   
	    buttonImageOnly: true,
	    dateFormat: 'yymmdd'});
	} );
	$.datepicker.setDefaults( $.datepicker.regional[ "ko" ] );
	
	$(document).ready(function(){ 
	     
	     $('img.ui-datepicker-trigger').attr('style', 'margin:0px 7px 0px 7px');
	     
	     $("#datepicker3, #datepicker4").next().click(function () {
	            
	            $("#ui-datepicker-div > .ui-datepicker-calendar").hide();
	            
	            $("#ui-datepicker-div").position({
	                my: "center top",
	                at: "center bottom",
	                of: $(this)
	            });
	        });
	    
	}); 
</script>
<!--// [D] add -->  


	<div class="top_title"><h2> 포인트 쿠폰 이용 내역 조회</h2></div>

    
    <form id="mainForm" name="mainForm" action="<c:url value='/clip3_0/KTCPMW04.do'/>" method="GET">
    <table width="100%" class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="">
            <col width="">
       
        </colgroup>
        <tbody>
            <tr>
                <th>검색조건</th>
                <td>
                    <select class="form_sel_style1" name="status" id="status">
                        <option value="" selected="">전체</option>
                        <option value="user_ci">user_ci</option>
                        <option value="cust_id">cust_id</option>
                        <option value="ga_id">ga_id</option>
                        <option value="ctn">ctn</option>
                    </select>
                    <input class="form_input_style2" id="keywd" name="keywd" style="width:200px" value="">
                </td>
            </tr>
            <tr>
                <th>겸색기간</th>
                <td>
                    <div id="datepickerDiv1">
						<%
							Calendar cal = Calendar.getInstance();
							cal.add(Calendar.MONTH, -1);
							Date date = cal.getTime();
							String past = new SimpleDateFormat("yyyyMMdd").format(date);
						%>
                       	<c:choose>
	              			<c:when test="${datepicker1 ne null}">
	              				<input name="datepicker1"  type="text"  id="datepicker1"  value="${datepicker1}" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
	              			</c:when>
	              			<c:otherwise>
	              				<c:set var="past" value="<%=new java.util.Date(new java.util.Date().getTime() - 60*60*24*1000*7*4)%>"/>
	              				<input name="datepicker1"  type="text"  id="datepicker1"  value="<%=past %>" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
	              			</c:otherwise>
                   		</c:choose>
                        
                   		~
                    		
                   		<c:choose>
                   			<c:when test="${datepicker2 ne null}">
                   				<input name="datepicker2" type="text" id="datepicker2"  value="${datepicker2}" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
                   			</c:when>
                   			<c:otherwise>
                   				<input name="datepicker2" type="text" id="datepicker2"  value="<fmt:formatDate value="<%=new java.util.Date() %>" pattern="yyyyMMdd"/>" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
                   			</c:otherwise>
                   		</c:choose> 
                    </div>
                </td>
            </tr>
            <tr>
                <th>쿠폰 번호</th>
                <td>
                    <input class="form_input_style2" id="couponNumber" name="couponNumber" style="width:200px" value="">
                </td>
            </tr>
        </tbody>
    </table>
    <div id="btn_area">
        <input type="button" id="goSearch" name="goSearch" class="btn_area_a" value="검색">
        <input type="button" id="goExcel" name="goExcel" class="btn_area_a" value="엑셀 다운로드">
    </div>
    
    </form>
    
   <br>
   
  <c:choose>
		<c:when test="${(not empty list) and ('0' ne totalcount) }">
			조회 목록 전체 : 총 <font color="red">${totalcount} </font>건 &nbsp; &nbsp; 
<%-- 			<fmt:parseNumber value="${totalvalue }" var="total"></fmt:parseNumber> --%>
			적립 예정 포인트 합계 : <font color="red"><fmt:formatNumber value="${totalvalue }"/></font>
		</c:when>
		<c:otherwise>
			조회 목록 전체 : 총 <font color="red">0</font>건 &nbsp; &nbsp; 적립 예정 포인트 합계 : <font color="red">0</font>
		</c:otherwise>
   </c:choose>
   <br>
   <br>
   <!-- 매체 목록 -->
   <table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
        	<col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
        </colgroup>
        <tbody>
            <tr>
            	<th>사용일</th>
                <th>유효기간 시작일</th>
                <th>유효기간 종료일</th>
                <th>쿠폰번호</th>
                <th>포인트</th>
                <th>적립 포인트계</th>
                <th>결과코드</th>
            </tr>            
             <c:choose>
				<c:when test="${(not empty list) and ('0' ne totalcount) }">
					<c:forEach var="item" items="${list}">
						<tr>
							<td align="center">
<%-- 								<fmt:parseDate var="date1" value="${item.yyyymmdd}" pattern="yyyyMMdd"/> --%>
<%-- 								<fmt:parseDate var="date2" value="${item.hhmmss }" pattern="kkmmss"/> --%>
<%-- 								<fmt:formatDate value="${date1 }" pattern="yyyy.MM.dd"/> --%>
<%-- 								<br><fmt:formatDate value="${date2 }" pattern="kk:mm:ss"/> --%>
								${item.yyyymmdd }<br>
								${item.hhmmss }
							</td>
							<td align="center">-</td>
							<td align="center">-</td>
							<td align="center">${item.couponNumber }</td>
							<td align="center"><fmt:formatNumber value="${item.balance }"/>P</td>
							<td align="center"><fmt:formatNumber value="${item.realBalance }"/>P</td>
							<td align="center">(${item.resultCode})${item.resultMessage}</td>
						</tr>					
					</c:forEach>
				</c:when>
				<c:otherwise>
					 <tr>
                		<td colspan="7"> 조회값이 없습니다. </td>
		            </tr>
				</c:otherwise>
			</c:choose>
        </tbody>
    </table>
    
    <table width="100%">
		<tbody>
			<tr height="45px">
				<td align="center">
					<c:out value="${boardPaging.pagingHTML}" escapeXml="false"></c:out>
				</td>
			</tr>
		</tbody>
    </table>
    <script>
    //페이징
    function boardPaging(pg){
    	location.href="<c:url value='/clip3_0/KTCPMW04.do?pg="+pg+"' />";
    }
    </script>

