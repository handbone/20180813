<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  

	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/common.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/style.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/ui/jquery-ui.min.css'/>">
	<!-- JQuery -->
	<script lang="javascript" src="<c:url value='/resources/js/jquery-3.1.0.js'/>"></script>

	<style>
	.dot1 {
	    width: 160px;
	    height: 20px;
	 /* border: 1px solid red;  */
	}
	.dot2 {
	    width: 315px;
	    height: 20px;
	   /*   border: 1px solid red;  */
	}​
	</style>
	<script src="<c:url value='/resources/js/jquery.dotdotdot.min.js'/>" type="text/javascript"></script>
	<script>

		

        $(document).ready(function(){ 
            $('.dot1').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,
               
            }); 
            $('.dot2').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,  
               
            }); 
            $('#goUpdate').click(function(){
	            var 	formData = $("#mainForm").serialize();
	       		 $.ajax({
	       	            type: "POST",
	       	            url: "<c:url value='/clip3_0/KTCPMI01.do'/>",
	       	            data: formData,
	       	            dataType: "Json",
	       	            success: function (resData) {
	       	                console.log(resData.result);  
	       	                if (resData.result == "success") {
	       	                		location.href='<c:url value="/clip3_0/KTCPMW07.do"/>';
	       	                }
	       	            },
	       	            error: function (e) {
	       	            	alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
	       	            }
	       	        });
            });
        }); 

	</script>

<div class="top_title"><h2> 일차 관리 - 수정</h2></div>
    <form method="post" name="mainForm" id="mainForm" action="">
        <table width="100%" class="table_style8" cellpadding="0" cellspacing="0">
            <colgroup>
                <col width="200">
                <col width="">
            </colgroup>
            <tbody>
                <tr>
                    <th>일차</th>
                    <td>
                        <c:out value="${screenlockvo.day_count}"/> 일차 ( * 일차명은 수정하실 수 없습니다.)
                        <input type="hidden" name="day_count" value="${screenlockvo.day_count}">
                    </td>
                </tr>
                <tr>
                    <th>문구</th>
                    <td>
						<textarea id="" name="content" rows="10" cols="100"><c:out value="${screenlockvo.content}"/></textarea> 
                   </td>
               </tr>
               <tr>
                   <th>연결 URL</th>
                   <td>
                        <input id=""  class="form_input_style1" name="link_url" size="120" maxlength="2000" type="text" value="<c:out value='${screenlockvo.link_url}'/>"> 
                   </td>
               </tr>
               <tr>
                   <th>버튼 노출</th>
                   <td>
                   		<c:choose>
                   			<c:when test="${screenlockvo.expose_yn == 'Y'}">
                   				<input id="" name="expose_yn" value="Y" type="radio" checked="checked"> 노출
                   				<input id="" name="expose_yn" value="N" type="radio"> 미노출
                   			</c:when>
                   			<c:otherwise>
                   				<input id="" name="expose_yn" value="Y" type="radio"> 노출
                   				<input id="" name="expose_yn" value="N" type="radio" checked="checked"> 미노출
                   			</c:otherwise>
                   		</c:choose>
                    </td>
               </tr>
            </tbody>
        </table>
        <div id="btn_area">
            <input type="button" class="btn_area_a" value="취소" onclick="javascript:history.go(-1)">
            <input type="button" id="goUpdate" name="goUpdate" class="btn_area_a" value="확인">
            
            <input type="hidden" id="requester_code" name="requester_code" value="kb_insure_car">
            <input type="hidden" id="page" name="page" value="1">
        </div>  
    </form>

    <style type="text/css">
    input.error, textarea.error{
      border:1px dashed red;
    }
    label.error{
      margin-left:10px;
      color:red;
    }
    </style>
    
