<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/common.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/style.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/ui/jquery-ui.min.css'/>">
	<!-- JQuery -->
	<script lang="javascript" src="<c:url value='/resources/js/jquery-3.1.0.js'/>"></script>

	<style>
	.dot1 {
	    width: 160px;
	    height: 20px;
	 /* border: 1px solid red;  */
	}
	.dot2 {
	    width: 315px;
	    height: 20px;
	   /*   border: 1px solid red;  */
	}​
	</style>
	<script src="<c:url value='/resources/js/jquery.dotdotdot.min.js'/>" type="text/javascript"></script>
	<script>

		

        $(document).ready(function(){ 
        	
        	
            $('.dot1').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,
               
            }); 
            $('.dot2').dotdotdot({ 
                ellipsis: '... ', 
                wrap: 'word', 
                after: null, 
                watch: true, 
                height: 20,  
               
            }); 
        }); 

	</script>

<!--Content layout -->

<!-- [D] add -->
<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
    
<!--// [D] add -->  

	<div class="top_title"><h2> 포인트 폭탄 내역 조회 - 상세</h2></div>
    <table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
        	<col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
        </colgroup>
        <tbody>
        	<tr>
            	<th colspan="9">사용자의 적립내역</th>
            </tr>
            <tr>
            	<th>발생일자</th>
                <th>Cust_id</th>
                <th>ctn</th>
                <th>Ga_id</th>
                <th>폭탄 레벨</th>
                <th>랜덤 포인트</th>
                <th>오퍼월 포인트</th>
                <th>Campaign ID</th>
                <th>당첨 포인트 합계</th>
            </tr>
            <c:choose>
				<c:when test="${(not empty list) and ('0' ne totalcount) }">
					<c:forEach var="item" items="${list}">
						<tr>
							<td align="center">
								<fmt:parseDate var="date2" value="${item.use_start_date }" pattern="yyyy-MM-dd HH:mm:ss"/>
								<fmt:formatDate value="${date2 }" pattern="yyyy.MM.dd"/>
								<br><fmt:formatDate value="${date2 }" pattern="HH:mm:ss"/>
							</td>
							<td align="center" style="word-break:break-all;">${item.cust_id }</td>
							<td align="center" style="word-break:break-all;">${item.ctn }</td>
							<td align="center" style="word-break:break-all;">${item.ga_id }</td>
							<td align="center">${item.level }</td>
							<td align="center">컬럼 없음</td>
							<td align="center">컬럼 없음</td>
							<td align="center">컬럼 없음</td>
							<td align="center">${item.total_reward_point }</td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr>
						<td colspan="9"> 조회값이 없습니다. </td>
					</tr>
				</c:otherwise>
			</c:choose>
        </tbody>
    </table>
    <style type="text/css">
    input.error, textarea.error{
      border:1px dashed red;
    }
    label.error{
      margin-left:10px;
      color:red;
    }
    </style>

