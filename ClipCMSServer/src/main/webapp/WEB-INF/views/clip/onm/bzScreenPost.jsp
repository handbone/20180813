<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>

<script>


$( function() {
    $( "#datepicker1, #datepicker2" ).datepicker({
    autoSize: true,
    showOn: 'button', 
    buttonImage: '<c:url value='/resources/css/ui/img/calendar.gif'/>',   
    buttonImageOnly: true,
    dateFormat: 'yymmdd'});
} );
$.datepicker.setDefaults( $.datepicker.regional[ "ko" ] );

 $(document).ready(function(){ 
	  
	 $('img.ui-datepicker-trigger').attr('style', 'margin:0px 7px 0px 7px');
	 
	// 검색
	 $("#goSearch").bind("click",function() {
		 $("#excelDownload").val("");
		 formSubmit();
	 });
	 
	 // 엑셀 다운로드
	 $("#goExcel").bind("click",function() {
		 formSubmit("goExcel");
	 });
	 
	 function formSubmit(goExcelString){
		 
		 if( goExcelString == "goExcel"){
			 $("#excelDownload").val("Y");
		 }
		 
			$("#startdate").val($("#datepicker1").val());
			$("#enddate").val($("#datepicker2").val());
			 
			/* if($("#keywd").val() == ""){
			 	alert("검색조건을 입력해주세요.");
				return;
			 } */
			 if($("#startdate").val() == ""){
				 	alert("시작일을 입력해주세요.");
					return;
				 }
			 if($("#enddate").val() == ""){
			 	alert("종료일을 입력해주세요.");
				return;
			 }
			 
			 if($("#enddate").val() < $("#startdate").val()){
			 	alert("시작일이 종료일보다 빠를수 없습니다.");
				return;
			 }
				
		 $('#mainForm').attr('action','<c:url value="/onm/bzScreenPostLog.do" />').attr('method', 'post').submit();
	 }

	 
	// 검색값 설정
    /* if('${searchBean.status}'!=''){
  		$("#status").val('${searchBean.status}').attr("selected", "selected");
    } */
	$("#user_id").val('${searchBean.user_id}')
    $("#datepicker1").val('${searchBean.startdate}');
	$("#datepicker2").val('${searchBean.enddate}');

	if( '' == $("#datepicker1").val()){
		$("#datepicker1").val($.datepicker.formatDate('yymmdd', new Date()));
	}
	if( '' == $("#datepicker2").val()){
		$("#datepicker2").val($.datepicker.formatDate('yymmdd', new Date()));
	}
	
	
}); 

</script>

	<div class="top_title"><h2> [로그] BZSCREEN </h2></div>
    <!-- 검색조건 -->
    <form id="mainForm" name="mainForm" >
    <table width="100%" class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="100">
            <col width="">
       
        </colgroup>
        <tr>
        	<th>사용자 ID</th>
        	<td>
            	<!-- <select class="form_sel_style1" name="status" id="status">
            		<option value="" selected>전체</option>
            		<option value="cust_id" >cust_id</option>
                	<option value="ga_id" >ga_id</option>
                	<option value="ctn" >ctn</option>
            	</select> -->
            	<input class="form_input_style2" id="user_id" name="user_id" style="width:200px" value="${searchBean.user_id}">
            </td>
        </tr>
		<tr>
       	<th>검색기간</th>
       	<td>
           	<input name="datepicker1"  type="text"  id="datepicker1"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
			~ <input name="datepicker2" type="text" id="datepicker2"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">		            
           </td>
       </tr>
       <tr></tr>
    </table>
    <div id="btn_area">
    <input type="button" id="goSearch" name="goSearch" class="btn_area_a" value="검색" >
    <input type="button" id="goExcel" name="goExcel" class="btn_area_a" value="엑셀 다운로드" >
    </div>
    <input type="hidden" id="startdate" name="startdate" value="" >
    <input type="hidden" id="enddate" name="enddate" value="" >
    <input type="hidden" id="excelDownload" name="excelDownload" value="" >
    <input type="hidden" id="pg" name="pg" value="" >
    </form>
    
    
   <br>

   <table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
    		<col width="">
    		<col width="">
    		<col width="">
    		<col width="">
    		<col width="">
    		<col width="">
    		<col width="">
    		<col width="">
    		<col width="">
    		<col width="">
    		<col width="">
    		<col width="">
    		<col width="">
            
        </colgroup>
        <tr>
        	<th>transaction_id</th>
            <th>user_id</th>
            <th>campaign_id</th>
            <th>campaign_name</th>
            <th>event_at</th>
            <th>is_media</th>
            <th>extra</th>
            <th>action_type</th>
            <th>point</th>
            <th>base_point</th>
            <th>data</th>
            <th>reg_date</th>
            <th>reg_time</th>
        </tr>
        
        <c:forEach var="list" items="${list}">
        <tr>
        	<%-- <td align="center" style="word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;">${list.cust_id}</td>
            <td align="center" style="word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;">${list.ga_id}</td>
            <td align="right" style="word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;">${list.ctn}</td> --%>
            <td align="center" style="word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;">${list.transaction_id}</td>
            <td align="center" style="word-break:break-all;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;">${list.user_id}</td>
            <td align="center">${list.campaign_id}</td>
            <td align="center">${list.campaign_name}</td>
            <td align="center">${list.event_at}</td>
            <td align="center">${list.is_media}</td>
            <td align="center">${list.extra}</td>
            <td align="center">${list.action_type}</td>
            <td align="center">${list.point}</td>
            <td align="center">${list.base_point}</td>
            <td align="center">${list.data}</td>
            <td align="center">${list.reg_date}</td>
            <td align="center">${list.reg_time}</td>
        </tr>
            
        </c:forEach>
        <c:if test="${fn:length(list) == 0}" >
        <tr>
        	<td colspan="13" > 조회값이 없습니다. </td>
        </tr>
        </c:if>
        
        
    </table>
    
    <table width="100%"><tr height="45px"><td align="center">${boardPaging.pagingHTML}</td></tr></table>
    <script>
    //페이징
    function boardPaging(pg){
    	
    	 $("#pg").val(pg);
    	 $("#excelDownload").val("");
    	 
    	 $("#startdate").val($("#datepicker1").val());
			$("#enddate").val($("#datepicker2").val());
			 
			/* if($("#keywd").val() == ""){
			 	alert("검색조건을 입력해주세요.");
				return;
			 } */
			 if($("#startdate").val() == ""){
				 	alert("시작일을 입력해주세요.");
					return;
				 }
			 if($("#enddate").val() == ""){
			 	alert("종료일을 입력해주세요.");
				return;
			 }
			 
			 if($("#enddate").val() < $("#startdate").val()){
			 	alert("시작일이 종료일보다 빠를수 없습니다.");
				return;
			 }
				
		 $('#mainForm').attr('action','<c:url value="/onm/bzScreenPostLog.do" />').attr('method', 'post').submit();

    }
    </script>