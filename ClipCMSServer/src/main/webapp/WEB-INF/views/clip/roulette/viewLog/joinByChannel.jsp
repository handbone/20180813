<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>

<script>


$( function() {
    $( "#datepicker1, #datepicker2" ).datepicker({
    autoSize: true,
    showOn: 'button', 
    buttonImage: '<c:url value='/resources/css/ui/img/calendar.gif'/>',   
    buttonImageOnly: true,
    dateFormat: 'yymmdd'});
} );
$.datepicker.setDefaults( $.datepicker.regional[ "ko" ] );

$( function() {
    $( "#datepicker3, #datepicker4" ).datepicker({
	    autoSize: true,
	    showOn: 'button', 
	    buttonImage: '<c:url value='/resources/css/ui/img/calendar.gif'/>',   
	    buttonImageOnly: true,
	    dateFormat: 'yymm',
	    changeMonth: true,
	    changeYear: true,
	    showButtonPanel: true 
	    ,onClose: function(dateText, inst) {
	        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        $(this).val($.datepicker.formatDate('yymm', new Date(year, month, 1)));
	    }
	    , onChangeMonthYear: function( year, month, inst ){
	    	
	    	var month2 = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	        var year2 = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        $(this).val($.datepicker.formatDate('yymm', new Date(year2, month2, 1)));
	        
	        $("#ui-datepicker-div > .ui-datepicker-calendar").hide();
	    }
	});
    
        
} );


 $(document).ready(function(){ 
	 
	 $('img.ui-datepicker-trigger').attr('style', 'margin:0px 7px 0px 7px');
	 
	 // 월별 레이어 감추기
	 $("#datepicker3, #datepicker4").next().click(function () {
			
			$("#ui-datepicker-div > .ui-datepicker-calendar").hide();
			
		    $("#ui-datepicker-div").position({
		        my: "center top",
		        at: "center bottom",
		        of: $(this)
		    });
		});
	 
	 // 라이오버튼 컨트롤
	 $("input[name=dateType]").change(function() {
		 $("#startdate").val("");
		 $("#enddate").val("");
		 /* $("#datepicker1").val("");
		 $("#datepicker2").val("");
		 $("#datepicker3").val("");
		 $("#datepicker4").val(""); */
		 if("day" == $(this).val() ){
			 $("#datepickerDiv1").show();
			 $("#datepickerDiv2").hide();
		 }else if("month" == $(this).val()){
			 $("#datepickerDiv1").hide();
			 $("#datepickerDiv2").show();
		 }else if("hour" == $(this).val()){
			 $("#datepickerDiv1").show();
			 $("#datepickerDiv2").hide();
		 }
	});
	 $("#datepickerDiv1").show();
	 $("#datepickerDiv2").hide();
	 
	 // 검색
	 $("#goSearch").bind("click",function() {
		 $("#excelDownload").val("");
		 formSubmit();
	 });
	 
	 // 엑셀 다운로드
	 $("#goExcel").bind("click",function() {
		 formSubmit("goExcel");
	 });
	 
	 function formSubmit(goExcelString){
		 
		 if( goExcelString == "goExcel"){
			 $("#excelDownload").val("Y");
		 }
		 
		 if("day" == $("input[name=dateType]:checked").val() ){
			 $("#startdate").val($("#datepicker1").val());
			 $("#enddate").val($("#datepicker2").val());
		 }else if("month" == $("input[name=dateType]:checked").val()){
			 $("#startdate").val($("#datepicker3").val());
			 $("#enddate").val($("#datepicker4").val());
		 }else if("hour" == $("input[name=dateType]:checked").val()){
			 $("#startdate").val($("#datepicker1").val());
			 $("#enddate").val($("#datepicker2").val());
		 }
		 
		 if($("#startdate").val() == ""){
			 	alert("시작일을 입력해주세요.");
				return;
			 }
		 if($("#enddate").val() == ""){
		 	alert("종료일을 입력해주세요.");
			return;
		 }
		 
		 if($("#enddate").val() < $("#startdate").val()){
		 	alert("시작일이 종료일보다 빠를수 없습니다.");
			return;
		 }
			
	 $('#mainForm').attr('action','<c:url value="/roulette/viewLog/joinByChannel.do" />').attr('method', 'post').submit();
	 }
	 
	// 검색값 설정
    if('${searchBean.roulette_id}'!=''){
  		$("#roulette_id").val('${searchBean.roulette_id}').attr("selected", "selected");
    }
	
	if( '' != '${searchBean.dateType}'){
		$("input[name=dateType][value=" + "${searchBean.dateType}" + "]").attr("checked", true);
	}
    
 	// 검색값 설정
    if('day' == '${searchBean.dateType}'){
    	$("#datepicker1").val('${searchBean.startdate}');
    	$("#datepicker2").val('${searchBean.enddate}');
    	$("#datepickerDiv1").show();
		$("#datepickerDiv2").hide();
    }else if('month' == '${searchBean.dateType}'){
    	$("#datepicker3").val('${searchBean.startdate}');
    	$("#datepicker4").val('${searchBean.enddate}');
    	$("#datepickerDiv1").hide();
		 $("#datepickerDiv2").show();
    }else if('hour' == '${searchBean.dateType}'){
    	$("#datepicker1").val('${searchBean.startdate}');
    	$("#datepicker2").val('${searchBean.enddate}');
    	$("#datepickerDiv1").show();
		$("#datepickerDiv2").hide();
    }
    

    	if( '' == $("#datepicker1").val()){
    		$("#datepicker1").val($.datepicker.formatDate('yymmdd', new Date()));
    	}
    	if( '' == $("#datepicker2").val()){
    		$("#datepicker2").val($.datepicker.formatDate('yymmdd', new Date()));
    	}
    	if( '' == $("#datepicker3").val()){
    		$("#datepicker3").val($.datepicker.formatDate('yymm', new Date()));
    	}
    	if( '' == $("#datepicker4").val()){
    		$("#datepicker4").val($.datepicker.formatDate('yymm', new Date()));
    	}
	
	
	
    
    
    
}); 

</script>

	<div class="top_title"><h2> [통계] 채널별 참여자</h2></div>
    <!-- 검색조건 -->
    <form id="mainForm" name="mainForm" >
    <table width="100%" class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="100">
            <col width="">
       
        </colgroup>
        <tr>
        	<th>검색조건</th>
            <td>
            <select class="form_sel_style1" name="roulette_id" id="roulette_id">
            	<option value="">전체</option>
             		<c:forEach items="${channelNameList}" var="channelNameList" varStatus="status">
		 				<option value="${channelNameList.roulette_id}" >${channelNameList.channelname}</option>
			 		</c:forEach>
			 </select>
            </td>
        </tr>
		<tr>
       	<th>검색기간</th>
           <td>
           <input id="dateType" name="dateType" value="day" checked="CHECKED" type="radio" /> 일자별
           <input id="dateType" name="dateType" value="month" type="radio"/> 월별
           <input id="dateType" name="dateType" value="hour" type="radio"/> 시간대별
           <br>
           <br>
           <div id="datepickerDiv1">
           	<input name="datepicker1"  type="text"  id="datepicker1"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
			~ <input name="datepicker2" type="text" id="datepicker2"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">		            
           </div>
           <div id="datepickerDiv2">
            <input name="datepicker3"  type="text"  id="datepicker3"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
			~ <input name="datepicker4" type="text" id="datepicker4"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">		            
            </div>
           </td>
       </tr>
       <tr></tr>
    </table>
    <div id="btn_area">
    <input type="button" id="goSearch" name="goSearch" class="btn_area_a" value="검색" >
    <input type="button" id="goExcel" name="goExcel" class="btn_area_a" value="엑셀 다운로드" >
    </div>
    <input type="hidden" id="startdate" name="startdate" value="" >
    <input type="hidden" id="enddate" name="enddate" value="" >
    <input type="hidden" id="excelDownload" name="excelDownload" value="" >
    <input type="hidden" id="pg" name="pg" value="" >
    
    </form>
    
    
   <br>

   <table class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="20%">
            <col width="20%">
            <col width="20%">
            <col width="20%">
            <col width="">
        </colgroup>
        <tr>
        	<th>날짜</th>
            <th>채널명</th>
            <th>당첨자수</th>
            <th>당첨자수 ('꽝' 포함)</th>
            <th>당첨포인트</th>
        </tr>
        <c:set var="totalJoinSum" value="0"/>
        <c:set var="totalJoinSumPlusZero" value="0"/>
        <c:set var="totalSavePoniSum" value="0"/>
        
        <c:forEach var="list" items="${list}">
        <tr>
        	<td align="right" style="text-align:center" >${list.sdate }</td>
            <td align="center">${list.channelname }</td>
            <td align="right"><fmt:formatNumber value="${list.joinSum}" pattern="#,###" /> 명</td>
            <td align="right"><fmt:formatNumber value="${list.joinSumPlusZero}" pattern="#,###" /> 명</td>
            <td align="right"><fmt:formatNumber value="${list.savePointSum}" pattern="#,###" /> P</td>
        </tr>
        
        <c:set var="totalJoinSum" value="${totalJoinSum + list.joinSum}"/>
        <c:set var="totalJoinSumPlusZero" value="${totalJoinSumPlusZero + list.joinSumPlusZero}"/>
        <c:set var="totalSavePoniSum" value="${totalSavePoniSum + list.savePointSum}"/>
        
        </c:forEach>
       
       
        <c:if test="${fn:length(list) == 0}" >
        <tr>
        	<td colspan="5" > 조회값이 없습니다. </td>
        </tr>
        </c:if>
        
        
        
       <tr>
        	<th align="center">합계</th>
            <td align="right"></td>
            <td align="right"><fmt:formatNumber value="${totalJoinSum}" pattern="#,###" /> 명</td>
            <td align="right"><fmt:formatNumber value="${totalJoinSumPlusZero}" pattern="#,###" /> 명</td>
            <td align="right"><fmt:formatNumber value="${totalSavePoniSum}" pattern="#,###" /> P</td>
        </tr>
    </table>
    <table width="100%"><tr height="45px"><td align="center">${boardPaging.pagingHTML}</td></tr></table>
    <script>
    //페이징
    function boardPaging(pg){
    	
    	 $("#pg").val(pg);
    	 $("#excelDownload").val("");
    	 
    	 if("day" == $("input[name=dateType]:checked").val() ){
			 $("#startdate").val($("#datepicker1").val());
			 $("#enddate").val($("#datepicker2").val());
		 }else if("month" == $("input[name=dateType]:checked").val()){
			 $("#startdate").val($("#datepicker3").val());
			 $("#enddate").val($("#datepicker4").val());
		 }else if("hour" == $("input[name=dateType]:checked").val()){
			 $("#startdate").val($("#datepicker1").val());
			 $("#enddate").val($("#datepicker2").val());
		 }
		 
		 if($("#startdate").val() == ""){
			 	alert("시작일을 입력해주세요.");
				return;
			 }
		 if($("#enddate").val() == ""){
		 	alert("종료일을 입력해주세요.");
			return;
		 }
		 
		 if($("#enddate").val() < $("#startdate").val()){
		 	alert("시작일이 종료일보다 빠를수 없습니다.");
			return;
		 }
			
	 $('#mainForm').attr('action','<c:url value="/roulette/viewLog/joinByChannel.do" />').attr('method', 'post').submit();
    }
    </script>
   
    