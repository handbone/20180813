<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script src="<c:url value='/resources/js/jquery.validate.min.js' />"></script>

<!-- [D] add -->
<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>

<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>
<script>
	$(function() {
		$("#datepicker1, #datepicker2")
				.datepicker(
						{
							autoSize : true,
							showOn : 'button',
							buttonImage : '<c:url value="/resources/css/ui/img/calendar.gif"/>',
							buttonImageOnly : true,
							dateFormat : 'yymmdd'
						});
	});
	$.datepicker.setDefaults($.datepicker.regional["ko"]);

	$(document).ready(function() {

		$('img.ui-datepicker-trigger').attr('style', 'margin:0px 7px 0px 7px');

		$("#datepicker3, #datepicker4").next().click(function() {

			$("#ui-datepicker-div > .ui-datepicker-calendar").hide();

			$("#ui-datepicker-div").position({
				my : "center top",
				at : "center bottom",
				of : $(this)
			});
		});

	});
</script>
<!--// [D] add -->


<script>
 $(document).ready(function(){

	 $("#requesterNameChk").bind("click", function(){
		 
		if( $("#requester_name").val() == "" || $("#requester_name").val() == null ){
			alert("매체명을 입력하세요.");
			return;
		}
			
		 formData = $("#mainForm").serialize();
		 $.ajax({
	            type: "POST",
	            url: "../acl/requesterNameChk.do",
	            data: formData,
	            dataType: "Json",
	            success: function (resData) {
	                console.log(resData.result);  
	                if (resData.result == "success") {
	                	alert("사용 할 수 있는 매체명입니다.");
	                	$("#requesterNameChkValue").val("Y");
	                } else {
	                	alert("사용 할 수 없는 매체명입니다.");
	                }

	            },
	            error: function (e) {
	            	alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
	            }
	        });
	 });
	 
	 $("#requesterCodeChk").bind("click", function(){
		 
			if( $("#requester_code").val() == "" || $("#requester_code").val() == null ){
				alert("매체코드를 입력하세요.");
				return;
			}
				
			 formData = $("#mainForm").serialize();
			 $.ajax({
		            type: "POST",
		            url: "../acl/requesterCodeChk.do",
		            data: formData,
		            dataType: "Json",
		            success: function (resData) {
		                console.log(resData.result);  
		                if (resData.result == "success") {
		                	alert("사용 할 수 있는 매체코드입니다.");
		                	$("#requesterCodeChkValue").val("Y");
		                } else {
		                	alert("사용 할 수 없는 매체코드입니다.");
		                }

		            },
		            error: function (e) {
		            	alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
		            }
		        });
		 });
		$('#datepicker1').change(function(){
        	var date1 = $('#datepicker1').val();
        	var date2 = $('#datepicker2').val();
        	
        	if(date2 == '' || date2 == null){
        		return;
        	}
        	
        	var startDate = new Date(date1.slice(0, 4), date1.slice(4, 6), date1.slice(6, 8));
        	var endDate = new Date(date2.slice(0, 4), date2.slice(4, 6), date2.slice(6, 8));
        	
        	if(startDate.getTime() > endDate.getTime()){
        		alert('사용 기간을 확인해주세요.');
        		$('#datepicker1').val('');
        		return;
        	}
        });
        $('#datepicker2').change(function(){
        	var date1 = $('#datepicker1').val();
        	var date2 = $('#datepicker2').val();
        	
        	var startDate = new Date(date1.slice(0, 4), date1.slice(4, 6), date1.slice(6, 8));
        	var endDate = new Date(date2.slice(0, 4), date2.slice(4, 6), date2.slice(6, 8));
        	
        	if(startDate.getTime() > endDate.getTime()){
        		alert('사용 기간을 확인해주세요.');
        		$('#datepicker2').val('');
        		return;
        	}
        });
     
 }); 
 
 
</script>

<div class="top_title"><h2>매체관리 - 신규등록</h2></div>
<form method="post" name="mainForm" id="mainForm" action="<c:url value='/manage/acl/aclInsert.do' />" >
<table width="100%" class="table_style8" cellpadding="0" cellspacing="0" dwcopytype="CopyTableCell">
  
   	<colgroup>
       		<col width="200">
            <col width="">
       </colgroup>
   	   <tr>
       	<th>매체명</th>
           <td>
           		<input id="requester_name" name="requester_name" size="50" maxlength="255" type="text"> (255byte 이내) <input type="button" id="requesterNameChk" name="requesterNameChk" class="btn_area_a" value="중복조회" >
           </td>
       </tr>
       <tr>
       	<th>매체코드</th>
           <td>
           		<input id="requester_code" name="requester_code" size="50" maxlength="255" type="text"> (255byte 이내) <input type="button" id="requesterCodeChk" name="requesterCodeChk" class="btn_area_a" value="중복조회" >
           </td>
       </tr>
       <tr>
       	<th>서비스 상태</th>
           <td>
       			<input id="use_status" name="use_status" value="Y" checked="CHECKED" type="radio" /> 사용중
          		<input id="use_status" name="use_status" value="N" type="radio"/> 중지
			</td>
       </tr> 
			    
       <tr>
       	<th>IP</th>
           <td>
           		<input id="ip_addr" name="ip_addr" size="120" maxlength="2000" type="text"> (2000byte 이내) 
           		<br>
           		다중 아이피의 경우 ',' 로 구분/대역 아이피의 경우 '-' 로 대역 표시
           </td>
       </tr>
       
       <tr>
       	<th>적립 제한 설정</th>
           <td>
       			<input id="acl_limit_yn" name="acl_limit_yn" value="Y"  type="radio" /> 사용중
          		<input id="acl_limit_yn" name="acl_limit_yn" value="N" checked="CHECKED" type="radio"/> 중지
          		<br>
          		* 적립 제한 설정을 하면 특정한 횟수 이상 적립 후에는 적립으로 전환되지 않는다..
			</td>
       </tr> 
        <tr>
       	<th>적립 허용 횟수</th>
           <td>
           		<input id="acl_limit_cnt" name="acl_limit_cnt" size="10" maxlength="10" type="text" value="0"> 
           		* 정수로 입력한다. ( ex: 4회까지 허용 할 경우 숫자 4 를 입력한다. )
           </td>
       </tr>
       <!-- [D] add -->
        <tr>
       	<th>사용 기간</th>
       	<td>
       		<div id="datepickerDiv1">
				<%
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.MONTH, +1);
				Date date = cal.getTime();
				String past = new SimpleDateFormat("yyyyMMdd").format(date);
				%>
				<c:set var="past" value="<%=new java.util.Date(new java.util.Date().getTime() - 60*60*24*1000*7*4)%>"/>
				<input name="datepicker1"  type="text"  id="datepicker1"  value="<fmt:formatDate value="<%=new java.util.Date() %>" pattern="yyyyMMdd"/>" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
				~ 
				<input name="datepicker2" type="text" id="datepicker2"  value="<%=past %>" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
			</div>
       	</td>
       </tr> 
       <!--// [D] add -->
       
       <tr>
       	<th>비고</th>
           <td>
	           	<textarea id="description" name="description" rows="10" cols="100">  </textarea> 
           </td>
       </tr>
</table>

<div id="btn_area">
 	<input type="button" class="btn_area_a" value="목록" onclick="javascript:history.go(-1)">
 	<input type="button" id="goInsert" name="goInsert" class="btn_area_a" value="등록" >
 	
 	<input type="hidden" id="requesterNameChkValue" name="requesterNameChkValue" value="N">
 	<input type="hidden" id="requesterCodeChkValue" name="requesterCodeChkValue" value="N">
 	
</div>   
</form>

<style type="text/css">
input.error, textarea.error{
  border:1px dashed red;
}
label.error{
  margin-left:10px;
  color:red;
}
</style>

<script>
	
	 $(document).ready(function() {
		 $("#goInsert").bind("click",function() {
			 $("#mainForm").submit();
		});
	});
 	

	
	$("#mainForm").validate({
		rules: {
			requester_name: { required: true,	maxlength: 255},
			requester_code: { required: true,	maxlength: 255},
			ip_addr: { required: true,	maxlength: 2000},
			description: { required: true,	maxlength: 2000},
			acl_limit_cnt: { digits : true }
		},
		messages: {
			
		},submitHandler: function(form) {
			
			var requesterNameChkValue = $("#requesterNameChkValue").val() ;
			var requesterCodeChkValue = $("#requesterCodeChkValue").val() ;
			
			if( requesterNameChkValue != "Y" ){
				alert("매체명 중복조회를 해주세요.");
				return;
			}
			if( requesterCodeChkValue != "Y"){
				alert("매체코드 중복조회를 해주세요.");
				return;
			}
			
			//alert($("input:checkbox[name=timegbn]:checked").val());
		    form.submit();
		}
	});

</script>