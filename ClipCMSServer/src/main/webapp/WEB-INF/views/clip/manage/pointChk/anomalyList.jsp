<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>

<script>


$( function() {
    $( "#datepicker1, #datepicker2"  ).datepicker({
    autoSize: true,
    showOn: 'button', 
    buttonImage: '<c:url value='/resources/css/ui/img/calendar.gif'/>',   
    buttonImageOnly: true,
    dateFormat: 'yymmdd'});
} );
$.datepicker.setDefaults( $.datepicker.regional[ "ko" ] );

 $(document).ready(function(){ 
	  
	 $('img.ui-datepicker-trigger').attr('style', 'margin:0px 7px 0px 7px');
	 
	// 검색
	 $("#goSearch").bind("click",function() {
		 $("#excelDownload").val("");
		 formSubmit();
	 });
	 
	 // 엑셀 다운로드
	 $("#goExcel").bind("click",function() {
		 formSubmit("goExcel");
	 });
	 
	 function formSubmit(goExcelString){
		 
		 if( goExcelString == "goExcel"){
			 $("#excelDownload").val("Y");
		 }
		 
			$("#startdate").val($("#datepicker1").val());
			$("#enddate").val($("#datepicker2").val());
			
			 if($("#startdate").val() == ""){
				 	alert("시작일을 입력해주세요.");
					return;
				 }
			 if($("#enddate").val() == ""){
				 	alert("종료일을 입력해주세요.");
					return;
				 }
		 $('#mainForm').attr('action','<c:url value="/manage/pointChk/anomalyList.do" />').attr('method', 'post').submit();
		 
		 
	 }
	 
	 $("#goLimitPointConfig").bind("click",function() {
		 $(location).attr('href','<c:url value="/manage/pointChk/anomalyConfigForm.do" />'); 
	 });
	 
	 
	 
	 // 검색값 설정
    if('${searchBean.media}'!=''){
  		$("#media").val('${searchBean.media}').attr("selected", "selected");
    }
    $("#datepicker1").val('${searchBean.startdate}');
    $("#datepicker2").val('${searchBean.enddate}');
    
    var preDate = new Date();
	preDate.setDate(preDate.getDate() );
	if( '' == $("#datepicker1").val()){
		$("#datepicker1").val($.datepicker.formatDate('yymmdd', preDate));
	}
	if( '' == $("#datepicker2").val()){
		$("#datepicker2").val($.datepicker.formatDate('yymmdd', preDate));
	}
	
}); 

</script>

	<div class="top_title"><h2> 이상탐지 </h2></div>
    <!-- 검색조건 -->
    <form id="mainForm" name="mainForm" >
    <table width="100%" class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="100">
            <col width="">
       
        </colgroup>
        <tr>
        	<th>매체</th>
            <td>
            <select class="form_sel_style1" name="media" id="media">
            	<option value="">전체</option>
             		<c:forEach items="${aclNamelist}" var="aclNamelist" varStatus="status">
		 				<option value="${aclNamelist.requester_code}" >${aclNamelist.requester_name}</option>
			 		</c:forEach>
			 </select>
            </td>
        </tr>
		<tr>
		<tr>
       	<th>검색기간</th>
       	<td>
           	<input name="datepicker1"  type="text"  id="datepicker1"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
		   ~ <input name="datepicker2" type="text" id="datepicker2"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
		   </td>
       </tr>
    </table>
    <div id="btn_area">
    <input type="button" id="goLimitPointConfig" name="goLimitPointConfig" class="btn_area_a" value="설정변경" >
    <input type="button" id="goSearch" name="goSearch" class="btn_area_a" value="검색" >
    <input type="button" id="goExcel" name="goExcel" class="btn_area_a" value="엑셀 다운로드" >
    </div>
    <input type="hidden" id="startdate" name="startdate" value="" >
    <input type="hidden" id="enddate" name="enddate" value="" >
    <input type="hidden" id="excelDownload" name="excelDownload" value="" >
    <input type="hidden" id="pg" name="pg" value="" >
    
    </form>
    
    
   <br>




<table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
            <col width="">
        </colgroup>
        <tr>
            <td align="center">3일 내 가장 최근 발생내역 : 
            <c:choose>
				 <c:when test='${tblStatAnomalyDetectionTop.media eq null  }'>
				  없음
				 </c:when>
				 <c:when test="${tblStatAnomalyDetectionTop.media ne null }">
				    ${tblStatAnomalyDetectionTop.stat_date}:${tblStatAnomalyDetectionTop.stat_hour}시
					${tblStatAnomalyDetectionTop.limit_point_value} 포인트 기준 / 초과 발생함.
				 </c:when>
			</c:choose>  
		</td>
        </tr>
    </table>
    
<br>
<br>
    
   <table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
    		<col width="">
            <col width="">
        </colgroup>
        <tr>
        	<th>매체</th>
        	<th>날짜</th>
        	<th>기준초과 적립자</th>
        	<th>기준초과 사용자</th>
        	<th>시간별 총 적립포인트</th>
        	<th>시간별 총 사용포인트</th>
        	
        </tr>
        
        <c:forEach var="list" items="${list}">
        <tr>
            <td align="center">${list.media}</td>
            <td align="center">${list.stat_date} ${list.stat_hour}</td>
            <td align="right"><fmt:formatNumber value="${list.limit_input_count}" pattern="#,###" /></td>
            <td align="right"><fmt:formatNumber value="${list.limit_output_count}" pattern="#,###" /></td>
            <td align="right"><fmt:formatNumber value="${list.input_point}" pattern="#,###" /> P</td>
            <td align="right"><fmt:formatNumber value="${list.output_point}" pattern="#,###" /> P</td>
        </tr>
        </c:forEach>
        <c:if test="${fn:length(list) == 0}" >
        <tr>
        	<td colspan="6" > 조회값이 없습니다. </td>
        </tr>
        </c:if>
    </table>
    
    <table width="100%"><tr height="45px"><td align="center">${boardPaging.pagingHTML}</td></tr></table>
    <script>
    //페이징
    function boardPaging(pg){
    	
    	 $("#pg").val(pg);
    	 $("#excelDownload").val("");
    	 
    	 $("#startdate").val($("#datepicker1").val());
			$("#enddate").val($("#datepicker2").val());
			
			 if($("#startdate").val() == ""){
				 	alert("시작일을 입력해주세요.");
					return;
				 }
			 if($("#enddate").val() == ""){
				 	alert("종료일을 입력해주세요.");
					return;
				 }
		 $('#mainForm').attr('action','<c:url value="/manage/pointChk/anomalyList.do" />').attr('method', 'post').submit();

    }
    </script>
    