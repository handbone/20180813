<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>

<script>


$( function() {
    $( "#datepicker1, #datepicker2" ).datepicker({
    autoSize: true,
    showOn: 'button', 
    buttonImage: '<c:url value='/resources/css/ui/img/calendar.gif'/>',   
    buttonImageOnly: true,
    dateFormat: 'yymmdd'});
} );
$.datepicker.setDefaults( $.datepicker.regional[ "ko" ] );

 $(document).ready(function(){ 
	  
	 $('img.ui-datepicker-trigger').attr('style', 'margin:0px 7px 0px 7px');
	 
	// 검색
	 $("#goSearch").bind("click",function() {
		 $("#excelDownload").val("");
		 formSubmit();
	 });
	 
	 // 엑셀 다운로드
	 $("#goExcel").bind("click",function() {
		 formSubmit("goExcel");
	 });
	 
	 function formSubmit(goExcelString){
		 
		 if( goExcelString == "goExcel"){
			 $("#excelDownload").val("Y");
		 }
		 
			$("#startdate").val($("#datepicker1").val());
			$("#enddate").val($("#datepicker2").val());
			 
			 if($("#keywd").val() == "" && $("#status").val() != ""){
			 	alert("검색조건을 입력해주세요.");
				return;
			 }
			 if($("#startdate").val() == ""){
				 	alert("시작일을 입력해주세요.");
					return;
				 }
			 if($("#enddate").val() == ""){
			 	alert("종료일을 입력해주세요.");
				return;
			 }
			 
			 if($("#enddate").val() < $("#startdate").val()){
			 	alert("시작일이 종료일보다 빠를수 없습니다.");
				return;
			 }
				
		 $('#mainForm').attr('action','<c:url value="/manage/manageLog/manageLog.do" />').attr('method', 'post').submit();
	 }
	 
	 
	// 검색값 설정
    if('${searchBean.status}'!=''){
  		$("#status").val('${searchBean.status}').attr("selected", "selected");
    }
    if('${searchBean.statusUseDiv}'!=''){
  		$("#statusUseDiv").val('${searchBean.statusUseDiv}').attr("selected", "selected");
    }
    if('${searchBean.requester_code}'!=''){
  		$("#requester_code").val('${searchBean.requester_code}').attr("selected", "selected");
  	}
    $("#keywd").val('${searchBean.keywd}')
    $("#datepicker1").val('${searchBean.startdate}');
	$("#datepicker2").val('${searchBean.enddate}');
	
	if('${searchBean.statusUseDiv}'=='W'){
  		$("#tableDiv1").hide();
  		$("#tableDiv2").show();
  	}else{
  		$("#tableDiv1").show();
  		$("#tableDiv2").hide();
  	}
	
	if( '' == $("#datepicker1").val()){
		$("#datepicker1").val($.datepicker.formatDate('yymmdd', new Date()));
	}
	if( '' == $("#datepicker2").val()){
		$("#datepicker2").val($.datepicker.formatDate('yymmdd', new Date()));
	}
	
	
}); 

</script>

	<div class="top_title"><h2> 간편조회 </h2></div>
    <!-- 검색조건 -->
    <form id="mainForm" name="mainForm" >
    <table width="100%" class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="100">
            <col width="">
       
        </colgroup>
        <tr>
        	<th>검색조건</th>
        	<td>
	        	<select class="form_sel_style1" name="status" id="status">
            		<option value="" selected>전체</option>
            		<option value="user_ci" >user_ci</option>
            		<option value="cust_id" >cust_id</option>
                	<option value="ga_id" >ga_id</option>
                	<option value="ctn" >ctn</option>
            	</select>
            	<input class="form_input_style2" id="keywd" name="keywd" style="width:200px" value="${searchBean.keywd}">
            </td>
        </tr>
		<tr>
       	<th>검색기간</th>
       	<td>
           	<input name="datepicker1"  type="text"  id="datepicker1"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
			~ <input name="datepicker2" type="text" id="datepicker2"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">		            
           </td>
       </tr>
      <tr>
        	<th>검색구분</th>
        	<td>
	        	매체 : <select class="form_sel_style1" name="requester_code" id="requester_code" >
	            	<option value="">전체</option>
	             		<c:forEach items="${requesterNameList}" var="requesterNameList" varStatus="status">
			 				<option value="${requesterNameList.requester_code}" >${requesterNameList.requester_name}</option>
				 		</c:forEach>
				 </select>
				 
				 
				 
				 사용구분 :  <select class="form_sel_style1" name="statusUseDiv" id="statusUseDiv" >
	            	<option value="">전체</option>
	             	<option value="I" >적립</option>
	             	<option value="O" >사용</option>
	             	<option value="W" >임시</option>
	             </select>
            </td>
        </tr>
    </table>
    <div id="btn_area">
    <input type="button" id="goSearch" name="goSearch" class="btn_area_a" value="검색" >
    <input type="button" id="goExcel" name="goExcel" class="btn_area_a" value="엑셀 다운로드" >
    </div>
    <input type="hidden" id="startdate" name="startdate" value="" >
    <input type="hidden" id="enddate" name="enddate" value="" >
    <input type="hidden" id="excelDownload" name="excelDownload" value="" >
    <input type="hidden" id="pg" name="pg" value="" >
    
    </form>
    
    
   <br>
   <br>
   <p>조회 목록    전체: 총   ${totalCnt}건</p>
   <br>
   
   
   
   <div id="tableDiv1">
   <table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
    		<col width="">
            <col width="">
            <col width="120px">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="300px">
        </colgroup>
        <tr>
        	<th>날짜</th>
            <th>시간</th>
            <th>매체</th>
            <th>사용구분</th>
            <th>포인트</th>
            <th>잔액</th>
            <th>전환</th>
            <th>TR_ID</th>
        </tr>
        
        <c:forEach var="list" items="${infolist}">
        <tr>
            <td align="center">${list.reg_date}</td>
            <td align="center">${list.reg_time}</td>
            <td align="center">${list.requester_code}</td>
            <td align="center"><c:choose>
								 <c:when test="${list.point_type=='I'}">적립</c:when>
								 <c:when test="${list.point_type=='O'}">사용</c:when>
								</c:choose></td>
            <td align="right"><fmt:formatNumber value="${list.point_value}" pattern="#,###" /></td>
            <td align="right"><fmt:formatNumber value="${list.balance}" pattern="#,###" /></td>
            <td align="center">${list.status}</td>
            <td align="right">${list.transaction_id}</td>
        </tr>
            
        </c:forEach>
        <c:if test="${fn:length(infolist) == 0}" >
        <tr>
        	<td colspan="8" > 조회값이 없습니다. </td>
        </tr>
        </c:if>
        
        
    </table>
    </div>
    
    <div id="tableDiv2">
    <table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
    		<col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
        </colgroup>
        <tr>
        	<th>날짜</th>
            <th>시간</th>
            <th>매체</th>
            <th>사용구분</th>
            <th>포인트</th>
            <th>전환</th>
            <th>TR_ID</th>
        </tr>
        
        <c:forEach var="list" items="${histlist}">
        <tr>
            <td align="center">${list.reg_date}</td>
            <td align="center">${list.reg_time}</td>
            <td align="center">${list.requester_code}</td>
            <td align="center"><c:choose>
								 <c:when test="${list.point_type=='I'}">적립</c:when>
								 <c:when test="${list.point_type=='O'}">사용</c:when>
								</c:choose></td>
            <td align="center">${list.point_value}</td>
            <td align="center">${list.status}</td>
            <td align="right">${list.transaction_id}</td>
        </tr>
            
        </c:forEach>
        <c:if test="${fn:length(histlist) == 0}" >
        <tr>
        	<td colspan="7" > 조회값이 없습니다. </td>
        </tr>
        </c:if>
        
        
    </table>
    </div>
    
    
    
    
    
    <table width="100%"><tr height="45px"><td align="center">${boardPaging.pagingHTML}</td></tr></table>
    <script>
    //페이징
    function boardPaging(pg){
    	
    	 $("#pg").val(pg);
    	 $("#excelDownload").val("");
    	 
    	 $("#startdate").val($("#datepicker1").val());
			$("#enddate").val($("#datepicker2").val());
			 
			/* if($("#keywd").val() == ""){
			 	alert("검색조건을 입력해주세요.");
				return;
			 } */
			 if($("#startdate").val() == ""){
				 	alert("시작일을 입력해주세요.");
					return;
				 }
			 if($("#enddate").val() == ""){
			 	alert("종료일을 입력해주세요.");
				return;
			 }
			 
			 if($("#enddate").val() < $("#startdate").val()){
			 	alert("시작일이 종료일보다 빠를수 없습니다.");
				return;
			 }
				
		 $('#mainForm').attr('action','<c:url value="/manage/manageLog/manageLog.do" />').attr('method', 'post').submit();
	
		 
    	 
    }
    </script>
    
    
    