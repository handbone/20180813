<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>

<script>


$( function() {
    $( "#datepicker1, #datepicker2" ).datepicker({
    autoSize: true,
    showOn: 'button', 
    buttonImage: '<c:url value='/resources/css/ui/img/calendar.gif'/>',   
    buttonImageOnly: true,
    dateFormat: 'yymmdd'});
} );
$.datepicker.setDefaults( $.datepicker.regional[ "ko" ] );

$( function() {
    $( "#datepicker3, #datepicker4" ).datepicker({
	    autoSize: true,
	    showOn: 'button', 
	    buttonImage: '<c:url value='/resources/css/ui/img/calendar.gif'/>',   
	    buttonImageOnly: true,
	    dateFormat: 'yymm',
	    changeMonth: true,
	    changeYear: true,
	    showButtonPanel: true 
	    ,onClose: function(dateText, inst) {
	        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        $(this).val($.datepicker.formatDate('yymm', new Date(year, month, 1)));
	    }
	    , onChangeMonthYear: function( year, month, inst ){
	    	
	    	var month2 = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	        var year2 = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        $(this).val($.datepicker.formatDate('yymm', new Date(year2, month2, 1)));
	        
	        $("#ui-datepicker-div > .ui-datepicker-calendar").hide();
	    }
	});
    
        
} );


 $(document).ready(function(){ 
	 
	 $('img.ui-datepicker-trigger').attr('style', 'margin:0px 7px 0px 7px');
	 
	 $("#datepicker3, #datepicker4").next().click(function () {
			
			$("#ui-datepicker-div > .ui-datepicker-calendar").hide();
			
		    $("#ui-datepicker-div").position({
		        my: "center top",
		        at: "center bottom",
		        of: $(this)
		    });
		});
	 
	
	 // 라이오버튼 컨트롤
	 $("input[name=dateType]").change(function() {
		 $("#startdate").val("");
		 $("#enddate").val("");
		 /* $("#datepicker1").val("");
		 $("#datepicker2").val("");
		 $("#datepicker3").val("");
		 $("#datepicker4").val(""); */
		 if("day" == $(this).val() ){
			 $("#datepickerDiv1").show();
			 $("#datepickerDiv2").hide();
			 $("#tableDivDay").show();
			 $("#tableDivMonth").hide();
			 $("#tableDivMedia").hide();
			 $("#status").hide();
		 }else if("month" == $(this).val()){
			 $("#datepickerDiv1").hide();
			 $("#datepickerDiv2").show();
			 $("#tableDivDay").hide();
			 $("#tableDivMonth").show();
			 $("#tableDivMedia").hide();
			 $("#status").hide();
		 }else if("media" == $(this).val()){
			 $("#datepickerDiv1").show();
			 $("#datepickerDiv2").hide();
			 $("#tableDivDay").hide();
			 $("#tableDivMonth").hide();
			 $("#tableDivMedia").show();
			 $("#status").show();
		 }
	});
	 $("#datepickerDiv1").show();
	 $("#datepickerDiv2").hide();
	 $("#tableDivDay").show();
	 $("#tableDivMonth").hide();
	 $("#tableDivMedia").hide();
	 $("#status").hide();
	 
	 // 검색
	 $("#goSearch").bind("click",function() {
		 $("#excelDownload").val("");
		 formSubmit();
	 });
	 
	 // 엑셀 다운로드
	 $("#goExcel").bind("click",function() {
		 formSubmit("goExcel");
	 });
	 
	 function formSubmit(goExcelString){
		 
		 if( goExcelString == "goExcel"){
			 $("#excelDownload").val("Y");
		 }
		 
		 if("day" == $("input[name=dateType]:checked").val() ){
			 $("#startdate").val($("#datepicker1").val());
			 $("#enddate").val($("#datepicker2").val());
		 }else if("month" == $("input[name=dateType]:checked").val()){
			 $("#startdate").val($("#datepicker3").val());
			 $("#enddate").val($("#datepicker4").val());
		 }else if("media" == $("input[name=dateType]:checked").val()){
			 $("#startdate").val($("#datepicker1").val());
			 $("#enddate").val($("#datepicker2").val());
		 }
		 
		 if($("#startdate").val() == ""){
			 	alert("시작일을 입력해주세요.");
				return;
			 }
		 if($("#enddate").val() == ""){
		 	alert("종료일을 입력해주세요.");
			return;
		 }
		 
		 if($("#enddate").val() < $("#startdate").val()){
		 	alert("시작일이 종료일보다 빠를수 없습니다.");
			return;
		 }
			
	 $('#mainForm').attr('action','<c:url value="/manage/stat/statLog.do" />').attr('method', 'post').submit();
	 }
	 
	// 검색값 설정
    if('${searchBean.status}'!=''){
  		$("#status").val('${searchBean.status}').attr("selected", "selected");
    }
	
	if( '' != '${searchBean.dateType}'){
		$("input[name=dateType][value=" + "${searchBean.dateType}" + "]").attr("checked", true);
	}
    
 	// 검색값 설정
    if('day' == '${searchBean.dateType}'){
    	$("#datepicker1").val('${searchBean.startdate}');
    	$("#datepicker2").val('${searchBean.enddate}');
    	$("#datepickerDiv1").show();
		$("#datepickerDiv2").hide();
		$("#tableDivDay").show();
		$("#tableDivMonth").hide();
		$("#tableDivMedia").hide();
		$("#status").hide();
    }else if('month' == '${searchBean.dateType}'){
    	$("#datepicker3").val('${searchBean.startdate}');
    	$("#datepicker4").val('${searchBean.enddate}');
    	$("#datepickerDiv1").hide();
		$("#datepickerDiv2").show();
		$("#tableDivDay").hide();
		$("#tableDivMonth").show();
		$("#tableDivMedia").hide();
		$("#status").hide();
    }else if('media' == '${searchBean.dateType}'){
    	$("#datepicker1").val('${searchBean.startdate}');
    	$("#datepicker2").val('${searchBean.enddate}');
    	$("#datepickerDiv1").show();
		$("#datepickerDiv2").hide();
		$("#tableDivDay").hide();
		$("#tableDivMonth").hide();
		$("#tableDivMedia").show();
		$("#status").show();
    }
    
    	var today = new Date();
    	var date = new Date(today.getTime() - 1000*60*60*24);
    
    	if( '' == $("#datepicker1").val()){
    		$("#datepicker1").val($.datepicker.formatDate('yymmdd', date));
    	}
    	if( '' == $("#datepicker2").val()){
    		$("#datepicker2").val($.datepicker.formatDate('yymmdd', date));
    	}
    	if( '' == $("#datepicker3").val()){
    		$("#datepicker3").val($.datepicker.formatDate('yymm', new Date()));
    	}
    	if( '' == $("#datepicker4").val()){
    		$("#datepicker4").val($.datepicker.formatDate('yymm', new Date()));
    	}
    
}); 

</script>

	<div class="top_title"><h2>  통계관리</h2></div>
    <!-- 검색조건 -->
    <form id="mainForm" name="mainForm" >
    <table width="100%" class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="100">
            <col width="">
       
        </colgroup>
        <tr>
       	<th>검색조건</th>
           <td>
           <input id="dateType" name="dateType" value="day" checked="CHECKED" type="radio" /> 일별
           <input id="dateType" name="dateType" value="month" type="radio"/> 월별
           <input id="dateType" name="dateType" value="media" type="radio"/> 매체별
           <br>
           <br>
           <div id="datepickerDiv1">
           <select class="form_sel_style1" name="status" id="status" >
            	<option value="">전체</option>
             		<c:forEach items="${requesterNameList}" var="requesterNameList" varStatus="status">
		 				<option value="${requesterNameList.requester_code}" >${requesterNameList.requester_name}</option>
			 		</c:forEach>
			 </select>
           	<input name="datepicker1"  type="text"  id="datepicker1"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
			~ <input name="datepicker2" type="text" id="datepicker2"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">		            
           </div>
           <div id="datepickerDiv2">
			<input name="datepicker3"  type="text"  id="datepicker3"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">
			~ <input name="datepicker4" type="text" id="datepicker4"  value="" class="form_input_style2"  maxlength="12" readonly style="text-align:center" style="width:140px">		            
            </div>
           </td>
       </tr>
       <tr></tr>
    </table>
    <div id="btn_area">
    <input type="button" id="goSearch" name="goSearch" class="btn_area_a" value="검색" >
    <input type="button" id="goExcel" name="goExcel" class="btn_area_a" value="엑셀 다운로드" >
    </div>
    <input type="hidden" id="startdate" name="startdate" value="" >
    <input type="hidden" id="enddate" name="enddate" value="" >
    <input type="hidden" id="excelDownload" name="excelDownload" value="" >
    <input type="hidden" id="pg" name="pg" value="" >
    
    </form>
    
    
   <br>
   <p>조회 목록    전체: 총   ${totalCnt}건</p>
   <br>
   
   
   
   
   <div id="tableDivDay">
    <table class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="">
            <col width="">
            <col width="">
            <col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width=""> 
            <col width="">
            <col width="">
            <col width="">
            <col width="">
        </colgroup>
        <tr>
        	<th>날짜</th>
            <th>요일</th>
            <th>잔액</th>
            <th>사용자</th>
            <th>적립포인트</th>
            <th>적립횟수</th>
            <th>사용포인트</th>
            <th>사용횟수</th>
            <th>대기포인트</th>
            <th>대기횟수</th>
            <th>소멸포인트</th>
            <th>탈퇴자</th>
        </tr>
        
        <c:forEach var="list" items="${daylist}">
        <tr>
        	<td align="center">${list.sdate }</td>
            <td align="center"><c:choose>
								 <c:when test="${list.d_day==0}">sun</c:when>
								 <c:when test="${list.d_day==1}">mon</c:when>
								 <c:when test="${list.d_day==2}">tues</c:when>
								 <c:when test="${list.d_day==3}">wed</c:when>
								 <c:when test="${list.d_day==4}">thur</c:when>
								 <c:when test="${list.d_day==5}">fri</c:when>
								 <c:when test="${list.d_day==6}">sat</c:when>
								</c:choose>
			</td>
            <td align="right"><fmt:formatNumber value="${list.total_balance}" pattern="#,###" /></td>
            <td align="right"><fmt:formatNumber value="${list.active_user}" pattern="#,###" /></td>
            <td align="right"><fmt:formatNumber value="${list.input_point}" pattern="#,###" /></td>
            <td align="right"><fmt:formatNumber value="${list.input_count}" pattern="#,###" /></td>
            <td align="right"><fmt:formatNumber value="${list.output_point}" pattern="#,###" /></td>
            <td align="right"><fmt:formatNumber value="${list.output_count}" pattern="#,###" /></td>
            <td align="right"><fmt:formatNumber value="${list.pre_point}" pattern="#,###" /></td>
            <td align="right"><fmt:formatNumber value="${list.pre_count}" pattern="#,###" /></td>
            <td align="right"><fmt:formatNumber value="${list.remove_point}" pattern="#,###" /></td>
            <td align="right"><fmt:formatNumber value="${list.remove_count}" pattern="#,###" /></td>
        </tr>
        </c:forEach>
       
        <c:if test="${fn:length(daylist) == 0}" >
        <tr>
        	<td colspan="12" > 조회값이 없습니다. </td>
        </tr>
        </c:if>
    </table> 
    </div>
    
    <div id="tableDivMonth">
     <table class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="">
            <col width="">
            <col width="">
            <col width="">
			<col width="">
			<col width="">
			<col width="">
        </colgroup>
        <tr>
        	<th>기준월</th>
            <th>잔액</th>
            <th>사용자</th>
            <th>적립포인트</th>
            <th>적립횟수</th>
            <th>사용포인트</th>
            <th>사용횟수</th>
        </tr>
        
        <c:forEach var="list" items="${monthlist}">
        <tr>
        	<td align="center">${list.sdate }</td>
            <td align="right"><fmt:formatNumber value="${list.total_balance}" pattern="#,###" /></td>
            <td align="right"><fmt:formatNumber value="${list.active_user}" pattern="#,###" /></td>
            <td align="right"><fmt:formatNumber value="${list.input_point}" pattern="#,###" /></td>
            <td align="right"><fmt:formatNumber value="${list.input_count}" pattern="#,###" /></td>
            <td align="right"><fmt:formatNumber value="${list.output_point}" pattern="#,###" /></td>
            <td align="right"><fmt:formatNumber value="${list.output_count}" pattern="#,###" /></td>
        </tr>
        </c:forEach>
       
        <c:if test="${fn:length(monthlist) == 0}" >
        <tr>
        	<td colspan="7" > 조회값이 없습니다. </td>
        </tr>
        </c:if>
    </table>  
    </div>

	<div id="tableDivMedia">
  	<table class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="">
            <col width="">
            <col width="">
            <col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width=""> 
            <col width="">
        </colgroup>
        <tr>
        	<th>날짜</th>
            <th>매체</th>
            <th>유형</th>
            <th>요일</th>
            <th>적립포인트</th>
            <th>적립횟수</th>
            
        </tr>
        
        <c:forEach var="list" items="${medialist}">
        <tr>
        	<td align="center">${list.sdate }</td>
            <td align="center">${list.media }</td>
            <td align="center"><c:choose>
								 <c:when test="${list.point_type=='I'}">적립</c:when>
								 <c:when test="${list.point_type=='O'}">사용</c:when>
								 <c:when test="${list.point_type=='P'}">임시</c:when>
								</c:choose></td>
            <td align="center"><c:choose>
								 <c:when test="${list.d_day==0}">sun</c:when>
								 <c:when test="${list.d_day==1}">mon</c:when>
								 <c:when test="${list.d_day==2}">tues</c:when>
								 <c:when test="${list.d_day==3}">wed</c:when>
								 <c:when test="${list.d_day==4}">thur</c:when>
								 <c:when test="${list.d_day==5}">fri</c:when>
								 <c:when test="${list.d_day==6}">sat</c:when>
								</c:choose></td>
            <td align="right"><fmt:formatNumber value="${list.point}" pattern="#,###" /></td>
            <td align="right"><fmt:formatNumber value="${list.count}" pattern="#,###" /></td>
            
        </tr>
        </c:forEach>
       
        <c:if test="${fn:length(medialist) == 0}" >
        <tr>
        	<td colspan="9" > 조회값이 없습니다. </td>
        </tr>
        </c:if>
    </table> 
    </div> 
   
   
   
    <table width="100%"><tr height="45px"><td align="center">${boardPaging.pagingHTML}</td></tr></table>
    <script>
    //페이징
    function boardPaging(pg){
    	
    	 $("#pg").val(pg);
    	
    	 if("day" == $("input[name=dateType]:checked").val() ){
			 $("#startdate").val($("#datepicker1").val());
			 $("#enddate").val($("#datepicker2").val());
		 }else if("month" == $("input[name=dateType]:checked").val()){
			 $("#startdate").val($("#datepicker3").val());
			 $("#enddate").val($("#datepicker4").val());
		 }else if("media" == $("input[name=dateType]:checked").val()){
			 $("#startdate").val($("#datepicker1").val());
			 $("#enddate").val($("#datepicker2").val());
		 }
		 
		 if($("#startdate").val() == ""){
			 	alert("시작일을 입력해주세요.");
				return;
			 }
		 if($("#enddate").val() == ""){
		 	alert("종료일을 입력해주세요.");
			return;
		 }
		 
		 if($("#enddate").val() < $("#startdate").val()){
		 	alert("시작일이 종료일보다 빠를수 없습니다.");
			return;
		 }
			
	 $('#mainForm').attr('action','<c:url value="/manage/stat/statLog.do" />').attr('method', 'post').submit();
    }
    </script>
    