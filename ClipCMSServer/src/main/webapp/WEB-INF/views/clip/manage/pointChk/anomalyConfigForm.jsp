<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script src="<c:url value='/resources/js/ui/jquery-ui-1.12.0.min.js' />"></script>
<script src="<c:url value='/resources/js/ui/i18n/datepicker-ko.js' />"></script>

<script>


$( function() {
    $( "#datepicker1" ).datepicker({
    autoSize: true,
    showOn: 'button', 
    buttonImage: '<c:url value='/resources/css/ui/img/calendar.gif'/>',   
    buttonImageOnly: true,
    dateFormat: 'yymmdd'});
} );
$.datepicker.setDefaults( $.datepicker.regional[ "ko" ] );

 $(document).ready(function(){ 
	  
	 $('img.ui-datepicker-trigger').attr('style', 'margin:0px 7px 0px 7px');
	 
	 
	// 검색
	 $("#goSearch").bind("click",function() {
		 $("#excelDownload").val("");
		 formSubmit();
	 });
	 
	 // 엑셀 다운로드
	 $("#goExcel").bind("click",function() {
		 formSubmit("goExcel");
	 });
	 
	 function formSubmit(goExcelString){
		 
		 if( goExcelString == "goExcel"){
			 $("#excelDownload").val("Y");
		 }
		 
			$("#startdate").val($("#datepicker1").val());
			 
			 if($("#startdate").val() == ""){
				 	alert("시작일을 입력해주세요.");
					return;
				 }
				
		 $('#mainForm').attr('action','<c:url value="/manage/pointChk/anomalyList.do" />').attr('method', 'post').submit();
	 }
	 
	 $("#goLimitPointChange").bind("click",function() {
		 
		 formData = $("#mainForm").serialize();
		 $.ajax({
	            type: "POST",
	            url: '<c:url value="/manage/pointChk/goLimitPointChange.do" />',
	            data: formData,
	            dataType: "Json",
	            success: function (resData) {
	                if (resData.result == "success") {
	                	alert("수정 되었습니다.");
	                	location.reload();  		                	
	                } else {
	                	alert("수정되지 않았습니다!");
	                }

	            },
	            error: function (e) {
	            	alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
	            }
	        });
		
	 });
	 
	 $("#insertTblStatReceiverInfo").bind("click",function() {
		if($("#receiver_tel_no2").val() == ""){
		 	alert("핸드폰 번호를 입력해주세요.");
			return;
		}
		if($("#receiver_tel_no3").val() == ""){
			alert("핸드폰 번호를 입력해주세요.");
			return;
		}
		 $("#receiver_tel_no").val( $("#receiver_tel_no1").val()+"-"+$("#receiver_tel_no2").val()+"-"+$("#receiver_tel_no3").val());
		 
		 formData = $("#mainForm").serialize();

		   $.ajax({
		             type: "POST",
		             url: "<c:url value='/manage/pointChk/insertTblStatReceiverInfo.do' />",
		             data: formData,
		             dataType: "Json",
		             success: function (resData) {
		                 if (resData.result == "success") {
		                	 alert("등록되었습니다.");
		                	 location.reload(); 
			     		} else {
			                 alert("등록이 되지 않았습니다!");
		                }
		             },
		             error: function (e) {
		                 alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요");
		             }
		         });
	 
	 });
	 
 $(".table_style1 #goDelete").bind("click",function() {
		 
		 if(confirm('정말 삭제하시겠습니까?')) {
			 
			 $("#no").val($(this).parent().parent().find("td:eq(0) > input:eq(0)").val());
			 
			 formData = $("#mainForm").serialize();
			 $.ajax({
		            type: "POST",
		            url: '<c:url value="/manage/pointChk/anomalyConfigDelete.do" />',
		            data: formData,
		            dataType: "Json",
		            success: function (resData) {
		                console.log(resData.result);  
		                if (resData.result == "success") {
		                	alert("삭제 되었습니다.");
		                	location.reload();  		                	
		                } else {
		                	alert("삭제 된 행이 없습니다!");
		                }

		            },
		            error: function (e) {
		            	alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
		            }
		        });
		 }
	 });
 
	 
}); 

</script>

	<div class="top_title"><h2> 이상탐지 설정 </h2></div>
    <!-- 검색조건 -->
    <form id="mainForm" name="mainForm" >
    <table width="100%" class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="100">
            <col width="">
       
        </colgroup>
        <tr>
        	<th>탐지 기준</th>
            <td>
            	<input class="form_input_style2" id="limitPoint" name="limitPoint" style="width:200px" value="${limitPoint}">
            	이상 적립 또는 차감 시 알림 메시지 발송
            </td>
        </tr>
    </table>
    <div id="btn_area">
    <input type="button" id="goLimitPointChange" name="goLimitPointChange" class="btn_area_a" value="적용" >
    </div>
    
    
   <br>
    <br>
    
    <table width="100%" class="table_style1" cellpadding="0" cellspacing="0">
    	<colgroup>
        	<col width="100">
            <col width="">
       
        </colgroup>
        <tr>
        	<th>수신자 번호</th>
            <td>
            	<select name="receiver_tel_no1" id="receiver_tel_no1">
	 				<option value="010" selected>010</option>
	 				<option value="011" >011</option>
	 				<option value="016" >016</option>
	 				<option value="017" >017</option>
	 				<option value="018" >018</option>
	 				<option value="019" >019</option>
			 	</select>
			 	-
			 	<input style="width:50px;" type="text" id="receiver_tel_no2" name="receiver_tel_no2" maxlength="4"  >
			 	-
			 	<input style="width:50px;" type="text" id="receiver_tel_no3" name="receiver_tel_no3" maxlength="4"  >
			 	

            </td>
        </tr>
        <tr>
        	<th>수신자 정보</th>
            <td>
           	 	<input class="form_input_style2" id="user_info" name="user_info" maxlength="80" style="width:600px">
            </td>
        </tr>
        
    </table>
    <div id="btn_area">
    <input type="button" id="insertTblStatReceiverInfo" name="insertTblStatReceiverInfo" class="btn_area_a" value="추가" >
    </div>
    <input type="hidden" id="no" name="no" value="" >
    <input type="hidden" id="receiver_tel_no" name="receiver_tel_no" value="" >
    </form>
    
    
   <br>
   <br> 
   <br>
   
   <table class="table_style1" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
    	<colgroup>
    		<col width="100px">
            <col width="160px">
	       <col width="">
	       <col width="100px">
        </colgroup>
        <tr>
        	<th>no</th>
        	<th>수신자 번호</th>
        	<th>담당자 정보</th>
        	<th>수신자 번호</th>
        </tr>
        
        <c:forEach var="list" items="${list}">
        <tr>
            <td align="center">${list.no}<input type="hidden" value="${list.no}"  ></td>
            <td align="center">${list.receiver_tel_no}</td>
            <td align="left">${list.user_info}</td>
            <td align="center">
            	<input type="button" id="goDelete" name="goDelete" class="btn_area_a" value="삭제" >
			</td>
        </tr>
        </c:forEach>
        <c:if test="${fn:length(list) == 0}" >
        <tr>
        	<td colspan="6" > 조회값이 없습니다. </td>
        </tr>
        </c:if>
    </table>
    