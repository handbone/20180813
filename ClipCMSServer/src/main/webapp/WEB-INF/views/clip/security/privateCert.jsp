<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<!--
/**********************************************************************
 프로그램 ID	: privateCert.jsp
 프로그램 명	    : 개인정보 취급동의  
 설명			: 개인정보 취급동의   
 작성자		: 김명운 
 작성일자		: 2016.04.21
 변경이력		: 2016.04.21  최초생성
***********************************************************************/
-->   
<script lang="javascript" src="<c:url value='/resources/js/jquery.syblockUI.js'/>"></script>  
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/resources/js/date.format.min.js"></script>
<script type="text/javascript">
$( document ).ready(function() {
	var date = new Date();
	var weekarr = new Array('일', '월', '화', '수', '목', '금', '토');
	var certdate = date.getFullYear() + '년 ' + (date.getMonth() + 1) + '월 ' + date.getDate() + '일&nbsp;';
	$(".certdate").html(certdate);
	
});

</script>
<style>
.table_style1 td {
    padding: 5px 5px;
}

</style>	
	<div class="top_title"><h2>개인정보 취급동의</h2></div>
	 <span class="view_explain">개인정보 취급동의 인내문</span>
    <br><br>
	<form method="post" name="myform"  action="<c:url value='/privateCertOk.do' />" >
	    <table width="100%" class="table_style10" cellpadding="0" cellspacing="0">
	    	<colgroup>
	        	<col width="200">
	            <col width="">
	        </colgroup>
	        <tr height="30px">
	        	<th colspan="2" align="center"></th>
	          
	        </tr>
	        <tr height="400px">
	        	<th>개인정보 수집ㆍ이용동의</th>
	            <td>
	           <br>
	            [필수] 개인정보 수집/이용 동의<br><br>
				회사는 기업용 휴대전화 통화연결음 서비스인 비즈링 서비스 제공을 위하여 아래와 같이 개인정보를 수집, 이용합니다.<br><br>
				 <table style="line-height:20px">
				 <tr>
				 	<th>목적</th> <th>항목</th> <th>보유기간</th>
				 </tr>
				 <tr>
				 	<td  style="width:80px">비즈링 서비스 가입처리 및 서비스 제공</td>
				 	<td rowspan="2" style="width:150px">이름, 휴대전화번호, 통신사, 식별번호</td>
				 	<td rowspan="2">- 서비스 가입기간 (가입일 ~ 해지일)<br>
						- 요금정산, 요금 과오납 등 분쟁 대비를 위해 해지 또는 요금 완납 후 6개월<br>
						- 요금관련 분쟁 등 서비스 이용과 관련된 민원/분쟁이 계속될 경우에는 민원/분쟁 해결 시까지 보유(단, 관계법령에 특별한 규정이 있을 경우 관련 법령에 따라 보관)
					</td>
				 </tr>
				 <tr>
				 	<td >비즈링 서비스 이용 관련 문의 및 민원 대응	</td>
				 </tr>
						
					
				</table>
				<span  style="line-height:20px">
				※ 식별번호는 본인 여부를 확인할 수 있는 번호로서 개인인 경우 주민등록번호 앞 6자리이며,<br>
				 &nbsp;&nbsp;&nbsp;&nbsp;법인인 경우에는 법인등록번호를 말합니다. 단, KT는 사업자 등록번호입니다.<br>
				※ 비즈링 서비스 제공을 위해서 필요한 최소한의 개인정보이므로 동의를 해주셔야 서비스를 이용하실 수 있습니다.<br>
				</span>
	            <br><br><br>
	            <span class="certdate" style="margin-left:280px;font-weight:bold"></span>
	            <br><br>
	            </td>
	        </tr>
	         <tr height="25px">
	        	
	            <td colspan="2"><input type="checkbox" name="chk">동의합니다.</td>
	        </tr>
	        
	    </table>
	    <input name="agentUserCode" type="hidden" value="${agentUserCode }">  
    </form>
    <br>
    <div id="btn_area">
    	    <a href="#" class="btn_area_a" onclick="send_reg()">확인</a>
    </div>
    
<script>
function send_reg(){
	
	if(!myform.chk.checked){
		alert("[동의합니다] 체크해주세요.");
		return;
	}
	document.myform.submit();
	
	
	
}

</script>    
    