<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    
<!--
/**********************************************************************
 프로그램 ID	: comsSoundBatchFormAction.jsp
 프로그램 명	    : 비즈링 예약가입  - 예약가입
 설명			: 예약가입 등록
 작성자		: 김명운 
 작성일자		: 2016.05.10
 변경이력		: 2016.05.10  최초생성
***********************************************************************/
-->
<script lang="javascript" src="<c:url value='/resources/js/jquery.syblockUI.js'/>"></script>   
<div class="top_title"><h2>기업음원 일괄 설정</h2></div>
<form method="post" name="myform" action="<c:url value='/comsoundbatch/comSoundBatchFormAction.do' />" >
<table width="100%" class="table_style8" cellpadding="0" cellspacing="0" dwcopytype="CopyTableCell">
   	<colgroup>
       	<col width="200">
           <col width="">
       </colgroup>
       <tr>
       	<th>영업대행사명</th>
           <td>
           <select name="code" onChange="selectValue()">
				<option value="" selected> ---------- 선 택 ---------- </option>
				<c:if test="${fn:length(agi) > 0}">
					<c:forEach var="a" items="${agi }" varStatus="status">
						 <!--state msg 셋팅-->
					    <c:choose>
					   	<c:when test="${a.state eq '1'}">
					   		<c:set value="승인" var="stateMsg"/>
					   	</c:when>
					   	<c:when test="${a.state eq '0'}">
					   		<c:set value="미승인" var="stateMsg"/>
					   	</c:when>
					    </c:choose>
						<option value="${a.code }" ${a.code == agent_code ? 'selected="selected"' : '' }> ${a.coname }- ${a.businessno } - ${stateMsg }</option>
					</c:forEach>
				</c:if>
				<c:if test="${fn:length(agi) == 0}">
					<option>영업대행사없음</option>
				</c:if>
			</select>
           </td>
       </tr>
       <tr>
       	<th>기업명 (법인명, 단체명, 상호)</th>
           <td>
	          	<select name="coinfo"  onChange="selectValue()" > 
				<option  value="" selected> ---------- 선 택 ---------- </option>
				<c:if test="${fn:length(ci) > 0}">
					<c:forEach var="a" items="${ci }" varStatus="status">
					    <!--state msg 셋팅-->
					    <c:choose>
					   	<c:when test="${a.state eq '1'}">
					   		<c:set value="승인" var="stateMsg"/>
					   	</c:when>
					   	<c:when test="${a.state eq '0'}">
					   		<c:set value="미승인" var="stateMsg"/>
					   	</c:when>
					    </c:choose>
							<option value="${a.code }" ${a.code == co_code ? 'selected="selected"' : '' }> ${a.coname } -  ${a.businessno } - ${stateMsg }</option>
					</c:forEach>
				</c:if>
				<c:if test="${fn:length(ci)== 0}">
					<option>기업정보없음</option>
				</c:if>
				</select>
				  <c:if test="${AgentCode ne '' and AgentCode ne null }">
				  	<input type="text" name="search" size="15" maxlength="32">
				  	<input type="button" value="검색" onclick="pop_search('${agent_code}')">
				  </c:if>
           </td>
       </tr>
       <tr>
       	<th>기본음원</th>
           <td>
         	  <select name="xring" id="xring">
				<option value ='' selected>::: 링음원 :::</option>
				<c:if test="${fn:length(reservedXringNotiList) != 0}">
					<c:forEach var="reservedXringNotiDto" items="${reservedXringNotiList }" varStatus="status">
						<option value="${reservedXringNotiDto.xringcode}" ${reservedViewDto.xringcode == reservedXringNotiDto.xringcode?'selected':''}>${reservedXringNotiDto.title}</option>
					</c:forEach>
				</c:if>
				<c:if test="${fn:length(reservedXringNotiList) == 0}">
					<option VALUE="">음원없음</option>
				</c:if>
			  </select>&nbsp;&nbsp;|&nbsp;
			  
			  <select name="Week1" id="Week1" style="width:80px">
				<option value =''>요일선택</option>
				<option value="1">월</option>
				<option value="2">화</option>
				<option value="3">수</option>
				<option value="4">목</option>
				<option value="5">금</option>
				<option value="6">토</option>
				<option value="7">일</option>
			  </select> ~ 
			  <select name="Week2" id="Week2" style="width:80px">
				<option value =''>요일선택</option>
				<option value="1">월</option>
				<option value="2">화</option>
				<option value="3">수</option>
				<option value="4">목</option>
				<option value="5">금</option>
				<option value="6">토</option>
				<option value="7">일</option>
			 </select>
			 <input type="button" value="기본" onclick="defaultWeek()"/>&nbsp;&nbsp;|&nbsp;
			 <select id="time1" name="time1" style="width:80px;text-align:center"><option value =''>시간선택</option></select>~<select id="time2" name="time2" style="width:80px;text-align:center"><option value =''>시간선택</option></select>
			 <input type="button" value="기본" onclick="defaultTime()"/> 
           </td>
       </tr>
   
    
</table>
</form>
<div id="btn_area">
	
 	<a href="#" onclick="javascript:history.go(-1)" class="btn_area_a">취소</a>
 	<a href="#" onclick="sendit('${sessionScope.AgentLevel}')" id="set" class="btn_area_a" >설정</a>
 	<a href="#" onclick="location.reload(true);" class="btn_area_a" id="reset" style="display:none">초기화</a>
</div>   
<br>

<!-- loading img --> 
<div id="throbber" style="display:none;">
	<img src="<c:url value='/resources/img/loader.gif'/>" style="width:100px;height:100px"/>
</div> 
<!--결과조회 -->
<div id="result" style="display:none"></div>
<div id="resultArea" ></div>

<script>


var d = new Date();
$( document ).ready(function() {
	//기본음원 요일 시간 콤보셋팅
	tzonSet();

});


function selectValue(){
	location.href="<c:url value='/comsoundbatch/comSoundBatchFormAction.do'/>?code="+myform.code.value.trim()+"&coinfo="+myform.coinfo.value.trim();
}

function pop_search(st_ag){
	var keywd = document.myform.search.value.trim();
   /*  if (keywd == ""){
	    alert("검색어를 입력하세요."+document.InputFile.search.value);
	    document.InputFile.search.focus();
	    return;
    } */
	popSetStr = "width=500,height=300,top=300,left=600,copyhistory=yes,resizable=true,status=yes,scrollbars=yes";
    window.open("<c:url value='/common/search/searchCoListPop2.do?agent_code="+st_ag+"&search="+keywd+"'/>","Pop",popSetStr);
}
//*******기본음원 요일 시간 셋팅*****
function tzonSet(){
	var optionStr1 ="";
	var optionStr2 ="";
	//시작시간
	for(var i=0;i<=23;i++){
		if(i<10) i='0'+i;
			optionStr1 += "<option value='"+i+"'>"+i+":00</option>";
	}
	//종료시간
	for(var i=0;i<=23;i++){
		if(i<10) i='0'+i;
			optionStr2 += "<option value='"+i+"'>"+i+":00</option>";
	}

	$("#time1").append(optionStr1);
	$("#time2").append(optionStr2);
}

//기본요일 버튼 이벤트
function defaultWeek(){
	$("#Week1").val('1');
	$("#Week2").val('5');
}
//기본시간 버튼 이벤트
function defaultTime(){
	$("#time1").val('09');
	$("#time2").val('18');
}


//현재시간 구하기
function getTimeStamp() {
	  var d = new Date();
	  var s =
	    leadingZeros(d.getFullYear(), 4) + 
	    leadingZeros(d.getMonth() + 1, 2) +
	    leadingZeros(d.getDate(), 2) +
	    leadingZeros(d.getHours(), 2) +
	    leadingZeros(d.getMinutes(), 2) +
	    leadingZeros(d.getSeconds(), 2);

	  return s;
}
function leadingZeros(n, digits) {
	  var zero = '';
	  n = n.toString();

	  if (n.length < digits) {
	    for (i = 0; i < digits - n.length; i++)
	      zero += '0';
	  }
	  return zero + n;
}
	
//등록 submit	
function sendit(level){

	var agentinfo = document.myform.code.value;		
	var coinfo    = document.myform.coinfo.value;
	var xring    = document.myform.xring.value;
	var cTime =getTimeStamp();
	
	// cplevel 에 따라서 콤보박스의 유무가 결정된다.
	if(level != 3){	
		if (CheckStr(agentinfo, " ", "") == 0){
			alert("영업대행사을 선택해 주세요.");
			agentinfo = "";
			document.myform.agent_code.focus();
			return;
		}
	}

	if (CheckStr(coinfo, " ", "") == 0){
		alert("기업명을 선택해 주세요.");
		coinfo = "";
		document.myform.coinfo.focus();
		return;
	}

	if (CheckStr(xring, " ", "") == 0){
		alert("음원을 선택해 주세요.");
		coinfo = "";
		document.myform.xring.focus();
		return;
	}

	var Week1 = document.myform.Week1.value;
	var Week2 = document.myform.Week2.value;
	var time1 = document.myform.time1.value;
	var time2 = document.myform.time2.value;
	if (CheckStr(Week1, " ", "") == 0){
		alert("시작요일을 선택하세요");
		coinfo = "";
		document.myform.Week1.focus();
		return;
	}
	if (CheckStr(Week2, " ", "") == 0){
		alert("종료요일을 선택하세요");
		coinfo = "";
		document.myform.Week2.focus();
		return;
	}
	if (CheckStr(time1, " ", "") == 0){
		alert("시작시간을 선택하세요");
		coinfo = "";
		document.myform.time1.focus();
		return;
	}
	if (CheckStr(time2, " ", "") == 0){
		alert("종료시간을 선택하세요");
		coinfo = "";
		document.myform.time2.focus();
		return; 
	}
	//기본음원 시간 
	var tZone = document.myform.Week1.value + document.myform.Week2.value + document.myform.time1.value + document.myform.time2.value;
	//음원설정 실행
	setSoundCom(coinfo,xring,tZone,agentinfo,cTime);			
	
}
var totalFlag = true; 
var headFlag = true; //해드플래그
var eTotalCnt=0;     //총건수
var eSucCnt =0;      //성공건수
var eFailCnt =0;     //실패건수
var noCnt=0;
function setSoundCom(coinfo,xring,tZone,agentinfo,cTime){
	//테이블 헤더생성
	if(headFlag){
		$("#resultArea").append("<label style='font-weight:bold;line-height:30px'>음원설정결과 리스트<p style='display:inline-block;float:right'>&nbsp;&nbsp;&nbsp;총건수 :<span id='eTotalCount'></span> &nbsp;&nbsp;&nbsp;성공건수 :<span id='eSucCount'></span> &nbsp;&nbsp;&nbsp;실패건수 :<span id='eFailCount'></span></p></label>"
				  +"<table class='table_style5' cellpadding='0' cellspacing='0'>"
				  +"<colgroup>"
				  +" 	   <col width='200px'>"
				  +"       <col width='200'>"
				  +"       <col width=''>"
				  +" </colgroup>"
				  +" <tr id='resultDetail'>"
				  +" 	   <th align='center'>NO</th>"
				  +" 	   <th align='center'>전화번호</th>"
				  +" 	   <th align='center'>음원설정결과</th>"
				  +" </tr>"
				 
				  +"</table>"
			   );
		
	}
	console.log("<c:out value='${pageContext.request.contextPath}'/>/comsoundbatch/comSoundBatchFormActionOk.do?comcode="+coinfo.trim()+"&xringcode="+xring.trim()+"&tzone="+tZone.trim()+"&agentinfo="+agentinfo.trim()+"&ctime="+cTime.trim()+"&noCnt="+noCnt);
	headFlag =false;
	$.blockUI({message: $('#throbber'),css: {border : 0,backgroundColor: 'transparent',left:'740px',top:'270px',position:'absolute'},overlayCSS:  {backgroundColor: 'transparent'}});
	$("#result").load("<c:out value='${pageContext.request.contextPath}'/>/comsoundbatch/comSoundBatchFormActionOk.do?comcode="+coinfo.trim()+"&xringcode="+xring.trim()+"&tzone="+tZone.trim()+"&agentinfo="+agentinfo.trim()+"&ctime="+cTime.trim()+"&noCnt="+noCnt,function(result){
		
		var el = $(result).filter(".el");
		
		eTotalCnt = parseInt($(result).filter("#eTotalCnt").val()); //토탈카운트
		eSucCnt += parseInt($(result).filter("#eSucCnt").val());     //성공 카운트
		eFailCnt += parseInt($(result).filter("#eFailCnt").val());   //실패 카운트
		noCnt = parseInt($(result).filter("#noCnt").val());
		
		$("#resultDetail").after(el);
		
		
		if(totalFlag) { // 총카운트는 한번만 셋팅
			$("#eTotalCount").text(eTotalCnt);
			totalFlag=false;
		}
		$("#eSucCount").text(eSucCnt);
		$("#eFailCount").text(eFailCnt);
		
		$.unblockUI();
	
		//종료 플래그 이전까지 계속 반복 sendit()
		if($(result).filter("#endFlag").val()=='T'){
			//mip 요청에 딜레이 1000, 실제 한건씩 DB처리 하기때문에 1000은 너무 느리다..상용테스트 후 timeout 필요성 검토
			setTimeout(function() {
				setSoundCom(coinfo,xring,tZone,agentinfo,cTime);
				}, 300);
			
		}else{
			$("#set").css("display","none");
			$("#reset").css("display","inline-block");
		} 		
		
	});
	 
}

//CheckStr asp코드...
function CheckStr(strOriginal, strFind, strChange){
	var position, strOri_Length;
	position = strOriginal.indexOf(strFind);

	while (position != -1){
		strOriginal = strOriginal.replace(strFind, strChange);
		position = strOriginal.indexOf(strFind);
	}

	strOri_Length = strOriginal.length;
	return strOri_Length;
}
</script>