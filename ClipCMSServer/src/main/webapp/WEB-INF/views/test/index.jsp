<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- <c:if test="${sessionScope.AgentID eq null}">
    <%response.sendRedirect("/clip/signin.do?error=SESSION"); %>
</c:if>  --%>  

<!-- Top layout -->
<jsp:include page="head.jsp"/>

<!--Content layout -->
<jsp:include page="${display}"/>

<!-- Tail layout -->
<jsp:include page="tail.jsp"/>

