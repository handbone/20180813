-- Function: make_daily_balance2(text)

-- DROP FUNCTION make_daily_balance2(text);

CREATE OR REPLACE FUNCTION make_daily_balance2(IN curdate text)
  RETURNS TABLE(a_user bigint, t_balance numeric) AS
$BODY$
	BEGIN
	RETURN QUERY

		SELECT COUNT(point_info_idx), SUM(balance) FROM "POINT_INFO_TBL" 
		WHERE reg_date <= curDate
			AND point_info_idx IN (
						select MAX(point_info_idx) AS point_info_idx
						from "POINT_INFO_TBL" 
						WHERE reg_date <= curDate
						GROUP BY user_ci
						); 
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_balance2(text)
  OWNER TO point;

  
  
  -- Function: make_daily_input2(text, text)

-- DROP FUNCTION make_daily_input2(text, text);

CREATE OR REPLACE FUNCTION make_daily_input2(
    IN curdate text,
    IN pointmodifycode text)
  RETURNS TABLE(i_count bigint, i_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(*),sum(point_value) 	
		from 	"POINT_INFO_TBL"
		where 	reg_date = curDate
		and point_type = 'I'
		and reg_source != pointModifyCode;
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_input2(text, text)
  OWNER TO point;

  
  
  -- Function: make_daily_input_unique_ci2(text, text)

-- DROP FUNCTION make_daily_input_unique_ci2(text, text);

CREATE OR REPLACE FUNCTION make_daily_input_unique_ci2(
    IN curdate text,
    IN pointmodifycode text)
  RETURNS TABLE(i_unique_ci bigint) AS
$BODY$
	BEGIN
	RETURN QUERY

				select COUNT(user_ci) from (
				select user_ci
				from 	"POINT_INFO_TBL"
				where 	reg_date = curDate
				and point_type = 'I'
				and reg_source != pointModifyCode
				GROUP BY user_ci
				) A;
END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_input_unique_ci2(text, text)
  OWNER TO point;

  
  
  -- Function: make_daily_io_sum2(text, text)

-- DROP FUNCTION make_daily_io_sum2(text, text);

CREATE OR REPLACE FUNCTION make_daily_io_sum2(
    IN curdate text,
    IN pointmodifycode text)
  RETURNS TABLE(io_sum_count bigint) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(*)	
		from 	"POINT_INFO_TBL"
		where 	reg_date = curDate
		and reg_source != pointModifyCode;
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_io_sum2(text, text)
  OWNER TO point;

  
  
  -- Function: make_daily_io_sum_unique_ci2(text, text)

-- DROP FUNCTION make_daily_io_sum_unique_ci2(text, text);

CREATE OR REPLACE FUNCTION make_daily_io_sum_unique_ci2(
    IN curdate text,
    IN pointmodifycode text)
  RETURNS TABLE(io_sum_unique_ci bigint) AS
$BODY$
	BEGIN
	RETURN QUERY

				select COUNT(user_ci) from (
				select user_ci
				from 	"POINT_INFO_TBL"
				where 	reg_date = curDate
				and reg_source != pointModifyCode
				GROUP BY user_ci
				) A;
END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_io_sum_unique_ci2(text, text)
  OWNER TO point;

  
  
  -- Function: make_daily_modify2(text, text)

-- DROP FUNCTION make_daily_modify2(text, text);

CREATE OR REPLACE FUNCTION make_daily_modify2(
    IN curdate text,
    IN pointmodifycode text)
  RETURNS TABLE(m_count bigint, m_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select COUNT(*), sum(CASE WHEN point_type = 'I' THEN point_value WHEN point_type = 'O' THEN -point_value END ) 	
		from 	"POINT_INFO_TBL"
		where 	reg_date = curDate
		and reg_source = pointModifyCode;
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_modify2(text, text)
  OWNER TO point;

  
  
  -- Function: make_daily_modify_unique_ci2(text, text)

-- DROP FUNCTION make_daily_modify_unique_ci2(text, text);

CREATE OR REPLACE FUNCTION make_daily_modify_unique_ci2(
    IN curdate text,
    IN pointmodifycode text)
  RETURNS TABLE(m_unique_ci bigint) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(user_ci) from (
			select user_ci	
			from 	"POINT_INFO_TBL"
			where 	reg_date = curDate
			and reg_source = pointModifyCode
			group by user_ci
		) A;
			END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_modify_unique_ci2(text, text)
  OWNER TO point;

  
  
  -- Function: make_daily_output2(text, text)

-- DROP FUNCTION make_daily_output2(text, text);

CREATE OR REPLACE FUNCTION make_daily_output2(
    IN curdate text,
    IN pointmodifycode text)
  RETURNS TABLE(o_count bigint, o_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(*),sum(point_value) 	
		from 	"POINT_INFO_TBL"
		where 	reg_date = curDate
		and point_type = 'O'
		and reg_source != pointModifyCode;
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_output2(text, text)
  OWNER TO point;

  
  
  -- Function: make_daily_output_unique_ci2(text, text)

-- DROP FUNCTION make_daily_output_unique_ci2(text, text);

CREATE OR REPLACE FUNCTION make_daily_output_unique_ci2(
    IN curdate text,
    IN pointmodifycode text)
  RETURNS TABLE(o_unique_ci bigint) AS
$BODY$
	BEGIN
	RETURN QUERY

				select COUNT(user_ci) from (
				select user_ci
				from 	"POINT_INFO_TBL"
				where 	reg_date = curDate
				and point_type = 'O'
				and reg_source != pointModifyCode
				GROUP BY user_ci
				) A;
END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_output_unique_ci2(text, text)
  OWNER TO point;

  
  
  -- Function: make_daily_pre2(text)

-- DROP FUNCTION make_daily_pre2(text);

CREATE OR REPLACE FUNCTION make_daily_pre2(IN curdate text)
  RETURNS TABLE(p_count bigint, p_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(*),sum(point_value)
   		from "POINT_HIST_TBL"
    		where status = 'W'
			and reg_date = curDate ;
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_pre2(text)
  OWNER TO point;

  
  
  
  
  -- Function: make_daily_remove2(text)

-- DROP FUNCTION make_daily_remove2(text);

CREATE OR REPLACE FUNCTION make_daily_remove2(IN curdate text)
  RETURNS TABLE(r_user bigint, r_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		Select count(*),sum(balance) 
  		 from "POINT_INFO_TBL" 
   		where point_hist_idx in (
			select max(p.point_hist_idx) 
			from 	"POINT_INFO_TBL" p,
				"USER_INFO_TBL" u
			where
				p.user_ci = u.exit_ci 
				and u.status = 'D'
				and u.exit_date = curDate
			group by p.user_ci);
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_remove2(text)
  OWNER TO point;

  
  
-- Function: make_daily_remove_unique_ci2(text)

-- DROP FUNCTION make_daily_remove_unique_ci2(text);

CREATE OR REPLACE FUNCTION make_daily_remove_unique_ci2(IN curdate text)
  RETURNS TABLE(r_unique_ci bigint) AS
$BODY$
	BEGIN
	RETURN QUERY
		
		select COUNT(user_ci)
		from (
			select user_ci
			from 	"USER_INFO_TBL"
			where status = 'D'
			and exit_date = curDate
			group by user_ci
			) A;
		
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_remove_unique_ci2(text)
  OWNER TO point;

  
  
  
-- Function: make_daily_stat2(text, text)

-- DROP FUNCTION make_daily_stat2(text, text);

CREATE OR REPLACE FUNCTION make_daily_stat2(
    predate text,
    pointmodifycode text)
  RETURNS boolean AS
$BODY$
	DECLARE
		_balance2 record;
		_input2 record;
		_input_unique_ci2 record;
		_io_sum2 record;
		_io_sum_unique_ci2 record;
		_modify2 record;
		_modify_unique_ci2 record;
		_output2 record;
		_output_unique_ci2 record;
		_pre2 record;
		_remove2 record;
		_remove_unique_ci2 record;
		today date;
	BEGIN
		select to_date(predate,'YYYYMMDD') into today;
		select a_user,t_balance into _balance2 from make_daily_balance2(predate) ;
		--RAISE NOTICE 'result = %',_balance.a_user;
		--RAISE NOTICE 'result = %',_balance.t_balance;
		select i_count,i_point 	into _input2 		from make_daily_input2(predate, pointmodifycode) ;
		select i_unique_ci 	into _input_unique_ci2 	from make_daily_input_unique_ci2(predate, pointmodifycode) ;
		select io_sum_count 	into _io_sum2 		from make_daily_io_sum2(predate, pointmodifycode) ;
		select io_sum_unique_ci into _io_sum_unique_ci2 from make_daily_io_sum_unique_ci2(predate, pointmodifycode) ;
		select m_count,m_point	into _modify2 		from make_daily_modify2(predate, pointmodifycode) ;
		select m_unique_ci  	into _modify_unique_ci2	from make_daily_modify_unique_ci2(predate, pointmodifycode) ;
		select o_count,o_point 	into _output2 		from make_daily_output2(predate, pointmodifycode) ;
		select o_unique_ci	into _output_unique_ci2	from make_daily_output_unique_ci2(predate, pointmodifycode) ;
		select p_count,p_point	into _pre2		from make_daily_pre2(predate) ;
		select r_user,r_point	into _remove2		from make_daily_remove2(predate) ;
		select r_unique_ci	into _remove_unique_ci2	from make_daily_remove_unique_ci2(predate) ;




		insert into tbl_stat_daily_point2(
			year
			,month
			,day
			,d_day

			,total_balance
			,active_user

			,input_unique_ci
			,input_point
			,input_count

			,output_unique_ci
			,output_point
			,output_count

			,remove_unique_ci
			,remove_point
			,remove_count
			
			,pre_point
			,pre_count

			,modify_unique_ci
			,modify_point
			,modify_count
			
			,io_sum_count
			,io_sum_unique_ci


			)
		values (
			substr(predate,1,4)
			,substr(predate,5,2)
			,substr(predate,7,2)
			,date_part('dow', today)

			,_balance2.t_balance
			,_balance2.a_user

			,_input_unique_ci2.i_unique_ci
			,_input2.i_point
			,_input2.i_count

			,_output_unique_ci2.o_unique_ci
			,_output2.o_point
			,_output2.o_count

			,_remove_unique_ci2.r_unique_ci
			,_remove2.r_point
			,_remove2.r_user

			,_pre2.p_point
			,_pre2.p_count
			
			,_modify_unique_ci2.m_unique_ci
			,_modify2.m_point
			,_modify2.m_count
			
			,_io_sum2.io_sum_count
			,_io_sum_unique_ci2.io_sum_unique_ci
			



			);
		
		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION make_daily_stat2(text, text)
  OWNER TO point;

  
  
  
  
  -- Table: tbl_stat_daily_point2

-- DROP TABLE tbl_stat_daily_point2;

CREATE TABLE tbl_stat_daily_point2
(
  year character(4) NOT NULL,
  month character(2) NOT NULL,
  day character(2) NOT NULL,
  d_day character(1) NOT NULL, -- 0 : sun...
  total_balance bigint,
  active_user bigint,
  input_unique_ci bigint,
  input_point bigint,
  input_count bigint,
  output_unique_ci bigint,
  output_point bigint,
  output_count bigint,
  remove_unique_ci bigint,
  remove_point bigint,
  remove_count bigint,
  pre_point bigint,
  pre_count bigint,
  modify_unique_ci bigint,
  modify_point bigint,
  modify_count bigint,
  io_sum_count bigint,
  io_sum_unique_ci bigint,
  CONSTRAINT pk_daily_stat2 PRIMARY KEY (year, month, day, d_day)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tbl_stat_daily_point2
  OWNER TO point;
COMMENT ON COLUMN tbl_stat_daily_point2.d_day IS '0 : sun
1 : mon
2 : tues
3: wed
4 : thur
5 : fri
6 : sat';







  
select make_daily_stat2('20160915','pointUpdate');


SELECT year
	, month
	, day
	, d_day
	
	, input_unique_ci
	, input_point
    , input_count
    
    , output_unique_ci
    , output_point
    , output_count
    
    , modify_unique_ci
    , modify_point
    , modify_count
    
	, active_user
	, total_balance
	
    , pre_point
    , pre_count
    
    , remove_unique_ci
    , remove_point
    , remove_count
    
    , io_sum_count
    , io_sum_unique_ci
    
FROM tbl_stat_daily_point2
order by year||month||day ASC;









  
  