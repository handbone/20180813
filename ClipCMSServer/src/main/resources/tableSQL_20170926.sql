-- 20170926 테이블 추가 (메인배너 관리)
CREATE TABLE mainbanner_info_tbl
(
  mb_id character varying(16) NOT NULL,
  mb_name character varying(256) NOT NULL,
  link_url character varying(256) NOT NULL,
  start_date timestamp without time zone NOT NULL,
  end_date timestamp without time zone NOT NULL,
  reg_date timestamp without time zone NOT NULL,
  service_status character varying(1) NOT NULL,
  rank character varying(10) NOT NULL,
  mbimg_url character varying(256),
  description character varying(256) NOT NULL,
  CONSTRAINT "mainbanner_info_tbl_pkey" PRIMARY KEY (mb_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE mainbanner_info_tbl
  OWNER TO point;

CREATE INDEX idx_mainbanner_info_tbl_01
  ON mainbanner_info_tbl
  USING btree
  (mb_id, reg_date, rank);
  
-- 20170926 컬럼 추가 (쇼핑적립 관리)
ALTER TABLE link_price_item_info
  ADD link_url character varying(256);
  
ALTER TABLE link_price_item_info
  ADD rank character varying(10) NOT NULL;