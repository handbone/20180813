-- Table: "POINT_HIST_TBL_COUNT"

-- DROP TABLE "POINT_HIST_TBL_COUNT";

CREATE TABLE "POINT_HIST_TBL_COUNT"
(
  point_hist_count_idx serial NOT NULL,
  point_hist_idx bigint,
  user_ci character varying(256),
  cust_id character varying(256),
  ctn character varying(256),
  ga_id character varying(256),
  transaction_id character varying(256) NOT NULL,
  point_value bigint,
  description character varying(1024),
  status character(1),
  reg_source character varying(30) NOT NULL,
  reg_date character varying(8),
  reg_time character varying(6),
  chg_date character varying(8),
  chg_time character varying(6),
  point_type character(1),
  CONSTRAINT "PK_POINT_HIST_TBL_COUNT" PRIMARY KEY (point_hist_count_idx)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "POINT_HIST_TBL_COUNT"
  OWNER TO point;

-- Index: "IDX_POINT_HIST_TBL_COUNT_01"

-- DROP INDEX "IDX_POINT_HIST_TBL_COUNT_01";

CREATE INDEX "IDX_POINT_HIST_TBL_COUNT_01"
  ON "POINT_HIST_TBL_COUNT"
  USING btree
  (user_ci COLLATE pg_catalog."default", cust_id COLLATE pg_catalog."default", ctn COLLATE pg_catalog."default", ga_id COLLATE pg_catalog."default", status COLLATE pg_catalog."default");


  
  
  
  
  
  ALTER TABLE "POINT_HIST_TBL"
ADD  COLUMN  acl_limit_yn character varying(1) 

ALTER TABLE "POINT_HIST_TBL"
ADD  COLUMN  acl_limit_cnt bigint
  
  
  
  
  
  
  
  