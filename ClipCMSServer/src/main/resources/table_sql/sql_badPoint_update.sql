-- Table: point.tbl_stat_anomaly_detection

-- DROP TABLE point.tbl_stat_anomaly_detection;

CREATE TABLE point.tbl_stat_anomaly_detection
(
  media character varying(255) NOT NULL,
  stat_date character(8) NOT NULL,
  stat_hour character(2) NOT NULL,
  limit_point_value numeric NOT NULL,
  limit_input_count integer NOT NULL,
  limit_output_count integer NOT NULL,
  input_point numeric NOT NULL,
  output_point numeric NOT NULL,
  CONSTRAINT pk_tbl_stat_anomaly_detection PRIMARY KEY (media, stat_date, stat_hour)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE point.tbl_stat_anomaly_detection
  OWNER TO point;

CREATE TABLE point.tbl_hourly_stat
(
  p_media character varying(255) NOT NULL,
  p_user_ci character varying(256) NOT NULL,
  p_point_type character(1) NOT NULL,
  p_point_value numeric NOT NULL,
  CONSTRAINT pk_tbl_hourly_stat PRIMARY KEY (p_media, p_user_ci, p_point_type)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE point.tbl_hourly_stat
  OWNER TO point;

CREATE OR REPLACE FUNCTION point.fn_make_anomaly_stat(curDate character, curTime character, limitPoint numeric)
RETURNS BOOLEAN AS $$
	DECLARE _limit_input_count integer;
	DECLARE _limit_output_count integer;
	DECLARE _input_point numeric;
	DECLARE _output_point numeric;
	DECLARE _reg_sources text[];
	DECLARE _i integer;

	BEGIN

		truncate table point.tbl_hourly_stat;

		insert into point.tbl_hourly_stat (p_media, p_user_ci, p_point_type, p_point_value)
		SELECT
			reg_source as p_media,
			user_ci as p_user_ci,
			point_type as p_point_type,
			SUM(point_value) as p_point_value
		FROM
			point."POINT_INFO_TBL"
		WHERE
			reg_date = curDate and
			SUBSTRING(reg_time, 1, 2) = curTime
		GROUP BY
			reg_source,
			user_ci,
			point_type;

		_reg_sources := array(SELECT requester_code FROM point."ACL_INFO_TBL" WHERE use_status = 'Y');

		FOR _i IN 1 .. array_upper(_reg_sources, 1) LOOP
			INSERT INTO point.tbl_stat_anomaly_detection (media, stat_date, stat_hour, limit_point_value, limit_input_count, limit_output_count, input_point, output_point) VALUES (_reg_sources[_i], curDate, curTime, limitPoint, 0, 0, 0, 0);
			
			SELECT
				COALESCE(COUNT(p_user_ci),0) into _limit_input_count
			FROM
				point.tbl_hourly_stat
			WHERE
				p_media = _reg_sources[_i] and
				p_point_type = 'I' and
				p_point_value > limitPoint;

			SELECT
				COALESCE(COUNT(p_user_ci),0) into _limit_output_count
			FROM
				point.tbl_hourly_stat
			WHERE
				p_media = _reg_sources[_i] and
				p_point_type = 'O' and
				p_point_value > limitPoint;

			SELECT
				COALESCE(SUM(p_point_value),0) into _input_point
			FROM
				point.tbl_hourly_stat
			WHERE
				p_media = _reg_sources[_i] and
				p_point_type = 'I';
				
			SELECT
				COALESCE(SUM(p_point_value),0) into _output_point
			FROM
				point.tbl_hourly_stat
			WHERE
				p_media = _reg_sources[_i] and
				p_point_type = 'O';
				
			UPDATE
				point.tbl_stat_anomaly_detection
			SET
				limit_point_value = limitPoint,
				limit_input_count = _limit_input_count,
				limit_output_count = _limit_output_count,
				input_point = _input_point,
				output_point = _output_point
			WHERE
				media = _reg_sources[_i] and
				stat_date = curDate and
				stat_hour = curTime;
				
		END LOOP;
		
		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
	$$
LANGUAGE PLPGSQL;

-- 시간 단위 실행
select point.fn_make_anomaly_stat('일자', '시간', 탐지기준);
select point.fn_make_anomaly_stat('20160831', '14', 3);
-- 조회
select media, stat_date, stat_hour, limit_point_value, limit_input_count, limit_output_count, input_point, output_point from point.tbl_stat_anomaly_detection where media = '' and stat_date = '';
