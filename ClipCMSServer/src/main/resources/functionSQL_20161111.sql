-- Function: fn_make_anomaly_stat(character, character, numeric)

-- DROP FUNCTION fn_make_anomaly_stat(character, character, numeric);

CREATE OR REPLACE FUNCTION fn_make_anomaly_stat(
    curdate character,
    curtime character,
    limitpoint numeric)
  RETURNS boolean AS
$BODY$
	DECLARE _limit_input_count integer;
	DECLARE _limit_output_count integer;
	DECLARE _input_point numeric;
	DECLARE _output_point numeric;
	DECLARE _reg_sources text[];
	DECLARE _i integer;

	BEGIN

		truncate table tbl_hourly_stat;

		insert into tbl_hourly_stat (p_media, p_user_ci, p_point_type, p_point_value)
		SELECT
			reg_source as p_media,
			user_ci as p_user_ci,
			point_type as p_point_type,
			SUM(point_value) as p_point_value
		FROM
			"POINT_INFO_TBL"
		WHERE
			reg_date = curDate and
			SUBSTRING(reg_time, 1, 2) = curTime
		GROUP BY
			reg_source,
			user_ci,
			point_type;

		_reg_sources := array(SELECT requester_code FROM "ACL_INFO_TBL" WHERE use_status = 'Y');

		FOR _i IN 1 .. array_upper(_reg_sources, 1) LOOP
			INSERT INTO tbl_stat_anomaly_detection (media, stat_date, stat_hour, limit_point_value, limit_input_count, limit_output_count, input_point, output_point) VALUES (_reg_sources[_i], curDate, curTime, limitPoint, 0, 0, 0, 0);
			
			SELECT
				COALESCE(COUNT(p_user_ci),0) into _limit_input_count
			FROM
				tbl_hourly_stat
			WHERE
				p_media = _reg_sources[_i] and
				p_point_type = 'I' and
				p_point_value >= limitPoint;

			SELECT
				COALESCE(COUNT(p_user_ci),0) into _limit_output_count
			FROM
				tbl_hourly_stat
			WHERE
				p_media = _reg_sources[_i] and
				p_point_type = 'O' and
				p_point_value >= limitPoint;

			SELECT
				COALESCE(SUM(p_point_value),0) into _input_point
			FROM
				tbl_hourly_stat
			WHERE
				p_media = _reg_sources[_i] and
				p_point_type = 'I';
				
			SELECT
				COALESCE(SUM(p_point_value),0) into _output_point
			FROM
				tbl_hourly_stat
			WHERE
				p_media = _reg_sources[_i] and
				p_point_type = 'O';
				
			UPDATE
				tbl_stat_anomaly_detection
			SET
				limit_point_value = limitPoint,
				limit_input_count = _limit_input_count,
				limit_output_count = _limit_output_count,
				input_point = _input_point,
				output_point = _output_point
			WHERE
				media = _reg_sources[_i] and
				stat_date = curDate and
				stat_hour = curTime;
				
		END LOOP;
		
		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_make_anomaly_stat(character, character, numeric)
  OWNER TO point;

  ========================================================================================================
  
  
  -- Function: fn_make_anomaly_stat2(character, character, numeric)

-- DROP FUNCTION fn_make_anomaly_stat2(character, character, numeric);

CREATE OR REPLACE FUNCTION fn_make_anomaly_stat2(
    curdate character,
    curtime character,
    limitpoint numeric)
  RETURNS boolean AS
$BODY$


	BEGIN
		-- 利赋贸府
		INSERT INTO tbl_stat_anomaly_detection_2 ( stat_date, stat_hour, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
		SELECT
			curdate,
			curtime,
			'I' as point_type, 
			(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
			a.point,
			ui.user_ci, 
			ui.cust_id, 
			ui.ctn, 
			ui.ga_id 
		FROM
			(


				SELECT
					user_ci,
					SUM(point_value) as point
				FROM
					"POINT_INFO_TBL"
				WHERE
					reg_date = curdate and
					SUBSTRING(reg_time, 1, 2) = curtime and
					point_type = 'I' 
				GROUP BY
					user_ci,
					point_type

			) as a,
			"USER_INFO_TBL" as ui
		WHERE
			a.user_ci = ui.user_ci
			AND point >= limitpoint;

		-- 瞒皑贸府
		INSERT INTO tbl_stat_anomaly_detection_2 ( stat_date, stat_hour, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
		SELECT
			curdate,
			curtime,
			'O' as point_type, 
			(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
			a.point,
			ui.user_ci, 
			ui.cust_id, 
			ui.ctn, 
			ui.ga_id 
		FROM
			(


				SELECT
					user_ci,
					SUM(point_value) as point
				FROM
					"POINT_INFO_TBL"
				WHERE
					reg_date = curdate and
					SUBSTRING(reg_time, 1, 2) = curtime and
					point_type = 'O' 
				GROUP BY
					user_ci,
					point_type

			) as a,
			"USER_INFO_TBL" as ui
		WHERE
			a.user_ci = ui.user_ci
			AND point >= limitpoint;

		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_make_anomaly_stat2(character, character, numeric)
  OWNER TO point;

  
  ==================================================================================================
  
  
  -- Function: fn_make_top_stat(character, integer)

-- DROP FUNCTION fn_make_top_stat(character, integer);

CREATE OR REPLACE FUNCTION fn_make_top_stat(
    curdate character,
    limitcount integer)
  RETURNS boolean AS
$BODY$
	BEGIN
		-- 利赋贸府
		INSERT INTO tbl_stat_top_user (media, stat_date, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
		SELECT
			'unUsed' as media,
			curDate, 
			'I' as point_type, 
			(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
			a.point,
			ui.user_ci, 
			ui.cust_id, 
			ui.ctn, 
			ui.ga_id 
		FROM
			(
				SELECT
					user_ci, 
					SUM(point_value) as point 
				FROM
					"POINT_INFO_TBL"
				WHERE
					reg_date = curDate and 
					point_type = 'I' 
				GROUP BY user_ci 
				ORDER BY point DESC
				LIMIT limitCount
			) as a,
			"USER_INFO_TBL" as ui
		WHERE
			a.user_ci = ui.user_ci;

		-- 瞒皑贸府
		INSERT INTO tbl_stat_top_user (media, stat_date, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
		SELECT
			'unUsed' as media,
			curDate, 
			'O' as point_type, 
			(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
			a.point,
			ui.user_ci, 
			ui.cust_id, 
			ui.ctn, 
			ui.ga_id 
		FROM
			(
				SELECT
					user_ci, 
					SUM(point_value) as point 
				FROM
					"POINT_INFO_TBL"
				WHERE
					reg_date = curDate and 
					point_type = 'O' 
				GROUP BY user_ci 
				ORDER BY point DESC
				LIMIT limitCount
			) as a,
			"USER_INFO_TBL" as ui
		WHERE
			a.user_ci = ui.user_ci;

		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_make_top_stat(character, integer)
  OWNER TO point;

  
  
  ===================================================================================================
  
  
  -- Function: fn_make_top_stat2(character, integer)

-- DROP FUNCTION fn_make_top_stat2(character, integer);

CREATE OR REPLACE FUNCTION fn_make_top_stat2(
    curdate character,
    limitcount integer)
  RETURNS boolean AS
$BODY$
	DECLARE _reg_sources text[];
	BEGIN

	
		_reg_sources := array(SELECT requester_code FROM "ACL_INFO_TBL" WHERE use_status = 'Y');


		FOR _i IN 1 .. array_upper(_reg_sources, 1) LOOP

			
			-- 利赋贸府
			INSERT INTO tbl_stat_top_user (media, stat_date, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
			SELECT
				(select requester_name from "ACL_INFO_TBL" where requester_code = a.reg_source ) as media,
				curDate, 
				'I' as point_type, 
				(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
				a.point,
				ui.user_ci, 
				ui.cust_id, 
				ui.ctn, 
				ui.ga_id 
			FROM
				( 
					SELECT
						reg_source, 
						user_ci, 
						SUM(point_value) as point 
					FROM
						"POINT_INFO_TBL"
					WHERE
						reg_source = _reg_sources[_i] and
						reg_date = curDate and 
						point_type = 'I' 
					GROUP BY reg_source, user_ci 
					ORDER BY point DESC
					LIMIT limitCount
					
				) as a,
				"USER_INFO_TBL" as ui
			WHERE
				a.user_ci = ui.user_ci;

			-- 瞒皑贸府
			INSERT INTO tbl_stat_top_user (media, stat_date, point_type, rank, point, user_ci, cust_id, ctn, ga_id)
			SELECT
				(select requester_name from "ACL_INFO_TBL" where requester_code = a.reg_source ) as media,
				curDate, 
				'O' as point_type, 
				(ROW_NUMBER() OVER(ORDER BY a.point DESC)) as rank, 
				a.point,
				ui.user_ci, 
				ui.cust_id, 
				ui.ctn, 
				ui.ga_id 
			FROM
				(
					SELECT
						reg_source, 
						user_ci, 
						SUM(point_value) as point 
					FROM
						"POINT_INFO_TBL"
					WHERE
						reg_source = _reg_sources[_i] and
						reg_date = curDate and 
						point_type = 'O' 
					GROUP BY reg_source, user_ci 
					ORDER BY point DESC
					LIMIT limitCount
					
				) as a,
				"USER_INFO_TBL" as ui
			WHERE
				a.user_ci = ui.user_ci;

		END LOOP;

		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fn_make_top_stat2(character, integer)
  OWNER TO point;

  
  
  =====================================================================================
  
  
  -- Function: make_daily_balance(text)

-- DROP FUNCTION make_daily_balance(text);

CREATE OR REPLACE FUNCTION make_daily_balance(IN nextdate text)
  RETURNS TABLE(a_user bigint, t_balance numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		Select count(*),sum(balance) 
   		from "POINT_INFO_TBL" 
   		where point_hist_idx in (
			select max(p.point_hist_idx) 
			from 	"POINT_INFO_TBL" p,
				"USER_INFO_TBL" u
			where
				p.user_ci = u.user_ci 
				and u.status = 'A'
AND p.reg_date != nextdate 
			group by p.user_ci);
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_balance(text)
  OWNER TO point;

  
  
  
  ========================================================================================
  
  
  -- Function: make_daily_input(text)

-- DROP FUNCTION make_daily_input(text);

CREATE OR REPLACE FUNCTION make_daily_input(IN curdate text)
  RETURNS TABLE(i_count bigint, i_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(*),sum(p.point_value) 
   		from 	"POINT_INFO_TBL" p,
   			"USER_INFO_TBL" u
   		where 	p.reg_date = curDate
			and p.point_type = 'I'
			and p.user_ci = u.user_ci 
			and u.status = 'A';
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_input(text)
  OWNER TO admin;

  
    ========================================================================================
    
    
    
    
-- Function: make_daily_output(text)

-- DROP FUNCTION make_daily_output(text);

CREATE OR REPLACE FUNCTION make_daily_output(IN curdate text)
  RETURNS TABLE(o_count bigint, o_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(*),sum(p.point_value) 
   		from 	"POINT_INFO_TBL" p,
   			"USER_INFO_TBL" u
   		where 	p.reg_date = curDate
			and p.point_type = 'O'
			and p.user_ci = u.user_ci 
			and u.status = 'A';
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_output(text)
  OWNER TO admin;

  
========================================================================================




-- Function: make_daily_pre(text)

-- DROP FUNCTION make_daily_pre(text);

CREATE OR REPLACE FUNCTION make_daily_pre(IN curdate text)
  RETURNS TABLE(p_count bigint, p_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(*),sum(point_value)
   		from "POINT_HIST_TBL"
    		where status = 'W'
			and reg_date = curDate ;
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_pre(text)
  OWNER TO admin;


  
  
  
  
  
  
  
========================================================================================  



-- Function: make_daily_remove(text)

-- DROP FUNCTION make_daily_remove(text);

CREATE OR REPLACE FUNCTION make_daily_remove(IN curdate text)
  RETURNS TABLE(r_user bigint, r_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		Select count(*),sum(balance) 
  		 from "POINT_INFO_TBL" 
   		where point_hist_idx in (
			select max(p.point_hist_idx) 
			from 	"POINT_INFO_TBL" p,
				"USER_INFO_TBL" u
			where
				p.user_ci = u.user_ci 
				and u.status = 'D'
				and u.exit_date = curDate
			group by p.user_ci);
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_daily_remove(text)
  OWNER TO admin;

  
  
  
========================================================================================  

-- Function: make_daily_stat(text, text)

-- DROP FUNCTION make_daily_stat(text, text);

CREATE OR REPLACE FUNCTION make_daily_stat(
    curdate text,
    nextdate text)
  RETURNS boolean AS
$BODY$
	DECLARE
		_balance record;
		_remove record;
		_input record;
		_output record;
		_pre record;
		today date;
	BEGIN
		select to_date(curDate,'YYYYMMDD') into today;
		select a_user,t_balance into _balance from make_daily_balance(nextdate) ;
		--RAISE NOTICE 'result = %',_balance.a_user;
		--RAISE NOTICE 'result = %',_balance.t_balance;
		select r_user,r_point into _remove from make_daily_remove(curDate);
		select i_count,i_point into _input from make_daily_input(curDate);
		select o_count,o_point into _output from make_daily_output(curDate);
		select p_count,p_point into _pre from make_daily_pre(curDate);


		insert into tbl_stat_daily_point(year,month,day,d_day,total_balance,input_point,output_point,remove_point,pre_point,input_count,output_count,remove_count,pre_count,active_user)
		values (substr(curDate,1,4),substr(curDate,5,2),substr(curDate,7,2),date_part('dow', today),_balance.t_balance,_input.i_point,_output.o_point,_remove.r_point,_pre.p_point,_input.i_count,_output.o_count,_remove.r_user,_pre.p_count,_balance.a_user);
		
		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION make_daily_stat(text, text)
  OWNER TO admin;

  
  
  
========================================================================================  
-- Function: make_media_input(text)

-- DROP FUNCTION make_media_input(text);

CREATE OR REPLACE FUNCTION make_media_input(IN curdate text)
  RETURNS TABLE(i_media character varying, i_count bigint, i_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select p.reg_source,count(*),sum(p.point_value) 
		from 	"POINT_INFO_TBL" p,
			"USER_INFO_TBL" u	
		where 	p.reg_date =curDate 
			and p.point_type = 'I'
			and p.user_ci = u.user_ci 
			and u.status = 'A'
		group by p.reg_source;
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_media_input(text)
  OWNER TO admin;

  
========================================================================================  
-- Function: make_media_output(text)

-- DROP FUNCTION make_media_output(text);

CREATE OR REPLACE FUNCTION make_media_output(IN curdate text)
  RETURNS TABLE(o_media character varying, o_count bigint, o_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select p.reg_source,count(*),sum(p.point_value) 
		from 	"POINT_INFO_TBL" p,
			"USER_INFO_TBL" u	
		where 	p.reg_date =curDate 
			and p.point_type = 'O'
			and p.user_ci = u.user_ci 
			and u.status = 'A'
		group by p.reg_source;
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_media_output(text)
  OWNER TO admin;


========================================================================================  
-- Function: make_media_pre(text)

-- DROP FUNCTION make_media_pre(text);

CREATE OR REPLACE FUNCTION make_media_pre(IN curdate text)
  RETURNS TABLE(p_media character varying, p_count bigint, p_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select reg_source,count(*),sum(point_value)
   		from "POINT_HIST_TBL"
    		where status = 'W'
			and reg_date = curDate 
		group by reg_source;
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_media_pre(text)
  OWNER TO admin;

========================================================================================  
-- Function: make_media_stat(text)

-- DROP FUNCTION make_media_stat(text);

CREATE OR REPLACE FUNCTION make_media_stat(curdate text)
  RETURNS boolean AS
$BODY$
	DECLARE
		_input record;
		_output record;
		_pre record;
		today date;
	BEGIN
		select to_date(curDate,'YYYYMMDD') into today;
		FOR _input IN select i_media,i_count,i_point from make_media_input(curDate) LOOP
			insert into tbl_stat_media_point(year,month,day,d_day,media,point_type,point,count)
			values (substr(curDate,1,4),substr(curDate,5,2),substr(curDate,7,2),date_part('dow', today),_input.i_media,'I',_input.i_point,_input.i_count);			
		END LOOP;

		FOR _output IN select o_media,o_count,o_point from make_media_output(curDate) LOOP
			insert into tbl_stat_media_point(year,month,day,d_day,media,point_type,point,count)
			values (substr(curDate,1,4),substr(curDate,5,2),substr(curDate,7,2),date_part('dow', today),_output.o_media,'O',_output.o_point,_output.o_count);			
		END LOOP;

		FOR _pre IN select p_media,p_count,p_point from make_media_pre(curDate) LOOP
			insert into tbl_stat_media_point(year,month,day,d_day,media,point_type,point,count)
			values (substr(curDate,1,4),substr(curDate,5,2),substr(curDate,7,2),date_part('dow', today),_pre.p_media,'P',_pre.p_point,_pre.p_count);			
		END LOOP;

		
		--select i_media,i_count,i_point into _input from point.make_media_input(curDate);
		--select o_media,o_count,o_point into _output from point.make_media_output(curDate);
		--select p_media,p_count,p_point into _pre from point.make_media_pre(curDate);
		
		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION make_media_stat(text)
  OWNER TO admin;

========================================================================================  
-- Function: make_monthly_balance(text)

-- DROP FUNCTION make_monthly_balance(text);

CREATE OR REPLACE FUNCTION make_monthly_balance(IN nextdate text)
  RETURNS TABLE(a_user bigint, t_balance numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		Select count(*),sum(balance) 
   		from "POINT_INFO_TBL" 
   		where point_hist_idx in (
			select max(p.point_hist_idx) 
			from 	point."POINT_INFO_TBL" p,
				point."USER_INFO_TBL" u
			where
				p.user_ci = u.user_ci 
				and u.status = 'A'
				AND substr(p.reg_date, 1, 6) != nextdate 
			group by p.user_ci);
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_monthly_balance(text)
  OWNER TO admin;

========================================================================================  
-- Function: make_monthly_input(text)

-- DROP FUNCTION make_monthly_input(text);

CREATE OR REPLACE FUNCTION make_monthly_input(IN curdate text)
  RETURNS TABLE(i_count bigint, i_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(*),sum(p.point_value) 
   		from 	"POINT_INFO_TBL" p,
   			"USER_INFO_TBL" u
   		where 	substr(p.reg_date, 1, 6) = curDate
			and p.point_type = 'I'
			and p.user_ci = u.user_ci 
			and u.status = 'A';
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_monthly_input(text)
  OWNER TO admin;

========================================================================================  
-- Function: make_monthly_output(text)

-- DROP FUNCTION make_monthly_output(text);

CREATE OR REPLACE FUNCTION make_monthly_output(IN curdate text)
  RETURNS TABLE(o_count bigint, o_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(*),sum(p.point_value) 
   		from 	"POINT_INFO_TBL" p,
   			"USER_INFO_TBL" u
   		where 	substr(p.reg_date, 1, 6) = curDate
			and p.point_type = 'O'
			and p.user_ci = u.user_ci 
			and u.status = 'A';
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_monthly_output(text)
  OWNER TO admin;

========================================================================================
-- Function: make_monthly_pre(text)

-- DROP FUNCTION make_monthly_pre(text);

CREATE OR REPLACE FUNCTION make_monthly_pre(IN curdate text)
  RETURNS TABLE(p_count bigint, p_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		select count(*),sum(point_value)
   		from "POINT_HIST_TBL"
    		where status = 'W' and substr(reg_date, 1, 6) = curDate ;
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_monthly_pre(text)
  OWNER TO admin;

========================================================================================
-- Function: make_monthly_remove(text)

-- DROP FUNCTION make_monthly_remove(text);

CREATE OR REPLACE FUNCTION make_monthly_remove(IN curdate text)
  RETURNS TABLE(r_user bigint, r_point numeric) AS
$BODY$
	BEGIN
	RETURN QUERY
		Select count(*),sum(balance) 
  		 from "POINT_INFO_TBL" 
   		where point_hist_idx in (
			select max(p.point_hist_idx) 
			from 	"POINT_INFO_TBL" p,
				"USER_INFO_TBL" u
			where
				p.user_ci = u.user_ci 
				and u.status = 'D'
				and substr(u.exit_date, 1, 6) = curDate
			group by p.user_ci);
		END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION make_monthly_remove(text)
  OWNER TO admin;

========================================================================================
-- Function: make_monthly_stat(text, text)

-- DROP FUNCTION make_monthly_stat(text, text);

CREATE OR REPLACE FUNCTION make_monthly_stat(
    curdate text,
    nextdate text)
  RETURNS boolean AS
$BODY$
	DECLARE
		_balance record;
		_remove record;
		_input record;
		_output record;
		_pre record;
	BEGIN
		select a_user,t_balance into _balance from make_daily_balance(nextdate) ;
		--RAISE NOTICE 'result = %',_balance.a_user;
		--RAISE NOTICE 'result = %',_balance.t_balance;
		select r_user,r_point into _remove from make_daily_remove(curDate);
		select i_count,i_point into _input from make_daily_input(curDate);
		select o_count,o_point into _output from make_daily_output(curDate);
		select p_count,p_point into _pre from make_daily_pre(curDate);

		insert into tbl_stat_monthly_point(year,month,total_balance,input_point,output_point,remove_point,pre_point,input_count,output_count,remove_count,pre_count,active_user)
		values (substr(curDate,1,4),substr(curDate,5,2),_balance.t_balance,_input.i_point,_output.o_point,_remove.r_point,_pre.p_point,_input.i_count,_output.o_count,_remove.r_user,_pre.p_count,_balance.a_user);
		
		RETURN true;
	EXCEPTION WHEN unique_violation THEN
		RETURN false;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION make_monthly_stat(text, text)
  OWNER TO admin;

  
  



















  
  