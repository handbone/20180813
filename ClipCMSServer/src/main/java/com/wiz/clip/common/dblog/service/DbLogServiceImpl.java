/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.common.dblog.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.wiz.clip.common.dblog.dao.DbLogDAO;

@Service("dbLogService")
public class DbLogServiceImpl implements DbLogService {

	@Resource(name="dbLogDao")
	private DbLogDAO dbLogDao;
	
	/*
	 * 영업대행사&사용자 로그
	 */
	@Override
	public void insertAgentUserLog(String id, String kind, String status,
			String agentcode, String suid, String suip) {
		Map<String,String> map = new HashMap<String,String>();
		
		map.put("id", id);
		map.put("kind", kind);
		map.put("status", status);
		map.put("agentcode", agentcode);
		map.put("suid", suid);
		map.put("suip", suip);
		
		dbLogDao.insertAgentUserLog(map);
		
	}
	/*
	 * 로그인 로그
	 */
	@Override
	public void saveLoginLog(Map<String, String> map) {
		dbLogDao.saveLoginLog(map);
		
	}
	/*
	 * 로그아웃 로그
	 */
	@Override
	public void saveLogoutLog(Map<String, String> map) {
		dbLogDao.saveLogoutLog(map);
	}
	
	/*
	 * 고객정보 조회 로그[현행화데이터, 고객정보조회, 출력]
	 */
	@Override
	public void insertAgentUseLog(Map<String, String> wMap) {
		dbLogDao.insertAgentUseLog(wMap);
		
	}
	/*
	 * 비즈링 고객정보 조회 수정 로그[비즈링고객정보조회 수정 조회]
	 */
	@Override 
	public void insertBizUserSearchLog(Map<String, String> hMap) {
		dbLogDao.insertBizUserSearchLog(hMap);
		
	}	
}