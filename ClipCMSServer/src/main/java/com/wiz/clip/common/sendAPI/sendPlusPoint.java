 package com.wiz.clip.common.sendAPI;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class sendPlusPoint {
	
	Logger logger = Logger.getLogger(sendPlusPoint.class.getName());

	@Autowired
	private SqlSession postgreSession;

	@Value("#{config['API_DOMAIN_SEND_PLUS_POINT']}")
	String API_DOMAIN_SEND_PLUS_POINT;
	
	@RequestMapping(value = "/sendPlusPoint.do")
	@ResponseBody
	public String send(
			@RequestParam(required = false) String user_ci,
			@RequestParam(required = false) String cust_id,
			@RequestParam String requester_code
			,@RequestParam(required = false) String ctn, 
			@RequestParam String point_value )  throws java.lang.Exception {
	
		String userCi = user_ci;
		/*if("1".equals(ctn)){
			userCi = "NmwrWysjrN/c0tQQp1EVg/iVjx4Rqt21X4XWE0gu54X9m2Z79lO4oYG6D9wZoBZmsk5OYPIbh/qJVKcVWjs9TA==";
		}else if("2".equals(ctn)){
			userCi = "DltHZOs7sBSbEkrGsnL6h90q7y/NgrYP+UGoBpbDLSIpoum3nGDvt1DTlTXv1c87MezhArG4Eyl0lqyii+wF/g==";
		} */
			
		String smsDomain = API_DOMAIN_SEND_PLUS_POINT;
				
		String tr_id = String.valueOf(System.currentTimeMillis());
		if("".equals(tr_id) || null == tr_id){
			throw new java.lang.Exception();
		}
		StringBuffer sb2 = new StringBuffer(tr_id);
		if (tr_id.length() < 16) {
			Random rand = new Random(System.currentTimeMillis()); 
			sb2.append(Math.abs(rand.nextInt(999)));
		}
		if (tr_id.length() < 16) {
			while (sb2.length() < 16) {
				sb2.append("0");
			}
		}
		tr_id = sb2.toString();
		
		StringBuffer sb = new StringBuffer();
		
		if(null != userCi){
			sb.append("&user_ci=");
			sb.append(java.net.URLEncoder.encode(userCi,"UTF-8"));
		}
		if(null != cust_id){
			sb.append("&cust_id=");
			sb.append(java.net.URLEncoder.encode(cust_id,"UTF-8"));
		}
		if(null != ctn){
			sb.append("&ctn=");
			sb.append(java.net.URLEncoder.encode(ctn,"UTF-8"));
		}
		
		sb.append("&transaction_id=");
		sb.append(tr_id);
		sb.append("&requester_code=");
		//sb.append("pointUpdate");
		sb.append(requester_code);
		sb.append("&point_value=");
		sb.append(point_value);
		sb.append("&description=");
		sb.append(java.net.URLEncoder.encode("포인트 조정","UTF-8"));
				
		logger.debug("sb ::::"+sb.toString());
		
		BufferedReader oBufReader = null;
		HttpURLConnection httpConn = null;
		String strBuffer = "";
		String strRslt = "";
		
		String strEncodeUrl = smsDomain;

		URL oOpenURL = new URL(strEncodeUrl);
		httpConn = (HttpURLConnection) oOpenURL.openConnection();
		httpConn.setDoOutput(true);
		httpConn.setRequestMethod("POST");
		httpConn.setConnectTimeout(10000);
		httpConn.setReadTimeout(10000);
		httpConn.setRequestProperty("Content-Length", String.valueOf(sb.length()));
		//httpConn.setRequestProperty("Content-Type", "application/soap+xml; charset=utf-8");
		OutputStream os = httpConn.getOutputStream();
		os.write(sb.toString().getBytes());
		os.flush();
		os.close(); 
		oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
		StringBuffer sb3 = new StringBuffer(strRslt);
			
		while ((strBuffer = oBufReader.readLine()) != null) {
			if (strBuffer.length() > 1) {
				sb3.append(strBuffer);
			}
		}
		oBufReader.close();
			
		return sb3.toString();
	}
	
	
}
