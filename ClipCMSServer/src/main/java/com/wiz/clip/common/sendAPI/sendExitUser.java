package com.wiz.clip.common.sendAPI;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wiz.clip.common.crypto.AES256CipherClip;

@Controller
public class sendExitUser {
	
	Logger logger = Logger.getLogger(sendExitUser.class.getName());

	@Autowired
	private SqlSession postgreSession;

	@Value("#{config['API_DOMAIN_EXIT_USER']}")
	String API_DOMAIN_EXIT_USER;
	
	@RequestMapping(value = "/sendExitUser.do")
	@ResponseBody
	public String sendExitUser(@RequestParam String user_ci,
			@RequestParam(required = false) String end_gbn
			)  throws java.lang.Exception {
	

		String smsDomain = API_DOMAIN_EXIT_USER;

				
		StringBuffer sb = new StringBuffer();
		
		sb.append("user_ci=");
		sb.append(java.net.URLEncoder.encode(user_ci,"UTF-8"));
		if(null != end_gbn){
			sb.append("&end_gbn=");
			sb.append(java.net.URLEncoder.encode(end_gbn,"UTF-8"));
		}
		logger.debug("sb ::::"+sb.toString());
		
		BufferedReader oBufReader = null;
		HttpURLConnection httpConn = null;
		String strBuffer = "";
		String strRslt = "";
		
		String strEncodeUrl = smsDomain;

		URL oOpenURL = new URL(strEncodeUrl);
		httpConn = (HttpURLConnection) oOpenURL.openConnection();
		httpConn.setDoOutput(true);
		httpConn.setRequestMethod("POST");
		httpConn.setConnectTimeout(10000);
		httpConn.setReadTimeout(10000);
		httpConn.setRequestProperty("Content-Length", String.valueOf(sb.length()));
		//httpConn.setRequestProperty("Content-Type", "application/soap+xml; charset=utf-8");
		OutputStream os = httpConn.getOutputStream();
		os.write(sb.toString().getBytes());
		os.flush();
		os.close();
		oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
		StringBuffer sb3 = new StringBuffer(strRslt);
			
		while ((strBuffer = oBufReader.readLine()) != null) {
			if (strBuffer.length() > 1) {
				sb3.append(strBuffer);
			}
		}
		oBufReader.close();
			
		return sb3.toString();
	}
	
	
}
