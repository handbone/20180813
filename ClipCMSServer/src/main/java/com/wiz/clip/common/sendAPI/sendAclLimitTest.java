package com.wiz.clip.common.sendAPI;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class sendAclLimitTest {
	
	Logger logger = Logger.getLogger(sendAclLimitTest.class.getName());
	
	@Autowired
	private SqlSession postgreSession;
	
	@Value("#{config['API_DOMAIN_SEND_ACLLIMITTEST']}")
	String API_DOMAIN_SEND_ACLLIMITTEST;

	
	@RequestMapping(value = "/sendAclLimitTest.do")
	@ResponseBody
	public String sendBZS(
			@RequestParam String transaction_id, 
			@RequestParam String user_id, 
			@RequestParam String campaign_name, 
			@RequestParam String point)  throws java.lang.Exception {
	
		String smsDomain = API_DOMAIN_SEND_ACLLIMITTEST;
		String parameter = "user_id="+user_id+
				"&transaction_id="+transaction_id+
				"&campaign_name="+campaign_name+
				"&point="+point;
		
		BufferedReader oBufReader = null;
		HttpURLConnection httpConn = null;
		String strBuffer = "";
		String strRslt = "";
		
			String strEncodeUrl = smsDomain ;

			URL oOpenURL;
				oOpenURL = new URL(strEncodeUrl);
			
				httpConn = (HttpURLConnection) oOpenURL.openConnection();
				httpConn.setDoOutput(true);
				httpConn.setRequestMethod("POST");
				httpConn.setConnectTimeout(10000);
				httpConn.setReadTimeout(10000);
				httpConn.setRequestProperty("Content-Length", String.valueOf(parameter.length()));
				OutputStream os = httpConn.getOutputStream();
				os.write(parameter.getBytes());
				os.flush();
				os.close();
	
				oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream(),"UTF-8"));
	
				StringBuffer sb = new StringBuffer(strRslt);
				
				while ((strBuffer = oBufReader.readLine()) != null) {
					if (strBuffer.length() > 1) {
						sb.append(strBuffer);
					}
				}
	
				oBufReader.close();
			
		return sb.toString();
	}

}
