/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.common.imageview;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
class ImageViewController extends HttpServlet {

	private static final long serialVersionUID = 1L;

@RequestMapping(value="/imgSave.do")
  protected void imgSave(HttpServletRequest req, HttpServletResponse res){

      // 불러와야할 이미지에 대한 식별값을 파라미터로 받아옴.
      String imgName = req.getParameter("imgName");
      
   
      String pathString = "D:\\storage\\imgSave";
      File file = new File((pathString + File.separator + imgName ));
      FileInputStream fis = null;
		
      BufferedInputStream in = null;
      ByteArrayOutputStream bStream = null;
      try{
          fis = new FileInputStream(file);
	  in = new BufferedInputStream(fis);
	  bStream = new ByteArrayOutputStream();
	  int imgByte;
	  while ((imgByte = in.read()) != -1) {
	     bStream.write(imgByte);
	  }

      //response.setHeader("Content-Type", type);
	  res.setContentLength(bStream.size());
			
      bStream.writeTo(res.getOutputStream());
      res.getOutputStream().flush();
      res.getOutputStream().close();

      } catch (Exception e) {
         e.printStackTrace();
      } finally {
         if (bStream != null) {
	   try {
	      bStream.close();
	   } catch (Exception est) {
	      est.printStackTrace();
	   }
      }
         if (in != null) {
	   try {in.close();}
           catch (Exception ei) { ei.printStackTrace();	}
	 }
	 if (fis != null) {
	    try {fis.close();}
            catch (Exception efis) { efis.printStackTrace(); }
	 }
      }
      
 }

}
