/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.common.download;

import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.wiz.clip.manage.manageLog.bean.PointHistTbl;
import com.wiz.clip.manage.manageLog.bean.PointInfoTbl;
import com.wiz.clip.manage.pointChk.bean.CmsPointModify;
import com.wiz.clip.manage.pointChk.bean.TblStatAnomalyDetection;
import com.wiz.clip.manage.pointChk.bean.TblStatTopUser;
import com.wiz.clip.manage.stat.bean.TblStatDailyPoint;
import com.wiz.clip.manage.stat.bean.TblStatDailyPoint2;
import com.wiz.clip.manage.stat.bean.TblStatMediaPoint;
import com.wiz.clip.manage.stat.bean.TblStatMonthlyPoint;
import com.wiz.clip.manage.userInfo.bean.UserInfoTbl;
import com.wiz.clip.manage.userInfo.bean.UserInfoTblLog;
import com.wiz.clip.onm.bean.BZADPostLog;
import com.wiz.clip.onm.bean.BZScreenPostLog;
import com.wiz.clip.onm.bean.SMSSend;
import com.wiz.clip.onm.bean.StatConnect;
import com.wiz.clip.roulette.viewLog.bean.PointRouletteJoin;
import com.wiz.clip.roulette.viewLog.bean.ViewLogBean;
import com.wiz.clip.user.bean.CmsLoginLog;
import com.wiz.clip.user.bean.CmsUseLog;

public class ExcelView extends AbstractExcelView{ 
	   Logger logger = Logger.getLogger(ExcelView.class.getName());
	
       @Override
       protected void buildExcelDocument(Map<String,Object> ModelMap,HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response) throws java.lang.Exception{
    	    logger.debug("excelView Page start!!!");
    	    String excelName = ModelMap.get("pageNm").toString().trim();
    	    String divString = ModelMap.get("divString").toString().trim();
            @SuppressWarnings("unchecked")
            //List<Map<String, String>> list = (List<Map<String, String>>) ModelMap.get("list");
            
            HSSFSheet worksheet = null;
            HSSFRow row = null;
         
            // 채널별참여자
            if(divString.equals("joinByChannel")){
            	
            	List<ViewLogBean> list = (List<ViewLogBean>)ModelMap.get("list");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                worksheet.setDefaultColumnWidth(15);
                //worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("채널명");
                row.createCell(1).setCellValue("날짜");
                row.createCell(2).setCellValue("당첨자수");
                row.createCell(3).setCellValue("당첨자수 ('꽝' 포함)");
                row.createCell(4).setCellValue("당첨포인트");

                int totalJoinSum = 0;
                int totalJoinSumPlusZero = 0;
                int totalSavePoniSum = 0;
                
                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getChannelname());
                       row.createCell(1).setCellValue(list.get(i-1).getSdate());
                       row.createCell(2).setCellValue(list.get(i-1).getJoinSum() + " 명");
                       row.createCell(3).setCellValue(list.get(i-1).getJoinSumPlusZero() + " 명");
                       row.createCell(4).setCellValue(list.get(i-1).getSavePointSum() + " P");
                
                totalJoinSum = totalJoinSum + Integer.parseInt(list.get(i-1).getJoinSum());
                totalJoinSumPlusZero = totalJoinSumPlusZero + Integer.parseInt(list.get(i-1).getJoinSumPlusZero());
                totalSavePoniSum = totalSavePoniSum + Integer.parseInt(list.get(i-1).getSavePointSum());
                
                }
                 
                row = worksheet.createRow(worksheet.getLastRowNum()+1);
                row.createCell(0).setCellValue("합계");
                row.createCell(1).setCellValue("");
                row.createCell(2).setCellValue(totalJoinSum + " 명");
                row.createCell(3).setCellValue(totalJoinSumPlusZero + " 명");
                row.createCell(4).setCellValue(totalSavePoniSum + " P");
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                  
            }
            
            
            
            
            
            // [통계] 채널별 당첨내역
            if(divString.equals("detailByChannel")){
            	List<ViewLogBean> list = (List<ViewLogBean>)ModelMap.get("list");
            	
            	worksheet = workbook.createSheet(excelName+ " WorkSheet");
            	excelName=URLEncoder.encode(excelName,"UTF-8");
                worksheet.setDefaultColumnWidth(15);
               
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("채널명");
                row.createCell(1).setCellValue("날짜");
                row.createCell(2).setCellValue("당첨내역");
                row.createCell(8).setCellValue("합계");
                
                worksheet.addMergedRegion(new CellRangeAddress(0, 1, 0, 0));
                worksheet.addMergedRegion(new CellRangeAddress(0, 1, 1, 1));
                worksheet.addMergedRegion(new CellRangeAddress(0, 0, 2, 7));
                worksheet.addMergedRegion(new CellRangeAddress(0, 1, 8, 8));
                
                row = worksheet.createRow(1);
                row.createCell(2).setCellValue("경품명 1 ");
                row.createCell(3).setCellValue("경품명 2 ");
                row.createCell(4).setCellValue("경품명 3 ");
                row.createCell(5).setCellValue("경품명 4 ");
                row.createCell(6).setCellValue("경품명 5 ");
                row.createCell(7).setCellValue("경품명 6 ");
                
                for(int i = 2  ; i < list.size() + 2 ; i++ ){
                       row = worksheet.createRow(i*2-2);
                       row.createCell(0).setCellValue(list.get(i-2).getChannelname());
                       row.createCell(1).setCellValue(list.get(i-2).getSdate());
                       row.createCell(2).setCellValue(list.get(i-2).getArea01() + " 명");
                       row.createCell(3).setCellValue(list.get(i-2).getArea02() + " 명");
                       row.createCell(4).setCellValue(list.get(i-2).getArea03() + " 명");
                       row.createCell(5).setCellValue(list.get(i-2).getArea04() + " 명");
                       row.createCell(6).setCellValue(list.get(i-2).getArea05() + " 명");
                       row.createCell(7).setCellValue(list.get(i-2).getArea06() + " 명");
                       row.createCell(8).setCellValue(Integer.parseInt(list.get(i-2).getArea01())+
                    		   Integer.parseInt(list.get(i-2).getArea02())+
                    		   Integer.parseInt(list.get(i-2).getArea03())+
                    		   Integer.parseInt(list.get(i-2).getArea04())+
                    		   Integer.parseInt(list.get(i-2).getArea05())+
                    		   Integer.parseInt(list.get(i-2).getArea06()) + " 명");
                       
                       row = worksheet.createRow( i*2 - 1 );
                       row.createCell(2).setCellValue(list.get(i-2).getSumPoint01() + " P");
                       row.createCell(3).setCellValue(list.get(i-2).getSumPoint02() + " P");
                       row.createCell(4).setCellValue(list.get(i-2).getSumPoint03() + " P");
                       row.createCell(5).setCellValue(list.get(i-2).getSumPoint04() + " P");
                       row.createCell(6).setCellValue(list.get(i-2).getSumPoint05() + " P");
                       row.createCell(7).setCellValue(list.get(i-2).getSumPoint06() + " P");
                       row.createCell(8).setCellValue(Integer.parseInt(list.get(i-2).getSumPoint01())+
                    		   Integer.parseInt(list.get(i-2).getSumPoint02())+
                    		   Integer.parseInt(list.get(i-2).getSumPoint03())+
                    		   Integer.parseInt(list.get(i-2).getSumPoint04())+
                    		   Integer.parseInt(list.get(i-2).getSumPoint05())+
                    		   Integer.parseInt(list.get(i-2).getSumPoint06()) + " P");
                       
                       worksheet.addMergedRegion(new CellRangeAddress(i*2-2, i*2-1, 0, 0));
                       worksheet.addMergedRegion(new CellRangeAddress(i*2-2, i*2-1, 1, 1));
                       
                
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
            }
            // [로그] 참여자 로그
            if(divString.equals("join")){

            	List<PointRouletteJoin> list = (List<PointRouletteJoin>)ModelMap.get("list");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                //worksheet.setDefaultColumnWidth(15);
                worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("채널명");
                row.createCell(1).setCellValue("cust_id");
                row.createCell(2).setCellValue("ctn");
                row.createCell(3).setCellValue("ga_id");
                row.createCell(4).setCellValue("날짜");
                row.createCell(5).setCellValue("당첨포인트");
                row.createCell(6).setCellValue("시간");

                int totalJoinSum = 0;
                int totalSavePoniSum = 0;
                
                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getChannelname());
                       row.createCell(1).setCellValue(list.get(i-1).getCust_id());
                       row.createCell(2).setCellValue(list.get(i-1).getCtn());
                       row.createCell(3).setCellValue(list.get(i-1).getGa_id());
                       row.createCell(4).setCellValue(list.get(i-1).getSdate());
                       row.createCell(5).setCellValue(list.get(i-1).getSave_point() + " P");
                       row.createCell(6).setCellValue(list.get(i-1).getRegdate());
                
                totalJoinSum = totalJoinSum + 1;
                totalSavePoniSum = totalSavePoniSum + Integer.parseInt(list.get(i-1).getSave_point());
                
                }
                 
                row = worksheet.createRow(worksheet.getLastRowNum()+1);
                row.createCell(0).setCellValue("합계");
                row.createCell(1).setCellValue("");
                row.createCell(4).setCellValue("총 참여 " + totalJoinSum + " 회");
                row.createCell(5).setCellValue("총 당첨포인트 " + totalSavePoniSum + " P");
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                  
                 
            }
            
            
            // [통계] 페이지뷰 
            if(divString.equals("pageView")){
            	
            	List<ViewLogBean> list = (List<ViewLogBean>)ModelMap.get("list");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                worksheet.setDefaultColumnWidth(15);
                //worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("채널명");
                row.createCell(1).setCellValue("날짜");
                row.createCell(2).setCellValue("페이지뷰");

                int totalPageView = 0;
                
                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getChannelname());
                       row.createCell(1).setCellValue(list.get(i-1).getSdate());
                       row.createCell(2).setCellValue(list.get(i-1).getPageView());
                
                       totalPageView = totalPageView + Integer.parseInt(list.get(i-1).getPageView());
                
                }
                 
                row = worksheet.createRow(worksheet.getLastRowNum()+1);
                row.createCell(0).setCellValue("합계");
                row.createCell(1).setCellValue("");
                row.createCell(2).setCellValue(totalPageView );
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                 
            }
            
            // [로그] API연동 로그
            if(divString.equals("connectAPI")){
            	
            	List<ViewLogBean> list = (List<ViewLogBean>)ModelMap.get("list");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                worksheet.setDefaultColumnWidth(20);
                //worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("cust_id");
                row.createCell(1).setCellValue("ga_id");
                row.createCell(2).setCellValue("ctn");
                row.createCell(3).setCellValue("apiurl");
                row.createCell(4).setCellValue("tr_id");
                row.createCell(5).setCellValue("응답값");
                row.createCell(6).setCellValue("IP");
                row.createCell(7).setCellValue("날짜");

                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getCust_id());
                       row.createCell(1).setCellValue(list.get(i-1).getGa_id());
                       row.createCell(2).setCellValue(list.get(i-1).getCtn());
                       row.createCell(3).setCellValue(list.get(i-1).getApiurl());
                       row.createCell(4).setCellValue(list.get(i-1).getTr_id());
                       row.createCell(5).setCellValue(list.get(i-1).getTr_response());
                       row.createCell(6).setCellValue(list.get(i-1).getIpaddr());
                       row.createCell(7).setCellValue(list.get(i-1).getRegdate());
                
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                  
                 
            }
            
            
            // [로그] 페이지뷰 로그
            if(divString.equals("joinEvent")){
            	
            	List<ViewLogBean> list = (List<ViewLogBean>)ModelMap.get("list");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                worksheet.setDefaultColumnWidth(20);
                //worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("채널명");
                row.createCell(1).setCellValue("cust_id");
                row.createCell(2).setCellValue("ga_id");
                row.createCell(3).setCellValue("ctn");
                row.createCell(4).setCellValue("IP");
                row.createCell(5).setCellValue("날짜");

                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getChannelname());
                       row.createCell(1).setCellValue(list.get(i-1).getCust_id());
                       row.createCell(2).setCellValue(list.get(i-1).getGa_id());
                       row.createCell(3).setCellValue(list.get(i-1).getCtn());
                       row.createCell(4).setCellValue(list.get(i-1).getIpaddr());
                       row.createCell(5).setCellValue(list.get(i-1).getRegdate());
                
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                  
                 
            }
            
            
            // [로그] 시간당첨값 로그
            if(divString.equals("timeGbn")){
            	
            	List<ViewLogBean> list = (List<ViewLogBean>)ModelMap.get("list");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                worksheet.setDefaultColumnWidth(20);
                //worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("채널명");
                row.createCell(1).setCellValue("시간설정값");
                row.createCell(2).setCellValue("당첨순번");
                row.createCell(3).setCellValue("날짜");

                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getChannelname());
                       row.createCell(1).setCellValue(list.get(i-1).getSettime());
                       row.createCell(2).setCellValue(list.get(i-1).getSetnum());
                       row.createCell(3).setCellValue(list.get(i-1).getSdate());
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                  
                 
                 
            }
            
            
            // [로그] 일자별 당첨자합계 로그
            if(divString.equals("dailyJoinSum")){
            	
        		List<ViewLogBean> list = (List<ViewLogBean>)ModelMap.get("list");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                worksheet.setDefaultColumnWidth(20);
                //worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("채널명");
                row.createCell(1).setCellValue("날짜");
                row.createCell(2).setCellValue("경품명 1");
                row.createCell(3).setCellValue("경품명 2");
                row.createCell(4).setCellValue("경품명 3");
                row.createCell(5).setCellValue("경품명 4");
                row.createCell(6).setCellValue("경품명 5");
                row.createCell(7).setCellValue("경품명 6");

                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getChannelname());
                       row.createCell(1).setCellValue(list.get(i-1).getSdate());
                       row.createCell(2).setCellValue(list.get(i-1).getArea01());
                       row.createCell(3).setCellValue(list.get(i-1).getArea02());
                       row.createCell(4).setCellValue(list.get(i-1).getArea03());
                       row.createCell(5).setCellValue(list.get(i-1).getArea04());
                       row.createCell(6).setCellValue(list.get(i-1).getArea05());
                       row.createCell(7).setCellValue(list.get(i-1).getArea06());
                
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                  
                 
            }
            
            
            
            
            
         // 로그인/아웃 내역확인
            if(divString.equals("viewLogLoginOut")){
            	
        		List<CmsLoginLog> list = (List<CmsLoginLog>)ModelMap.get("list");
            	
        		worksheet = workbook.createSheet(excelName+ " WorkSheet");
            	excelName=URLEncoder.encode(excelName,"UTF-8");
                worksheet.setDefaultColumnWidth(20);
                //worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("로그인 시간");
                row.createCell(1).setCellValue("로그아웃 시간");
                row.createCell(2).setCellValue("사용자 ID");
                row.createCell(3).setCellValue("IP");

                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getLogintime());
                       row.createCell(1).setCellValue(list.get(i-1).getLogouttime());
                       row.createCell(2).setCellValue(list.get(i-1).getUserid());
                       row.createCell(3).setCellValue(list.get(i-1).getIpaddr());
                
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                  
                 
            }
            
            
         // 메뉴 접속로그
            if(divString.equals("viewLogMenu")){
            	
        		List<CmsUseLog> list = (List<CmsUseLog>)ModelMap.get("list");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                worksheet.setDefaultColumnWidth(20);
                //worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("접속시간");
                row.createCell(1).setCellValue("접속 메뉴");
                row.createCell(2).setCellValue("접속 페이지");
                row.createCell(3).setCellValue("ID");
                row.createCell(4).setCellValue("IP");

                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getRegdate());
                       row.createCell(1).setCellValue(list.get(i-1).getMenu());
                       row.createCell(2).setCellValue(list.get(i-1).getAction());
                       row.createCell(3).setCellValue(list.get(i-1).getUserid());
                       row.createCell(4).setCellValue(list.get(i-1).getIpaddr());
                
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                  
                 
            }
            
            
            
         // 통계관리 1
            if(divString.equals("statLogDay")){
            	
        		List<TblStatDailyPoint> list = (List<TblStatDailyPoint>)ModelMap.get("daylist");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                worksheet.setDefaultColumnWidth(20);
                //worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("날짜");
                row.createCell(1).setCellValue("요일");
                row.createCell(2).setCellValue("잔액");
                row.createCell(3).setCellValue("사용자");
                row.createCell(4).setCellValue("적립포인트");
                row.createCell(5).setCellValue("적립횟수");
                row.createCell(6).setCellValue("사용포인트");
                row.createCell(7).setCellValue("사용횟수");
                row.createCell(8).setCellValue("대기포인트");
                row.createCell(9).setCellValue("대기횟수");
                row.createCell(10).setCellValue("소멸포인트");
                row.createCell(11).setCellValue("탈퇴자");

                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getSdate());
                       if("0".equals(list.get(i-1).getD_day())){
                    	   row.createCell(1).setCellValue("sun");
            		   }else if("1".equals(list.get(i-1).getD_day())){
                    	   row.createCell(1).setCellValue("mon");
            		   }else if("2".equals(list.get(i-1).getD_day())){
                    	   row.createCell(1).setCellValue("tues");
            		   }else if("3".equals(list.get(i-1).getD_day())){
                    	   row.createCell(1).setCellValue("wed");
            		   }else if("4".equals(list.get(i-1).getD_day())){
                    	   row.createCell(1).setCellValue("thur");
            		   }else if("5".equals(list.get(i-1).getD_day())){
                    	   row.createCell(1).setCellValue("fri");
            		   }else if("6".equals(list.get(i-1).getD_day())){
                    	   row.createCell(1).setCellValue("sat");
            		   }
                       row.createCell(2).setCellValue(list.get(i-1).getTotal_balance());
                       row.createCell(3).setCellValue(list.get(i-1).getActive_user());
                       row.createCell(4).setCellValue(list.get(i-1).getInput_point());
                       row.createCell(5).setCellValue(list.get(i-1).getInput_count());
                       row.createCell(6).setCellValue(list.get(i-1).getOutput_point());
                       row.createCell(7).setCellValue(list.get(i-1).getOutput_count());
                       row.createCell(8).setCellValue(list.get(i-1).getPre_point());
                       row.createCell(9).setCellValue(list.get(i-1).getPre_count());
                       row.createCell(10).setCellValue(list.get(i-1).getRemove_point());
                       row.createCell(11).setCellValue(list.get(i-1).getRemove_count());
                       
                
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                  
                 
            }
            
            
         // 통계관리 2
            if(divString.equals("statLogMonth")){
            	
        		List<TblStatMonthlyPoint> list = (List<TblStatMonthlyPoint>)ModelMap.get("monthlist");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                worksheet.setDefaultColumnWidth(20);
                //worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("기준월");
                row.createCell(1).setCellValue("잔액");
                row.createCell(2).setCellValue("사용자");
                row.createCell(3).setCellValue("적립포인트");
                row.createCell(4).setCellValue("적립횟수");
                row.createCell(5).setCellValue("사용포인트");
                row.createCell(6).setCellValue("사용횟수");

                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getSdate());
                       row.createCell(1).setCellValue(list.get(i-1).getTotal_balance());
                       row.createCell(2).setCellValue(list.get(i-1).getActive_user());
                       row.createCell(3).setCellValue(list.get(i-1).getInput_point());
                       row.createCell(4).setCellValue(list.get(i-1).getInput_count());
                       row.createCell(5).setCellValue(list.get(i-1).getOutput_point());
                       row.createCell(6).setCellValue(list.get(i-1).getOutput_count());
                
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                  
                 
            }
            
         // 통계관리 3
            if(divString.equals("statLogMedia")){
            	
        		List<TblStatMediaPoint> list = (List<TblStatMediaPoint>)ModelMap.get("medialist");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                worksheet.setDefaultColumnWidth(20);
                //worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("매체");
                row.createCell(1).setCellValue("날짜");
                row.createCell(2).setCellValue("요일");
                row.createCell(3).setCellValue("적립포인트");
                row.createCell(4).setCellValue("적립횟수");
                row.createCell(5).setCellValue("유형");


                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getMedia());
                       row.createCell(1).setCellValue(list.get(i-1).getSdate());
                       if("0".equals(list.get(i-1).getD_day())){
                    	   row.createCell(2).setCellValue("sun");
            		   }else if("1".equals(list.get(i-1).getD_day())){
                    	   row.createCell(2).setCellValue("mon");
            		   }else if("2".equals(list.get(i-1).getD_day())){
                    	   row.createCell(2).setCellValue("tues");
            		   }else if("3".equals(list.get(i-1).getD_day())){
                    	   row.createCell(2).setCellValue("wed");
            		   }else if("4".equals(list.get(i-1).getD_day())){
                    	   row.createCell(2).setCellValue("thur");
            		   }else if("5".equals(list.get(i-1).getD_day())){
                    	   row.createCell(2).setCellValue("fri");
            		   }else if("6".equals(list.get(i-1).getD_day())){
                    	   row.createCell(2).setCellValue("sat");
            		   }
                       row.createCell(3).setCellValue(list.get(i-1).getPoint());
                       row.createCell(4).setCellValue(list.get(i-1).getCount());
                       if("I".equals(list.get(i-1).getPoint_type())){
                    	   row.createCell(5).setCellValue("적립");
            		   }else if("O".equals(list.get(i-1).getPoint_type())){
                    	   row.createCell(5).setCellValue("사용");
            		   }else if("P".equals(list.get(i-1).getPoint_type())){
                    	   row.createCell(5).setCellValue("임시");
            		   }
                
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                  
                 
            }
            
         // 통계확장
            if(divString.equals("statLogDay2")){
            	
        		List<TblStatDailyPoint2> list = (List<TblStatDailyPoint2>)ModelMap.get("daylist");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                worksheet.setDefaultColumnWidth(20);
                //worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("날짜");
                row.createCell(1).setCellValue("요일");
                
                row.createCell(2).setCellValue("적립 Unique CI");
                row.createCell(3).setCellValue("적립 건수");
                row.createCell(4).setCellValue("적립 소계");
                
                row.createCell(5).setCellValue("사용 Unique CI");
                row.createCell(6).setCellValue("사용 건수");
                row.createCell(7).setCellValue("사용 소계");
                
                row.createCell(8).setCellValue("강제조정 Unique CI");
                row.createCell(9).setCellValue("강제조정 건수");
                row.createCell(10).setCellValue("강제조정 소계");
                
                row.createCell(11).setCellValue("잔고 Unique CI");
                row.createCell(12).setCellValue("잔고 소계");
                
                row.createCell(13).setCellValue("임시 건수");
                row.createCell(14).setCellValue("임시 소계");
                
                row.createCell(15).setCellValue("완전탈퇴 Unique CI");
                row.createCell(16).setCellValue("완전탈퇴 건수");
                row.createCell(17).setCellValue("완전탈퇴 소계");
                
                row.createCell(18).setCellValue("이용건수 Unique CI"); 
                row.createCell(19).setCellValue("이용건수 건수");

                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getSdate());
                       if("0".equals(list.get(i-1).getD_day())){
                    	   row.createCell(1).setCellValue("sun");
            		   }else if("1".equals(list.get(i-1).getD_day())){
                    	   row.createCell(1).setCellValue("mon");
            		   }else if("2".equals(list.get(i-1).getD_day())){
                    	   row.createCell(1).setCellValue("tues");
            		   }else if("3".equals(list.get(i-1).getD_day())){
                    	   row.createCell(1).setCellValue("wed");
            		   }else if("4".equals(list.get(i-1).getD_day())){
                    	   row.createCell(1).setCellValue("thur");
            		   }else if("5".equals(list.get(i-1).getD_day())){
                    	   row.createCell(1).setCellValue("fri");
            		   }else if("6".equals(list.get(i-1).getD_day())){
                    	   row.createCell(1).setCellValue("sat");
            		   }
                       row.createCell(2).setCellValue(list.get(i-1).getInput_unique_ci());
                       row.createCell(3).setCellValue(list.get(i-1).getInput_count());
                       row.createCell(4).setCellValue(list.get(i-1).getInput_point());
                       
                       row.createCell(5).setCellValue(list.get(i-1).getOutput_unique_ci());
                       row.createCell(6).setCellValue(list.get(i-1).getOutput_count());
                       row.createCell(7).setCellValue(list.get(i-1).getOutput_point());
                       
                       row.createCell(8).setCellValue(list.get(i-1).getModify_unique_ci());
                       row.createCell(9).setCellValue(list.get(i-1).getModify_count());
                       row.createCell(10).setCellValue(list.get(i-1).getModify_point());
                       
                       row.createCell(11).setCellValue(list.get(i-1).getActive_user());
                       row.createCell(12).setCellValue(list.get(i-1).getTotal_balance());
                       
                       row.createCell(13).setCellValue(list.get(i-1).getPre_count());
                       row.createCell(14).setCellValue(list.get(i-1).getPre_point());
                       
                       row.createCell(15).setCellValue(list.get(i-1).getRemove_unique_ci());
                       row.createCell(16).setCellValue(list.get(i-1).getRemove_count());
                       row.createCell(17).setCellValue(list.get(i-1).getRemove_point());
                       
                       row.createCell(18).setCellValue(list.get(i-1).getIo_sum_unique_ci());
                       row.createCell(19).setCellValue(list.get(i-1).getIo_sum_count());
                       
                
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                  
                 
            }
            
         // 이용조회 1
            if(divString.equals("manageLog1")){
            	
        		List<PointInfoTbl> list = (List<PointInfoTbl>)ModelMap.get("infolist");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                worksheet.setDefaultColumnWidth(20);
                //worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("날짜");
                row.createCell(1).setCellValue("시간");
                row.createCell(2).setCellValue("매체");
                row.createCell(3).setCellValue("사용구분");
                row.createCell(4).setCellValue("포인트");
                row.createCell(5).setCellValue("잔액");
                row.createCell(6).setCellValue("전환");
                row.createCell(7).setCellValue("TR_ID");


                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getReg_date());
                       row.createCell(1).setCellValue(list.get(i-1).getReg_time());
                       row.createCell(2).setCellValue(list.get(i-1).getRequester_code());
                       if("I".equals(list.get(i-1).getPoint_type())){
                    	   row.createCell(3).setCellValue("적립");
            		   }else if("O".equals(list.get(i-1).getPoint_type())){
                    	   row.createCell(3).setCellValue("사용");
            		   }
                       row.createCell(4).setCellValue(list.get(i-1).getPoint_value());
                       row.createCell(5).setCellValue(list.get(i-1).getBalance());
                       row.createCell(6).setCellValue(list.get(i-1).getStatus());
                       row.createCell(7).setCellValue(list.get(i-1).getTransaction_id());
                
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                  
                 
            }
            
            // 이용조회 2
            if(divString.equals("manageLog2")){
            	
        		List<PointHistTbl> list = (List<PointHistTbl>)ModelMap.get("histlist");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                worksheet.setDefaultColumnWidth(20);
                //worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("날짜");
                row.createCell(1).setCellValue("시간");
                row.createCell(2).setCellValue("매체");
                row.createCell(3).setCellValue("사용구분");
                row.createCell(4).setCellValue("포인트");
                row.createCell(5).setCellValue("전환");
                row.createCell(6).setCellValue("TR_ID");


                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getReg_date());
                       row.createCell(1).setCellValue(list.get(i-1).getReg_time());
                       row.createCell(2).setCellValue(list.get(i-1).getRequester_code());
                       if("I".equals(list.get(i-1).getPoint_type())){
                    	   row.createCell(3).setCellValue("적립");
            		   }else if("O".equals(list.get(i-1).getPoint_type())){
                    	   row.createCell(3).setCellValue("사용");
            		   }
                       row.createCell(4).setCellValue(list.get(i-1).getPoint_value());
                       row.createCell(5).setCellValue(list.get(i-1).getStatus());
                       row.createCell(6).setCellValue(list.get(i-1).getTransaction_id());
                
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                  
                 
            }
            
            
            // 사용자조회
            if(divString.equals("userInfo")){

            	List<UserInfoTbl> list = (List<UserInfoTbl>)ModelMap.get("list");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                //worksheet.setDefaultColumnWidth(15);
                worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("user_ci");
                row.createCell(1).setCellValue("cust_id");
                row.createCell(2).setCellValue("ctn");
                row.createCell(3).setCellValue("ga_id");
                row.createCell(4).setCellValue("상태");
                row.createCell(5).setCellValue("등록날짜");
                row.createCell(6).setCellValue("등록시간");
                row.createCell(7).setCellValue("exit_ci");
                row.createCell(8).setCellValue("exit_date");
                row.createCell(9).setCellValue("exit_time");

                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getUser_ci());
                       row.createCell(1).setCellValue(list.get(i-1).getCust_id());
                       row.createCell(2).setCellValue(list.get(i-1).getCtn());
                       row.createCell(3).setCellValue(list.get(i-1).getGa_id());
                       row.createCell(4).setCellValue(list.get(i-1).getStatus());
                       row.createCell(5).setCellValue(list.get(i-1).getReg_date());
                       row.createCell(6).setCellValue(list.get(i-1).getReg_time());
                       row.createCell(7).setCellValue(list.get(i-1).getExit_ci());
                       row.createCell(8).setCellValue(list.get(i-1).getExit_date());
                       row.createCell(9).setCellValue(list.get(i-1).getExit_time());
                
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                 
            }
            
         // 사용자 변경기록
            if(divString.equals("userLog")){

            	List<UserInfoTblLog> list = (List<UserInfoTblLog>)ModelMap.get("list");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                //worksheet.setDefaultColumnWidth(15);
                worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("user_ci");
                row.createCell(1).setCellValue("cust_id");
                row.createCell(2).setCellValue("ctn");
                row.createCell(3).setCellValue("ga_id");
                row.createCell(4).setCellValue("상태");
                row.createCell(7).setCellValue("exit_ci");
                row.createCell(8).setCellValue("구분값");
                row.createCell(9).setCellValue("변경일자");

                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getUser_ci());
                       row.createCell(1).setCellValue(list.get(i-1).getCust_id());
                       row.createCell(2).setCellValue(list.get(i-1).getCtn());
                       row.createCell(3).setCellValue(list.get(i-1).getGa_id());
                       row.createCell(4).setCellValue(list.get(i-1).getStatus());
                       row.createCell(7).setCellValue(list.get(i-1).getExit_ci());
                       row.createCell(8).setCellValue(list.get(i-1).getUpdate_gbn());
                       row.createCell(9).setCellValue(list.get(i-1).getRegdate());
                
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                 
            }
            
            // TOP 30
            if(divString.equals("pointChk2")){

            	List<TblStatTopUser> list = (List<TblStatTopUser>)ModelMap.get("list");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                //worksheet.setDefaultColumnWidth(15);
                worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("매체");
                row.createCell(1).setCellValue("일자");
                row.createCell(2).setCellValue("사용/적입");
                row.createCell(3).setCellValue("user_ci");
                row.createCell(4).setCellValue("cust_id");
                row.createCell(5).setCellValue("ctn");
                row.createCell(6).setCellValue("ga_id");
                row.createCell(7).setCellValue("포인트");
                
                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getMedia());
                       row.createCell(1).setCellValue(list.get(i-1).getStat_date());
                       if("I".equals(list.get(i-1).getPoint_type())){
                    	   row.createCell(2).setCellValue("적립");
            		   }else if("O".equals(list.get(i-1).getPoint_type())){
                    	   row.createCell(2).setCellValue("사용");
            		   }
                       row.createCell(3).setCellValue(list.get(i-1).getUser_ci());
                       row.createCell(4).setCellValue(list.get(i-1).getCust_id());
                       row.createCell(5).setCellValue(list.get(i-1).getCtn());
                       row.createCell(6).setCellValue(list.get(i-1).getGa_id());
                       row.createCell(7).setCellValue(list.get(i-1).getPoint());
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                 
            }
            
         // TOP 30 (매체별)
            if(divString.equals("pointChk")){

            	List<TblStatTopUser> list = (List<TblStatTopUser>)ModelMap.get("list");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                //worksheet.setDefaultColumnWidth(15);
                worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("매체");
                row.createCell(1).setCellValue("일자");
                row.createCell(2).setCellValue("사용/적입");
                row.createCell(3).setCellValue("user_ci");
                row.createCell(4).setCellValue("cust_id");
                row.createCell(5).setCellValue("ctn");
                row.createCell(6).setCellValue("ga_id");
                row.createCell(7).setCellValue("포인트");
                
                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getMedia());
                       row.createCell(1).setCellValue(list.get(i-1).getStat_date());
                       if("I".equals(list.get(i-1).getPoint_type())){
                    	   row.createCell(2).setCellValue("적립");
            		   }else if("O".equals(list.get(i-1).getPoint_type())){
                    	   row.createCell(2).setCellValue("사용");
            		   }
                       row.createCell(3).setCellValue(list.get(i-1).getUser_ci());
                       row.createCell(4).setCellValue(list.get(i-1).getCust_id());
                       row.createCell(5).setCellValue(list.get(i-1).getCtn());
                       row.createCell(6).setCellValue(list.get(i-1).getGa_id());
                       row.createCell(7).setCellValue(list.get(i-1).getPoint());
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                 
            }
            
            
            // 이상탐지
            if(divString.equals("anomalyList")){

            	List<TblStatAnomalyDetection> list = (List<TblStatAnomalyDetection>)ModelMap.get("list");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                //worksheet.setDefaultColumnWidth(15);
                worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("매체");
                row.createCell(1).setCellValue("날짜");
                row.createCell(2).setCellValue("기준초과 적립자");
                row.createCell(3).setCellValue("기준초과 사용자");
                row.createCell(4).setCellValue("시간별 총 적립포인트");
                row.createCell(5).setCellValue("시간별 총 사용포인트");
                
                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getMedia());
                       row.createCell(1).setCellValue(list.get(i-1).getStat_date() + list.get(i-1).getStat_hour());
                       row.createCell(2).setCellValue(list.get(i-1).getLimit_input_count());
                       row.createCell(3).setCellValue(list.get(i-1).getLimit_output_count());
                       row.createCell(4).setCellValue(list.get(i-1).getInput_point());
                       row.createCell(5).setCellValue(list.get(i-1).getOutput_point());
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                 
            }
            
            
            
            
            
            // 이상탐지(합계)
            if(divString.equals("anomalyList2")){

            	List<TblStatTopUser> list = (List<TblStatTopUser>)ModelMap.get("list");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                //worksheet.setDefaultColumnWidth(15);
                worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("매체");
                row.createCell(1).setCellValue("날짜 : 시간");
                row.createCell(2).setCellValue("사용/적립");
                row.createCell(3).setCellValue("user_ci");
                row.createCell(4).setCellValue("cust_id");
                row.createCell(5).setCellValue("ctn");
                row.createCell(6).setCellValue("ga_id");
                row.createCell(7).setCellValue("포인트");
                
                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue("합계");
                       row.createCell(1).setCellValue(list.get(i-1).getStat_date() + " : " + list.get(i-1).getStat_hour());
                       if("I".equals(list.get(i-1).getPoint_type())){
                    	   row.createCell(2).setCellValue("적립");
            		   }else if("O".equals(list.get(i-1).getPoint_type())){
                    	   row.createCell(2).setCellValue("사용");
            		   }
                       row.createCell(3).setCellValue(list.get(i-1).getUser_ci());
                       row.createCell(4).setCellValue(list.get(i-1).getCust_id());
                       row.createCell(5).setCellValue(list.get(i-1).getCtn());
                       row.createCell(6).setCellValue(list.get(i-1).getGa_id());
                       row.createCell(7).setCellValue(list.get(i-1).getPoint());
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                 
            }
            
         // 사용자 기록조회
            if(divString.equals("userHistory")){

            	List<PointInfoTbl> list = (List<PointInfoTbl>)ModelMap.get("infolist");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                //worksheet.setDefaultColumnWidth(15);
                worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("등록날짜");
                row.createCell(1).setCellValue("등록시간");
                row.createCell(2).setCellValue("point_type");
                row.createCell(3).setCellValue("point_value");
                row.createCell(4).setCellValue("balance");
                row.createCell(5).setCellValue("point_info_idx");
                row.createCell(6).setCellValue("point_hist_idx");
                
                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getReg_date());
                       row.createCell(1).setCellValue(list.get(i-1).getReg_time());
                       row.createCell(2).setCellValue(list.get(i-1).getPoint_type());
                       row.createCell(3).setCellValue(list.get(i-1).getPoint_value());
                       row.createCell(4).setCellValue(list.get(i-1).getBalance());
                       row.createCell(5).setCellValue(list.get(i-1).getPoint_info_idx());
                       row.createCell(6).setCellValue(list.get(i-1).getPoint_hist_idx());
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                 
            }
            
         // 포인트 조정 이력 조회
            if(divString.equals("pointUpdateHistory")){

            	List<CmsPointModify> list = (List<CmsPointModify>)ModelMap.get("list");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                //worksheet.setDefaultColumnWidth(15);
                worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("조정일");
                row.createCell(1).setCellValue("시간");
                row.createCell(2).setCellValue("user_ci");
                row.createCell(3).setCellValue("cust_id");
                row.createCell(4).setCellValue("ctn");
                row.createCell(5).setCellValue("ga_id");
                row.createCell(6).setCellValue("조정포인트");
                row.createCell(7).setCellValue("잔액");
                row.createCell(8).setCellValue("조정 사유");
                row.createCell(9).setCellValue("조정자 ID");
                
                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getSdate());
                       row.createCell(1).setCellValue(list.get(i-1).getStime());
                       row.createCell(2).setCellValue(list.get(i-1).getUser_ci());
                       row.createCell(3).setCellValue(list.get(i-1).getCust_id());
                       row.createCell(4).setCellValue(list.get(i-1).getCtn());
                       row.createCell(5).setCellValue(list.get(i-1).getGa_id());
                       row.createCell(6).setCellValue(list.get(i-1).getPoint_type());
                       row.createCell(7).setCellValue(list.get(i-1).getBalance());
                       row.createCell(8).setCellValue(list.get(i-1).getDescription());
                       row.createCell(9).setCellValue(list.get(i-1).getSend_userid());
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                 
            }
            
         // [로그] BZAD
            if(divString.equals("bzadPostLog")){
            	
            	List<BZADPostLog> list = (List<BZADPostLog>)ModelMap.get("list");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                //worksheet.setDefaultColumnWidth(20);
                worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("app_key");
                row.createCell(1).setCellValue("campaign_id");
                row.createCell(2).setCellValue("title");
                row.createCell(3).setCellValue("user_id");
                row.createCell(4).setCellValue("point");
                row.createCell(5).setCellValue("transaction_id");
                row.createCell(6).setCellValue("event_at");
                row.createCell(7).setCellValue("extra");
                row.createCell(8).setCellValue("is_media");
                row.createCell(9).setCellValue("action_type");
                row.createCell(10).setCellValue("reg_date");
                row.createCell(11).setCellValue("reg_time");

                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getApp_key());
                       row.createCell(1).setCellValue(list.get(i-1).getCampaign_id());
                       row.createCell(2).setCellValue(list.get(i-1).getTitle());
                       row.createCell(3).setCellValue(list.get(i-1).getUser_id());
                       row.createCell(4).setCellValue(list.get(i-1).getPoint());
                       row.createCell(5).setCellValue(list.get(i-1).getTransaction_id());
                       row.createCell(6).setCellValue(list.get(i-1).getEvent_at());
                       row.createCell(7).setCellValue(list.get(i-1).getExtra());
                       row.createCell(8).setCellValue(list.get(i-1).getIs_media());
                       row.createCell(9).setCellValue(list.get(i-1).getAction_type());
                       row.createCell(10).setCellValue(list.get(i-1).getReg_date());
                       row.createCell(11).setCellValue(list.get(i-1).getReg_time());
                
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                  
                 
            }
            
         // [로그] BZSCREEN
            if(divString.equals("bzScreenPostLog")){
            	
            	List<BZScreenPostLog> list = (List<BZScreenPostLog>)ModelMap.get("list");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                //worksheet.setDefaultColumnWidth(20);
                worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("transaction_id");
                row.createCell(1).setCellValue("user_id");
                row.createCell(2).setCellValue("campaign_id");
                row.createCell(3).setCellValue("campaign_name");
                row.createCell(4).setCellValue("event_at");
                row.createCell(5).setCellValue("is_media");
                row.createCell(6).setCellValue("extra");
                row.createCell(7).setCellValue("action_type");
                row.createCell(8).setCellValue("point");
                row.createCell(9).setCellValue("base_point");
                row.createCell(10).setCellValue("data");
                row.createCell(11).setCellValue("reg_date");
                row.createCell(12).setCellValue("reg_time");

                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getTransaction_id());
                       row.createCell(1).setCellValue(list.get(i-1).getUser_id());
                       row.createCell(2).setCellValue(list.get(i-1).getCampaign_id());
                       row.createCell(3).setCellValue(list.get(i-1).getCampaign_name());
                       row.createCell(4).setCellValue(list.get(i-1).getEvent_at());
                       row.createCell(5).setCellValue(list.get(i-1).getIs_media());
                       row.createCell(6).setCellValue(list.get(i-1).getExtra());
                       row.createCell(7).setCellValue(list.get(i-1).getAction_type());
                       row.createCell(8).setCellValue(list.get(i-1).getPoint());
                       row.createCell(9).setCellValue(list.get(i-1).getBase_point());
                       row.createCell(10).setCellValue(list.get(i-1).getData());
                       row.createCell(11).setCellValue(list.get(i-1).getReg_date());
                       row.createCell(12).setCellValue(list.get(i-1).getReg_time());
                
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                  
                 
            }
            
         // SMS 발송기록
            if(divString.equals("smsSend")){
            	
            	List<SMSSend> list = (List<SMSSend>)ModelMap.get("list");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                worksheet.setDefaultColumnWidth(20);
                //worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("sms_idx");
                row.createCell(1).setCellValue("send_userid");
                row.createCell(2).setCellValue("username");
                row.createCell(3).setCellValue("msg_content");
                row.createCell(4).setCellValue("call_ctn");
                row.createCell(5).setCellValue("rcv_ctn");
                row.createCell(6).setCellValue("success_yn");
                row.createCell(7).setCellValue("response");
                row.createCell(8).setCellValue("ipaddr");
                row.createCell(9).setCellValue("regdate");

                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getSms_idx());
                       row.createCell(1).setCellValue(list.get(i-1).getSend_userid());
                       row.createCell(2).setCellValue(list.get(i-1).getUsername());
                       row.createCell(3).setCellValue(list.get(i-1).getMsg_content());
                       row.createCell(4).setCellValue(list.get(i-1).getCall_ctn());
                       row.createCell(5).setCellValue(list.get(i-1).getRcv_ctn());
                       row.createCell(6).setCellValue(list.get(i-1).getSuccess_yn());
                       row.createCell(7).setCellValue(list.get(i-1).getResponse());
                       row.createCell(8).setCellValue(list.get(i-1).getIpaddr());
                       row.createCell(9).setCellValue(list.get(i-1).getRegdate());
                
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                  
                 
            }
            
            
            
         // 엑세스로그
            if(divString.equals("statConnect")){
            	
            	List<StatConnect> list = (List<StatConnect>)ModelMap.get("list");
            	
                worksheet = workbook.createSheet(excelName+ " WorkSheet");
                excelName=URLEncoder.encode(excelName,"UTF-8");
                worksheet.setDefaultColumnWidth(20);
                //worksheet.autoSizeColumn(0);
                
                row = worksheet.createRow(0);
                row.createCell(0).setCellValue("year");
                row.createCell(1).setCellValue("month");
                row.createCell(2).setCellValue("day");
                row.createCell(3).setCellValue("hour");
                row.createCell(4).setCellValue("stat_type");
                row.createCell(5).setCellValue("stat_code");
                row.createCell(6).setCellValue("count");
                row.createCell(7).setCellValue("regdate");

                for(int i=1 ; i < list.size()+1 ; i++){
                       row = worksheet.createRow(i);
                       row.createCell(0).setCellValue(list.get(i-1).getYear());
                       row.createCell(1).setCellValue(list.get(i-1).getMonth());
                       row.createCell(2).setCellValue(list.get(i-1).getDay());
                       row.createCell(3).setCellValue(list.get(i-1).getHour());
                       row.createCell(4).setCellValue(list.get(i-1).getStat_type());
                       row.createCell(5).setCellValue(list.get(i-1).getStat_code());
                       row.createCell(6).setCellValue(list.get(i-1).getCount());
                       row.createCell(7).setCellValue(list.get(i-1).getRegdate());
                
                }
                 
                 response.setContentType("Application/Msexcel");
                 response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xls");
                  
                 
            }
            
            
            
            
       }
}
