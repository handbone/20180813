/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.common.download;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


@Controller
public class ExcelDownloadController {
	
	Logger logger = Logger.getLogger(ExcelDownloadController.class.getName());
	
	@RequestMapping("/excelDownload.do")
	public String download(HttpServletRequest request,
			@RequestParam(value="data",required=false) String excelJson,
			@RequestParam(value="pageNm",required=false) String pageNm,
			@RequestParam(value="page",required=false) String page,
			Map<String,Object> ModelMap) throws JsonParseException, JsonMappingException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		
		logger.debug("excelDownload Page start!!!");
		logger.debug("[excelJson] :"+excelJson);
		logger.debug("[pageNm] :"+pageNm);
		logger.debug("[page] :"+page);
		
		//Map형태로 변환위해 일부 문자열 변환
		String arrJsonStr = excelJson.replaceAll("&quot;", "\"");
		arrJsonStr = arrJsonStr.replace("[", "");
		arrJsonStr = arrJsonStr.replace("]", "");
		arrJsonStr = arrJsonStr.replace("},{", "}_{");
		
		
		Pattern p =Pattern.compile("_");
		
		//json str 구분자로 잘라서 Map에 넣어서 List에 추가
		String[] spStr =  p.split(arrJsonStr);
		ObjectMapper mapper  = new ObjectMapper();
		List<Map<String,String>> list = new ArrayList<Map<String,String>>();
		HashMap<String, String> map = new HashMap<String, String>();
		for(String s:spStr){
			map = mapper.readValue(s, 
	                new TypeReference<HashMap<String, String>>() {});  
			list.add(map);
		}
		 
		ModelMap.put("list", list); //리스트
		ModelMap.put("pageNm", pageNm); //페이지 이름 (출력 파일명)
		ModelMap.put("page", page); //페이지 구분명 플래그
		return "excelView";
	}
	
	
}
