package com.wiz.clip.common.sendAPI;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wiz.clip.common.crypto.AES256CipherClip;

@Controller
public class sendMinusPoint {
	
	Logger logger = Logger.getLogger(sendMinusPoint.class.getName());

	@Autowired
	private SqlSession postgreSession;

	
	//TODO plusPoint도 가져오는 로직으로 바꿔야 한다.
	
	@RequestMapping(value = "/sendMinusPoint.do")
	@ResponseBody
	public String sendSMStest(@RequestParam String ctn, @RequestParam String point_value )  throws java.lang.Exception {
	
		//String criptCtn = new String(AES256CipherClip.AES_Encode(ctn).getBytes());
		//logger.debug("criptCtn::::::::::::::::::::::::"+criptCtn);
		//logger.debug("point_value::::::::::::::::::::::::"+point_value);
		
		//TODO user_ci도 가져오는 로직으로 바꿔야 한다.
		String userCi = "";
		if("1".equals(ctn)){
			userCi = "gSM4rEFd57zU/2+TMYC2MWxR+galievaew7nZ19+M+bWa7XQuZvbzbKHZI3XoRFNuDGWJT8MTcno6dvCBEEaOA==";
		}
		
		
		
		//TODO 프로퍼티 파일로 빼내야 한다.
		//test
		//String smsDomain = "http://clippointsvr.smarthubs.co.kr/minusPoint.do";
		//real 내부
		//String smsDomain = "http://172.31.50.29:8080/minusPoint.do";		
		//real 외부
		String smsDomain = "http://clippointsvc.clipwallet.co.kr/minusPoint.do";
				
		String tr_id = String.valueOf(System.currentTimeMillis());
		if("".equals(tr_id) || null == tr_id){
			throw new java.lang.Exception();
		}
		StringBuffer sb2 = new StringBuffer(tr_id);
		if (tr_id.length() < 16) {
			Random rand = new Random(System.currentTimeMillis()); 
			sb2.append(Math.abs(rand.nextInt(999)));
		}
		if (tr_id.length() < 16) {
			while (sb2.length() < 16) {
				sb2.append("0");
			}
		}
		tr_id = sb2.toString();
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("user_ci=");
		sb.append(java.net.URLEncoder.encode(userCi,"UTF-8"));
		sb.append("&transaction_id=");
		sb.append(tr_id);
		sb.append("&requester_code=");
		sb.append("pointUpdate");
		sb.append("&point_value=");
		sb.append(point_value);
		sb.append("&description=");
		sb.append(java.net.URLEncoder.encode("포인트 조정","UTF-8"));
				
		logger.debug("sb ::::"+sb.toString());
		
		BufferedReader oBufReader = null;
		HttpURLConnection httpConn = null;
		String strBuffer = "";
		String strRslt = "";
		
		String strEncodeUrl = smsDomain;

		URL oOpenURL = new URL(strEncodeUrl);
		httpConn = (HttpURLConnection) oOpenURL.openConnection();
		httpConn.setDoOutput(true);
		httpConn.setRequestMethod("POST");
		httpConn.setConnectTimeout(10000);
		httpConn.setReadTimeout(10000);
		httpConn.setRequestProperty("Content-Length", String.valueOf(sb.length()));
		//httpConn.setRequestProperty("Content-Type", "application/soap+xml; charset=utf-8");
		OutputStream os = httpConn.getOutputStream();
		os.write(sb.toString().getBytes());
		os.flush();
		os.close();
		oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
		StringBuffer sb3 = new StringBuffer(strRslt);
			
		while ((strBuffer = oBufReader.readLine()) != null) {
			if (strBuffer.length() > 1) {
				sb3.append(strBuffer);
			}
		}
		oBufReader.close();
			
		return sb3.toString();
	}
	
	
}
