package com.wiz.clip.common.sendAPI;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wiz.clip.common.crypto.AES256CipherClip;
import com.wiz.clip.common.crypto.HmacCrypto;

@Controller
public class sendPush {
	
	Logger logger = Logger.getLogger(sendPush.class.getName());
	
	
	@Value("#{config['API_DOMAIN_SEND_PUSH']}")
	String API_DOMAIN_SEND_PUSH;
	
	@Value("#{config['API_DOMAIN_SEND_MESSAGE']}")
	String API_DOMAIN_SEND_MESSAGE;
	@Value("#{config['API_DOMAIN_SEND_TITLE']}")
	String API_DOMAIN_SEND_TITLE;
	
	
	@RequestMapping(value = "/sendPush.do")
	@ResponseBody
	public String sendPush(
			@RequestParam String type, 
			@RequestParam String id)  throws java.lang.Exception {
	
		String result="";
		try{
			
			String smsMessage = API_DOMAIN_SEND_MESSAGE;
			String pushTitle = API_DOMAIN_SEND_TITLE;
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MINUTE, 30);
			
			
			SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmss");

			String expireDate = format1.format(cal.getTime());
			System.out.println(expireDate);
			
		
			//java.net.URLEncoder.encode(user_id,"UTF-8")
			//AES256CipherClip.AES_Encode(textStr);
			String date_type =type;
			String data_id =id;
			String title =pushTitle;
			String content = smsMessage;
			String agreeCheckYn	 ="N";
			String sendServiceType	 ="01";
			String linkId	 ="clippoint";
			String sendexpireDate =AES256CipherClip.AES_Encode(expireDate);
			String imgUrl =null;
			String randingUrl ="KTolleh00114://point";
			String signdata = agreeCheckYn + sendServiceType + linkId + date_type +AES256CipherClip.AES_Decode(data_id) + imgUrl + randingUrl + expireDate + title +content;
			signdata = signdata +"push" + "external" + "getPushInfo";
			String encSigndata = HmacCrypto.getHmacVal(signdata);
			
			JSONObject jObject = new JSONObject();
			
			jObject.put("type", type);
			jObject.put("id", id);
			jObject.put("title", pushTitle);
			jObject.put("content", smsMessage);
			
			jObject.put("agreeCheckYn", agreeCheckYn);
			jObject.put("sendServiceType", sendServiceType);
			jObject.put("linkId", linkId);
			jObject.put("expireDate", sendexpireDate);
			jObject.put("sign", encSigndata);
			
			String params = jObject.toJSONString();
			
		
	
			System.out.println("parameter ::::: "+params);
			
	
		 result =	sendPush(params);
		} catch (Exception e){
			logger.error("",e);
			
		}
		return result;
	}
	
	
	public String sendPush(String params) throws java.lang.Exception {
		
		BufferedReader oBufReader = null;
		String strBuffer = "";
		String strRslt = "";
		
		String strEncodeUrl = API_DOMAIN_SEND_PUSH;
		
		logger.debug("::::::::: "+strEncodeUrl+"?"+params);
		
		URL oOpenURL = new URL(strEncodeUrl);
		
		//HttpURLConnection httpConn = null;
		//httpConn = (HttpURLConnection) oOpenURL.openConnection();
		
		HttpURLConnection httpConn = (HttpURLConnection) oOpenURL.openConnection();
//		HttpsURLConnection.setDefaultHostnameVerifier( new HostnameVerifier(){
//		    public boolean verify(String string,SSLSession ssls) {
//		     return true;
//		    }
//		});
		
		httpConn.setDoOutput(true);
		httpConn.setRequestMethod("POST");
		httpConn.setConnectTimeout(5000);
		httpConn.setReadTimeout(10000);
		
		httpConn.setDoInput(true);
		httpConn.setRequestProperty("Accept", "application/json");
		httpConn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		
		httpConn.setRequestProperty("Content-Length", String.valueOf(params.length()));
		OutputStream os = httpConn.getOutputStream();
		os.write(params.getBytes());
		os.flush();
		os.close();
		
		int responseCode = httpConn.getResponseCode();
		
		if( HttpURLConnection.HTTP_OK == responseCode) {
			logger.info("responseCode >>>> : " +responseCode);
			oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream(),"UTF-8"));
		} else {
			logger.info("responseCode etc >>>> : " +responseCode);
			oBufReader = new BufferedReader(new InputStreamReader(httpConn.getErrorStream(),"UTF-8"));
		}
	

		StringBuffer sb = new StringBuffer(strRslt);
		
		int i = 1;
		while ((strBuffer = oBufReader.readLine()) != null && (i < 2)) {
			if (strBuffer.length() > 1) {
				sb.append(strBuffer);
			}
		}

		oBufReader.close();
			
		
		return sb.toString();
	}
	
	




}
