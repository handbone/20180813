package com.wiz.clip.test.dao;

public class PushDAO {
	
	private String TYPE;	
	private String ID;	
	private String TITLE;	
	private String CONTENT;	
	private String IMGURL;	
	private String RANDINGURL;	
	private String AGREECHECKYN;	
	private String SENDSERVICETYPE;	
	private String LINKID;	
	private String EXPIREDATE;
	
	
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getTITLE() {
		return TITLE;
	}
	public void setTITLE(String tITLE) {
		TITLE = tITLE;
	}
	public String getCONTENT() {
		return CONTENT;
	}
	public void setCONTENT(String cONTENT) {
		CONTENT = cONTENT;
	}
	public String getIMGURL() {
		return IMGURL;
	}
	public void setIMGURL(String iMGURL) {
		IMGURL = iMGURL;
	}
	public String getRANDINGURL() {
		return RANDINGURL;
	}
	public void setRANDINGURL(String rANDINGURL) {
		RANDINGURL = rANDINGURL;
	}
	public String getAGREECHECKYN() {
		return AGREECHECKYN;
	}
	public void setAGREECHECKYN(String aGREECHECKYN) {
		AGREECHECKYN = aGREECHECKYN;
	}
	public String getSENDSERVICETYPE() {
		return SENDSERVICETYPE;
	}
	public void setSENDSERVICETYPE(String sENDSERVICETYPE) {
		SENDSERVICETYPE = sENDSERVICETYPE;
	}
	public String getLINKID() {
		return LINKID;
	}
	public void setLINKID(String lINKID) {
		LINKID = lINKID;
	}
	public String getEXPIREDATE() {
		return EXPIREDATE;
	}
	public void setEXPIREDATE(String eXPIREDATE) {
		EXPIREDATE = eXPIREDATE;
	}

}
