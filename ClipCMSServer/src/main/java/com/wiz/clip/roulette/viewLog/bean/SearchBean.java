/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.roulette.viewLog.bean;

import com.wiz.clip.common.page.PageBean;

public class SearchBean  extends PageBean {
	private String status;
	private String keywd;
	private String roulette_id;
	private String 	dateType;
	private String startdate;
	private String enddate;
	
	private String cust_id;
	private String ga_id;
	private String ctn;
	
	private String excelDownload;
	
	private String idx;
	private String area01;
	private String area02;
	private String area03;
	private String area04;
	private String area05;
	private String area06;
	
	
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getCust_id() {
		return cust_id;
	}
	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}
	public String getGa_id() {
		return ga_id;
	}
	public void setGa_id(String ga_id) {
		this.ga_id = ga_id;
	}
	public String getCtn() {
		return ctn;
	}
	public void setCtn(String ctn) {
		this.ctn = ctn;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getKeywd() {
		return keywd;
	}
	public void setKeywd(String keywd) {
		this.keywd = keywd;
	}
	public String getRoulette_id() {
		return roulette_id;
	}
	public void setRoulette_id(String roulette_id) {
		this.roulette_id = roulette_id;
	}
	public String getDateType() {
		return dateType;
	}
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getExcelDownload() {
		return excelDownload;
	}
	public void setExcelDownload(String excelDownload) {
		this.excelDownload = excelDownload;
	}
	public String getArea01() {
		return area01;
	}
	public void setArea01(String area01) {
		this.area01 = area01;
	}
	public String getArea02() {
		return area02;
	}
	public void setArea02(String area02) {
		this.area02 = area02;
	}
	public String getArea03() {
		return area03;
	}
	public void setArea03(String area03) {
		this.area03 = area03;
	}
	public String getArea04() {
		return area04;
	}
	public void setArea04(String area04) {
		this.area04 = area04;
	}
	public String getArea05() {
		return area05;
	}
	public void setArea05(String area05) {
		this.area05 = area05;
	}
	public String getArea06() {
		return area06;
	}
	public void setArea06(String area06) {
		this.area06 = area06;
	}
	
	
	
}
