/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.roulette.viewLog.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.wiz.clip.common.paging.BoardPaging;
import com.wiz.clip.roulette.channel.bean.PointRoulette;
import com.wiz.clip.roulette.viewLog.bean.PointRouletteJoin;
import com.wiz.clip.roulette.viewLog.bean.SearchBean;
import com.wiz.clip.roulette.viewLog.bean.ViewLogBean;
import com.wiz.clip.roulette.viewLog.service.ViewLogService;

@Controller
public class ViewLogController  {
	
	Logger logger = Logger.getLogger(ViewLogController.class.getName());

	@Value("#{config['PAGE_SIZE']}") int PAGE_SIZE;
	
	@Autowired
	private ViewLogService viewLogService;

	/*
	 * [통계] 채널별 참여자
	 */
	@RequestMapping(value = "/roulette/viewLog/joinByChannel.do")
	public ModelAndView joinByChannel(@RequestParam(value = "pg", required = false) String page,
			SearchBean searchBean, ModelMap map) {

		/*logger.debug("status :" + searchBean.getRoulette_id());
		logger.debug("dateType :" + searchBean.getDateType());
		logger.debug("startdate :" + searchBean.getStartdate());
		logger.debug("enddate :" + searchBean.getEnddate());*/


		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));

		int totalCnt = viewLogService.joinByChannelCnt(searchBean);
		
		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		
		List<PointRoulette> channelNameList = viewLogService.getChannelNameList(map);
		
		List<ViewLogBean> list = viewLogService.joinByChannel(searchBean);

		ModelAndView mav = new ModelAndView();
		mav.addObject("channelNameList", channelNameList);
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("menu1", "룰렛관리");
		mav.addObject("menu2", "[통계] 채널별 참여자");
		mav.addObject("display", "../clip/roulette/viewLog/joinByChannel.jsp");
		
		if("Y".equals(searchBean.getExcelDownload())){
			mav.addObject("pageNm", "채널별참여자");
			mav.addObject("divString", "joinByChannel");
			mav.setViewName("excelView");
		}else{
			mav.setViewName("/main/index");
		}
		
		return mav;
	}

	
	/*
	 * [통계] 채널별 당첨내역
	 */
	@RequestMapping(value = "/roulette/viewLog/detailByChannel.do")
	public ModelAndView detailByChannel(@RequestParam(value = "pg", required = false) String page,
			SearchBean searchBean, ModelMap map) {


		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));

		int totalCnt = viewLogService.detailByChannelCnt(searchBean);
		
		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		
		List<PointRoulette> channelNameList = viewLogService.getChannelNameList(map);
		
		List<ViewLogBean> list = viewLogService.detailByChannel(searchBean);

		ModelAndView mav = new ModelAndView();
		mav.addObject("channelNameList", channelNameList);
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("menu1", "룰렛관리");
		mav.addObject("menu2", "[통계] 채널별 당첨내역");
		mav.addObject("display", "../clip/roulette/viewLog/detailByChannel.jsp");

		if("Y".equals(searchBean.getExcelDownload())){
			mav.addObject("pageNm", "채널별당첨내역");
			mav.addObject("divString", "detailByChannel");
			mav.setViewName("excelView");
		}else{
			mav.setViewName("/main/index");
		}
		
		return mav;
	}
	
	/*
	 * [통계] 페이지뷰		- 현재 사용하고 있지 않은 페이지
	 */
	@RequestMapping(value = "/roulette/viewLog/pageView.do")
	public ModelAndView pageView(@RequestParam(value = "pg", required = false) String page,
			SearchBean searchBean, ModelMap map) {


		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));

		int totalCnt = viewLogService.pageViewCnt(searchBean);
		
		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		
		List<PointRoulette> channelNameList = viewLogService.getChannelNameList(map);
		
		List<ViewLogBean> list = null;
		if(searchBean.getStartdate() != "" && searchBean.getStartdate() != null ){
			list = viewLogService.pageView(searchBean);
		}

		ModelAndView mav = new ModelAndView();
		mav.addObject("channelNameList", channelNameList);
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("menu1", "룰렛관리");
		mav.addObject("menu2", "[통계] 페이지뷰");
		mav.addObject("display", "../clip/roulette/viewLog/pageView.jsp");

		if("Y".equals(searchBean.getExcelDownload())){
			mav.addObject("pageNm", "페이지뷰");
			mav.addObject("divString", "pageView");
			mav.setViewName("excelView");
		}else{
			mav.setViewName("/main/index");
		}
		
		return mav;
	}
	
	/*
	 * [로그] 참여자 로그
	 */
	@RequestMapping(value = "/roulette/viewLog/join.do")
	public ModelAndView join(@RequestParam(value = "pg", required = false) String page,
			SearchBean searchBean, ModelMap map) {


		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));

		int totalCnt = viewLogService.joinCnt(searchBean);
		
		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		
		List<PointRouletteJoin> list = viewLogService.join(searchBean);

		ModelAndView mav = new ModelAndView();
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("menu1", "룰렛관리");
		mav.addObject("menu2", "[로그] 참여자 로그");
		mav.addObject("display", "../clip/roulette/viewLog/join.jsp");

		if("Y".equals(searchBean.getExcelDownload())){
			mav.addObject("pageNm", "참여자로그");
			mav.addObject("divString", "join");
			mav.setViewName("excelView");
		}else{
			mav.setViewName("/main/index");
		}
		
		return mav;
	}
	
	
	
	/*
	 * [로그] API연동 로그
	 */
	@RequestMapping(value = "/roulette/viewLog/connectAPI.do")
	public ModelAndView connectAPI(@RequestParam(value = "pg", required = false) String page,
			SearchBean searchBean, ModelMap map) {


		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));

		int totalCnt = viewLogService.connectAPICnt(searchBean);
		
		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		
		List<PointRoulette> list = viewLogService.connectAPI(searchBean);

		ModelAndView mav = new ModelAndView();
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("menu1", "룰렛관리");
		mav.addObject("menu2", "[로그] API연동 로그");
		mav.addObject("display", "../clip/roulette/viewLog/connectAPI.jsp");

		if("Y".equals(searchBean.getExcelDownload())){
			mav.addObject("pageNm", "API연동로그");
			mav.addObject("divString", "connectAPI");
			mav.setViewName("excelView");
		}else{
			mav.setViewName("/main/index");
		}
		
		return mav;
	}
	
	/*
	 * [로그] 페이지뷰 로그		- 현재 사용하고 있지 않은 페이지
	 */
	@RequestMapping(value = "/roulette/viewLog/joinEvent.do")
	public ModelAndView joinEvent(@RequestParam(value = "pg", required = false) String page,
			SearchBean searchBean, ModelMap map) {


		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));

		int totalCnt = viewLogService.joinEventCnt(searchBean);
		
		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		
		List<PointRoulette> list = viewLogService.joinEvent(searchBean);

		ModelAndView mav = new ModelAndView();
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("menu1", "룰렛관리");
		mav.addObject("menu2", "[로그] 페이지뷰 로그");
		mav.addObject("display", "../clip/roulette/viewLog/joinEvent.jsp");

		if("Y".equals(searchBean.getExcelDownload())){
			mav.addObject("pageNm", "페이지뷰로그");
			mav.addObject("divString", "joinEvent");
			mav.setViewName("excelView");
		}else{
			mav.setViewName("/main/index");
		}
		
		return mav;
	}
	
	/*
	 * [로그] 일자별 당첨자합계 로그
	 */
	@RequestMapping(value = "/roulette/viewLog/dailyJoinSum.do")
	public ModelAndView dailyJoinSum(@RequestParam(value = "pg", required = false) String page,
			SearchBean searchBean, ModelMap map) {


		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));

		int totalCnt = viewLogService.dailyJoinSumCnt(searchBean);
		
		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		
		List<PointRoulette> channelNameList = viewLogService.getChannelNameList(map);
		
		List<PointRoulette> list = viewLogService.dailyJoinSum(searchBean);

		ModelAndView mav = new ModelAndView();
		mav.addObject("channelNameList", channelNameList);
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("menu1", "룰렛관리");
		mav.addObject("menu2", "[로그] 일자별 당첨자합계 로그");
		mav.addObject("display", "../clip/roulette/viewLog/dailyJoinSum.jsp");

		if("Y".equals(searchBean.getExcelDownload())){
			mav.addObject("pageNm", "일자별당첨자합계로그");
			mav.addObject("divString", "dailyJoinSum");
			mav.setViewName("excelView");
		}else{
			mav.setViewName("/main/index");
		}
		
		return mav;
	}
	
	/*
	 * [로그] 시간당첨값 로그
	 */
	@RequestMapping(value = "/roulette/viewLog/timeGbn.do")
	public ModelAndView timeGbn(@RequestParam(value = "pg", required = false) String page,
			SearchBean searchBean, ModelMap map) {

		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));

		int totalCnt = viewLogService.timeGbnCnt(searchBean);
		
		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		
		List<PointRoulette> channelNameList = viewLogService.getChannelNameList(map);
		
		List<PointRoulette> list = viewLogService.timeGbn(searchBean);

		ModelAndView mav = new ModelAndView();
		mav.addObject("channelNameList", channelNameList);
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("menu1", "룰렛관리");
		mav.addObject("menu2", "[로그] 시간당첨값 로그");
		mav.addObject("display", "../clip/roulette/viewLog/timeGbn.jsp");

		if("Y".equals(searchBean.getExcelDownload())){
			mav.addObject("pageNm", "시간당첨값로그");
			mav.addObject("divString", "timeGbn");
			mav.setViewName("excelView");
		}else{
			mav.setViewName("/main/index");
		}
		
		return mav;
	}
	
	
	
	/*
	 * [로그] 일자별 당첨자합계 로그 업데이트
	 */
	@RequestMapping(value = "/roulette/viewLog/dailyJoinSumUpdate.do", method = { RequestMethod.POST })
	@ResponseBody
	public String dailyJoinSumUpdate(
			SearchBean searchBean, 
			HttpServletRequest request, HttpServletResponse response
	) {
		logger.debug("dailyJoinSumUpdate Page start!!!");

		int result = viewLogService.dailyJoinSumUpdate(searchBean);
		
		ModelMap map = new ModelMap();
		if (result >= 1) {
			map.put("result", "success");
			return JSONValue.toJSONString(map);
		} else {
			map.put("result", "fail");
			return JSONValue.toJSONString(map);
		}
		
	}
	
	

}
