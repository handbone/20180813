/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.roulette.viewLog.service;

import java.util.List;

import org.springframework.ui.ModelMap;

import com.wiz.clip.roulette.channel.bean.PointRoulette;
import com.wiz.clip.roulette.viewLog.bean.PointRouletteJoin;
import com.wiz.clip.roulette.viewLog.bean.SearchBean;
import com.wiz.clip.roulette.viewLog.bean.ViewLogBean;

public interface ViewLogService {

	List<ViewLogBean> joinByChannel(SearchBean searchBean);

	List<ViewLogBean> detailByChannel(SearchBean searchBean);

	List<PointRouletteJoin> join(SearchBean searchBean);

	List<ViewLogBean> pageView(SearchBean searchBean);

	List<PointRoulette> connectAPI(SearchBean searchBean);

	List<PointRoulette> joinEvent(SearchBean searchBean);

	List<PointRoulette> timeGbn(SearchBean searchBean);

	List<PointRoulette> dailyJoinSum(SearchBean searchBean);

	List<PointRoulette> getChannelNameList(ModelMap map);

	int dailyJoinSumUpdate(SearchBean searchBean);

	int joinByChannelCnt(SearchBean searchBean);

	int detailByChannelCnt(SearchBean searchBean);

	int pageViewCnt(SearchBean searchBean);

	int joinCnt(SearchBean searchBean);

	int connectAPICnt(SearchBean searchBean);

	int joinEventCnt(SearchBean searchBean);

	int dailyJoinSumCnt(SearchBean searchBean);

	int timeGbnCnt(SearchBean searchBean);


}
