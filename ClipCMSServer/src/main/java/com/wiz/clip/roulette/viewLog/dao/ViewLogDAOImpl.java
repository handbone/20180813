/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.roulette.viewLog.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.wiz.clip.roulette.channel.bean.PointRoulette;
import com.wiz.clip.roulette.viewLog.bean.PointRouletteJoin;
import com.wiz.clip.roulette.viewLog.bean.SearchBean;
import com.wiz.clip.roulette.viewLog.bean.ViewLogBean;



@Repository
public class ViewLogDAOImpl implements ViewLogDAO  {

	@Autowired
	private SqlSession postgreSession;

	Logger logger = Logger.getLogger(ViewLogDAOImpl.class.getName());

	@Override
	public List<ViewLogBean> joinByChannel(SearchBean searchBean) {
		if("day".equals(searchBean.getDateType())){
			return postgreSession.selectList("VIEWLOG.joinByChannelDay", searchBean);
		}else if("month".equals(searchBean.getDateType())){
			return postgreSession.selectList("VIEWLOG.joinByChannelMonth", searchBean);
		}else if("hour".equals(searchBean.getDateType())){
			return postgreSession.selectList("VIEWLOG.joinByChannelHour", searchBean);
		}
		return null;
	}
	@Override
	public int joinByChannelCnt(SearchBean searchBean) {
		if("day".equals(searchBean.getDateType())){
			return postgreSession.selectOne("VIEWLOG.joinByChannelDayCnt", searchBean);
		}else if("month".equals(searchBean.getDateType())){
			return postgreSession.selectOne("VIEWLOG.joinByChannelMonthCnt", searchBean);
		}else if("hour".equals(searchBean.getDateType())){
			return postgreSession.selectOne("VIEWLOG.joinByChannelHourCnt", searchBean);
		}
		return 0;
	}
	

	@Override
	public List<ViewLogBean> detailByChannel(SearchBean searchBean) {
		if("day".equals(searchBean.getDateType())){
			return postgreSession.selectList("VIEWLOG.detailByChannelDay", searchBean);
		}else if("month".equals(searchBean.getDateType())){
			return postgreSession.selectList("VIEWLOG.detailByChannelMonth", searchBean);
		}
		return null;
	}
	@Override
	public int detailByChannelCnt(SearchBean searchBean) {
		if("day".equals(searchBean.getDateType())){
			return postgreSession.selectOne("VIEWLOG.detailByChannelDayCnt", searchBean);
		}else if("month".equals(searchBean.getDateType())){
			return postgreSession.selectOne("VIEWLOG.detailByChannelMonthCnt", searchBean);
		}
		return 0;
	}


	@Override
	public List<PointRouletteJoin> join(SearchBean searchBean) {
		return postgreSession.selectList("VIEWLOG.join", searchBean);
	}
	@Override
	public int joinCnt(SearchBean searchBean) {
		return postgreSession.selectOne("VIEWLOG.joinCnt", searchBean);
	}

	
	@Override
	public List<ViewLogBean> pageViewDay(SearchBean searchBean) {
		return postgreSession.selectList("VIEWLOG.pageViewDay", searchBean);
	}
	@Override
	public int pageViewDayCnt(SearchBean searchBean) {
		return postgreSession.selectOne("VIEWLOG.pageViewDayCnt", searchBean);
	}
	
	@Override
	public List<ViewLogBean> pageViewMonth(SearchBean searchBean) {
		return postgreSession.selectList("VIEWLOG.pageViewMonth", searchBean);
	}
	@Override
	public int pageViewMonthCnt(SearchBean searchBean) {
		return postgreSession.selectOne("VIEWLOG.pageViewMonthCnt", searchBean);
	}
	
	@Override
	public List<PointRoulette> connectAPI(SearchBean searchBean) {
		return postgreSession.selectList("VIEWLOG.connectAPI", searchBean);
	}
	@Override
	public int connectAPICnt(SearchBean searchBean) {
		return postgreSession.selectOne("VIEWLOG.connectAPICnt", searchBean);
	}
	
	@Override
	public List<PointRoulette> joinEvent(SearchBean searchBean) {
		return postgreSession.selectList("VIEWLOG.joinEvent", searchBean);
	}
	@Override
	public int joinEventCnt(SearchBean searchBean) {
		return postgreSession.selectOne("VIEWLOG.joinEventCnt", searchBean);
	}

	@Override
	public List<PointRoulette> timeGbn(SearchBean searchBean) {
		return postgreSession.selectList("VIEWLOG.timeGbn", searchBean);
	}
	@Override
	public int timeGbnCnt(SearchBean searchBean) {
		return postgreSession.selectOne("VIEWLOG.timeGbnCnt", searchBean);
	}
	
	@Override
	public List<PointRoulette> dailyJoinSum(SearchBean searchBean) {
		return postgreSession.selectList("VIEWLOG.dailyJoinSum", searchBean);
	}
	@Override
	public int dailyJoinSumCnt(SearchBean searchBean) {
		return postgreSession.selectOne("VIEWLOG.dailyJoinSumCnt", searchBean);
	}
	
	@Override
	public List<PointRoulette> getChannelNameList(ModelMap map) {
		return postgreSession.selectList("VIEWLOG.getChannelNameList", map);
	}

	@Override
	public int dailyJoinSumUpdate(SearchBean searchBean) {
		return postgreSession.update("VIEWLOG.dailyJoinSumUpdate", searchBean);
	}

	

	
	
	

	

	

	
	

	
	
	
	

}
