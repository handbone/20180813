/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.roulette.viewLog.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.wiz.clip.common.crypto.AES256CipherClip;
import com.wiz.clip.roulette.channel.bean.PointRoulette;
import com.wiz.clip.roulette.viewLog.bean.PointRouletteJoin;
import com.wiz.clip.roulette.viewLog.bean.SearchBean;
import com.wiz.clip.roulette.viewLog.bean.ViewLogBean;
import com.wiz.clip.roulette.viewLog.dao.ViewLogDAO;
	 

@Service
@Transactional
public class ViewLogServiceImpl implements ViewLogService {
	
	@Autowired
	private ViewLogDAO viewLogDao;

	Logger logger = Logger.getLogger(ViewLogServiceImpl.class.getName());

	@Override
	public List<ViewLogBean> joinByChannel(SearchBean searchBean) {
		return viewLogDao.joinByChannel(searchBean);
	}

	@Override
	public int joinByChannelCnt(SearchBean searchBean) {
		return viewLogDao.joinByChannelCnt(searchBean);
	}
	
	@Override
	public List<ViewLogBean> detailByChannel(SearchBean searchBean) {
		return viewLogDao.detailByChannel(searchBean);
	}
	@Override
	public int detailByChannelCnt(SearchBean searchBean) {
		return viewLogDao.detailByChannelCnt(searchBean);
	}

	@Override
	public List<PointRouletteJoin> join(SearchBean searchBean) {
		try {
			if("cust_id".equals(searchBean.getStatus())){
				searchBean.setCust_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
				searchBean.setCtn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}else if("ctn".equals(searchBean.getStatus())){
				searchBean.setCtn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}else if("ga_id".equals(searchBean.getStatus())){
				searchBean.setGa_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return viewLogDao.join(searchBean);
	}

	@Override
	public int joinCnt(SearchBean searchBean) {
		try {
			if("cust_id".equals(searchBean.getStatus())){
				searchBean.setCust_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
				searchBean.setCtn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}else if("ctn".equals(searchBean.getStatus())){
				searchBean.setCtn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}else if("ga_id".equals(searchBean.getStatus())){
				searchBean.setGa_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return viewLogDao.joinCnt(searchBean);
	}
	
	@Override
	public List<ViewLogBean> pageView(SearchBean searchBean) {
		if("day".equals(searchBean.getDateType())){
			return viewLogDao.pageViewDay(searchBean);
		}else if("month".equals(searchBean.getDateType())){
			return viewLogDao.pageViewMonth(searchBean);
		}
		return null;
	}
	
	@Override
	public int pageViewCnt(SearchBean searchBean) {
		if("day".equals(searchBean.getDateType())){
			return viewLogDao.pageViewDayCnt(searchBean);
		}else if("month".equals(searchBean.getDateType())){
			return viewLogDao.pageViewMonthCnt(searchBean);
		}
		return 0;
	}
	

	@Override
	public List<PointRoulette> connectAPI(SearchBean searchBean) {
		try {
			if("cust_id".equals(searchBean.getStatus())){
				searchBean.setCust_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}else if("ctn".equals(searchBean.getStatus())){
				searchBean.setCtn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}else if("ga_id".equals(searchBean.getStatus())){
				searchBean.setGa_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}
			if("".equals(searchBean.getStatus())){
				searchBean.setCust_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
				searchBean.setCtn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
				searchBean.setGa_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return viewLogDao.connectAPI(searchBean);
	}


	@Override
	public int connectAPICnt(SearchBean searchBean) {
		try {
			if("cust_id".equals(searchBean.getStatus())){
				searchBean.setCust_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}else if("ctn".equals(searchBean.getStatus())){
				searchBean.setCtn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}else if("ga_id".equals(searchBean.getStatus())){
				searchBean.setGa_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}
			if("".equals(searchBean.getStatus())){
				searchBean.setCust_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
				searchBean.setCtn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
				searchBean.setGa_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return viewLogDao.connectAPICnt(searchBean);
	}
	
	@Override
	public List<PointRoulette> joinEvent(SearchBean searchBean) {
		try {
			if("cust_id".equals(searchBean.getStatus())){
				searchBean.setCust_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}else if("ctn".equals(searchBean.getStatus())){
				searchBean.setCtn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}else if("ga_id".equals(searchBean.getStatus())){
				searchBean.setGa_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}
			if("".equals(searchBean.getStatus())){
				searchBean.setCust_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
				searchBean.setCtn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
				searchBean.setGa_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return viewLogDao.joinEvent(searchBean);
	}

	@Override
	public int joinEventCnt(SearchBean searchBean) {
		try {
			if("cust_id".equals(searchBean.getStatus())){
				searchBean.setCust_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}else if("ctn".equals(searchBean.getStatus())){
				searchBean.setCtn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}else if("ga_id".equals(searchBean.getStatus())){
				searchBean.setGa_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}
			if("".equals(searchBean.getStatus())){
				searchBean.setCust_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
				searchBean.setCtn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
				searchBean.setGa_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return viewLogDao.joinEventCnt(searchBean);
	}
	
	@Override
	public List<PointRoulette> timeGbn(SearchBean searchBean) {
		return viewLogDao.timeGbn(searchBean);
	}

	@Override
	public int timeGbnCnt(SearchBean searchBean) {
		return viewLogDao.timeGbnCnt(searchBean);
	}
	
	@Override
	public List<PointRoulette> dailyJoinSum(SearchBean searchBean) {
		return viewLogDao.dailyJoinSum(searchBean);
	}

	@Override
	public int dailyJoinSumCnt(SearchBean searchBean) {
		return viewLogDao.dailyJoinSumCnt(searchBean);
	}
	
	@Override
	public List<PointRoulette> getChannelNameList(ModelMap map) {
		return viewLogDao.getChannelNameList(map);
	}

	@Override
	public int dailyJoinSumUpdate(SearchBean searchBean) {
		return viewLogDao.dailyJoinSumUpdate(searchBean);
	}

	

	

	

	


	

	

	
	
	
	

}
