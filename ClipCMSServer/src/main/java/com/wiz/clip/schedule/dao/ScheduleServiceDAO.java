/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.schedule.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.wiz.clip.roulette.channel.bean.PointRoulette;
import com.wiz.clip.roulette.viewLog.bean.PointRouletteJoin;
import com.wiz.clip.roulette.viewLog.bean.ViewLogBean;
import com.wiz.clip.schedule.bean.PointHistVo;
import com.wiz.clip.schedule.bean.PointInfoVo;

@Repository("scheduleServiceDao")
public class ScheduleServiceDAO {
	
	@Autowired
	private SqlSession postgreSession;
	
	public void makeDailyStat(String predate) {
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("predate", predate);
		postgreSession.update("SCHEDULE.makeDailyStat",param);
		
	}
	
	public void makeMediaStat(String curdate) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("curdate", curdate);
		postgreSession.update("SCHEDULE.makeMediaStat",param);
		
	}

	public void deleteTblStatDailyPoint(String curDate) {
		postgreSession.delete("SCHEDULE.deleteTblStatDailyPoint",curDate);
		
	}

	public void deleteTblStatMediaPoint(String curDate) {
		postgreSession.delete("SCHEDULE.deleteTblStatMediaPoint",curDate);
	}

	public List<PointRoulette> chkPointRoulette(String curDate) {
		return postgreSession.selectList("SCHEDULE.chkPointRoulette",curDate);
		
	}

	public int selectPointRouletteJoinSumByDate(PointRoulette pointRoulette) {
		return postgreSession.selectOne("SCHEDULE.selectPointRouletteJoinSumByDate", pointRoulette);
	}

	public void insertPointRouletteJoinSum(PointRoulette pointRoulette) {
		postgreSession.insert("SCHEDULE.insertPointRouletteJoinSum", pointRoulette);		
		
	}

	public List<PointRoulette> falsePointChk() {
		return postgreSession.selectList("SCHEDULE.falsePointChk");
	}

	public void makeMonthlyStat(String curdate, String nextDate) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("curdate", curdate);
		param.put("nextDate", nextDate);
		postgreSession.update("SCHEDULE.makeMonthlyStat",param);
	}

	public void deleteTblStatMonthlyPoint(String curDate) {
		postgreSession.delete("SCHEDULE.deleteTblStatMonthlyPoint",curDate);
	}

	public void deletePointRouletteConnLogSum(String curDate) {
		postgreSession.delete("SCHEDULE.deletePointRouletteConnLogSum",curDate);
	}

	public List<ViewLogBean> selectPointRouletteConnLogSum(String curDate) {
		return postgreSession.selectList("SCHEDULE.selectPointRouletteConnLogSum", curDate);
	}

	public void insertPointRouletteConnLogSum(ViewLogBean viewLogBean) {
		postgreSession.insert("SCHEDULE.insertPointRouletteConnLogSum", viewLogBean);
	}

	public String makeTop30(String curDate, int limitCount, String POINT_MODIFY_ACL_REQUEST_CODE) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("curDate", curDate);
		param.put("limitCount", limitCount);
		param.put("POINT_MODIFY_ACL_REQUEST_CODE", POINT_MODIFY_ACL_REQUEST_CODE);
		return postgreSession.selectOne("SCHEDULE.makeTop30",param);
	}

	public void deleteTbl_stat_top_user(String curDate) {
		postgreSession.delete("SCHEDULE.deleteTbl_stat_top_user",curDate);
	}

	public String makeTop30_2(String curDate, int limitCount, String POINT_MODIFY_ACL_REQUEST_CODE) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("curDate", curDate);
		param.put("limitCount", limitCount);
		param.put("POINT_MODIFY_ACL_REQUEST_CODE", POINT_MODIFY_ACL_REQUEST_CODE);
		return postgreSession.selectOne("SCHEDULE.makeTop30_2",param);
	}

	public void deleteTbl_stat_top_user_2(String curDate) {
		postgreSession.delete("SCHEDULE.deleteTbl_stat_top_user_2",curDate);
	}
	
	public void deleteTbl_stat_anomaly_detection(String curDate, String curTime, int limitPoint) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("curDate", curDate);
		param.put("curTime", curTime);
		param.put("limitPoint", limitPoint);
		postgreSession.delete("SCHEDULE.deleteTbl_stat_anomaly_detection",param);
		
	}
	
	public void deleteTbl_stat_anomaly_detection2(String curDate, String curTime, int limitPoint) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("curDate", curDate);
		param.put("curTime", curTime);
		param.put("limitPoint", limitPoint);
		postgreSession.delete("SCHEDULE.deleteTbl_stat_anomaly_detection2",param);
		
	}

	public String makeAnomaly(String curDate, String curTime, int limitPoint, String POINT_MODIFY_ACL_REQUEST_CODE) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("curDate", curDate);
		param.put("curTime", curTime);
		param.put("limitPoint", limitPoint);
		param.put("POINT_MODIFY_ACL_REQUEST_CODE", POINT_MODIFY_ACL_REQUEST_CODE);
		return postgreSession.selectOne("SCHEDULE.makeAnomaly",param);
	}

	public String makeAnomaly2(String curDate, String curTime, int limitPoint, String POINT_MODIFY_ACL_REQUEST_CODE) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("curDate", curDate);
		param.put("curTime", curTime);
		param.put("limitPoint", limitPoint);
		param.put("POINT_MODIFY_ACL_REQUEST_CODE", POINT_MODIFY_ACL_REQUEST_CODE);
		return postgreSession.selectOne("SCHEDULE.makeAnomaly2",param);
	}
	
	public String getCmsCommonCode(String code_name) {
		return postgreSession.selectOne("SCHEDULE.getCmsCommonCode",code_name);
	}

	public void makeDailyStat2(String preDate, String POINT_MODIFY_ACL_REQUEST_CODE) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("predate", preDate);
		param.put("POINT_MODIFY_ACL_REQUEST_CODE", POINT_MODIFY_ACL_REQUEST_CODE);
		postgreSession.update("SCHEDULE.makeDailyStat2",param);
	}

	public void deleteTblStatDailyPoint2(String preDate, String POINT_MODIFY_ACL_REQUEST_CODE) {
		postgreSession.delete("SCHEDULE.deleteTblStatDailyPoint2",preDate);
	}

	public void point_roulette_join_delete_pre_3days(String preDate) {
		postgreSession.delete("SCHEDULE.point_roulette_join_delete_pre_3days",preDate);
	}

	public void point_roulette_join_log_delete_pre_1day(String preDate) {
		postgreSession.delete("SCHEDULE.point_roulette_join_log_delete_pre_1day",preDate);
	}

	public List<PointRouletteJoin> point_roulette_join_select_pre_1day(String preDate) {
		return postgreSession.selectList("SCHEDULE.point_roulette_join_select_pre_1day",preDate);
	}

	public void insert_point_roulette_join_log(PointRouletteJoin pointRouletteJoin) {
		postgreSession.insert("SCHEDULE.insert_point_roulette_join_log",pointRouletteJoin);
	}

	public int insertPointHist(PointHistVo phv) {
		return postgreSession.insert("SCHEDULE.insertPointHist",phv);
	}

	public PointHistVo getPointHistByTrId(String tr_id) {
		return postgreSession.selectOne("SCHEDULE.getPointHistByTrId",tr_id);
	}

	public int insertPointInfo(PointInfoVo piv) {
		return postgreSession.insert("SCHEDULE.insertPointInfo",piv);
	}

	public void updateTblStatDailyPoint(Map<String, String> parameters2) {
		postgreSession.update("SCHEDULE.updateTblStatDailyPoint",parameters2);
	}

	public void updateAclStatus() throws Exception {
		postgreSession.update("SCHEDULE.updateAclStatus");
	}

	

}
