package com.wiz.clip.schedule.service;

public interface AdvScheduleService {
	
	public void getBuzzAdList();
	
	public void calculateLinkpricePoint();
	
	public void calculateLinkpricePointManual(String targetMonth);
	
}
