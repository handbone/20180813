/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.schedule.web;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SchedulerController  {
	
	Logger logger = Logger.getLogger(SchedulerController.class.getName());

	@Autowired
	private SqlSession postgreSession;

	/*
	 * 스케쥴러 뷰
	 */
	@RequestMapping(value = "/schedulerView.do")
	public ModelAndView sendQuery(@RequestParam Map map) {

		ModelAndView mav = new ModelAndView();
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "TOP 30");
		mav.addObject("map", map);
		mav.addObject("display", "../clip/common/scheduler/schedulerView.jsp");

		mav.setViewName("/main/index");
		
		return mav;
	}

	

}
