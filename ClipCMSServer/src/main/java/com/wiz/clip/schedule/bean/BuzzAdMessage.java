package com.wiz.clip.schedule.bean;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BuzzAdMessage {

	private String code; 	//처리 결과 코드. 200인 경우에만 정상.
	private String msg;		//처리 결과 메세지
	private List<BuzzAdItem> ads;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public List<BuzzAdItem> getAds() {
		return ads;
	}
	public void setAds(List<BuzzAdItem> ads) {
		this.ads = ads;
	}
	
}
