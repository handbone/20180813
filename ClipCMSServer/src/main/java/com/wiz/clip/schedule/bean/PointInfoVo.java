/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.schedule.bean;

public class PointInfoVo {
	private String userCi;
	private int pointHistIdx;
	private String pointType;
	private long pointValue;
	private long balance;
	private String description;
	private String regSource;
	private String regDate;
	private String regTime;
	private String status;
	private StringBuffer log;
	private int joinLimitFailCnt;
	
	public int getJoinLimitFailCnt() {
		return joinLimitFailCnt;
	}
	public void setJoinLimitFailCnt(int joinLimitFailCnt) {
		this.joinLimitFailCnt = joinLimitFailCnt;
	}
	public String getUserCi() {
		return userCi;
	}
	public void setUserCi(String userCi) {
		this.userCi = userCi;
	}
	public int getPointHistIdx() {
		return pointHistIdx;
	}
	public void setPointHistIdx(int pointHistIdx) {
		this.pointHistIdx = pointHistIdx;
	}
	public String getPointType() {
		return pointType;
	}
	public void setPointType(String pointType) {
		this.pointType = pointType;
	}
	public long getPointValue() {
		return pointValue;
	}
	public void setPointValue(long pointValue) {
		this.pointValue = pointValue;
	}
	public long getBalance() {
		return balance;
	}
	public void setBalance(long balance) {
		this.balance = balance;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRegSource() {
		return regSource;
	}
	public void setRegSource(String regSource) {
		this.regSource = regSource;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public StringBuffer getLog() {
		return log;
	}
	public void setLog(StringBuffer log) {
		this.log = log;
	}

}
