package com.wiz.clip.schedule.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BuzzAdItem {

	private String id; 				// Integer 광고 아이디
	private String title; 			// String 광고 제목
	private String description; 	// String 광고 설명
	private String action_description; // String 광고 적립방법 설명
	private String icon; 			// String 아이콘 이미지 주소
	private String revenue; 		// String 매체사 단가
	private String revenue_type; 	// String 광고 타입. 추후 새로운 타입이 추가될 수 있으므로 연동시 이를
									// 고려해야 한다.
									// cpi: 앱 설치형
									// cpe: 앱 실행형 또는 앱 실행후 액션형
									// cpa: 액션형
									// cpl: 페이스북 좋아요
									// cpb: 페이스북 공유
									// cpy: 동영상 시청
									// cpinsta: 인스타그램 팔로우
	private String package_name; 	// String 안드로이드 package_name
	private String url_scheme; 		// String 아이폰 url scheme
	private String platform; 		// String 광고가 지원되는 플랫폼 A: 안드로이드, I: 아이폰, W:
									// 안드로이드/아이폰 둘다 지원
	private String age_from; 		// Integer 나이 하한 타게팅. 타게팅이 없는 경우 null.
	private String age_to; 			// Integer 나이 상한 타게팅. 타게팅이 없는 경우 null.
	private String sex; 			// String 성별 타게팅. 타게팅이 없는 경우 null.
									// M: 남자
									// F: 여자
	private String relationship; 	// String 결혼 유무 타게팅. 타게팅이 없는 경우 null.
									// S: 미혼
									// M: 기혼
	private String carrier; 		// String 통신사 타게팅. 타게팅이 없는 경우 null.
									// kt: KT 통신사
									// skt: SKT 통신사
									// lgt: LGT 통신사
	private String udid_required; 	// Bool true: 광고 참여/참여완료 요청시 udid 파라미터 필수
									// false: 광고 참여/참여완료 요청시 udid 파라미터 필수 아님
	private String device_name; // String 디바이스 모델 타게팅. 콤마로 구분된 디바이스 모델명으로 되어 있고
								// or조건이다.
								// example)
								// SHV-E250S,SHV-E275K,SM-G928L ->SHV-E250S 또는
								// SHV-E275K 또는 SM-G928L 인 유저에게 타게팅
	private String target_app; 	// String 앱 타게팅. 안드로이드만 지원. 콤마로 구분된 패키지 네임으로 되어
								// 있고 or조건이다. 해당 앱이 설치된 유저에게 타게팅한다.
								// 패키지 네임 앞에 ‘-’ 기호가 있는 경우 설치 안된 유저에게 타게팅한다.
								// example)
								// Pl.idreams.skyforcehd,com.imangi.templerun2
								// ->Pl.idreams.skyforcehd 또는
								// com.imangi.templerun2 가 설치된 유저에게 타게팅
								// com.thinkreals.couponmoa,-com.supercell.clashofclans
								// -> com.thinkreals.couponmoa가 설치되어 있거나
								// com.supercell.clashofclans가 설치되어 있지 않은 유저에게
								// 타게팅
	private String region; 		// String 지역 타게팅. 콤마로 구분된 지역으로 되어 있고 or조건이다. 도/시 단위로
								// 타게팅이 가능하다.
								// example)
								// 서울특별시 강남구,경상남도 ->서울특별시 강남구 또는 경상남도에 거주중인 유저에게 타게팅
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAction_description() {
		return action_description;
	}
	public void setAction_description(String action_description) {
		this.action_description = action_description;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getRevenue() {
		return revenue;
	}
	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}
	public String getRevenue_type() {
		return revenue_type;
	}
	public void setRevenue_type(String revenue_type) {
		this.revenue_type = revenue_type;
	}
	public String getPackage_name() {
		return package_name;
	}
	public void setPackage_name(String package_name) {
		this.package_name = package_name;
	}
	public String getUrl_scheme() {
		return url_scheme;
	}
	public void setUrl_scheme(String url_scheme) {
		this.url_scheme = url_scheme;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getAge_from() {
		return age_from;
	}
	public void setAge_from(String age_from) {
		this.age_from = age_from;
	}
	public String getAge_to() {
		return age_to;
	}
	public void setAge_to(String age_to) {
		this.age_to = age_to;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public String getUdid_required() {
		return udid_required;
	}
	public void setUdid_required(String udid_required) {
		this.udid_required = udid_required;
	}
	public String getDevice_name() {
		return device_name;
	}
	public void setDevice_name(String device_name) {
		this.device_name = device_name;
	}
	public String getTarget_app() {
		return target_app;
	}
	public void setTarget_app(String target_app) {
		this.target_app = target_app;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	
	
}
