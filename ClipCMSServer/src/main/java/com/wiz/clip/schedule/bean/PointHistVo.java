/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.schedule.bean;

public class PointHistVo {
	private int pointHistIdx;
	private String userCi;
	private String custId;
	private String ctn;
	private String gaId;
	private String transactionId;
	private String pointType;
	private long pointValue;
	private String description;
	private String status;
	private String regSource;
	private String regDate;
	private String regTime;
	private String cngDate;
	private String cngTime;
	private StringBuffer log;
	private String acl_limit_yn;
	private int acl_limit_cnt;
	private int joinLimitFailCnt;
	
	public int getJoinLimitFailCnt() {
		return joinLimitFailCnt;
	}
	public void setJoinLimitFailCnt(int joinLimitFailCnt) {
		this.joinLimitFailCnt = joinLimitFailCnt;
	}
	
	public int getPointHistIdx() {
		return pointHistIdx;
	}
	public void setPointHistIdx(int pointHistIdx) {
		this.pointHistIdx = pointHistIdx;
	}
	public String getUserCi() {
		return userCi;
	}
	public void setUserCi(String userCi) {
		this.userCi = userCi;
	}
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	public String getCtn() {
		return ctn;
	}
	public void setCtn(String ctn) {
		this.ctn = ctn;
	}
	public String getGaId() {
		return gaId;
	}
	public void setGaId(String gaId) {
		this.gaId = gaId;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getPointType() {
		return pointType;
	}
	public void setPointType(String pointType) {
		this.pointType = pointType;
	}
	public long getPointValue() {
		return pointValue;
	}
	public void setPointValue(long pointValue) {
		this.pointValue = pointValue;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRegSource() {
		return regSource;
	}
	public void setRegSource(String regSource) {
		this.regSource = regSource;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	
	public String getCngDate() {
		return cngDate;
	}
	public void setCngDate(String cngDate) {
		this.cngDate = cngDate;
	}
	public String getCngTime() {
		return cngTime;
	}
	public void setCngTime(String cngTime) {
		this.cngTime = cngTime;
	}
	public StringBuffer getLog() {
		return log;
	}
	public void setLog(StringBuffer log) {
		this.log = log;
	}
	public String getAcl_limit_yn() {
		return acl_limit_yn;
	}
	public void setAcl_limit_yn(String acl_limit_yn) {
		this.acl_limit_yn = acl_limit_yn;
	}
	public int getAcl_limit_cnt() {
		return acl_limit_cnt;
	}
	public void setAcl_limit_cnt(int acl_limit_cnt) {
		this.acl_limit_cnt = acl_limit_cnt;
	}
}
