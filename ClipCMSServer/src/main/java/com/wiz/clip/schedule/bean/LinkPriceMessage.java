package com.wiz.clip.schedule.bean;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkPriceMessage {
	
	private int idx;
	private String day;
	private String resultMsg;
	
	private String status;
	private int total;
	private int success;
	private int fail;
	
	private String result;							//응답 코드(응답 코드 참조)
	private String list_count;						//조회 갯수 (있을 경우)
	private List<LinkPriceOrderItem> order_list;	//조회 목록 (있을 경우)
	
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getList_count() {
		return list_count;
	}
	public void setList_count(String list_count) {
		this.list_count = list_count;
	}
	public List<LinkPriceOrderItem> getOrder_list() {
		return order_list;
	}
	public void setOrder_list(List<LinkPriceOrderItem> order_list) {
		this.order_list = order_list;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getSuccess() {
		return success;
	}
	public void setSuccess(int success) {
		this.success = success;
	}
	public int getFail() {
		return fail;
	}
	public void setFail(int fail) {
		this.fail = fail;
	}
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	
}
