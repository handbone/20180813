/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.schedule.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.wiz.clip.schedule.bean.BuzzAdItem;
import com.wiz.clip.schedule.bean.LinkPriceMessage;
import com.wiz.clip.schedule.bean.LinkPriceOrderItem;

@Repository("advScheduleServiceDAO")
public class AdvScheduleServiceDAO {
	
	@Autowired
	private SqlSession postgreSession;
	
	public void deleteAllBuzzAdItem() {
		postgreSession.delete("ADV_SCHEDULE.deleteAllBuzzAdItem", null);
	}

	public void insertMultiBuzzAdItem(List<BuzzAdItem> data) {
		Map<String, List<BuzzAdItem>> tempMap = new HashMap<String, List<BuzzAdItem>>(); 
		tempMap.put("list", data);
		postgreSession.insert("ADV_SCHEDULE.insertMultiBuzzAdItem", tempMap);
	}
	
	public void insertLinkpriceShopHistMain(LinkPriceMessage data) {
		postgreSession.insert("ADV_SCHEDULE.insertLinkpriceShopHistMain", data);
	}
	
	public LinkPriceMessage selectLinkpriceShopHistMain(String date) {
		return postgreSession.selectOne("ADV_SCHEDULE.selectLinkpriceShopHistMain", date);
	}
	
	public void insertLinkpriceShopHistDtl(LinkPriceOrderItem data) {
		postgreSession.insert("ADV_SCHEDULE.insertLinkpriceShopHistDtl", data);
	}
	
	public LinkPriceMessage selectLinkpriceShopHistDtl(String trId) {
		return postgreSession.selectOne("ADV_SCHEDULE.selectLinkpriceShopHistDtl", trId);
	}
	
	public void updateLinkpriceShopHistMain(LinkPriceMessage data) {
		postgreSession.insert("ADV_SCHEDULE.updateLinkpriceShopHistMain", data);
	}
	
	public void updateLinkpriceShopHistDtl(LinkPriceOrderItem data) {
		postgreSession.insert("ADV_SCHEDULE.updateLinkpriceShopHistMain", data);
	}
}
