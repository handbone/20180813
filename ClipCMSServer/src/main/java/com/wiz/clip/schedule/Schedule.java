/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.schedule;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wiz.clip.schedule.service.AdvScheduleService;
import com.wiz.clip.schedule.service.ScheduleService;

@Controller
public class Schedule {

	@Resource(name="scheduleService")
	private ScheduleService scheduleService;
	
	@Resource(name="advScheduleService")
	private AdvScheduleService advScheduleService;
	
	@Value("#{config['POINT_MODIFY_ACL_REQUEST_CODE']}") String POINT_MODIFY_ACL_REQUEST_CODE;
	
	Logger logger = Logger.getLogger(Schedule.class.getName());
	
	/*
	 * make_daily_stat 프로시져 호출 =================================================================================
	 */
	@Scheduled(cron="0 0 4 * * *")//초 분 시 일 월 주(년) , 매일 4시 0분에 실행
	public void makeDailyStat(){
		logger.debug("makeDailyStat start!!!");
		
		scheduleService.makeDailyStat();	
		
		logger.debug("makeDailyStat END!!!");
	}
	
	@Scheduled(cron="0 0 1 * * *")//초 분 시 일 월 주(년) , 매일 2시 0분에 실행
	public void makeDailyStat_edit() throws Exception{
		logger.debug("makeDailyStat_edit start!!!");
		
		scheduleService.makeDailyStat_edit();
		
		logger.debug("makeDailyStat_edit END!!!");
	}
	
	/*
	 * make_daily_stat 프로시져 호출 즉시 실행 
	 */
	@RequestMapping(value ="/makeDailyStat.do")
	@ResponseBody
	public ResponseEntity<String> makeDailyStat(
			@RequestParam(required = true) String preDate,
			HttpServletRequest request, ModelMap model){
		logger.debug("즉시 실행 :: makeDailyStat start!!!");
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");

		scheduleService.deleteTblStatDailyPoint(preDate);
		scheduleService.makeDailyStat(preDate);
		
		return new ResponseEntity<String>("makeDailyStat complete",responseHeaders, HttpStatus.CREATED);
	}
	
	/*
	 * make_daily_stat2 프로시져 호출 =================================================================================
	 */
	@Scheduled(cron="0 30 4 * * *")//초 분 시 일 월 주(년) , 매일 4시 30분에 실행
	public void makeDailyStat2(){
			logger.debug("makeDailyStat2 start!!!");
			
			scheduleService.makeDailyStat2(POINT_MODIFY_ACL_REQUEST_CODE);
			
			logger.debug("makeDailyStat2 END!!!");
	}
	
//	@Scheduled(cron="0 30 1 * * *")//초 분 시 일 월 주(년) , 매일 4시 30분에 실행
	public void makeDailyStat2_edit() throws Exception{
			logger.debug("makeDailyStat2_edit start!!!");
			
			scheduleService.makeDailyStat2_edit(POINT_MODIFY_ACL_REQUEST_CODE);
			
			logger.debug("makeDailyStat2_edit END!!!");
	}
	
	/*
	 * make_daily_stat2 프로시져 호출 즉시 실행 
	 */
	@RequestMapping(value ="/makeDailyStat2.do")
	@ResponseBody
	public ResponseEntity<String> makeDailyStat2(
			@RequestParam(required = true) String preDate,
			HttpServletRequest request, ModelMap model){
		logger.debug("즉시 실행 :: makeDailyStat2 start!!!");
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");

		scheduleService.deleteTblStatDailyPoint2(preDate, POINT_MODIFY_ACL_REQUEST_CODE);
		scheduleService.makeDailyStat2(preDate, POINT_MODIFY_ACL_REQUEST_CODE);
		
		return new ResponseEntity<String>("makeDailyStat complete",responseHeaders, HttpStatus.CREATED);
	}
	
	/*
	 * make_media_stat 프로시져 호출 =================================================================================
	 */
	@Scheduled(cron="0 30 3 * * *")//초 분 시 일 월 주(년), 매일 3시 30분에 실행
	public void makeMediaStat(){
		logger.debug("makeMediaStat start!!!");
		
		scheduleService.makeMediaStat();
		
		
	}
	
	
	
	/*
	 * make_media_stat 프로시져 호출 즉시 실행 
	 */
	@RequestMapping(value ="/makeMediaStat.do")
	@ResponseBody
	public ResponseEntity<String> makeMediaStat(@RequestParam(required = true) String preDate
			,HttpServletRequest request, ModelMap model){
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");

		scheduleService.deleteTblStatMediaPoint(preDate);
		scheduleService.makeMediaStat(preDate);
		
		return new ResponseEntity<String>("makeMediaStat complete",responseHeaders, HttpStatus.CREATED);
	}
	
	/*
	 * 당첨자 합계테이블 생성 =================================================================================
	 */
	@Scheduled(cron="0 0 1 * * *")//초 분 시 일 월 주(년), 매일 1시 0분에 실행
	public void makePointRouletteJoinSum(){
		logger.debug("makePointRouletteJoinSum start!!!");
		
		scheduleService.makePointRouletteJoinSum();
		
		
	}
	
	/*
	 * 당첨자 합계테이블 생성 즉시 실행 
	 */
	@RequestMapping(value ="/makePointRouletteJoinSum.do")
	@ResponseBody
	public ResponseEntity<String> makePointRouletteJoinSum(@RequestParam(required = true) String curDate,HttpServletRequest request, ModelMap model){
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");

		scheduleService.makePointRouletteJoinSum(curDate);
		
		return new ResponseEntity<String>("makePointRouletteJoinSum complete",responseHeaders, HttpStatus.CREATED);
	}
	
	/*
	 * 부정정립 체크 =================================================================================
	 */
	/*@Scheduled(fixedDelay = 600000) //프로그램이 시작된 시점부터 10분마다 실행 됩니다.
	public void falsePointChk(){
		logger.debug("falsePointChk start!!!");
		
		//scheduleService.falsePointChk();
		
	}*/
	
	/*
	 * 부정정립 체크 즉시 실행 
	 */
	@RequestMapping(value ="/falsePointChkCurr.do")
	@ResponseBody
	public ResponseEntity<String> falsePointChkCurr(){
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");
		scheduleService.falsePointChk();
		return new ResponseEntity<String>("falsePointChk complete",responseHeaders, HttpStatus.CREATED);
	}
	
	
	
	/*
	 * 페이지뷰 통계 생성  =================================================================================
	 * 
	 * 현재 사용하고 있지 않은 페이지
	 * 
	 */
//	@Scheduled(cron="0 0 3 * * *")//초 분 시 일 월 주(년), 매일 3시 0분에 실행
//	public void makePointRouletteConnLogSum(){
//		logger.debug("makePointRouletteConnLogSum start!!!");
//		
//		scheduleService.makePointRouletteConnLogSum();
//		
//	}
//	
//	/*
//	 * 페이지뷰 통계 생성  즉시 실행 
//	 */
//	@RequestMapping(value ="/makePointRouletteConnLogSum.do")
//	@ResponseBody
//	public ResponseEntity<String> makePointRouletteConnLogSum(@RequestParam(required = true) String curDate,HttpServletRequest request, ModelMap model) throws java.lang.Exception {
//		HttpHeaders responseHeaders = new HttpHeaders(); 
//		responseHeaders.add("Content-Type", "text/html; charset=utf-8");
//
//		int resultCnt = 0;
//		resultCnt = scheduleService.makePointRouletteConnLogSum(curDate);
//		
//		return new ResponseEntity<String>("makePointRouletteConnLogSum "+resultCnt+" complete",responseHeaders, HttpStatus.CREATED);
//	}
	
	
	
	/*
	 * TOP 30 통계 생성  =================================================================================
	 */
	@Scheduled(cron="0 15 4 * * *")//초 분 시 일 월 주(년), 매일 4시 15분에 실행
	//@Scheduled(fixedDelay = 5000)
	public void makeTop30(){
		scheduleService.makeTop30(POINT_MODIFY_ACL_REQUEST_CODE);
	}
	
	/*
	 *  TOP 30 통계 생성  즉시 실행 
	 */
	@RequestMapping(value ="/makeTop30.do")
	@ResponseBody
	public ResponseEntity<String> makeTop30(@RequestParam(required = true) String curDate,
			@RequestParam(required = true) String limitCount,
			HttpServletRequest request, ModelMap model) throws java.lang.Exception {
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");

		String result = "f";
		result = scheduleService.makeTop30(curDate, Integer.parseInt(limitCount),POINT_MODIFY_ACL_REQUEST_CODE);
		
		return new ResponseEntity<String>("makeTop30 "+result+" !!! ( t or f return. t(rue) or f(alse). )",responseHeaders, HttpStatus.CREATED);
	}
	
	
	/*
	 * TOP 30(매체별) 통계 생성  =================================================================================
	 */
	@Scheduled(cron="0 0 5 * * *")//초 분 시 일 월 주(년), 매일 5시 0분에 실행
	//@Scheduled(fixedDelay = 6000)
	public void makeTop30_2(){
		scheduleService.makeTop30_2(POINT_MODIFY_ACL_REQUEST_CODE);
	}
	
	/*
	 *  TOP 30(매체별) 통계 생성  즉시 실행 
	 */
	@RequestMapping(value ="/makeTop30_2.do")
	@ResponseBody
	public ResponseEntity<String> makeTop30_2(@RequestParam(required = true) String curDate,
			@RequestParam(required = true) String limitCount,
			HttpServletRequest request, ModelMap model) throws java.lang.Exception {
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");

		String result = "f";
		result = scheduleService.makeTop30_2(curDate, Integer.parseInt(limitCount), POINT_MODIFY_ACL_REQUEST_CODE);
		
		return new ResponseEntity<String>("makeTop30_2 "+result+" !!! ( t or f return. t(rue) or f(alse). )",responseHeaders, HttpStatus.CREATED);
	}
	
	
	
	/*
	 * 이상탐지 통계 생성  =================================================================================
	 */
	@Scheduled(cron="0 10 * * * *") // 매시 10분에 실행
	//@Scheduled(fixedDelay = 7000)
	public void makeAnomaly() {
		scheduleService.makeAnomaly(POINT_MODIFY_ACL_REQUEST_CODE);
	}
	
	/*
	 *  이상탐지 통계 생성  즉시 실행 
	 */
	@RequestMapping(value ="/makeAnomaly.do")
	@ResponseBody
	public ResponseEntity<String> makeAnomaly(
			@RequestParam(required = true) String curDate,
			@RequestParam(required = true) String curTime,
			@RequestParam(required = true) String limitPoint,
			HttpServletRequest request, ModelMap model) throws java.lang.Exception {
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");

		String result = "f";
		result = scheduleService.makeAnomaly(curDate, curTime, Integer.parseInt(limitPoint),POINT_MODIFY_ACL_REQUEST_CODE);
		

		return new ResponseEntity<String>("makeAnomaly "+result+" !!! ( t or f return. t(rue) or f(alse). )",responseHeaders, HttpStatus.CREATED);
	}
	
	
	
	/*
	 * 이상탐지 통계 생성(합계)  =================================================================================
	 */
	@Scheduled(cron="0 5 * * * *")//초 분 시 일 월 주(년), 매시 5분에 실행
	//@Scheduled(fixedDelay = 8000)
	public void makeAnomaly2(){
		scheduleService.makeAnomaly2(POINT_MODIFY_ACL_REQUEST_CODE);
	}
	
	/*
	 *  이상탐지 통계 생성  즉시 실행 (합계)
	 */
	@RequestMapping(value ="/makeAnomaly2.do")
	@ResponseBody
	public ResponseEntity<String> makeAnomaly2(
			@RequestParam(required = true) String curDate,
			@RequestParam(required = true) String curTime,
			@RequestParam(required = true) String limitPoint,
			HttpServletRequest request, ModelMap model) throws java.lang.Exception {
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");

		String result = "f";
		result = scheduleService.makeAnomaly2(curDate, curTime, Integer.parseInt(limitPoint), POINT_MODIFY_ACL_REQUEST_CODE);

		return new ResponseEntity<String>("makeAnomaly2 "+result+" !!! ( t or f return. t(rue) or f(alse). )",responseHeaders, HttpStatus.CREATED);
	}
	
	/*
	 * point_roulette_join 백업  =================================================================================
	 */
	@Scheduled(cron="0 0 3 * * *")//초 분 시 일 월 주(년), 매일 0시 10분에 실행
	//@Scheduled(fixedDelay = 10000)
	public void point_roulette_join_log(){
		scheduleService.point_roulette_join_log();
	}
	
	/*
	 *  point_roulette_join 백업 즉시 실행 
	 */
	@RequestMapping(value ="/point_roulette_join_log.do")
	@ResponseBody
	public ResponseEntity<String> point_roulette_join_log(
			@RequestParam(required = true) String preDate,
			HttpServletRequest request, ModelMap model) throws java.lang.Exception {
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");

		String result = "0";
		result = scheduleService.point_roulette_join_log(preDate);

		return new ResponseEntity<String>("point_roulette_join_log INSERT COUNT "+result+" !!! ( t or f return. t(rue) or f(alse). )",responseHeaders, HttpStatus.CREATED);
	}

	
	/****************************
	 * 이하 고도화 추가 배치	*
	 ****************************/
	
	/**
	 * 버즈빌 잠금화면 광고 목록 가져오기
	 */
//	@Scheduled(cron="0 */10 * * * *") //초 분 시 일 월 주(년) , 10분 마다 실행
	public void getBuzzAdList(){
		logger.debug("getBuzzAdList start!!!");
		
		advScheduleService.getBuzzAdList();
	}

	/**
	 * 버즈빌 잠금화면 광고 목록 가져오기 즉시 실행
	 * @param preDate
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value ="/adv/getBuzzAdList.do")
	@ResponseBody
	public ResponseEntity<String> getBuzzAdList(HttpServletRequest request, ModelMap model){
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");

		advScheduleService.getBuzzAdList();
		
		return new ResponseEntity<String>("makeDailyStat complete",responseHeaders, HttpStatus.CREATED);
	}

	/**
	 * 링크 프라이스 정산 배치
	 */
	@Scheduled(cron="0 0 3 * * *") //초 분 시 일 월 주(년) , 매일 01시 10분에 실행 => 매일 3시 변경
	public void calculateLinkpricePoint(){

		logger.debug("calculateLinkpricePoint start!!!");
		
		advScheduleService.calculateLinkpricePoint();
	}

	/**
	 * 링크 프라이스 정산 배치 즉시 실행
	 * @param preDate
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value ="/adv/calculateLinkpricePoint.do")
	@ResponseBody
	public ResponseEntity<String> calculateLinkpricePoint(HttpServletRequest request, ModelMap model, String targetMonth){
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");

		advScheduleService.calculateLinkpricePointManual(targetMonth);
		
		return new ResponseEntity<String>("calculateLinkpricePoint complete",responseHeaders, HttpStatus.CREATED);
	}
	
	/**
	 * 매체관리 사용기간 조회 배치(use_status 변경)
	 * @author jyi
	 */
	@Scheduled(cron="0 0 0 * * *")//초 분 시 일 월 주(년) , 매일 4시 0분에 실행
	public void updateAclStatus() throws Exception {
		logger.debug("makeDailyStat start!!!");
		
		scheduleService.updateAclStatus();	
	}
}

