/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.schedule.service;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.wiz.clip.common.util.DateUtil;
import com.wiz.clip.common.util.HttpClientUtil;
import com.wiz.clip.schedule.bean.BuzzAdItem;
import com.wiz.clip.schedule.bean.BuzzAdMessage;
import com.wiz.clip.schedule.bean.ClipPointMessage;
import com.wiz.clip.schedule.bean.LinkPriceMessage;
import com.wiz.clip.schedule.bean.LinkPriceOrderItem;
import com.wiz.clip.schedule.dao.AdvScheduleServiceDAO;

@Service("advScheduleService")
public class AdvScheduleServiceImpl implements AdvScheduleService {
	Logger logger = Logger.getLogger(AdvScheduleServiceImpl.class.getName());
	
	@Resource(name="advScheduleServiceDAO")
	private AdvScheduleServiceDAO advScheduleServiceDAO;
	
	@Value("#{config['MSG_CONTENT_YN']}") String MSG_CONTENT_YN;
	
	
	@Value("#{config['BUZZVIL_AD_LIST']}")
	String BUZZVIL_AD_LIST;
	
	@Value("#{config['BUZZVIL_UNIT_ID']}")
	String BUZZVIL_UNIT_ID;
	
	@Value("#{config['LINKPRICE_RESULT_LIST']}")
	String LINKPRICE_RESULT_LIST;
	
	@Value("#{config['LINKPRICE_A_ID']}")
	String LINKPRICE_A_ID;
	
	@Value("#{config['LINKPRICE_AUTH_KEY']}")
	String LINKPRICE_AUTH_KEY;
	
	@Value("#{config['API_DOMAIN_SEND_ADV_PLUS_POINT']}")
	String API_DOMAIN_SEND_ADV_PLUS_POINT;
	
	@Value("#{config['API_DOMAIN_SEND_ADV_MINUS_POINT']}")
	String API_DOMAIN_SEND_ADV_MINUS_POINT;
	
	private static final String STATUS_PROCESSING 	= "P";	//처리중
	private static final String STATUS_WAITING 		= "W";	//처리 대기(일시 오류)
	private static final String STATUS_FAIL			= "F";	//최종 실패
	private static final String STATUS_COMPLETE		= "C";	//최종 완료
	private static final String STATUS_DUPLICATE	= "D";	//중복 무시

	@Override
	public void getBuzzAdList() {

		HttpClientUtil httpClient = null;
		List<BuzzAdItem> newList = new ArrayList<BuzzAdItem>();
		
		try {
			
			//TODO 임시
			//BUZZVIL_AD_LIST = "http://localhost:8081/api/v1/list";
			
			httpClient = new HttpClientUtil("GET", BUZZVIL_AD_LIST);
			
			logger.debug(BUZZVIL_UNIT_ID);
			httpClient.addParameter("unit_id", BUZZVIL_UNIT_ID);
			
			int http_status = httpClient.sendRequest();
			
			logger.info("BUZZVIL getBuzzAdList response status : " + http_status);
			if(http_status == HttpStatus.SC_OK){
				BuzzAdMessage res = httpClient.getResponseJson(BuzzAdMessage.class);
				logger.debug("Result : "+res.getCode()+"("+res.getMsg()+")");
				
				//200 : 정상
				if("200".equals(res.getCode())) {
					List<BuzzAdItem> list = res.getAds();

					Map<String, Integer> checkMap = new HashMap<String, Integer>();
					StringBuffer listLogBuf = new StringBuffer();
					
					if(list != null && list.size() > 0) {
						for(BuzzAdItem data : list) {

							if("cpi".equals(data.getRevenue_type()))
								continue;
							
							if(StringUtils.isEmpty(data.getRevenue()))
								data.setRevenue("0");
							
							//cpi 형만 제거
							if(isValidBuzzData(data)) {
								if(checkMap.get(data.getId()) == null)
									checkMap.put(data.getId(), 1);
								else
									continue;

							} else {
								continue;
							}
								
							listLogBuf.append("");
							newList.add(data);
						}
					}
					
					logger.debug("BUZZ AD LIST size : "+newList.size()+" / "+list.size());	
					logger.info("BUZZVIL getBuzzAdList Success Response");
				} else {
					logger.info("BUZZVIL getBuzzAdList Error Fail Response");
				}

			} else {
				logger.info("BUZZVIL getBuzzAdList Error HTTP : code["+http_status+"] ");
			}

			//테이블 초기화
			advScheduleServiceDAO.deleteAllBuzzAdItem();
			
			if(newList !=null && newList.size() > 0)  {
				//멀티 인서트
				advScheduleServiceDAO.insertMultiBuzzAdItem(newList);	
			}
			
		} catch (Exception e) {
			httpClient.closeConnection();
			e.printStackTrace();
			logger.info("BUZZVIL getBuzzAdList Error System : message["+e.getMessage()+"] ");
		}
	}
	
	private static boolean isValidBuzzData(BuzzAdItem data){
		if(!StringUtils.isNumeric(data.getId())) {
			return false;
		}

		if(StringUtils.isEmpty(data.getTitle()) || StringUtils.isEmpty(data.getDescription())) {
			return false;
		}
		
		return true;
	}

	@Override
	public void calculateLinkpricePoint() {
		//링크프라이스 정산 처리는 매월 6일 전전월 건을 대상으로 처리됨.
		//따라서 매월 7일 새벽에 처리하도록 하는게 바람직함.
		
		//현재 일자를 구해서 7일인지 확인
		String today = DateUtil.getToday("yyyyMMdd");

		if(today.endsWith("07")) {
			Date targetMonth = DateUtil.addMonth(-2);		// 정산 대상은 두달
			
			//1일부터 일자별로 순차적으로 돌린다.
			Date firstDate = DateUtil.getDate(DateUtil.getDate2String(targetMonth, "yyyyMM")+"01", "yyyyMMdd");
			for(int i = 0; i < 31; i++){
				Date targetDate = DateUtil.addDate(firstDate, i);
				String targetDay = DateUtil.getDate2String(targetDate, "yyyyMMdd");
				
				if(targetDay.endsWith("01") && i > 0) {
					break;
				}

				LinkPriceMessage dayResult = null;
				
				try {
					dayResult = getDailyLinkpricePoint(targetDay);
					
					if(STATUS_PROCESSING.equals(dayResult.getStatus())){
						
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				//일자별 메세지 로그
				if(dayResult == null) {
					dayResult = new LinkPriceMessage();
					dayResult.setStatus(STATUS_FAIL);
				}

				advScheduleServiceDAO.insertLinkpriceShopHistMain(dayResult);	

				int total = 0;
				int success = 0;
				int fail = 0;
				
				if(STATUS_PROCESSING.equals(dayResult.getStatus())) {
					
					for ( LinkPriceOrderItem item : dayResult.getOrder_list()) {

						item.setMain_idx(dayResult.getIdx());
						item.setDay(targetDay);
						
						logger.debug(" LinkPriceOrderItem : token "+item.getUser_id()+", Status "+item.getStatus()+" / "+item.getP_nm());
						
						if(StringUtils.isEmpty(item.getUser_id())){
							item.setUser_token("NULL");
							item.setStatus(STATUS_FAIL);
						} else {
							if("210".equals(item.getStatus()))
								plusPoint(item);
							else if("310".equals(item.getStatus()))
								minusPoint(item);
							else {
								item.setStatus(STATUS_FAIL);
							}
						}

						total ++;
						if(STATUS_COMPLETE.equals(item.getStatus()))
							success ++;
						else
							fail ++;

						advScheduleServiceDAO.insertLinkpriceShopHistDtl(item);
					}
					
					dayResult.setStatus(STATUS_COMPLETE);
				}
				
				//최종 처리 결과 업데이트
				dayResult.setTotal(total);
				dayResult.setSuccess(success);
				dayResult.setFail(fail);
				advScheduleServiceDAO.updateLinkpriceShopHistMain(dayResult);
				
			}
			
		} else {
			//TODO 미처리 건을 어찌할지를 고민하자.
		}
	}
	
	private LinkPriceMessage getDailyLinkpricePoint(String date){
		LinkPriceMessage result = new LinkPriceMessage();
		HttpClientUtil httpClient = null;
		try {
			
			/*
			a_id			필수 링크프라이스에 가입된 어필리에이트 아이디
			auth_key		필수 링크프라이스가 발행한 인증키값
			yyyymmdd		필수 조회일자
						ex) 20150515 또는 201505 (실적취소 조회 시에만 가능함)
			cancel_flag		옵션 취소주문 조회
						Y: 취소 실적 조회
			currency		옵션 화폐 명시 (판매금액 및 커미션이 환율 적용됨)
						USD: 달러화
						EUR: 유로화
						CNY: 위안화
			cmd			옵션 부가 명령어
						total: 전체 실적 조회
						addon: 조회월 이후에 생성된 추가 실적 조회
			merchant_id		옵션 해당 머천트 ID 의 실적만 조회
			*/
			
			httpClient = new HttpClientUtil("GET", LINKPRICE_RESULT_LIST);
			httpClient.addHeader("Content-Type", "text/html");

			httpClient.addParameter("a_id", LINKPRICE_A_ID);
			httpClient.addParameter("auth_key", LINKPRICE_AUTH_KEY);
			httpClient.addParameter("yyyymmdd", date);
			//httpClient.addParameter("cancel_flag", "");
			//httpClient.addParameter("currency", "");
			httpClient.addParameter("status", "210");
			//httpClient.addParameter("merchant_id", "");

			int http_status = httpClient.sendRequest();
			
			logger.info("LinkPrice getDailyLinkpricePoint response status : " + http_status);
			
			List<LinkPriceOrderItem> newList = new ArrayList<LinkPriceOrderItem>();
			if(http_status == HttpStatus.SC_OK){
				result = httpClient.getResponseJson(LinkPriceMessage.class);
				result.setDay(date);	//처리일자 셋팅
				
				logger.debug("Result : "+result.getResult()+", count ("+result.getList_count()+")");
				
				//0 : 정상 (0 조회 성공  100 어필 아이디 없음  200 조회일자 없음  300 인증키가 맞지 않음)
				if("0".equals(result.getResult())) {
					List<LinkPriceOrderItem> orgList = result.getOrder_list();
					
					int cnt = 0;
					if(orgList != null && orgList.size() > 0) {
						for(LinkPriceOrderItem data : orgList) {
							data.setIt_cnt(toNumber(data.getIt_cnt()));
							
							// 20171107, 100%로 변경
							int commission = Integer.parseInt(toNumber(data.getCommission()));
							data.setCommission(""+commission);
							data.setSales(toNumber(data.getSales()));
							data.setUser_token(data.getUser_id());
							
							//TODO 데이터 검증 후 newList 에 add
							newList.add(data);
							cnt ++;
						}
					}
					
					result.setTotal(cnt);
					result.setStatus(STATUS_PROCESSING); //처리 중으로 셋팅
					logger.debug("LinkPrice OrderList Size : "+newList.size()+" / "+orgList.size());
					logger.info("LinkPrice getDailyLinkpricePoint Success Response");
				} else {
					result.setTotal(0);
					result.setStatus(STATUS_FAIL); //처리 실패로 셋팅
					result.setResultMsg(result.getResult());
					logger.info("LinkPrice getDailyLinkpricePoint Error Fail Response");
				}
			} else {
				result.setTotal(0);
				result.setStatus(STATUS_FAIL); //처리 실패로 셋팅
				result.setResultMsg("통신 실패");
				logger.info("LinkPrice getDailyLinkpricePoint Error HTTP : code["+http_status+"] ");
			}
			
		} catch (Exception e) {
			httpClient.closeConnection();
			e.printStackTrace();
			
			result.setTotal(0);
			result.setStatus(STATUS_FAIL); //처리 실패로 셋팅
			result.setResultMsg("서버 오류 : "+e.getMessage());
			logger.info("LinkPrice getDailyLinkpricePoint Error System : message["+e.getMessage()+"] ");
		}
		
		return result;	
	}
	
	private void plusPoint(LinkPriceOrderItem data) {

		ClipPointMessage result = new ClipPointMessage();
		
		HttpClientUtil conn = null;
		try {
			conn = new HttpClientUtil("POST", API_DOMAIN_SEND_ADV_PLUS_POINT);
			conn.addHeader("Content-Type", "application/x-www-form-urlencoded");
			
			if(StringUtils.isEmpty(data.getUser_token())) {
				data.setStatus(STATUS_FAIL);
			} else {
				conn.addParameter("user_token", URLEncoder.encode(data.getUser_token(), "UTF-8"));
			}

			conn.addParameter("transaction_id", data.getTrlog_id());
			conn.addParameter("requester_code", "shopbuy");
			conn.addParameter("point_value", toNumber(data.getCommission()));
			
			String desc = "["+data.getM_id()+"] 상품 구매 ("+data.getP_nm()+")";
			conn.addParameter("description", URLEncoder.encode(desc, "UTF-8"));
			
			int ret = conn.sendRequest();
			logger.info("LinkPrice plusPoint response status : " + ret);

			if(HttpStatus.SC_OK == ret) {
				result = conn.getResponseJson(ClipPointMessage.class);
				data.setStatus(STATUS_COMPLETE);
				logger.info("LinkPrice plusPoint Success Response");
			} else if (HttpStatus.SC_NOT_FOUND == ret) {
				data.setStatus(STATUS_FAIL);
				result.setResultMsg("처리도중 오류가 발생하였습니다.");
				logger.info("LinkPrice plusPoint Error HTTP : code["+ret+"] ");
			} else {
				String resMsg = conn.getResponseString();
				
				if(resMsg != null && resMsg.contains("code: 600")) {
					data.setStatus(STATUS_DUPLICATE);
					result.setResultMsg("중복 적립.");
				} else {
					data.setStatus(STATUS_FAIL);
					result.setResultMsg("처리도중 오류가 발생하였습니다.");
				}
				
				logger.info("LinkPrice plusPoint Error HTTP : code["+ret+"] ");
			}
			
		} catch (Exception e) {
			conn.closeConnection();
			logger.error(e.getMessage());
			data.setStatus(STATUS_WAITING);
			result.setResultMsg("처리도중 오류가 발생하였습니다.");
			logger.info("LinkPrice plusPoint Error System : message["+e.getMessage()+"] ");
		}

	}
	
	private void minusPoint(LinkPriceOrderItem data) {

		ClipPointMessage result = new ClipPointMessage();
		
		HttpClientUtil conn = null;
		try {
			conn = new HttpClientUtil("POST", API_DOMAIN_SEND_ADV_MINUS_POINT);
			conn.addHeader("Content-Type", "application/x-www-form-urlencoded");
			
			if(StringUtils.isEmpty(data.getUser_token())) {
				data.setStatus(STATUS_FAIL);
			} else {
				conn.addParameter("user_token", URLEncoder.encode(data.getUser_token(), "UTF-8"));
			}

			conn.addParameter("transaction_id", data.getTrlog_id());
			conn.addParameter("requester_code", "shopbuy");
			conn.addParameter("point_value", toNumber(data.getCommission()));
			
			String desc = "["+data.getM_id()+"] 상품 구매 취소 ("+data.getP_nm()+")";
			conn.addParameter("description", URLEncoder.encode(desc, "UTF-8"));
			
			int ret = conn.sendRequest();
			logger.info("LinkPrice minusPoint response status : " + ret);

			if(HttpStatus.SC_OK == ret) {
				result = conn.getResponseJson(ClipPointMessage.class);
				data.setStatus(STATUS_COMPLETE);
				logger.info("LinkPrice minusPoint Success Response");
			} else if (HttpStatus.SC_NOT_FOUND == ret) {
				data.setStatus(STATUS_FAIL);
				result.setResultMsg("처리도중 오류가 발생하였습니다.");
				logger.info("LinkPrice minusPoint Error HTTP : code["+ret+"] ");
			} else {
				String resMsg = conn.getResponseString();
				
				if(resMsg != null && resMsg.contains("code: 600")) {
					data.setStatus(STATUS_DUPLICATE);
					result.setResultMsg("중복 차감.");
				} else {
					data.setStatus(STATUS_FAIL);
					result.setResultMsg("처리도중 오류가 발생하였습니다.");
				}
				
				logger.info("LinkPrice minusPoint Error HTTP : code["+ret+"] ");
			}
			
		} catch (Exception e) {
			conn.closeConnection();
			logger.error(e.getMessage());
			data.setStatus(STATUS_WAITING);
			result.setResultMsg("처리도중 오류가 발생하였습니다.");
			logger.info("LinkPrice minusPoint Error System : message["+e.getMessage()+"] ");
		}

	}
	
	private static String toNumber(String value) {
		try {
			return Integer.valueOf(value).toString();
		} catch (Exception e){}
		
		return "0";
	}

	@Override
	public void calculateLinkpricePointManual(String targetMonth) {
		//링크프라이스 정산 처리 수동 처리 로직
		//따라서 매월 7일 새벽에 처리하도록 하는게 바람직함.
					
		//1일부터 일자별로 순차적으로 돌린다.
		Date firstDate = DateUtil.getDate(targetMonth+"01", "yyyyMMdd");
		for(int i = 0; i < 31; i++){
			Date targetDate = DateUtil.addDate(firstDate, i);
			String targetDay = DateUtil.getDate2String(targetDate, "yyyyMMdd");
			
			if(targetDay.endsWith("01") && i > 0) {
				break;
			}

			LinkPriceMessage dayResult = null;
			
			try {
				dayResult = getDailyLinkpricePoint(targetDay);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			//일자별 메세지 로그
			if(dayResult == null) {
				dayResult = new LinkPriceMessage();
				dayResult.setStatus(STATUS_FAIL);
			}

			LinkPriceMessage linkMainInfo = advScheduleServiceDAO.selectLinkpriceShopHistMain(targetDay);
			if(linkMainInfo == null) {
				advScheduleServiceDAO.insertLinkpriceShopHistMain(dayResult);
			}else {
				dayResult.setIdx(linkMainInfo.getIdx());
				advScheduleServiceDAO.updateLinkpriceShopHistMain(dayResult);
			}

			int total = 0;
			int success = 0;
			int fail = 0;
			
			if(STATUS_PROCESSING.equals(dayResult.getStatus())) {
				
				for ( LinkPriceOrderItem item : dayResult.getOrder_list()) {

					item.setMain_idx(dayResult.getIdx());
					item.setDay(targetDay);
					
					logger.debug(" LinkPriceOrderItem : token "+item.getUser_id()+", Status "+item.getStatus()+" / "+item.getP_nm());
					
					LinkPriceMessage linkDtlInfo = advScheduleServiceDAO.selectLinkpriceShopHistDtl(item.getTrlog_id());
					if(linkDtlInfo != null) {	// 이미처리된 건은 건수 카운트만 플러스해서 main테이블에 업데이트할때 합산되도록 한다.
						total ++;
						if(STATUS_COMPLETE.equals(linkDtlInfo.getStatus())) {
							success ++;
						}else {
							fail ++;
						}
						//logger.debug("Already processed");
					}else { // 신규 처리건
						if(StringUtils.isEmpty(item.getUser_id())){
							item.setUser_token("NULL");
							item.setStatus(STATUS_FAIL);
						} else {
							if("210".equals(item.getStatus()))
								plusPoint(item);
							else if("310".equals(item.getStatus()))
								minusPoint(item);
							else {
								item.setStatus(STATUS_FAIL);
							}
						}

						total ++;
						if(STATUS_COMPLETE.equals(item.getStatus()))
							success ++;
						else
							fail ++;

						advScheduleServiceDAO.insertLinkpriceShopHistDtl(item);
					}
					
				}
				dayResult.setStatus(STATUS_COMPLETE);
			}
			
			//최종 처리 결과 업데이트
			dayResult.setTotal(total);
			dayResult.setSuccess(success);
			dayResult.setFail(fail);
			advScheduleServiceDAO.updateLinkpriceShopHistMain(dayResult);
			
		}
		
	}

}