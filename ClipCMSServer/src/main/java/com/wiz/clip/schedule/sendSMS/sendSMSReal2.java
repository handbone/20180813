package com.wiz.clip.schedule.sendSMS;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class sendSMSReal2 {
	
	Logger logger = Logger.getLogger(sendSMS.class.getName());

	@Autowired
	private SqlSession postgreSession;

	@RequestMapping(value = "/sendSMSReal2.do")
	@ResponseBody
	public String sendSMSReal2(@RequestParam Map map)  throws java.lang.Exception {
	
		//String smsDomain = "https://msg.tbapi.kt.com/MESG";
		//String smsDomain = "https://msg.api.kt.com/MESG";
		String smsDomain = map.get("smsDomain").toString();
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mes=\"http://xml.kt.com/sdp/so/BS/MessageSendSMSReportNoNetCharge\">");
		sb.append("<soapenv:Header>");
		sb.append("      <sec:Security xmlns:sec=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">");
		sb.append("         <sec:UsernameToken>");
//		sb.append("			<sec:Username>AII9200051990CEIFZI</sec:Username>");
//		sb.append("         <sec:Password>TBK9200051990CBIXCI</sec:Password>");
		sb.append("			<sec:Username>AII0240060263GAAIXU</sec:Username>");
		sb.append("         <sec:Password>SVK0240060263FXBAAU</sec:Password>");
		sb.append("      </sec:UsernameToken>");
		sb.append("   </sec:Security>");
		sb.append("</soapenv:Header>");
		sb.append("<soapenv:Body>");
		sb.append("   <mes:MessageSendSMSReportNoNetCharge>");
		sb.append("      <MSG_TYPE>S001</MSG_TYPE>");
		sb.append("      <MSG_CONTENT>SMS Send Test </MSG_CONTENT>");
		sb.append("      <PFORM_TYPE>1</PFORM_TYPE>");
		sb.append("      <SERVICE_TYPE>A</SERVICE_TYPE>");
		sb.append("      <CALL_CTN>01085865753</CALL_CTN>");
		//sb.append("      <RCV_CTN>01073481452</RCV_CTN>");
		sb.append("      <RCV_CTN>"+map.get("ctn").toString()+"</RCV_CTN>");
		sb.append("     </mes:MessageSendSMSReportNoNetCharge>");
		sb.append(" </soapenv:Body>");
		sb.append("</soapenv:Envelope>");

		logger.debug("::::"+sb.toString());
		
		BufferedReader oBufReader = null;
		HttpURLConnection httpConn = null;
		String strBuffer = "";
		String strRslt = "";
		
		String strEncodeUrl = smsDomain;

		URL oOpenURL = new URL(strEncodeUrl);
		httpConn = (HttpURLConnection) oOpenURL.openConnection();
		httpConn.setDoOutput(true);
		httpConn.setRequestMethod("POST");
		httpConn.setConnectTimeout(10000);
		httpConn.setReadTimeout(10000);
		httpConn.setRequestProperty("Content-Length", String.valueOf(sb.length()));
		httpConn.setRequestProperty("Content-Type", "application/soap+xml; charset=utf-8");
		OutputStream os = httpConn.getOutputStream();
		os.write(sb.toString().getBytes());
		os.flush();
		os.close();
		oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
		StringBuffer sb2 = new StringBuffer(strRslt);
			
		while ((strBuffer = oBufReader.readLine()) != null) {
			if (strBuffer.length() > 1) {
				sb2.append(strBuffer);
			}
		}
		oBufReader.close();
			
		return sb2.toString();
	}
	
	
}
