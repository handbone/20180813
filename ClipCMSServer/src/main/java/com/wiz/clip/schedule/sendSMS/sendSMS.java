package com.wiz.clip.schedule.sendSMS;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Component
public class sendSMS {
	
	Logger logger = Logger.getLogger(sendSMS.class.getName());
	
	@Autowired
	private SqlSession postgreSession;
	
	@Value("#{config['API_DOMAIN_SEND_SMS']}") String API_DOMAIN_SEND_SMS;
	
	@Value("#{config['CHARSET_ENCODING_TYPE']}") String CHARSET_ENCODING_TYPE;


	public String sendSMS(@RequestParam Map map)  throws java.lang.Exception {
	
		String smsDomain = API_DOMAIN_SEND_SMS;
		
		StringBuffer sb = new StringBuffer();
//		StringBuffer sb2 = null;
		

		sb.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mes=\"http://xml.kt.com/sdp/so/BS/MessageSendSMSReportNoNetCharge\">");
		sb.append("<soapenv:Header>");
		sb.append("      <sec:Security xmlns:sec=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">");
		sb.append("         <sec:UsernameToken>");
		// tb test
//		sb.append("			<sec:Username>AII9200051990CEIFZI</sec:Username>");
//		sb.append("         <sec:Password>TBK9200051990CBIXCI</sec:Password>");
		// kt 상용
		sb.append("			<sec:Username>AII0240060263GAAIXU</sec:Username>");
		sb.append("			<sec:Password>SVK0240060263FXBAAU</sec:Password>");
		sb.append("      </sec:UsernameToken>");
		sb.append("   </sec:Security>");
		sb.append("</soapenv:Header>");
		sb.append("<soapenv:Body>");
		sb.append("   <mes:MessageSendSMSReportNoNetCharge>");
		sb.append("      <MSG_TYPE>S001</MSG_TYPE>");
		sb.append("      <MSG_CONTENT>"+map.get("msg_content").toString()+"</MSG_CONTENT>");
		sb.append("      <PFORM_TYPE>1</PFORM_TYPE>");
		sb.append("      <SERVICE_TYPE>A</SERVICE_TYPE>");
		// CALL_CTN 은 "발신자 정보 없음" 으로 출력된다. 하지만 형식은 맞춰서 로그에 남김
		sb.append("      <CALL_CTN>"+map.get("call_ctn").toString()+"</CALL_CTN>");
//		sb.append("      <RCV_CTN>01029331810</RCV_CTN>");
		sb.append("      <RCV_CTN>"+map.get("rcv_ctn").toString()+"</RCV_CTN>");
		sb.append("     </mes:MessageSendSMSReportNoNetCharge>");
		sb.append(" </soapenv:Body>");
		sb.append("</soapenv:Envelope>");

		logger.debug("::::"+sb.toString());
		
		BufferedReader oBufReader = null;
		HttpURLConnection httpConn = null;
		String strBuffer = "";
		String strRslt = "";
		
		String strEncodeUrl = smsDomain;

		URL oOpenURL = new URL(strEncodeUrl);
		httpConn = (HttpURLConnection) oOpenURL.openConnection();
		httpConn.setDoOutput(true);
		httpConn.setRequestMethod("POST");
		httpConn.setConnectTimeout(10000);
		httpConn.setReadTimeout(10000);
		httpConn.setRequestProperty("Content-Length", String.valueOf(sb.length()));
		httpConn.setRequestProperty("Content-Type", "application/soap+xml; charset=" + CHARSET_ENCODING_TYPE);
		OutputStream os = httpConn.getOutputStream();
		os.write(sb.toString().getBytes());
		os.flush();
		os.close();
		oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
		StringBuffer sb2 = new StringBuffer(strRslt);
			
		while ((strBuffer = oBufReader.readLine()) != null) {
			if (strBuffer.length() > 1) {
				sb2.append(strBuffer);
			}
		}
		oBufReader.close();
			
		return sb2.toString();
	}
	
	
}
