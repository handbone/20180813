package com.wiz.clip.clip3_0.bean;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 쇼핑몰 립 상품 테이블
 */
public class LinkPriceItemInfo {
	
	private String mall_id;
	private String mall_name;
	private String mall_image_url;
	private String item_id;
	private String item_name;
	private String item_image_url;
	private String reward;
	private String content;
	private String description;
	private String use_yn;
	private String regdate;
	
	private String ord;
	private String excelDownload;
	
	// 컬럼 추가
	private String reward_kt;
	
	
	public String getReward_kt() {
		return reward_kt;
	}
	public void setReward_kt(String reward_kt) {
		this.reward_kt = reward_kt;
	}
	public String getMall_id() {
		return mall_id;
	}
	public void setMall_id(String mall_id) {
		this.mall_id = mall_id;
	}
	public String getMall_name() {
		return mall_name;
	}
	public void setMall_name(String mall_name) {
		this.mall_name = mall_name;
	}
	public String getMall_image_url() {
		return mall_image_url;
	}
	public void setMall_image_url(String mall_image_url) {
		this.mall_image_url = mall_image_url;
	}
	public String getItem_id() {
		return item_id;
	}
	public void setItem_id(String item_id) {
		this.item_id = item_id;
	}
	public String getItem_name() {
		return item_name;
	}
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
	public String getItem_image_url() {
		return item_image_url;
	}
	public void setItem_image_url(String item_image_url) {
		this.item_image_url = item_image_url;
	}
	public String getReward() {
		return reward;
	}
	public void setReward(String reward) {
		this.reward = reward;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUse_yn() {
		return use_yn;
	}
	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	public String getOrd() {
		return ord;
	}
	public void setOrd(String ord) {
		this.ord = ord;
	}
	public String getExcelDownload() {
		return excelDownload;
	}
	public void setExcelDownload(String excelDownload) {
		this.excelDownload = excelDownload;
	}
	
	@Override
	public String toString(){
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	} 
	
}
