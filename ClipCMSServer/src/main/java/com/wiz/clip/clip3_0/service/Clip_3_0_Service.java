/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.clip3_0.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;

import com.wiz.clip.clip3_0.bean.LinkPriceItemInfo;
import com.wiz.clip.clip3_0.bean.LinkPriceReportItem;
import com.wiz.clip.clip3_0.bean.LinkPriceReportMessege;
import com.wiz.clip.clip3_0.bean.LinkpriceShopHistDtl;
import com.wiz.clip.clip3_0.bean.MainBannerDO;
import com.wiz.clip.clip3_0.bean.PointCouponHist;
import com.wiz.clip.clip3_0.bean.PointbombInfo;
import com.wiz.clip.clip3_0.bean.ScreenLockDAO;
import com.wiz.clip.clip3_0.bean.ScreenLockRandomPointMDAO;
import com.wiz.clip.clip3_0.bean.UserTokenInfo;
import com.wiz.clip.manage.pointChk.bean.SearchBean;

public interface Clip_3_0_Service {

	public List<?> getCardpointChangeList(ModelMap map) throws Exception;

	public String getCardpointChangeListTotalValue(ModelMap map) throws Exception;

	public String getCardpointChangeListTotalCount(ModelMap map) throws Exception;

	/**
	 * User Token 조회(user_ci, ctn, ga_ci, cust_id)
	 * @author jyi
	 * @param searchBean
	 * @return UserTokenInfo
	 * @throws java.lang.Exception
	 */	
	public List<UserTokenInfo> selectUserToken(SearchBean searchBean) throws java.lang.Exception;

	/**
	 * 익월적립내역
	 * 
	 * @param LinkPriceReportMessege
	 * @return LinkPriceReportMessege
	 * @throws java.lang.Exception
	 */
	public LinkPriceReportMessege getNextSavingHistory(LinkPriceReportMessege param) throws java.lang.Exception;
	
	/**
	 * 익익월적립내역
	 * 
	 * @param LinkPriceReportMessege
	 * @return LinkPriceReportMessege
	 * @throws java.lang.Exception
	 */
	public LinkPriceReportMessege getNext2SavingHistory(LinkPriceReportMessege param) throws java.lang.Exception;

	/**
	 * 주문수량 조회
	 * @author jyi
	 * @param order_code
	 * @return String
	 * @throws java.lang.Exception
	 */
	public String selectItemCount(LinkPriceReportItem item) throws java.lang.Exception;

	/**
	 * 포인트 쿠폰 이용내역 리스트 개수 조회
	 * @author jyi
	 * @param ModelMap
	 * @return String
	 * @throws java.lang.Exception
	 */
	public String selectCouponListCount(ModelMap model) throws java.lang.Exception;

	/**
	 * user_ci 조회 (user_ci, ctn, ga_ci, cust_id)
	 * @author jyi
	 * @param ModelMap
	 * @return UserTokenInfo
	 * @throws java.lang.Exception
	 */
	public List<UserTokenInfo> selectUserCi(ModelMap model) throws java.lang.Exception;

	/**
	 * 포인트 쿠폰 이용내역 적립 예정 포인트 합계 조회
	 * @author jyi
	 * @param ModelMap
	 * @return String
	 * @throws java.lang.Exception
	 */
	public String selectCoupontotalPoint(ModelMap model) throws java.lang.Exception;

	/**
	 * 포인트 쿠폰 이용내역 리스트 조회
	 * @author jyi
	 * @param ModelMap
	 * @return PointCouponHist
	 * @throws java.lang.Exception
	 */
	public List<PointCouponHist> selectPointCouponHistList(ModelMap model) throws java.lang.Exception;

	/**
	 * User Info tbl 조회 test
	 * @author jyi
	 * @return UserTokenInfo
	 * @throws java.lang.Exception
	 */
	public List<UserTokenInfo> selectUserInfoList() throws java.lang.Exception;

	/** 쇼핑 적립 상품 리스트 */
	public List<LinkPriceItemInfo> getShoppinMallList() throws Exception;
	
	public List<LinkpriceShopHistDtl> getShoppinMallSavingDetail(LinkpriceShopHistDtl linkpriceShopHistDtl) throws Exception;

	/**
	 * 쇼핑 적립 내역 리스트 개수
	 * @author jyi
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	public String selectShopHistCount(ModelMap model) throws Exception;

	/**
	 * 쇼핑 적립 내역 리스트 조회
	 * @author jyi
	 * @param model
	 * @return LinkpriceShopHistDtl
	 * @throws Exception
	 */
	public List<LinkpriceShopHistDtl> selectShopHistList(ModelMap model) throws Exception;
	
	/**
	 * 쇼핑 적립 내역 포인트 합계
	 * @author jyi
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public String selectShopHistTotalvalue(ModelMap model) throws Exception;
	
	public List<ScreenLockDAO> getScreenLockList() throws Exception;

	public ScreenLockDAO getScreenLockListDetail(String index) throws Exception;

	public int setScreenLockData(ScreenLockDAO obj) throws Exception;

	public ScreenLockRandomPointMDAO getScreenLockRandomPointManagment() throws Exception;

	public int setLock_Screen_Randompoint(ScreenLockRandomPointMDAO arg) throws Exception;

	/**
	 * 메인 배너 신규 등록
	 * @author jyi
	 * @param arg
	 * @throws Exception
	 */
	public void insertMainBannerInfoTbl(MainBannerDO arg) throws Exception;

	/**
	 * 메인 배너 노출순위 조회
	 * @return MainBannerDO
	 * @throws Exception
	 */
	public MainBannerDO selectMainbannerInfoMaxRank() throws Exception;

	/**
	 * 메인 배너 row counting
	 * @param ModelMap
	 * @return
	 * @throws Exception
	 */
	public String selectMainbannerInfoTotalcount(ModelMap model) throws Exception;

	/**
	 * mainbanner_info_tbl 조회
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public List<MainBannerDO> selectMainbannerInfoList(ModelMap model) throws Exception;

	/**
	 * 메인 배너 상세 조회
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public MainBannerDO selectMainbannerInfoOne(ModelMap model) throws Exception;

	/**
	 * 메인 베너 수정
	 * @param MainBannerDO
	 * @throws Exception
	 */
	public void updateMainBannerInfoTbl(MainBannerDO arg) throws Exception;

	/**
	 * 메인 배너 항목 삭제
	 * @param MainBannerDO
	 * @throws Exception
	 */
	public void deleteMainBannerInfoTbl(MainBannerDO arg) throws Exception;

	/**
	 * 포인트 폭탄 리스트 개수 조회
	 * @author jyi
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public String selectPointbombTotalcount(ModelMap model) throws Exception;

	/**
	 * 포인트 폭탄 리스트 조회
	 * @author jyi
	 * @param model
	 * @return PointbombInfo
	 * @throws Exception
	 */
	public List<PointbombInfo> selectPointbombList(ModelMap model) throws Exception;

	/**
	 * 쇼핑적립 관리 노출 순서 조회
	 * @author jyi
	 * @return
	 * @throws Exception
	 */
	public LinkPriceItemInfo selectLinkPriceItemInfoMaxRank() throws Exception;

	/**
	 * 쇼핑적립 관리 항목 신규등록
	 * @author jyi
	 * @param LinkPriceItemInfo
	 * @throws Exception
	 */
	public void insertLinkPriceItemInfo(LinkPriceItemInfo arg) throws Exception;

	/**
	 * 쇼핑적립 관리 리스트 조회
	 * @author jyi
	 * @return LinkPriceItemInfo
	 * @throws Exception
	 */
	public List<LinkPriceItemInfo> selectLinkPriceItemInfoList() throws Exception;

	/**
	 * 쇼핑적립 관리 상세 조회
	 * @author jyi
	 * @param model
	 * @return LinkPriceItemInfo
	 * @throws Exception
	 */
	public LinkPriceItemInfo selectLinkPriceItemInfoOne(ModelMap model) throws Exception;

	/**
	 * 쇼핑적립 관리 노출 순서 수정
	 * @param LinkPriceItemInfo
	 * @throws Exception
	 */
	public void updateLinkPriceItemInfo(LinkPriceItemInfo arg) throws Exception;

	/**
	 * 쇼핑적립 관리 항목 삭제
	 * @param LinkPriceItemInfo
	 * @throws Exception
	 */
	public void deleteLinkPriceItemInfo(LinkPriceItemInfo arg) throws Exception;

	/**
	 * 쇼핑적립 관리 중복 id check
	 * @param arg 
	 * @return
	 * @throws Exception
	 */
	public String selectLinkPriceItemInfoIdcheck(LinkPriceItemInfo arg) throws Exception;

	/**
	 * 쇼핑적립 당월 적립 예정 내역 조회
	 * @author jyi
	 * @param LinkPriceReportMessege
	 * @return LinkPriceReportMessege
	 */
	public LinkPriceReportMessege getNowSavingHistory(LinkPriceReportMessege param) throws Exception;

	/**
	 * 카드포인트 전환조회 대용량 엑셀 다운로드
	 * @param map
	 * @param res
	 * @param req 
	 * @param model 
	 * @throws Exception
	 */
	public void selectExcelCardpointChangeList(ModelMap map, HttpServletResponse res, HttpServletRequest req, Model model) throws Exception;

	/**
	 * 카드 포인트 전환조회 망취소 응답포인트 합계
	 * @param map
	 * @return String
	 * @throws Exception
	 */
	public String selectCardpointChangeCancelvalue(ModelMap map) throws Exception;

	/**
	 * 카드 포인트 전환조회 원거래 조회
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public String selectCardpointTransId(ModelMap map) throws Exception;

	/**
	 * 카드 포인트 전환조회 전환 요청포인트 합계 조회
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public String selectCardpointTotalPoint(ModelMap map) throws Exception;


}
