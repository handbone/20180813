package com.wiz.clip.clip3_0.bean;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class CardPointSwapList {
	
	private String trans_div;//	거래 구분
	private String trans_req_date;//	거래 요청 일자
	private String trans_res_date;//	거래 응답 일자
	private String reg_source;//	등록 매체코드
	private String user_ci;//	사용자 CI
	private String trans_id;//	거래 고유 번호
	private String ref_trans_id;//_id	거래 고유 번호
	private String res_point;//	거래 요청 포인트
	private String req_point;//	거래 응답 포인트
	private String res_code;//	거래 응답 코드
	private String result_code;//	거래 결과 코드
	private String approve_no;//	승인번호
	private String approve_date;//	승인일자
	private String approve_time;//	승인시간
	private String regdate;//	등록일자
	private String cust_id_org;
	
	/** 2017.10.16 jyi 거래 응답 코드 문구 */
	private String res_message;
	
	// 2018.01.29 정산금액 추가
	private String price_point;

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
	public String getPrice_point() {
		return price_point;
	}
	public void setPrice_point(String price_point) {
		this.price_point = price_point;
	}
	public String getRes_message() {
		return res_message;
	}
	public void setRes_message(String res_message) {
		this.res_message = res_message;
	}
	public String getTrans_div() {
		return trans_div;
	}
	public void setTrans_div(String trans_div) {
		this.trans_div = trans_div;
	}
	public String getTrans_req_date() {
		return trans_req_date;
	}
	public void setTrans_req_date(String trans_req_date) {
		this.trans_req_date = trans_req_date;
	}
	public String getTrans_res_date() {
		return trans_res_date;
	}
	public void setTrans_res_date(String trans_res_date) {
		this.trans_res_date = trans_res_date;
	}
	public String getReg_source() {
		return reg_source;
	}
	public void setReg_source(String reg_source) {
		this.reg_source = reg_source;
	}
	public String getUser_ci() {
		return user_ci;
	}
	public void setUser_ci(String user_ci) {
		this.user_ci = user_ci;
	}
	public String getTrans_id() {
		return trans_id;
	}
	public void setTrans_id(String trans_id) {
		this.trans_id = trans_id;
	}
	public String getRef_trans_id() {
		return ref_trans_id;
	}
	public void setRef_trans_id(String ref_trans_id) {
		this.ref_trans_id = ref_trans_id;
	}
	public String getRes_point() {
		return res_point;
	}
	public void setRes_point(String res_point) {
		this.res_point = res_point;
	}
	public String getReq_point() {
		return req_point;
	}
	public void setReq_point(String req_point) {
		this.req_point = req_point;
	}
	public String getRes_code() {
		return res_code;
	}
	public void setRes_code(String res_code) {
		this.res_code = res_code;
	}
	public String getResult_code() {
		return result_code;
	}
	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}
	public String getApprove_no() {
		return approve_no;
	}
	public void setApprove_no(String approve_no) {
		this.approve_no = approve_no;
	}
	public String getApprove_date() {
		return approve_date;
	}
	public void setApprove_date(String approve_date) {
		this.approve_date = approve_date;
	}
	public String getApprove_time() {
		return approve_time;
	}
	public void setApprove_time(String approve_time) {
		this.approve_time = approve_time;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	public String getCust_id_org() {
		return cust_id_org;
	}
	public void setCust_id_org(String cust_id_org) {
		this.cust_id_org = cust_id_org;
	}
	
	

}
