package com.wiz.clip.clip3_0.bean;

public class PointbombInfo {
	private String cust_id;
	private String user_ci;
	private String ctn;
	private String ga_id;
	private String day_count;
	private String level;
	private String use_yn;
	private String use_start_date;
	private String base_date;
	private String direct_yn;
	private String banner_id;
	private String base_point;
	private String reward_point;
	private String reward_level;
	private String reward_base;
	private String bomb_count;
	private String modidate;
	
	//20117.11.01 누적 수령 포인트 추가
	private String total_reward_point;
	
	@Override
	public String toString() {
		return "PointbombInfo[ user_ci = " + user_ci + " | cust_id = " + cust_id + " | ctn = " + ctn + " | ga_id = " + ga_id + " | "
				+ "day_count = " + day_count + " | level = " + level + " | use_yn = " + use_yn +  " ]";
	}
	
	
	public String getTotal_reward_point() {
		return total_reward_point;
	}


	public void setTotal_reward_point(String total_reward_point) {
		this.total_reward_point = total_reward_point;
	}


	public String getBase_date() {
		return base_date;
	}

	public void setBase_date(String base_date) {
		this.base_date = base_date;
	}

	public String getDirect_yn() {
		return direct_yn;
	}

	public void setDirect_yn(String direct_yn) {
		this.direct_yn = direct_yn;
	}

	public String getBanner_id() {
		return banner_id;
	}

	public void setBanner_id(String banner_id) {
		this.banner_id = banner_id;
	}

	public String getBase_point() {
		return base_point;
	}

	public void setBase_point(String base_point) {
		this.base_point = base_point;
	}

	public String getReward_point() {
		return reward_point;
	}

	public void setReward_point(String reward_point) {
		this.reward_point = reward_point;
	}

	public String getReward_level() {
		return reward_level;
	}

	public void setReward_level(String reward_level) {
		this.reward_level = reward_level;
	}

	public String getReward_base() {
		return reward_base;
	}

	public void setReward_base(String reward_base) {
		this.reward_base = reward_base;
	}

	public String getBomb_count() {
		return bomb_count;
	}

	public void setBomb_count(String bomb_count) {
		this.bomb_count = bomb_count;
	}

	public String getModidate() {
		return modidate;
	}

	public void setModidate(String modidate) {
		this.modidate = modidate;
	}

	public String getUse_start_date() {
		return use_start_date;
	}

	public void setUse_start_date(String use_start_date) {
		this.use_start_date = use_start_date;
	}

	public String getCust_id() {
		return cust_id;
	}
	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}
	public String getUser_ci() {
		return user_ci;
	}
	public void setUser_ci(String user_ci) {
		this.user_ci = user_ci;
	}
	public String getCtn() {
		return ctn;
	}
	public void setCtn(String ctn) {
		this.ctn = ctn;
	}
	public String getGa_id() {
		return ga_id;
	}
	public void setGa_id(String ga_id) {
		this.ga_id = ga_id;
	}
	public String getDay_count() {
		return day_count;
	}
	public void setDay_count(String day_count) {
		this.day_count = day_count;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getUse_yn() {
		return use_yn;
	}
	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}
	
	
}
