/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.clip3_0.dao;

import java.util.List;

import org.springframework.ui.ModelMap;

import com.wiz.clip.clip3_0.bean.LinkPriceItemInfo;
import com.wiz.clip.clip3_0.bean.LinkPriceReportItem;
import com.wiz.clip.clip3_0.bean.LinkpriceShopHistDtl;
import com.wiz.clip.clip3_0.bean.MainBannerDO;
import com.wiz.clip.clip3_0.bean.PointCouponHist;
import com.wiz.clip.clip3_0.bean.PointbombInfo;
import com.wiz.clip.clip3_0.bean.ScreenLockDAO;
import com.wiz.clip.clip3_0.bean.ScreenLockRandomPointMDAO;
import com.wiz.clip.clip3_0.bean.UserTokenInfo;
import com.wiz.clip.manage.pointChk.bean.SearchBean;

public interface Clip_3_0_DAO  {

	List<?> select_cardpoint_list(ModelMap obj) throws Exception;

	String select_cardpoint_list_totalcount(ModelMap map) throws Exception;

	String select_cardpoint_listcount(ModelMap map) throws Exception;

	/**
	 * user_token 조회(검색조건 전체)
	 * @author jyi
	 * @param searchBean
	 * @return UserTokenInfo
	 */
	List<UserTokenInfo> selectUserTokenSearchAll(SearchBean searchBean) throws Exception;

	/**
	 * user_token 조회(검색조건 선택)
	 * @author jyi
	 * @param searchBean
	 * @return UserTokenInfo
	 */
	List<UserTokenInfo> selectUserTokenSelectOne(SearchBean searchBean) throws Exception;

	String selectItemCount(LinkPriceReportItem item) throws Exception;

	String selectCouponListCount(ModelMap model) throws Exception;

	/**
	 * user_ci 조회(검색조건 전체)
	 * @author jyi
	 * @param searchBean
	 * @return UserTokenInfo
	 */
	List<UserTokenInfo> selectUserCiAll(ModelMap model) throws Exception;

	/**
	 * user_ci 조회(검색조건 선택)
	 * @author jyi
	 * @param searchBean
	 * @return UserTokenInfo
	 */
	List<UserTokenInfo> selectUserCiOne(ModelMap model) throws Exception;

	String selectCoupontotalPoint(ModelMap model) throws Exception;

	List<PointCouponHist> selectPointCouponHistList(ModelMap model) throws Exception;

	List<UserTokenInfo> selectUserInfoList() throws Exception;
	
	
	/** 쇼핑 적립 상품 상점 리스트 */
	List<LinkPriceItemInfo> select_shoppin_mall_list() throws Exception;

	/** 쇼핑 적립 상품 적립 검색 결과 */ 
	List<LinkpriceShopHistDtl> select_shopping_mall_saving_detail() throws Exception;

	String selectShopHistCount(ModelMap model) throws Exception;

	List<LinkpriceShopHistDtl> selectShopHistList(ModelMap model) throws Exception;

	String selectShopHistTotalvalue(ModelMap model) throws Exception;
	
	List<ScreenLockDAO> select_screen_lock_list() throws Exception;

	ScreenLockDAO select_screen_lock(String index) throws Exception;

	int update_screen_lock(ScreenLockDAO obj) throws Exception;

	ScreenLockRandomPointMDAO select_getScreenLockRandomPointManagment() throws Exception;

	int update_Screen_Randompoint(ScreenLockRandomPointMDAO arg) throws Exception;

	void insertMainBannerInfoTbl(MainBannerDO arg) throws Exception;

	MainBannerDO selectMainbannerInfoMaxRank() throws Exception;

	String selectMainbannerInfoTotalcount(ModelMap model) throws Exception;

	List<MainBannerDO> selectMainbannerInfoList(ModelMap model) throws Exception;

	MainBannerDO selectMainbannerInfoOne(ModelMap model) throws Exception;

	void updateMainBannerInfoTbl(MainBannerDO arg) throws Exception;

	void deleteMainBannerInfoTbl(MainBannerDO arg) throws Exception;

	String selectPointbombTotalcount(ModelMap model) throws Exception;

	List<PointbombInfo> selectPointbombList(ModelMap model) throws Exception;

	LinkPriceItemInfo selectLinkPriceItemInfoMaxRank() throws Exception;

	void insertLinkPriceItemInfo(LinkPriceItemInfo arg) throws Exception;

	List<LinkPriceItemInfo> selectLinkPriceItemInfoList() throws Exception;

	LinkPriceItemInfo selectLinkPriceItemInfoOne(ModelMap model) throws Exception;

	void updateLinkPriceItemInfo(LinkPriceItemInfo arg) throws Exception;

	void deleteLinkPriceItemInfo(LinkPriceItemInfo arg) throws Exception;

	String selectLinkPriceItemInfoIdcheck(LinkPriceItemInfo arg) throws Exception;

	String selectCardpointChangeCancelvalue(ModelMap map) throws Exception;

	String selectCardpointTransId(ModelMap map) throws Exception;

	String selectCardpointTotalPoint(ModelMap map) throws Exception;

}
