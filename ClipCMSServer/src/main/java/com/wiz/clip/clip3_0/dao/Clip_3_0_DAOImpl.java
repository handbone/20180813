/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.clip3_0.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.wiz.clip.clip3_0.bean.LinkPriceItemInfo;
import com.wiz.clip.clip3_0.bean.LinkPriceReportItem;
import com.wiz.clip.clip3_0.bean.LinkpriceShopHistDtl;
import com.wiz.clip.clip3_0.bean.MainBannerDO;
import com.wiz.clip.clip3_0.bean.PointCouponHist;
import com.wiz.clip.clip3_0.bean.PointbombInfo;
import com.wiz.clip.clip3_0.bean.ScreenLockDAO;
import com.wiz.clip.clip3_0.bean.ScreenLockRandomPointMDAO;
import com.wiz.clip.clip3_0.bean.UserTokenInfo;
import com.wiz.clip.manage.pointChk.bean.SearchBean;


@Repository
public class Clip_3_0_DAOImpl implements Clip_3_0_DAO  {

	@Autowired
	private SqlSession postgreSession;

	Logger logger = Logger.getLogger(Clip_3_0_DAOImpl.class.getName());

	@Override
	public List<?> select_cardpoint_list(ModelMap obj) throws Exception {
		return postgreSession.selectList("clip30.select_cardpoint_list", obj);
	}

	@Override
	public String select_cardpoint_list_totalcount(ModelMap map) throws Exception {
		return postgreSession.selectOne("clip30.select_cardpoint_list_totalcount", map);
	}

	@Override
	public String select_cardpoint_listcount(ModelMap map) throws Exception {
		return postgreSession.selectOne("clip30.select_cardpoint_listcount", map);
	}

	@Override
	public List<UserTokenInfo> selectUserTokenSearchAll(SearchBean searchBean) throws Exception {
		return postgreSession.selectList("clip30.selectUserTokenSearchAll", searchBean);
	}

	@Override
	public List<UserTokenInfo> selectUserTokenSelectOne(SearchBean searchBean) throws Exception {
		List<UserTokenInfo> data =  postgreSession.selectList("clip30.selectUserTokenSelectOne", searchBean);
		return data;
	}

	@Override
	public String selectItemCount(LinkPriceReportItem item) throws Exception {
		return postgreSession.selectOne("clip30.selectItemCount", item);
	}

	@Override
	public String selectCouponListCount(ModelMap model) throws Exception {
		return postgreSession.selectOne("clip30.selectCouponListCount", model);
	}

	@Override
	public List<UserTokenInfo> selectUserCiAll(ModelMap model) throws Exception {
		return postgreSession.selectList("clip30.selectUserCiAll", model);
	}

	@Override
	public List<UserTokenInfo> selectUserCiOne(ModelMap model) throws Exception {
		return postgreSession.selectList("clip30.selectUserCiOne", model);
	}

	@Override
	public String selectCoupontotalPoint(ModelMap model) throws Exception {
		return postgreSession.selectOne("clip30.selectCoupontotalPoint", model);
	}

	@Override
	public List<PointCouponHist> selectPointCouponHistList(ModelMap model) throws Exception {
		return postgreSession.selectList("clip30.selectPointCouponHistList", model);
	}

	@Override
	public List<UserTokenInfo> selectUserInfoList() throws Exception {
		return postgreSession.selectList("clip30.selectUserInfoList");
	}
	/** 쇼핑 적립 상품 상점 리스트 */
	@Override
	public List<LinkPriceItemInfo> select_shoppin_mall_list() throws Exception {
		return postgreSession.selectList("clip30.select_shoppin_mall_list");
	}
	
	/** 쇼핑 적립 상품 적립 검색 결과 */ 
	@Override
	public List<LinkpriceShopHistDtl> select_shopping_mall_saving_detail() throws Exception {
		return postgreSession.selectList("clip30.select_shopping_mall_saving_detail");
	}

	@Override
	public String selectShopHistCount(ModelMap model) throws Exception {
		return postgreSession.selectOne("clip30.selectShopHistCount", model);
	}

	@Override
	public List<LinkpriceShopHistDtl> selectShopHistList(ModelMap model) throws Exception {
		return postgreSession.selectList("clip30.selectShopHistList", model);
	}

	@Override
	public String selectShopHistTotalvalue(ModelMap model) throws Exception {
		return postgreSession.selectOne("clip30.selectShopHistTotalvalue", model);
	}
	
	@Override
	public List<ScreenLockDAO> select_screen_lock_list() throws Exception {
		return postgreSession.selectList("clip30.select_screen_lock_list");
	}

	@Override
	public ScreenLockDAO select_screen_lock(String index) throws Exception {
		return postgreSession.selectOne("clip30.select_screen_lock", index);
	}

	@Override
	public int update_screen_lock(ScreenLockDAO obj) throws Exception {
		return postgreSession.update("clip30.update_screen_lock", obj);
	}

	@Override
	public ScreenLockRandomPointMDAO select_getScreenLockRandomPointManagment() throws Exception {
		return postgreSession.selectOne("clip30.select_getScreenLockRandomPointManagment");
	}

	@Override
	public int update_Screen_Randompoint(ScreenLockRandomPointMDAO arg) throws Exception {
		return postgreSession.update("clip30.update_Screen_Randompoint", arg);
	}

	@Override
	public void insertMainBannerInfoTbl(MainBannerDO arg) throws Exception {
		postgreSession.insert("clip30.insertMainBannerInfoTbl", arg);
	}

	@Override
	public MainBannerDO selectMainbannerInfoMaxRank() throws Exception {
		return postgreSession.selectOne("clip30.selectMainbannerInfoMaxRank");
	}

	@Override
	public String selectMainbannerInfoTotalcount(ModelMap model) throws Exception {
		return postgreSession.selectOne("clip30.selectMainbannerInfoTotalcount", model);
	}

	@Override
	public List<MainBannerDO> selectMainbannerInfoList(ModelMap model) throws Exception {
		return postgreSession.selectList("clip30.selectMainbannerInfoList", model);
	}

	@Override
	public MainBannerDO selectMainbannerInfoOne(ModelMap model) throws Exception {
		return postgreSession.selectOne("clip30.selectMainbannerInfoOne", model);
	}

	@Override
	public void updateMainBannerInfoTbl(MainBannerDO arg) throws Exception {
		postgreSession.update("clip30.updateMainBannerInfoTbl", arg);
	}

	@Override
	public void deleteMainBannerInfoTbl(MainBannerDO arg) throws Exception {
		postgreSession.delete("clip30.deleteMainBannerInfoTbl", arg);
	}

	@Override
	public String selectPointbombTotalcount(ModelMap model) throws Exception {
		return postgreSession.selectOne("clip30.selectPointbombTotalcount", model);
	}

	@Override
	public List<PointbombInfo> selectPointbombList(ModelMap model) throws Exception {
		return postgreSession.selectList("clip30.selectPointbombList", model);
	}

	@Override
	public LinkPriceItemInfo selectLinkPriceItemInfoMaxRank() throws Exception {
		return postgreSession.selectOne("clip30.selectLinkPriceItemInfoMaxRank");
	}

	@Override
	public void insertLinkPriceItemInfo(LinkPriceItemInfo arg) throws Exception {
		postgreSession.insert("clip30.insertLinkPriceItemInfo", arg);
	}

	@Override
	public List<LinkPriceItemInfo> selectLinkPriceItemInfoList() throws Exception {
		return postgreSession.selectList("clip30.selectLinkPriceItemInfoList");
	}

	@Override
	public LinkPriceItemInfo selectLinkPriceItemInfoOne(ModelMap model) throws Exception {
		return postgreSession.selectOne("clip30.selectLinkPriceItemInfoOne", model);
	}

	@Override
	public void updateLinkPriceItemInfo(LinkPriceItemInfo arg) throws Exception {
		postgreSession.update("clip30.updateLinkPriceItemInfo", arg);
	}

	@Override
	public void deleteLinkPriceItemInfo(LinkPriceItemInfo arg) throws Exception {
		postgreSession.delete("clip30.deleteLinkPriceItemInfo", arg);
	}

	@Override
	public String selectLinkPriceItemInfoIdcheck(LinkPriceItemInfo arg) throws Exception {
		return postgreSession.selectOne("clip30.selectLinkPriceItemInfoIdcheck", arg);
	}

	@Override
	public String selectCardpointChangeCancelvalue(ModelMap map) throws Exception {
		return postgreSession.selectOne("clip30.selectCardpointChangeCancelvalue", map);
	}

	@Override
	public String selectCardpointTransId(ModelMap map) throws Exception {
		return postgreSession.selectOne("clip30.selectCardpointTransId", map);
	}

	@Override
	public String selectCardpointTotalPoint(ModelMap map) throws Exception {
		return postgreSession.selectOne("clip30.selectCardpointTotalPoint", map);
	}
	
}
