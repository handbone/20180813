package com.wiz.clip.clip3_0.bean;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkPriceReportMessege {
	//년월(필수)
	private String yyyymm;
	//사용자 아이디(필수)
	private String uId;
	
	//성공 여부 1-성공, 0-실패
	private String result;
	//에러 코드
	private String resultCode;
	//에러 내용
	private String resultMsg;
	//적립 예정 금액
	private String order_sum;
	//적립 내역 목록
	private List<LinkPriceReportItem> order_list;
	
	private String order_count;
	
	
	public String getOrder_count() {
		return order_count;
	}
	public void setOrder_count(String order_count) {
		this.order_count = order_count;
	}
	public String getYyyymm() {
		return yyyymm;
	}
	public void setYyyymm(String yyyymm) {
		this.yyyymm = yyyymm;
	}
	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	public String getOrder_sum() {
		return order_sum;
	}
	public void setOrder_sum(String order_sum) {
		this.order_sum = order_sum;
	}
	public List<LinkPriceReportItem> getOrder_list() {
		return order_list;
	}
	public void setOrder_list(List<LinkPriceReportItem> order_list) {
		this.order_list = order_list;
	}
	public String getParameters() {
		StringBuffer sb = new StringBuffer();
		sb.append("&yyyymm=");
		sb.append(this.yyyymm);
		sb.append("&u_id=");
		sb.append(this.uId);
		return sb.toString();
	}
	
}
