package com.wiz.clip.clip3_0.bean;

public class PointCouponHist {
	private String userCi;
	private String couponName;
	private String couponNumber;
	private String balance;
	private String yyyymmdd;
	private String hhmmss;
	private String resultCode;
	private String resultMessage;
	
	private String realBalance;
	
	@Override
	public String toString() {
		return "PointCouponHist[ userCi = " + userCi + " | couponName = " + couponName + " | couponNumber = " + couponNumber + " | balance = " + balance + " | "
				+ "yyyymmdd = " + yyyymmdd + " | hhmmss = " + hhmmss + " | resultCode = " + resultCode + " | resultMessage = " + resultMessage + " ]";
	}
	
	public String getRealBalance() {
		if(resultCode.equals("00")){
			realBalance = balance;
			return realBalance;
		}
		else
			return "0";
	}

	public void setRealBalance(String realBalance) {
		this.realBalance = realBalance;
	}

	public String getUserCi() {
		return userCi;
	}
	public void setUserCi(String userCi) {
		this.userCi = userCi;
	}
	public String getCouponName() {
		return couponName;
	}
	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}
	public String getCouponNumber() {
		return couponNumber;
	}
	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getYyyymmdd() {
		return yyyymmdd;
	}
	public void setYyyymmdd(String yyyymmdd) {
		this.yyyymmdd = yyyymmdd;
	}
	public String getHhmmss() {
		return hhmmss;
	}
	public void setHhmmss(String hhmmss) {
		this.hhmmss = hhmmss;
	}
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultMessage() {
		return resultMessage;
	}
	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
	
}
