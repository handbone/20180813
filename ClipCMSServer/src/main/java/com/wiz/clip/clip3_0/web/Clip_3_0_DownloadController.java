package com.wiz.clip.clip3_0.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wiz.clip.clip3_0.bean.LinkPriceReportItem;
import com.wiz.clip.clip3_0.bean.LinkPriceReportMessege;
import com.wiz.clip.clip3_0.bean.LinkpriceShopHistDtl;
import com.wiz.clip.clip3_0.bean.PointCouponHist;
import com.wiz.clip.clip3_0.bean.PointbombInfo;
import com.wiz.clip.clip3_0.bean.UserTokenInfo;
import com.wiz.clip.clip3_0.service.Clip_3_0_Service;

@Controller
public class Clip_3_0_DownloadController {

	Logger logger = Logger.getLogger(Clip_3_0_DownloadController.class.getName());

	@Autowired
	public Clip_3_0_Service clip_3_0_service;

	/************************
	 * Excel Download Controller
	 ******************************/

	/**
	 * 카드 포인트 전환 조회 엑셀 다운로드
	 * 
	 * @param req
	 * @param res
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/clip3_0/KTCPMW01/excelDownload.do")
	public void Excel_Download(HttpServletRequest req, HttpServletResponse res, Model model) throws Exception {
		logger.debug("Card Point Excel_Download============================================");
		HttpSession session = req.getSession();
		String status = (String) session.getAttribute("status");
		String keywd = (String) session.getAttribute("keywd");
		String datepicker1 = (String) session.getAttribute("datepicker1");
		String datepicker2 = (String) session.getAttribute("datepicker2");
		String card_com = (String) session.getAttribute("card_com");
		String use_status = (String) session.getAttribute("use_status");
		String value = (String) session.getAttribute("value");
		String userCi = (String) session.getAttribute("user_ci");
		@SuppressWarnings("unused")
		String totalcount = (String) session.getAttribute("totalcount");

		if(datepicker1 != null && !datepicker1.isEmpty()) {
			datepicker1 = datepicker1 + "000000";
		}
		if(datepicker2 != null && !datepicker2.isEmpty()) {
			datepicker2 = datepicker2 + "235959";
		}
		
		ModelMap map = new ModelMap();
		if (status != null && !status.isEmpty()) {
			map.put("status", status);
		}
		if (keywd != null && !keywd.isEmpty()) {
			map.put("keywd", keywd);
		}
		if (datepicker1 != null && !datepicker1.isEmpty()) {
			map.put("datepicker1", datepicker1);
		}
		if (datepicker2 != null && !datepicker2.isEmpty()) {
			map.put("datepicker2", datepicker2);
		}
		if (card_com != null && !card_com.isEmpty() && !card_com.equals("0")) {
			map.put("card_com", card_com);
		}
		if (use_status != null && !use_status.isEmpty() && !use_status.equals("0")) {
			map.put("use_status", use_status);
		}
		if (value != null && !value.isEmpty() && !value.equals("0")) {
			map.put("value", value);
		}
		if (userCi != null && !userCi.isEmpty()) {
			map.put("user_ci", userCi);
		}
//		map.put("startNum", 1);
//		map.put("endNum", Integer.parseInt(totalcount));

		String labels[] = { "거래 요청일", "거래 응답일", "카드사", "원 거래번호", "참조 거래번호", "요청포인트", "응답 포인트",
		"거래결과", "응답코드", "거래구분", "정산금액" };
		model.addAttribute("labels", labels);// 셀타이틀
		int columnWidth[] = { 30, 30, 10, 30, 30, 10, 10, 5, 5, 5, 5 };// 셀크기 가로
		model.addAttribute("columnWidth", columnWidth);
		model.addAttribute("filename", "카드포인트_전환_조회.xlsx");// 파일명
		model.addAttribute("sheetName", "카드포인트 전환 조회");// 시트명
		 
		clip_3_0_service.selectExcelCardpointChangeList(map, res, req, model);

		// if (users != null) {
		//
		// HashMap<String, Object> row = new HashMap<String, Object>();
		// HashMap<String, String> cell = null;
		// List<HashMap<String, String>> list = new ArrayList<HashMap<String,
		// String>>();
		//
		// int rowName = 1;
		// for (int i = 0; i < users.size(); i++) {
		// CardPointSwapList data = users.get(i);
		// cell = new HashMap<String, String>();
		// cell.put("row" + rowName + "_cell1", data.getTrans_req_date());
		// cell.put("row" + rowName + "_cell2", data.getTrans_res_date());
		// cell.put("row" + rowName + "_cell3", data.getReg_source());
		// cell.put("row" + rowName + "_cell4", data.getTrans_id());
		// cell.put("row" + rowName + "_cell5", data.getRes_point());
		// cell.put("row" + rowName + "_cell6", data.getReq_point());
		// cell.put("row" + rowName + "_cell7", data.getResult_code());
		// cell.put("row" + rowName + "_cell8", data.getRes_code());
		// list.add(cell);
		// rowName++;
		// }
		//
		// row.put("dataList", list);// 데이터목록
		// row.put("dataLength", 8);// 엑셀셀갯수
		// model.addAttribute("dataMap", row);
		//
		// }
		//
//		 return "excelView2";
	}

	/**
	 * 적립 예정 표인트 엑셀 다운로드
	 * 
	 * @param req
	 * @param res
	 * @param model
	 * @param searchBean
	 * @param labels
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/clip3_0/preSavingCheckList/excelDownload.do", method = RequestMethod.POST)
	public String preSavingCheckList_Excel_Download(HttpServletRequest req, HttpServletResponse res, Model model)
			throws Exception {

		HttpSession session = req.getSession();

		LinkPriceReportMessege msg = new LinkPriceReportMessege();
		msg.setuId(session.getAttribute("userToken").toString());
		try {
			if (session.getAttribute("point_type").equals("I")) {
				msg = clip_3_0_service.getNextSavingHistory(msg);
			} else if (session.getAttribute("point_type").equals("O")) {
				msg = clip_3_0_service.getNext2SavingHistory(msg);
			} else {
				msg = clip_3_0_service.getNowSavingHistory(msg);
			}
			for (LinkPriceReportItem item : msg.getOrder_list()) {
				String itemCount = clip_3_0_service.selectItemCount(item);
				item.setItemCount(itemCount);
			}
		} catch (Exception e) {
			msg = clip_3_0_service.getNextSavingHistory(msg);
		}

		if (msg != null) {

			HashMap<String, Object> row = new HashMap<String, Object>();
			HashMap<String, String> cell = null;
			List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

			SimpleDateFormat df1 = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat df2 = new SimpleDateFormat("kkmmss");
			int rowName = 1;
			for (int i = 0; i < msg.getOrder_list().size(); i++) {
				LinkPriceReportItem data = msg.getOrder_list().get(i);
				cell = new HashMap<String, String>();

				Date date1 = df1.parse(data.getYyyymmdd());
				Date date2 = df2.parse(data.getHhmiss());
				cell.put("row" + rowName + "_cell1", new SimpleDateFormat("yyyy.MM.dd").format(date1) + "\n"
						+ new SimpleDateFormat("kk:mm:ss").format(date2));
				cell.put("row" + rowName + "_cell2", data.getMerchant_name());
				cell.put("row" + rowName + "_cell3", data.getOrder_code());
				cell.put("row" + rowName + "_cell4", data.getProduct_name());
				cell.put("row" + rowName + "_cell5", data.getItemCount());
				cell.put("row" + rowName + "_cell6", data.getSales());
				cell.put("row" + rowName + "_cell7", data.getCommission());
				list.add(cell);
				rowName++;
			}

			row.put("dataList", list);// 데이터목록
			row.put("dataLength", 7);// 엑셀셀갯수

			String[] labels = { "주문일자", "쇼핑몰", "주문번호", "상품명", "주문수량", "판매금액 합계", "적립예정 포인트" };
			int columnWidth[] = { 15, 10, 15, 15, 10, 10, 20 };// 셀크기 가로
			model.addAttribute("columnWidth", columnWidth);
			model.addAttribute("labels", labels);// 셀타이틀
			model.addAttribute("filename", "적립_예정_포인트_조회.xlsx");// 파일명
			model.addAttribute("sheetName", "적립 예정 포인트 조회");// 시트명
			model.addAttribute("dataMap", row);
		}

		return "excelView2";
	}

	/**
	 * 포인트 쿠폰 이용 내역 조회 엑셀 다운로드
	 * 
	 * @param req
	 * @param res
	 * @param model
	 * @param searchBean
	 * @param labels
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/clip3_0/KTCPMW04/excelDownload.do", method = RequestMethod.POST)
	public String pointCouponList_Excel_Download(HttpServletRequest req, HttpServletResponse res, ModelMap model)
			throws Exception {

		HttpSession session = req.getSession();

		String status = (String) session.getAttribute("status");
		String keywd = (String) session.getAttribute("keywd");
		String datepicker1 = (String) session.getAttribute("datepicker1");
		String datepicker2 = (String) session.getAttribute("datepicker2");
		String couponNumber = (String) session.getAttribute("couponNumber");
		String user_ci = (String) session.getAttribute("user_ci");

		if (status != null && !status.isEmpty()) {
			model.put("status", status);
		} else {
			model.put("status", "");
		}
		if (keywd != null && !keywd.isEmpty()) {
			model.put("keywd", keywd);
		}
		if (datepicker1 != null && !datepicker1.isEmpty()) {
			model.put("datepicker1", datepicker1);
		}
		if (datepicker2 != null && !datepicker2.isEmpty()) {
			model.put("datepicker2", datepicker2);
		}
		if (couponNumber != null && !couponNumber.isEmpty() && !couponNumber.equals("0")) {
			model.put("couponNumber", couponNumber);
		}

		model.put("user_ci", user_ci);
		List<PointCouponHist> datas = clip_3_0_service.selectPointCouponHistList(model);

		if (datas != null) {

			HashMap<String, Object> row = new HashMap<String, Object>();
			HashMap<String, String> cell = null;
			List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

			SimpleDateFormat df1 = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat df2 = new SimpleDateFormat("kkmmss");
			int rowName = 1;
			for (int i = 0; i < datas.size(); i++) {
				PointCouponHist data = datas.get(i);
				cell = new HashMap<String, String>();

				Date date1 = df1.parse(data.getYyyymmdd());
				Date date2 = df2.parse(data.getHhmmss());
				cell.put("row" + rowName + "_cell1", new SimpleDateFormat("yyyy.MM.dd").format(date1) + "\n"
						+ new SimpleDateFormat("kk:mm:ss").format(date2));
				cell.put("row" + rowName + "_cell2", "미구현 : 테이블에 데이터 없음");
				cell.put("row" + rowName + "_cell3", "미구현 : 테이블에 데이터 없음");
				cell.put("row" + rowName + "_cell4", data.getCouponNumber());
				cell.put("row" + rowName + "_cell5", data.getBalance());
				cell.put("row" + rowName + "_cell6", data.getBalance());
				cell.put("row" + rowName + "_cell7", data.getResultMessage());
				list.add(cell);
				rowName++;
			}

			row.put("dataList", list);// 데이터목록
			row.put("dataLength", 7);// 엑셀셀갯수

			String[] labels = { "사용일", "유효기간 시작일", "유효기간 종료일", "쿠폰번호", "포인트", "적립 포인트계", "결과코드" };
			int columnWidth[] = { 15, 15, 15, 15, 10, 10, 20 };// 셀크기 가로
			model.addAttribute("columnWidth", columnWidth);
			model.addAttribute("labels", labels);// 셀타이틀
			model.addAttribute("filename", "포인트_쿠폰_이용_내역_조회.xlsx");// 파일명
			model.addAttribute("sheetName", "포인트 쿠폰 이용 내역 조회");// 시트명
			model.addAttribute("dataMap", row);
		}

		return "excelView2";
	}

	/**
	 * 쇼핑 적립 내역 조회 엑셀 다운로드
	 * 
	 * @param req
	 * @param res
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/clip3_0/KTCPMW03/excelDownload.do", method = RequestMethod.POST)
	public String shoppingsaving_Excel_Download(HttpServletRequest req, HttpServletResponse res, ModelMap model)
			throws Exception {

		HttpSession session = req.getSession();

		String status = (String) session.getAttribute("status");
		String keywd = (String) session.getAttribute("keywd");
		String searchYear = (String) session.getAttribute("searchYear");
		String searchDate = (String) session.getAttribute("searchDate");
		String requester_code = (String) session.getAttribute("requester_code");
		String user_ci = (String) session.getAttribute("user_ci");

		if (status != null && !status.isEmpty()) {
			model.put("status", status);
		} else {
			model.put("status", "");
		}
		if (keywd != null && !keywd.isEmpty()) {
			model.put("keywd", keywd);
		}
		if (searchYear != null && !searchYear.isEmpty()) {
			model.put("searchYear", searchYear);
		}
		if (searchDate != null && !searchDate.isEmpty()) {
			model.put("searchDate", searchDate);
		}
		if (requester_code != null && !requester_code.isEmpty() && !requester_code.equals("0")) {
			model.put("requester_code", requester_code);
		}

		model.put("user_ci", user_ci);
		List<LinkpriceShopHistDtl> datas = clip_3_0_service.selectShopHistList(model);

		if (datas != null) {

			HashMap<String, Object> row = new HashMap<String, Object>();
			HashMap<String, String> cell = null;
			List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

			SimpleDateFormat df1 = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat df2 = new SimpleDateFormat("kkmmss");
			SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
			int rowName = 1;
			for (int i = 0; i < datas.size(); i++) {
				LinkpriceShopHistDtl data = datas.get(i);
				cell = new HashMap<String, String>();

				Date date1 = df1.parse(data.getDay());
				Date date2 = df2.parse(data.getTime());
				Date date3 = df3.parse(data.getRegdate());
				cell.put("row" + rowName + "_cell1", new SimpleDateFormat("yyyy.MM.dd").format(date1) + " "
						+ new SimpleDateFormat("kk:mm:ss").format(date2));
				cell.put("row" + rowName + "_cell2", new SimpleDateFormat("yyyy.MM.dd kk:mm:ss").format(date3));
				cell.put("row" + rowName + "_cell3", data.getM_name());
				cell.put("row" + rowName + "_cell4", data.getO_cd());
				cell.put("row" + rowName + "_cell5", data.getP_nm());
				cell.put("row" + rowName + "_cell6", data.getIt_cnt());
				cell.put("row" + rowName + "_cell7", data.getSales());
				cell.put("row" + rowName + "_cell8", data.getCommission());
				cell.put("row" + rowName + "_cell9", data.getStatus());
				list.add(cell);
				rowName++;
			}

			row.put("dataList", list);// 데이터목록
			row.put("dataLength", 9);// 엑셀셀갯수

			String[] labels = { "주문일자", "적립기준일", "쇼핑몰", "주문번호", "상품명", "주문수량", "판매 금액 합계", "포인트", "정산 처리 상태" };
			int columnWidth[] = { 15, 15, 10, 15, 15, 10, 10, 10, 20 };// 셀크기 가로
			model.addAttribute("columnWidth", columnWidth);
			model.addAttribute("labels", labels);// 셀타이틀
			model.addAttribute("filename", "쇼핑_적립_내역_조회.xlsx");// 파일명
			model.addAttribute("sheetName", "쇼핑 적립 내역 조회");// 시트명
			model.addAttribute("dataMap", row);
		}

		return "excelView2";
	}

	/**
	 * 쇼핑 적립 내역 조회 엑셀 다운로드
	 * 
	 * @param req
	 * @param res
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/clip3_0/KTCPMW06/excelDownload.do", method = RequestMethod.POST)
	public String pointbomb_Excel_Download(HttpServletRequest req, HttpServletResponse res, ModelMap model)
			throws Exception {

		HttpSession session = req.getSession();

		String status = (String) session.getAttribute("status");
		String keywd = (String) session.getAttribute("keywd");
		String datepicker1 = (String) session.getAttribute("datepicker1");
		String datepicker2 = (String) session.getAttribute("datepicker2");
		String day_count = (String) session.getAttribute("day_count");
		List<UserTokenInfo> userinfo = (List<UserTokenInfo>) session.getAttribute("userinfo");

		if (status != null && !status.isEmpty()) {
			model.put("status", status);
		} else {
			model.put("status", "");
		}
		if (keywd != null && !keywd.isEmpty()) {
			model.put("keywd", keywd);
		}
		if (datepicker1 != null && !datepicker1.isEmpty()) {
			model.put("datepicker1", datepicker1);
		}
		if (datepicker2 != null && !datepicker2.isEmpty()) {
			model.put("datepicker2", datepicker2);
		}
		if (day_count != null && !day_count.isEmpty()) {
			model.put("day_count", day_count);
		}

		List<PointbombInfo> datas = new ArrayList<PointbombInfo>();
		if(userinfo != null){
			for(UserTokenInfo user : userinfo){
				if (user.getCustId() != null && !user.getCustId().isEmpty() && !user.getCustId().equals("'")) {
					model.put("cust_id", user.getCustId());
				
					datas.addAll(clip_3_0_service.selectPointbombList(model));
				}
				
			}
		}
		else if(keywd.equals("") && status.equals("")) {
			datas = clip_3_0_service.selectPointbombList(model);
		}

		if (datas != null) {

			HashMap<String, Object> row = new HashMap<String, Object>();
			HashMap<String, String> cell = null;
			List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

			SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
			int rowName = 1;
			for (int i = 0; i < datas.size(); i++) {
				PointbombInfo data = datas.get(i);
				cell = new HashMap<String, String>();

				Date date1 = df1.parse(data.getUse_start_date());
				cell.put("row" + rowName + "_cell1", new SimpleDateFormat("yyyy.MM.dd").format(date1) + "\n"
						+ new SimpleDateFormat("kk:mm:ss").format(date1));
				cell.put("row" + rowName + "_cell2", data.getUser_ci());
				cell.put("row" + rowName + "_cell3", data.getCust_id());
				cell.put("row" + rowName + "_cell4", data.getCtn());
				cell.put("row" + rowName + "_cell5", data.getGa_id());
				cell.put("row" + rowName + "_cell6", data.getDay_count());
				cell.put("row" + rowName + "_cell7", data.getLevel());
				cell.put("row" + rowName + "_cell8", data.getBomb_count());
				cell.put("row" + rowName + "_cell9", data.getTotal_reward_point());
				cell.put("row" + rowName + "_cell10", data.getUse_yn());
				list.add(cell);
				rowName++;
			}

			row.put("dataList", list);// 데이터목록
			row.put("dataLength", 10);// 엑셀셀갯수

			String[] labels = { "잠금화면 On 일자", "CI", "cust_id", "ctn", "Ga_id", "현재일차", "현재 레벨", "폭탄수령 누적", "누적 수령 포인트",
					"현재 상태" };
			int columnWidth[] = { 10, 10, 10, 10, 10, 7, 7, 10, 10, 10 };// 셀크기 가로
			model.addAttribute("columnWidth", columnWidth);
			model.addAttribute("labels", labels);// 셀타이틀
			model.addAttribute("filename", "포인트_폭탄_내역_조회.xlsx");// 파일명
			model.addAttribute("sheetName", "포인트 폭탄 내역 조회");// 시트명
			model.addAttribute("dataMap", row);
		}

		return "excelView2";
	}
}
