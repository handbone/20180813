/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.userinfo.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.wiz.clip.userinfo.bean.UserInfoDTO;
import com.wiz.clip.userinfo.bean.UserInfoViewDTO;

@Repository("userInfoDao")
public class UserInfoDAO {
	
	@Autowired
	private SqlSession postgreSession;

	public List<UserInfoDTO> getUserInfoList(ModelMap map) {
		// TODO Auto-generated method stub
		return postgreSession.selectList("USERINFO.getUserInfoList",map);
	}

	public int getTotalArticle(ModelMap map) {
		return postgreSession.selectOne("USERINFO.getTotalArticle",map);
	}

	public UserInfoViewDTO getUserInfoView(String code) {
		return postgreSession.selectOne("USERINFO.getUserInfoView",code);
	}

	public HashMap<String, String> getPhoneInfo(String call_ctn) {
		return postgreSession.selectOne("USERINFO.getPhoneInfo",call_ctn);
	}

	public String getUserInfoView() {
		return postgreSession.selectOne("USERINFO.selectSeq");
	}

	public void txtComSave(HashMap<String, String> hMap) {
		postgreSession.insert("USERINFO.txtComSave",hMap);
		
	}

	public void insertUserInfo(ModelMap map) {
		postgreSession.insert("USERINFO.insertUserInfo",map);		
	}

	public List<WeakHashMap<String, String>> userInfoSearchList(String code) {
		return postgreSession.selectList("USERINFO.userInfoSearchList",code);
	}


	
	
}
