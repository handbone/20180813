/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.security.web;
import java.io.IOException;
import java.net.InetAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.web.context.request.RequestContextHolder;

import com.wiz.clip.common.dblog.service.DbLogService;
import com.wiz.clip.security.bean.Role;
import com.wiz.clip.security.bean.CmsUserInfo;
import com.wiz.clip.security.dao.UserDAO;
import com.wiz.clip.security.service.UserService;

public class SigninSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler{
	//private static final Logger logger = LoggerFactory.getLogger(SigninSuccessHandler.class);
	
	@Value("#{config['LOCAL_IP_6']}") String LOCAL_IP_6;
	
	@Resource(name="userService")
	private UserService userService;
	
	@Resource(name="dbLogService")
	private DbLogService dbLogService;
	
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth) throws IOException, ServletException {
    	
        String accept = request.getHeader("accept");
        String idRememberCk = request.getParameter("idRememberCk");
        String user_id = request.getParameter("user_id").trim();
        String captcha = request.getParameter("captcha").trim();
  
        //captcha 체크
        String correctAnswer = (String) request.getSession().getAttribute("correctAnswer");

        if(!correctAnswer.equals(captcha)){
        	request.setAttribute("error", "CAPTCHA");
			request.getRequestDispatcher("/signin.do").forward(request, response); //IP 또는 권한 오류 시 컨트롤러로 포워딩
			return;
        }
        
        Cookie cookie = null;  
        //1)id저장 체크 여부 검사하여 id 쿠키에 저장 , 2),3) 관계없이 ID PW 맞을 시 쿠키저장
  		if(idRememberCk.equals("true")){
  			cookie = new Cookie("id",java.net.URLEncoder.encode(user_id));
  			cookie.setMaxAge(60*60*24*7);
  			response.addCookie(cookie);
  		}else{
  			cookie = new Cookie("id",null);
  			cookie.setMaxAge(0);
  			response.addCookie(cookie);
  		}
  		
        CmsUserInfo user = (CmsUserInfo)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
      
        //2)ip체크 
 		String USER_IP = request.getRemoteAddr();
 		
		if (USER_IP.equalsIgnoreCase(LOCAL_IP_6)) {
		    InetAddress inetAddress = InetAddress.getLocalHost();
		    String ipAddress = inetAddress.getHostAddress();
		    USER_IP = ipAddress;
		}
		//System.out.println(USER_IP);
		String ipCheckYN =""; //ip 체크 1:성공
		
		
		//System.out.println("user.getArrowip()"+user.getArrowip());
		for( String arrowip : user.getArrowip().split(",")){
			//System.out.println("arrowip"+user.getArrowip());
			if(USER_IP.equals(arrowip)){
				ipCheckYN = "1";
			}
			if(arrowip.equals("*.*.*.*")){
				ipCheckYN = "1";
			}
		}
		
		
		
		
		
		
		
		
		
		//2012-09-18
		//if(USER_IP.contains("222.107.254.") || USER_IP.contains("172.23.20.")) ipCheckYN = "1";
		// 엠하우스 관리자 계정의경우 아이피 관계없이 접속 - 대형대리 요청 20060425
		//if(user_id.equals("biz2ring")) {
		//	ipCheckYN = "1";
		//}
		//테스트 아이디 영업대행사권한
		//if(user_id.equals("test0001")) {
		//	ipCheckYN = "1";
		//}
		
		if(!ipCheckYN.equals("1")){
			request.setAttribute("error", "IP");
			request.getRequestDispatcher("/signin.do").forward(request, response); //IP 또는 권한 오류 시 컨트롤러로 포워딩
			return;
		}
		
		//3)권한체크(서비스 유무)   0:미승인 1:승인 2:패스워드 변경대상자 3:패스워드 실패 잠금
		if(user.getState().equals("0")){
			request.setAttribute("error", "STATUS");
			request.getRequestDispatcher("/signin.do").forward(request, response); //미승인 오류 시 컨트롤러로 포워딩
			return;
		}else if(user.getState().equals("2")){ 
			request.setAttribute("error", "PWCHANGE");
			//request.setAttribute("agentUserCode", user.getAgent_user_code());
			//request.setAttribute("agentUserCode", user.getAgent_user_code());
			request.getRequestDispatcher("/signin.do").forward(request, response); //패스워드 변경 대상시 컨트롤러로 포워딩
			return;
		}else if(user.getState().equals("3")){ 
			request.setAttribute("error", "PWBLOCK");
			//request.setAttribute("agentUserCode", user.getAgent_user_code());
			request.getRequestDispatcher("/signin.do").forward(request, response); //패스워드 연속실패로 잠금상태
			return;
		}
		
		//4)IP,권한 통과시 유저정보 세션 저장
		HttpSession session = (HttpSession)request.getSession(false);
		String sessionId = session.getId();
		session.setAttribute("AgentCode", "user.getAgent_code()");
		session.setAttribute("AgentUserCode", "user.getAgent_user_code()");
		session.setAttribute("AgentUserId", user_id);
		session.setAttribute("AgentUserIp", user.getIpaddr());
		session.setAttribute("AgentLevel", user.getUserauth());	
		session.setAttribute("AgentCoName", "user.getConame()");	
		session.setAttribute("AgentState", user.getState());	
		session.setAttribute("AgentPassdate", user.getPassdate());	
		session.setAttribute("LastLoginTime", user.getLastlogintime());
		session.setAttribute("SessionId", sessionId.trim());
		
		//해지자 조회권한 세션셋팅
		/*SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA);
		Date currentTime = new Date ( );
		String cDate = mSimpleDateFormat.format ( currentTime ); //현재시간
		Date curDate=null;
		Date authDate=null;
		System.out.println(cDate);
			
		if(user.getHejiauth().equals("T")){
			try {
				curDate = mSimpleDateFormat.parse(cDate);
				authDate = mSimpleDateFormat.parse(user.getHejienddate());
				System.out.println(user.getHejienddate());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			System.out.println(curDate.compareTo(authDate));
			if(curDate.compareTo(authDate) <=0 || !user.getAgent_level().equals("3"))
				session.setAttribute("AgentHejiAuth", "T"); //해지자 조회권한 있음(biz2ring,운영,권한부여자)
			else
				session.setAttribute("AgentHejiAuth", "F");
		}else{
			session.setAttribute("AgentHejiAuth", "F");
		}
		 */
		for(GrantedAuthority role : user.getAuthorities()){
			session.setAttribute("RoleName", role.getAuthority());
		}
		
		//5)로그인 성공시 로그인 실패카운트 0으로 초기화
		if(Integer.parseInt(user.getLoginfailcnt()) > 0){
			userService.loginFailCntReset(user_id);
		}
		
		
		//6)로그 남기기
		Map<String,String> map = new HashMap<String,String>();
		map.put("userId", user_id);
		map.put("sessionId",(String)session.getAttribute("SessionId"));
		map.put("userIp",USER_IP);
	
		dbLogService.saveLoginLog(map);
	
		//※)IE8에서 success **분석중
	   /* String ua = request.getHeader("User-Agent").toLowerCase();
		if (ua.matches(".*msie 8.0.*")) {
			request.getRequestDispatcher("/main.do").forward(request, response);
		}*/
		
		super.onAuthenticationSuccess(request, response, auth);
    }
}
