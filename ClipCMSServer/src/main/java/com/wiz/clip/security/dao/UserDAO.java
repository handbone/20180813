/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.security.dao;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.wiz.clip.security.bean.CmsUserInfo;



@Repository("userDao")
public class UserDAO {
	@Autowired
    private SqlSession postgreSession;
	
	public CmsUserInfo getUserInfo(HashMap<String, String> paramMap) {
		return postgreSession.selectOne("USER.getUserInfo",paramMap);
	}

	public Map<String,String> getUserPwChange(String code) {
		return postgreSession.selectOne("USER.getUserPwChange",code);
	}

	public void pwChange(Map<String, String> hMap) {
		postgreSession.update("USER.pwChange",hMap);	
		
	}

	public void privateCertOk(String agentUserCode) {
		postgreSession.update("USER.privateCertOk",agentUserCode);	
		
	}

	public void passFailCntUp(Map<String, String> map) {
		postgreSession.update("USER.passFailCntUp",map);	
		
	}

	public int selectFailCnt(String username) {
		return postgreSession.selectOne("USER.selectFailCnt",username);
	}

	public void blockLogin(String username) {
		postgreSession.update("USER.blockLogin",username);	
		
	}

	public void loginFailCntReset(String user_id) {
		postgreSession.update("USER.loginFailCntReset",user_id);
		
	}
}
