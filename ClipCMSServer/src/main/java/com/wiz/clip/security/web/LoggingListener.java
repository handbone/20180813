/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.security.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.wiz.clip.common.dblog.service.DbLogService;
import com.wiz.clip.security.service.UserService;


@Component
public class LoggingListener implements HttpSessionListener{

	
	@Override
	public void sessionCreated(HttpSessionEvent hse) {}

	@Override
	public void sessionDestroyed(HttpSessionEvent hse) {
		WebApplicationContext wb =WebApplicationContextUtils.getWebApplicationContext(hse.getSession().getServletContext());
		DbLogService dbLogService = (DbLogService)wb.getBean("dbLogService");
		
		HttpSession session = hse.getSession();
		
		if(session.getId()==(String)session.getAttribute("SessionId")){
			Map<String,String> map = new HashMap<String,String>();
			map.put("userId", (String)session.getAttribute("AgentUserId"));
			map.put("sessionId",(String)session.getAttribute("SessionId"));
			map.put("userIp",(String)session.getAttribute("AgentUserIp"));
			dbLogService.saveLogoutLog(map);
			//System.out.println(session.getId()+": 세션이 소멸되었습니다."); 
		}		
		
	}

}
