/**********************************************************************
/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */


package com.wiz.clip.security.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.wiz.clip.security.bean.CmsUserInfo;
import com.wiz.clip.security.service.UserService;

@Controller
public class SignController {
	private static final Logger logger = LoggerFactory.getLogger(SignController.class);

	@Resource(name="userService")
	private UserService userService;
	
    /*@Autowired
    private PasswordEncoder passwordEncoder;*/
    
    //로그인
    @RequestMapping(value="/signin.do")
    public ModelAndView signin(HttpServletRequest request,
    		@RequestParam(value="error", required=false) String err) {
    	 logger.debug("login Page start!!!");
    
    	//cookie에서 저장된 id 검색
 		Cookie[] cookie = request.getCookies();
 		String id = "";
 		if(cookie!=null){
 			for(int i=0;i<cookie.length;i++){
 				if(cookie[i].getName().trim().equals("id")){
 					id = cookie[i].getValue();
 				}
 			}
 		}
 		
 		String error = err;
 		//error 값 가져오기 
 		if(error == null){ 
 			error = (String) request.getAttribute("error"); 
 		}
 		
 		
 		ModelAndView mav = new ModelAndView();
 		mav.addObject("id",id);
 		mav.addObject("error", error); 
		mav.setViewName("/clip/security/signin");
		return mav;
         
    }

    @PreAuthorize("authenticated")
    @RequestMapping(value="/main.do")
    public String main(Model model) {
    	logger.debug("main Page start!!!");
         // Authentication auth = SecurityContextHolder.getContext().getAuthentication();
         //model.addAttribute("USER_NAME", auth.getName());
    	/* HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
    	        .getRequestAttributes()).getRequest();
    	 HttpSession session = (HttpSession)request.getSession(false);
    	 if(session.getAttribute("AgentID") == null){
    		 return "redirect:" + "/signin.do?error=SESSION";
    	 }*/
    	
    	//4)3개월 개인정보 취급동의 대상자의 경우/ 권한: ROLE_GUEST
    	/*User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		for(GrantedAuthority role : user.getAuthorities()){
			if(role.getAuthority().equals("ROLE_GUEST")){
				  return "redirect:" + "/privateCert.do";
			}
		}*/
		
         return "redirect:" + "/roulette/channel/channelList.do";
    }
    //3개월 개인정보 취급동의 폼
    /*@RequestMapping(value="/privateCert.do")
    public ModelAndView privateCert(HttpServletRequest request) {
    	 logger.debug("privateCert Page start!!!");
    
    	 User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	 
 		 ModelAndView mav = new ModelAndView();
 		 mav.addObject("agentUserCode",user.getAgent_user_code());
		 mav.addObject("display","../clip/security/privateCert.jsp");
		 mav.setViewName("/main/index");
		 return mav;
         
    }*/
   //3개월 개인정보 취급동의 실행
    /*@RequestMapping(value="/privateCertOk.do")
    public String privateCertOk(HttpServletRequest request,
    		@RequestParam(value="agentUserCode") String code) {
    	 logger.debug("privateCertOk Page start!!!");
    
    	 userService.privateCertOk(code,request);
    	 
    	 return "redirect:" + "/notice/noticeList.do";
    }*/
    //3개월 비밀번호 변경 팝업폼
    @RequestMapping(value="/pwChange.do")
    public ModelAndView pwChange(HttpServletRequest request,
    		@RequestParam(value="agentUserCode") String code,
    		@RequestParam(value="userId") String id) {
    	 logger.debug("pwChange Page start!!!");
    
 		
 		 ModelAndView mav = new ModelAndView();
 		 mav.addObject("code",code);
 		 mav.addObject("id",id);
		 mav.setViewName("/clip/security/pwChangePop");
		 return mav;
         
    }
    //3개월 비밀번호 변경 실행
    @RequestMapping(value="/pwChangeOk.do")
    public ModelAndView pwChangeOk(HttpServletRequest request,
    		@RequestParam(value="agentUserCode") String code,
    		@RequestParam(value="oldpassword") String oldpassword,
    		@RequestParam(value="newpassword") String newpassword,
    		@RequestParam(value="agentUserId") String id,
    		ModelMap model) {
    	 logger.debug("pwChangeOk Page start!!!");

    	Map<String,String> map = userService.loadUserByUsercode(code,oldpassword,newpassword);

 		ModelAndView mav = new ModelAndView();
 		mav.addObject("code",code);
 		mav.addObject("id",id);
 		mav.addObject("result",map.get("result"));
		mav.setViewName("/clip/security/pwChangePop");
		return mav;
         
    }
    
    
    
    
}
