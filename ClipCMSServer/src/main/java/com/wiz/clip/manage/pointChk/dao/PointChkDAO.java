/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.pointChk.dao;

import java.util.List;

import org.springframework.ui.ModelMap;

import com.wiz.clip.manage.pointChk.bean.CmsPointModify;
import com.wiz.clip.manage.pointChk.bean.ProdTester;
import com.wiz.clip.manage.pointChk.bean.SearchBean;
import com.wiz.clip.manage.pointChk.bean.TblStatAnomalyDetection;
import com.wiz.clip.manage.pointChk.bean.TblStatReceiverInfo;
import com.wiz.clip.manage.pointChk.bean.TblStatTopUser;


public interface PointChkDAO  {

	List<TblStatTopUser> getPointInfoList(SearchBean searchBean);

	List<TblStatAnomalyDetection> getTblStatAnomalyDetection(SearchBean searchBean);

	String getCmsCommonCode(String code_name);

	List<TblStatReceiverInfo> getTblStatReceiverInfo();

	int anomalyConfigDelete(SearchBean searchBean);

	int insertTblStatReceiverInfo(TblStatReceiverInfo tblStatReceiverInfo);

	TblStatAnomalyDetection tblStatAnomalyDetectionTop(int i);
	
	List<TblStatAnomalyDetection> tblStatAnomalyDetectionTopSMS(ModelMap model);

	int goLimitPointChange(SearchBean searchBean);

	List<TblStatTopUser> viewDetail(SearchBean searchBean);

	List<TblStatTopUser> anomalyList2(SearchBean searchBean);

	List<TblStatTopUser> viewDetailAnomaly(SearchBean searchBean);

	int anomalyList2Cnt(SearchBean searchBean);

	int getTblStatAnomalyDetectionCnt(SearchBean searchBean);

	int getPointInfoListCnt(SearchBean searchBean);

	int viewDetailAnomalyCnt(SearchBean searchBean);

	List<CmsPointModify> CmsPointModifyList(SearchBean searchBean);

	int CmsPointModifyListCnt(SearchBean searchBean);

	List<SearchBean> ctnChk(SearchBean searchBean);

	void insertCmsPointModify(CmsPointModify cmsPointModify);

	List<SearchBean> ctnChk2(SearchBean searchBean);

	int viewDetailCnt(SearchBean searchBean);

	List<ProdTester> selectProdTestList();

	int deleteTesterList(String key);

	int updateTesterState(ProdTester key);

	int insertTester(ProdTester protester);

}
