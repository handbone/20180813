/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.pointChk.bean;

import com.wiz.clip.common.page.PageBean;

public class SearchBean  extends PageBean {
	
	private String ctnChkDiv;
	private String ctn2;
	
	private String startdate;
	private String enddate;
	private String selectdate;
	private String keywd;
	private String ctn;
	private String user_ci;
	private String ga_id;
	private String cust_id;
	private String excelDownload;
	private String point_type;
	private String limit;
	private String media;
	private String no;
	private String limitPoint;
	private String stat_hour;
	private String status;
	
	/** 09/14 jyi cms3.0 검색기간 */
	private String datepicker1;
	private String datepicker2;
	
	@Override
	public String toString() {
		
		return "SearchBean([ctnChkDiv = " + ctnChkDiv + "] [ctn2 = " + ctn2 + "] [startdate = " + startdate + "]"
				+ "[enddate = " + enddate + "] [selectdate = " + selectdate + "] [keywd = " + keywd + "] [ctn = " + ctn + "]"
						+ "[user_ci = " + user_ci + "] [ga_id = " + ga_id + "] [cust_id = " + cust_id + "] [excelDownload = " + excelDownload + "]"
						+ "[point_type = " + point_type + "] [limit = " + limit + "] [media = " + media + "]  [no = " + no + "]  [limitPoint = " + limitPoint + "]"
								+ " [stat_hour = " + stat_hour + "]  [status = " + status + "])";
	}
	
	
	public String getDatepicker1() {
		return datepicker1;
	}


	public void setDatepicker1(String datepicker1) {
		this.datepicker1 = datepicker1;
	}


	public String getDatepicker2() {
		return datepicker2;
	}


	public void setDatepicker2(String datepicker2) {
		this.datepicker2 = datepicker2;
	}


	public String getCtnChkDiv() {
		return ctnChkDiv;
	}
	public void setCtnChkDiv(String ctnChkDiv) {
		this.ctnChkDiv = ctnChkDiv;
	}
	public String getStat_hour() {
		return stat_hour;
	}
	public void setStat_hour(String stat_hour) {
		this.stat_hour = stat_hour;
	}
	public String getUser_ci() {
		return user_ci;
	}
	public void setUser_ci(String user_ci) {
		this.user_ci = user_ci;
	}
	public String getLimitPoint() {
		return limitPoint;
	}
	public void setLimitPoint(String limitPoint) {
		this.limitPoint = limitPoint;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getMedia() {
		return media;
	}
	public void setMedia(String media) {
		this.media = media;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getSelectdate() {
		return selectdate;
	}
	public void setSelectdate(String selectdate) {
		this.selectdate = selectdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getExcelDownload() {
		return excelDownload;
	}
	public void setExcelDownload(String excelDownload) {
		this.excelDownload = excelDownload;
	}
	
	public String getPoint_type() {
		return point_type;
	}
	public void setPoint_type(String point_type) {
		this.point_type = point_type;
	}
	public String getLimit() {
		return limit;
	}
	public void setLimit(String limit) {
		this.limit = limit;
	}
	public String getKeywd() {
		return keywd;
	}
	public void setKeywd(String keywd) {
		this.keywd = keywd;
	}
	public String getCtn() {
		return ctn;
	}
	public void setCtn(String ctn) {
		this.ctn = ctn;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getGa_id() {
		return ga_id;
	}
	public void setGa_id(String ga_id) {
		this.ga_id = ga_id;
	}
	public String getCust_id() {
		return cust_id;
	}
	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}
	public String getCtn2() {
		return ctn2;
	}
	public void setCtn2(String ctn2) {
		this.ctn2 = ctn2;
	}
	
	
}
