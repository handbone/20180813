/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.stat.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.ModelMap;

import com.wiz.clip.manage.acl.bean.AclInfoTbl;
import com.wiz.clip.manage.stat.bean.SearchBean;
import com.wiz.clip.manage.stat.bean.TblStatDailyPoint;
import com.wiz.clip.manage.stat.bean.TblStatDailyPoint2;
import com.wiz.clip.manage.stat.bean.TblStatMediaPoint;
import com.wiz.clip.manage.stat.bean.TblStatMonthlyPoint;
import com.wiz.clip.roulette.viewLog.bean.ViewLogBean;

public interface StatService {

	List<AclInfoTbl> getRequesterNameList(ModelMap map);

	int getStatLogMediaCnt(SearchBean searchBean);
	int getStatLogDailyCnt(SearchBean searchBean);
	int getStatLogMonthlyCnt(SearchBean searchBean);
	
	List<TblStatMediaPoint> statLogMedia(SearchBean searchBean);
	List<TblStatDailyPoint> statLogDaily(SearchBean searchBean);
	List<TblStatMonthlyPoint> statLogMonthly(SearchBean searchBean);

	List<TblStatDailyPoint2> statLogDaily2(SearchBean searchBean);
	int getStatLogDailyCnt2(SearchBean searchBean);

	


	



}
