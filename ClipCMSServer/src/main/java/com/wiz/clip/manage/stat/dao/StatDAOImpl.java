/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.stat.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.wiz.clip.manage.acl.bean.AclInfoTbl;
import com.wiz.clip.manage.stat.bean.SearchBean;
import com.wiz.clip.manage.stat.bean.TblStatDailyPoint;
import com.wiz.clip.manage.stat.bean.TblStatDailyPoint2;
import com.wiz.clip.manage.stat.bean.TblStatMediaPoint;
import com.wiz.clip.manage.stat.bean.TblStatMonthlyPoint;
import com.wiz.clip.roulette.viewLog.bean.ViewLogBean;



@Repository
public class StatDAOImpl implements StatDAO  {

	@Autowired
	private SqlSession postgreSession;

	Logger logger = Logger.getLogger(StatDAOImpl.class.getName());

	@Override
	public List<AclInfoTbl> getRequesterNameList(ModelMap map) {
		return postgreSession.selectList("STAT.getRequesterNameList", map);
	}

	@Override
	public int getStatLogMediaCnt(SearchBean searchBean) {
		return postgreSession.selectOne("STAT.getStatLogMediaCnt", searchBean);
	}

	@Override
	public int getStatLogDailyCnt(SearchBean searchBean) {
		return postgreSession.selectOne("STAT.getStatLogDailyCnt", searchBean);
	}

	@Override
	public int getStatLogMonthlyCnt(SearchBean searchBean) {
		return postgreSession.selectOne("STAT.getStatLogMonthlyCnt", searchBean);
	}

	@Override
	public List<TblStatMediaPoint> statLogMedia(SearchBean searchBean) {
		return postgreSession.selectList("STAT.statLogMedia", searchBean);
	}

	@Override
	public List<TblStatDailyPoint> statLogDaily(SearchBean searchBean) {
		return postgreSession.selectList("STAT.statLogDaily", searchBean);
	}

	@Override
	public List<TblStatMonthlyPoint> statLogMonthly(SearchBean searchBean) {
		return postgreSession.selectList("STAT.statLogMonthly", searchBean);
	}

	@Override
	public List<TblStatDailyPoint2> statLogDaily2(SearchBean searchBean) {
		return postgreSession.selectList("STAT.statLogDaily2", searchBean);
	}

	@Override
	public int getStatLogDailyCnt2(SearchBean searchBean) {
		return postgreSession.selectOne("STAT.getStatLogDailyCnt2", searchBean);
	}
	
	
}
