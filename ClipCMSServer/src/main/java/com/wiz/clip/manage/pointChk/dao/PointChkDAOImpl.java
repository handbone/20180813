/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.pointChk.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.wiz.clip.manage.pointChk.bean.CmsPointModify;
import com.wiz.clip.manage.pointChk.bean.ProdTester;
import com.wiz.clip.manage.pointChk.bean.SearchBean;
import com.wiz.clip.manage.pointChk.bean.TblStatAnomalyDetection;
import com.wiz.clip.manage.pointChk.bean.TblStatReceiverInfo;
import com.wiz.clip.manage.pointChk.bean.TblStatTopUser;

@Repository
public class PointChkDAOImpl implements PointChkDAO  {

	@Autowired
	private SqlSession postgreSession;

	Logger logger = Logger.getLogger(PointChkDAOImpl.class.getName());

	@Override
	public List<TblStatTopUser> getPointInfoList(SearchBean searchBean) {
		return postgreSession.selectList("POINTCHK.getPointInfoList", searchBean);
	}

	@Override
	public List<TblStatAnomalyDetection> getTblStatAnomalyDetection(SearchBean searchBean) {
		return postgreSession.selectList("POINTCHK.getTblStatAnomalyDetection", searchBean);
	}

	@Override
	public String getCmsCommonCode(String code_name) {
		return postgreSession.selectOne("POINTCHK.getCmsCommonCode", code_name);
	}

	@Override
	public List<TblStatReceiverInfo> getTblStatReceiverInfo() {
		return postgreSession.selectList("POINTCHK.getTblStatReceiverInfo");
	}

	@Override
	public int anomalyConfigDelete(SearchBean searchBean) {
		return postgreSession.update("POINTCHK.anomalyConfigDelete", searchBean);
	}

	@Override
	public int insertTblStatReceiverInfo(TblStatReceiverInfo tblStatReceiverInfo) {
		return postgreSession.insert("POINTCHK.insertTblStatReceiverInfo", tblStatReceiverInfo);
	}

	@Override
	public TblStatAnomalyDetection tblStatAnomalyDetectionTop(int i) {
		return postgreSession.selectOne("POINTCHK.tblStatAnomalyDetectionTop", i);
	}

	@Override
	public List<TblStatAnomalyDetection> tblStatAnomalyDetectionTopSMS(ModelMap model) {
		// TODO Auto-generated method stub
		return postgreSession.selectList("POINTCHK.tblStatAnomalyDetectionTopSMS", model);
	}
	

	@Override
	public int goLimitPointChange(SearchBean searchBean) {
		return postgreSession.update("POINTCHK.goLimitPointChange", searchBean);
	}

	@Override
	public List<TblStatTopUser> viewDetail(SearchBean searchBean) {
		return postgreSession.selectList("POINTCHK.viewDetail", searchBean);
	}

	@Override
	public List<TblStatTopUser> anomalyList2(SearchBean searchBean) {
		return postgreSession.selectList("POINTCHK.anomalyList2", searchBean);
	}

	@Override
	public List<TblStatTopUser> viewDetailAnomaly(SearchBean searchBean) {
		return postgreSession.selectList("POINTCHK.viewDetailAnomaly", searchBean);
	}

	@Override
	public int anomalyList2Cnt(SearchBean searchBean) {
		return postgreSession.selectOne("POINTCHK.anomalyList2Cnt", searchBean);
	}

	@Override
	public int getTblStatAnomalyDetectionCnt(SearchBean searchBean) {
		return postgreSession.selectOne("POINTCHK.getTblStatAnomalyDetectionCnt", searchBean);
	}

	@Override
	public int getPointInfoListCnt(SearchBean searchBean) {
		return postgreSession.selectOne("POINTCHK.getPointInfoListCnt", searchBean);
	}

	@Override
	public int viewDetailAnomalyCnt(SearchBean searchBean) {
		return postgreSession.selectOne("POINTCHK.viewDetailAnomalyCnt", searchBean);
	}

	@Override
	public List<CmsPointModify> CmsPointModifyList(SearchBean searchBean) {
		return postgreSession.selectList("POINTCHK.CmsPointModifyList", searchBean);
	}

	@Override
	public int CmsPointModifyListCnt(SearchBean searchBean) {
		return postgreSession.selectOne("POINTCHK.CmsPointModifyListCnt", searchBean);
	}

	@Override
	public List<SearchBean> ctnChk(SearchBean searchBean) {
		return postgreSession.selectList("POINTCHK.ctnChk", searchBean);
	}

	@Override
	public void insertCmsPointModify(CmsPointModify cmsPointModify) {
		 postgreSession.insert("POINTCHK.insertCmsPointModify", cmsPointModify);
	}

	@Override
	public List<SearchBean> ctnChk2(SearchBean searchBean) {
		return postgreSession.selectList("POINTCHK.ctnChk2", searchBean);
	}

	@Override
	public int viewDetailCnt(SearchBean searchBean) {
		return postgreSession.selectOne("POINTCHK.viewDetailCnt", searchBean);
	}

	@Override
	public List<ProdTester> selectProdTestList() {
		return postgreSession.selectList("POINTCHK.prodTester");
	}

	@Override
	public int deleteTesterList(String key) {
		return postgreSession.delete("POINTCHK.delete_prodTester", key);
	}

	@Override
	public int updateTesterState(ProdTester key) {
		return postgreSession.update("POINTCHK.update_TesterState", key);
	}

	@Override
	public int insertTester(ProdTester protester) {
		return postgreSession.insert("POINTCHK.insert_TesterState", protester);
	}
	
}
