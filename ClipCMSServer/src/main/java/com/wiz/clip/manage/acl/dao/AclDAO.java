/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.acl.dao;

import java.util.List;

import org.springframework.ui.ModelMap;

import com.wiz.clip.manage.acl.bean.AclInfoTbl;




public interface AclDAO  {

	List<AclInfoTbl> getAclList(ModelMap map);

	int getAclListCnt(ModelMap map);

	int aclInsert(AclInfoTbl aclInfoTbl);

	AclInfoTbl getAcl(String requester_code);

	int aclUpdate(AclInfoTbl aclInfoTbl);

	int aclDelete(String requester_code);

	int requesterNameChk(String requester_name);

	int requesterCodeChk(String requester_code);

	List<AclInfoTbl> getAclListNoPaging(ModelMap map);

}
