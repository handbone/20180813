/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.manageLog.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.wiz.clip.manage.acl.bean.AclInfoTbl;
import com.wiz.clip.manage.acl.dao.AclDAO;
import com.wiz.clip.manage.manageLog.bean.PointHistTbl;
import com.wiz.clip.manage.manageLog.bean.PointInfoTbl;
import com.wiz.clip.manage.manageLog.dao.ManageLogDAO;
import com.wiz.clip.manage.manageLog.bean.SearchBean;
import com.wiz.clip.manage.stat.bean.TblStatDailyPoint;
import com.wiz.clip.manage.stat.bean.TblStatMediaPoint;
import com.wiz.clip.manage.stat.bean.TblStatMonthlyPoint;
import com.wiz.clip.manage.stat.dao.StatDAO;
import com.wiz.clip.roulette.viewLog.bean.ViewLogBean;

@Service
@Transactional
public class ManageLogServiceImpl implements ManageLogService {
	
	@Autowired
	private ManageLogDAO manageLogDao;

	Logger logger = Logger.getLogger(ManageLogServiceImpl.class.getName());

	@Override
	public int getPointInfoTblCnt(SearchBean searchBean) {
		return manageLogDao.getPointInfoTblCnt(searchBean);
	}

	@Override
	public int getPointHistTblCnt(SearchBean searchBean) {
		return manageLogDao.getPointHistTblCnt(searchBean);
	}
	
	@Override
	public List<PointInfoTbl> getPointInfoTbl(SearchBean searchBean) {
		return manageLogDao.getPointInfoTbl(searchBean);
	}

	@Override
	public List<PointHistTbl> getPointHistTbl(SearchBean searchBean) {
		return manageLogDao.getPointHistTbl(searchBean);
	}

	@Override
	public List<PointInfoTbl> getPointInfoTblAll(SearchBean searchBean) {
		return manageLogDao.getPointInfoTblAll(searchBean);
	}

	@Override
	public int getPointInfoTblCntAll(SearchBean searchBean) {
		return manageLogDao.getPointInfoTblCntAll(searchBean);
	}

	@Override
	public PointHistTbl getPointHist(SearchBean searchBean) {
		return manageLogDao.getPointHist(searchBean);
	}

	

	
	

}
