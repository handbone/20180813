/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.manageLog.service;

import java.util.List;

import com.wiz.clip.manage.manageLog.bean.PointHistTbl;
import com.wiz.clip.manage.manageLog.bean.PointInfoTbl;
import com.wiz.clip.manage.manageLog.bean.SearchBean;

public interface ManageLogService {


	int getPointInfoTblCnt(SearchBean searchBean);
	int getPointHistTblCnt(SearchBean searchBean);

	List<PointInfoTbl> getPointInfoTbl(SearchBean searchBean);
	List<PointHistTbl> getPointHistTbl(SearchBean searchBean);
	
	List<PointInfoTbl> getPointInfoTblAll(SearchBean searchBean);
	int getPointInfoTblCntAll(SearchBean searchBean);
	PointHistTbl getPointHist(com.wiz.clip.manage.manageLog.bean.SearchBean searchBean2);



	


}
