/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.manageLog.web;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.wiz.clip.roulette.channel.bean.PointRoulette;
import com.wiz.clip.roulette.viewLog.bean.PointRouletteJoin;
import com.wiz.clip.common.crypto.AES256CipherClip;
import com.wiz.clip.common.paging.BoardPaging;
import com.wiz.clip.manage.acl.bean.AclInfoTbl;
import com.wiz.clip.manage.manageLog.bean.PointHistTbl;
import com.wiz.clip.manage.manageLog.bean.PointInfoTbl;
import com.wiz.clip.manage.manageLog.bean.SearchBean;
import com.wiz.clip.manage.manageLog.service.ManageLogService;
import com.wiz.clip.manage.stat.bean.TblStatDailyPoint;
import com.wiz.clip.manage.stat.bean.TblStatMediaPoint;
import com.wiz.clip.manage.stat.bean.TblStatMonthlyPoint;
import com.wiz.clip.manage.stat.service.StatService;
import com.wiz.clip.roulette.viewLog.bean.ViewLogBean;
import com.wiz.clip.roulette.viewLog.service.ViewLogService;

@Controller
public class ManageLogController  {
	
	Logger logger = Logger.getLogger(ManageLogController.class.getName());
	
	@Value("#{config['PAGE_SIZE']}") int PAGE_SIZE;
	
	@Autowired
	private ManageLogService manageLogService;
	
	@Autowired
	private StatService statService;

	/*
	 * 이용조회
	 */
	@RequestMapping(value = "/manage/manageLog/manageLog.do")
	public ModelAndView manageLog(
			@RequestParam(value = "pg", required = false) String page,
			SearchBean searchBean, ModelMap map) throws InvalidKeyException, UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

		/*logger.debug("status :" + searchBean.getRoulette_id());
		logger.debug("dateType :" + searchBean.getDateType());
		logger.debug("startdate :" + searchBean.getStartdate());
		logger.debug("enddate :" + searchBean.getEnddate());*/

		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));
				
		List<AclInfoTbl> requesterNameList = statService.getRequesterNameList(map);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("requesterNameList", requesterNameList);
		mav.addObject("searchBean", searchBean);
		
		if("ctn".equals(searchBean.getStatus()) && null != searchBean.getKeywd() ){
			searchBean.setKeywd(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
		}
		
		
		if(!"W".equals(searchBean.getStatusUseDiv())){
			//'전체' 검색일때
			if("".equals(searchBean.getStatusUseDiv())){
				List<PointInfoTbl> infolist = manageLogService.getPointInfoTblAll(searchBean);
				int totalCnt = manageLogService.getPointInfoTblCntAll(searchBean);
				paging.makePagingHtml(totalCnt);
				mav.addObject("infolist", infolist);
				mav.addObject("totalCnt", totalCnt);
			}else{
				List<PointInfoTbl> infolist = manageLogService.getPointInfoTbl(searchBean);
				int totalCnt = manageLogService.getPointInfoTblCnt(searchBean);
				paging.makePagingHtml(totalCnt);
				mav.addObject("infolist", infolist);
				mav.addObject("totalCnt", totalCnt);
			}
			mav.addObject("divString", "manageLog1");
		}else {
			List<PointHistTbl> histlist = manageLogService.getPointHistTbl(searchBean);
			int totalCnt = manageLogService.getPointHistTblCnt(searchBean);
			paging.makePagingHtml(totalCnt);
			mav.addObject("histlist", histlist);
			mav.addObject("totalCnt", totalCnt);
			mav.addObject("divString", "manageLog2");
		}
		
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);
		
		mav.addObject("menu1", "포인트관리");
		mav.addObject("menu2", "간편조회");
		mav.addObject("display", "../clip/manage/manageLog/manageLog.jsp");
		
		if("Y".equals(searchBean.getExcelDownload())){
			mav.addObject("pageNm", "로그관리");
			mav.setViewName("excelView");
		}else{
			mav.setViewName("/main/index");
		}
		
		searchBean.setKeywd(new String(AES256CipherClip.AES_Decode(searchBean.getKeywd()).getBytes()));
		
		return mav;
	}

	

}
