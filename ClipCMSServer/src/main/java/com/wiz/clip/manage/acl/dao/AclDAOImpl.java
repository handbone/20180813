/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.acl.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.wiz.clip.manage.acl.bean.AclInfoTbl;



@Repository
public class AclDAOImpl implements AclDAO  {

	@Autowired
	private SqlSession postgreSession;

	Logger logger = Logger.getLogger(AclDAOImpl.class.getName());
	
	@Override
	public List<AclInfoTbl> getAclList(ModelMap map) {
		return postgreSession.selectList("ACL.getAclList", map);
	}

	@Override
	public List<AclInfoTbl> getAclListNoPaging(ModelMap map) {
		return postgreSession.selectList("ACL.getAclListNoPaging", map);
	}
	
	
	@Override
	public int getAclListCnt(ModelMap map) {
		return postgreSession.selectOne("ACL.getAclListCnt",map);
	}

	@Override
	public int aclInsert(AclInfoTbl aclInfoTbl) {
		return postgreSession.insert("ACL.aclInsert", aclInfoTbl);
	}

	@Override
	public AclInfoTbl getAcl(String requester_code) {
		return postgreSession.selectOne("ACL.getAcl",requester_code);
	}

	@Override
	public int aclUpdate(AclInfoTbl aclInfoTbl) {
		return postgreSession.update("ACL.aclUpdate", aclInfoTbl);
	}

	@Override
	public int aclDelete(String requester_code) {
		return postgreSession.delete("ACL.aclDelete",requester_code);
	}

	@Override
	public int requesterNameChk(String requester_name) {
		return postgreSession.selectOne("ACL.requesterNameChk",requester_name);
	}
	
	@Override
	public int requesterCodeChk(String requester_code) {
		return postgreSession.selectOne("ACL.requesterCodeChk",requester_code);
	}

}
