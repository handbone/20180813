/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.userInfo.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.wiz.clip.manage.userInfo.bean.SearchBean;
import com.wiz.clip.manage.userInfo.bean.UserInfoTbl;
import com.wiz.clip.manage.userInfo.bean.UserInfoTblLog;



@Repository
public class UserInfoDAOImpl implements UserInfoDAO  {

	@Autowired
	private SqlSession postgreSession;

	Logger logger = Logger.getLogger(UserInfoDAOImpl.class.getName());

	@Override
	public int getUserInfoCnt(SearchBean searchBean) {
		return postgreSession.selectOne("USERINFO.getUserInfoCnt", searchBean);
	}

	@Override
	public List<UserInfoTbl> getUserInfoList(SearchBean searchBean) {
		return postgreSession.selectList("USERINFO.getUserInfoList", searchBean);
	}

	@Override
	public int getUserLogCnt(SearchBean searchBean) {
		return postgreSession.selectOne("USERINFO.getUserLogCnt", searchBean);
	}

	@Override
	public List<UserInfoTblLog> getUserLogList(SearchBean searchBean) {
		return postgreSession.selectList("USERINFO.getUserLogList", searchBean);
	}
	
	@Override
	public UserInfoTbl getUserInfo(SearchBean searchBean) {
		return postgreSession.selectOne("USERINFO.getUserInfo", searchBean);
	}
	
	
}
