/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.pointChk.bean;

public class TblStatAnomalyDetection  {
	
	
	private String media;
	private String stat_date;
	private String stat_hour;
	private String limit_point_value;
	private String limit_input_count;
	private String limit_output_count;
	private String input_point;
	private String output_point;
	public String getMedia() {
		return media; 
	}
	public void setMedia(String media) {
		this.media = media;
	}
	public String getStat_date() {
		return stat_date;
	}
	public void setStat_date(String stat_date) {
		this.stat_date = stat_date;
	}
	public String getStat_hour() {
		return stat_hour;
	}
	public void setStat_hour(String stat_hour) {
		this.stat_hour = stat_hour;
	}
	public String getLimit_point_value() {
		return limit_point_value;
	}
	public void setLimit_point_value(String limit_point_value) {
		this.limit_point_value = limit_point_value;
	}
	public String getLimit_input_count() {
		return limit_input_count;
	}
	public void setLimit_input_count(String limit_input_count) {
		this.limit_input_count = limit_input_count;
	}
	public String getLimit_output_count() {
		return limit_output_count;
	}
	public void setLimit_output_count(String limit_output_count) {
		this.limit_output_count = limit_output_count;
	}
	public String getInput_point() {
		return input_point;
	}
	public void setInput_point(String input_point) {
		this.input_point = input_point;
	}
	public String getOutput_point() {
		return output_point;
	}
	public void setOutput_point(String output_point) {
		this.output_point = output_point;
	}
	
	
	
	
}
