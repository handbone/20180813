/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.pointChk.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.ModelMap;

import com.wiz.clip.manage.pointChk.bean.CmsPointModify;
import com.wiz.clip.manage.pointChk.bean.ProdTester;
import com.wiz.clip.manage.pointChk.bean.SearchBean;
import com.wiz.clip.manage.pointChk.bean.TblStatAnomalyDetection;
import com.wiz.clip.manage.pointChk.bean.TblStatReceiverInfo;
import com.wiz.clip.manage.pointChk.bean.TblStatTopUser;

public interface PointChkService {

	List<TblStatTopUser> getPointInfoList(SearchBean searchBean)  throws java.lang.Exception ;

	List<TblStatAnomalyDetection> getTblStatAnomalyDetection(SearchBean searchBean) throws java.lang.Exception ;

	String getCmsCommonCode(String string) throws java.lang.Exception ;

	List<TblStatReceiverInfo> getTblStatReceiverInfo()throws java.lang.Exception ;

	int anomalyConfigDelete(SearchBean searchBean) throws java.lang.Exception ;

	int insertTblStatReceiverInfo(TblStatReceiverInfo tblStatReceiverInfo) throws java.lang.Exception ;

	TblStatAnomalyDetection tblStatAnomalyDetectionTop(int i)  throws java.lang.Exception ;
	
	List<TblStatAnomalyDetection> tblStatAnomalyDetectionTopSMS(ModelMap model) throws java.lang.Exception;

	int goLimitPointChange(SearchBean searchBean) throws java.lang.Exception ;

	List<TblStatTopUser> viewDetail(SearchBean searchBean)  throws java.lang.Exception ;

	List<TblStatTopUser> anomalyList2(SearchBean searchBean) throws java.lang.Exception ;

	List<TblStatTopUser> viewDetailAnomaly(SearchBean searchBean) throws java.lang.Exception ;

	int anomalyList2Cnt(SearchBean searchBean) throws java.lang.Exception ;

	int getTblStatAnomalyDetectionCnt(SearchBean searchBean) throws java.lang.Exception ;

	int getPointInfoListCnt(SearchBean searchBean) throws java.lang.Exception ;

	int viewDetailAnomalyCnt(SearchBean searchBean) throws java.lang.Exception ;

	List<CmsPointModify> CmsPointModifyList(SearchBean searchBean);

	int CmsPointModifyListCnt(SearchBean searchBean);

	List<SearchBean> ctnChk(SearchBean searchBean) throws java.lang.Exception ;

	void insertCmsPointModify(HttpServletRequest request, SearchBean searchBean, CmsPointModify cmsPointModify, String result, String balanceJson) throws java.lang.Exception ;

	List<SearchBean> ctnChk2(SearchBean searchBean) throws java.lang.Exception ;

	int viewDetailCnt(SearchBean searchBean) throws java.lang.Exception ;

	List<ProdTester> getProdTestList() throws java.lang.Exception ;

	int removeTesterList(String key) throws java.lang.Exception ;

	int setTesterState(ProdTester key) throws java.lang.Exception ;

	int addTester(ProdTester protester) throws java.lang.Exception ;

}
