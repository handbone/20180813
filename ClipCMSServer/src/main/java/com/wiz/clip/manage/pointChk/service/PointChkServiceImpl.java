/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.manage.pointChk.service;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.wiz.clip.common.crypto.AES256CipherClip;
import com.wiz.clip.manage.pointChk.bean.CmsPointModify;
import com.wiz.clip.manage.pointChk.bean.ProdTester;
import com.wiz.clip.manage.pointChk.bean.SearchBean;
import com.wiz.clip.manage.pointChk.bean.TblStatAnomalyDetection;
import com.wiz.clip.manage.pointChk.bean.TblStatReceiverInfo;
import com.wiz.clip.manage.pointChk.bean.TblStatTopUser;
import com.wiz.clip.manage.pointChk.dao.PointChkDAO;

@Service
@Transactional
public class PointChkServiceImpl implements PointChkService {
	
	@Autowired
	private PointChkDAO pointChkDao;

	Logger logger = Logger.getLogger(PointChkServiceImpl.class.getName());
	
	@Override
	public List<TblStatTopUser> getPointInfoList(SearchBean searchBean) throws java.lang.Exception {
		if(null != searchBean.getKeywd()){
			searchBean.setCtn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
		}
		return pointChkDao.getPointInfoList(searchBean);
	}
	
	@Override
	public List<TblStatAnomalyDetection> getTblStatAnomalyDetection(SearchBean searchBean) throws java.lang.Exception {
		return pointChkDao.getTblStatAnomalyDetection(searchBean);
	}

	@Override
	public String getCmsCommonCode(String code_name) throws java.lang.Exception {
		return pointChkDao.getCmsCommonCode(code_name);
	}

	@Override
	public List<TblStatReceiverInfo> getTblStatReceiverInfo()  throws java.lang.Exception {
		return pointChkDao.getTblStatReceiverInfo();
	}

	@Override
	public int anomalyConfigDelete(SearchBean searchBean) throws java.lang.Exception {
		return pointChkDao.anomalyConfigDelete(searchBean);
	}

	@Override
	public int insertTblStatReceiverInfo(TblStatReceiverInfo tblStatReceiverInfo) throws java.lang.Exception {
		return pointChkDao.insertTblStatReceiverInfo(tblStatReceiverInfo);
	}

	@Override
	public TblStatAnomalyDetection tblStatAnomalyDetectionTop(int i) throws java.lang.Exception {
		return pointChkDao.tblStatAnomalyDetectionTop(i);
	}

	@Override
	public List<TblStatAnomalyDetection> tblStatAnomalyDetectionTopSMS(ModelMap model) throws Exception {
		// TODO Auto-generated method stub
		return pointChkDao.tblStatAnomalyDetectionTopSMS(model);
	}

	@Override
	public int goLimitPointChange(SearchBean searchBean) throws java.lang.Exception {
		return pointChkDao.goLimitPointChange(searchBean);
	}

	@Override
	public List<TblStatTopUser> viewDetail(SearchBean searchBean) throws java.lang.Exception {
		return pointChkDao.viewDetail(searchBean);
	}

	@Override
	public List<TblStatTopUser> anomalyList2(SearchBean searchBean) throws java.lang.Exception {
		if(null != searchBean.getKeywd()){
			searchBean.setCtn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
		}
		return pointChkDao.anomalyList2(searchBean);
	}

	@Override
	public List<TblStatTopUser> viewDetailAnomaly(SearchBean searchBean) throws java.lang.Exception {
		return pointChkDao.viewDetailAnomaly(searchBean);
	}

	@Override
	public int anomalyList2Cnt(SearchBean searchBean) throws Exception {
		return pointChkDao.anomalyList2Cnt(searchBean);
	}

	@Override
	public int getTblStatAnomalyDetectionCnt(SearchBean searchBean) throws Exception {
		return pointChkDao.getTblStatAnomalyDetectionCnt(searchBean);
	}

	@Override
	public int getPointInfoListCnt(SearchBean searchBean) throws Exception {
		return pointChkDao.getPointInfoListCnt(searchBean);
	}

	@Override
	public int viewDetailAnomalyCnt(SearchBean searchBean) throws Exception {
		return pointChkDao.viewDetailAnomalyCnt(searchBean);
	}

	@Override
	public List<CmsPointModify> CmsPointModifyList(SearchBean searchBean) {
		try {
			if(!"".equals(searchBean.getKeywd())){
				searchBean.setUser_ci(searchBean.getKeywd());
				searchBean.setCust_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
				searchBean.setCtn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
				searchBean.setGa_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pointChkDao.CmsPointModifyList(searchBean);
	}

	@Override
	public int CmsPointModifyListCnt(SearchBean searchBean) {
		try {
			if(!"".equals(searchBean.getKeywd())){
				searchBean.setUser_ci(searchBean.getKeywd());
				searchBean.setCust_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
				searchBean.setCtn(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
				searchBean.setGa_id(new String(AES256CipherClip.AES_Encode(searchBean.getKeywd()).getBytes()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pointChkDao.CmsPointModifyListCnt(searchBean);
	}

	@Override
	public List<SearchBean> ctnChk(SearchBean searchBean) throws Exception {
		searchBean.setCtn(new String(AES256CipherClip.AES_Encode(searchBean.getCtn()).getBytes()));
		return pointChkDao.ctnChk(searchBean);
	}

	ContainerFactory containerFactory = new ContainerFactory(){

		@Override
		public Map createObjectContainer() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public List creatArrayContainer() {
			// TODO Auto-generated method stub
			return null;
		}};
	
	@Override
	public void insertCmsPointModify(HttpServletRequest request, SearchBean searchBean, CmsPointModify cmsPointModify, String result, String balanceJson) throws Exception {
		
		String balance = "";
		
		String jsonText = balanceJson;
		JSONParser parser = new JSONParser();
		Map json = (Map)parser.parse(jsonText, containerFactory);
		Iterator iter = json.entrySet().iterator();
		while(iter.hasNext()){
		      Map.Entry entry = (Map.Entry)iter.next();
		      if("balance".equals(entry.getKey())){
		    	  balance = entry.getValue().toString();
		      }
		}
		  
		cmsPointModify.setCtn(new String(AES256CipherClip.AES_Encode(cmsPointModify.getCtn()).getBytes()));
		HttpSession session = (HttpSession)request.getSession(false);
		cmsPointModify.setSend_userid(session.getAttribute("AgentUserId").toString());
		cmsPointModify.setBalance(balance);
		
		pointChkDao.insertCmsPointModify(cmsPointModify);
	}

	@Override
	public List<SearchBean> ctnChk2(SearchBean searchBean) throws Exception {
		searchBean.setCtn2(new String(AES256CipherClip.AES_Encode(searchBean.getCtn2()).getBytes()));
		return pointChkDao.ctnChk2(searchBean);
	}

	@Override
	public int viewDetailCnt(SearchBean searchBean) throws Exception {
		return pointChkDao.viewDetailCnt(searchBean);
	}

	@Override
	public List<ProdTester> getProdTestList() throws Exception {
		return pointChkDao.selectProdTestList();
	}

	@Override
	public int removeTesterList(String key) throws Exception {
		return pointChkDao.deleteTesterList(key);
	}

	@Override
	public int setTesterState(ProdTester key) throws Exception {
		return pointChkDao.updateTesterState(key);
	}

	@Override
	public int addTester(ProdTester protester) throws Exception {
		return pointChkDao.insertTester(protester);
	}
	
}
