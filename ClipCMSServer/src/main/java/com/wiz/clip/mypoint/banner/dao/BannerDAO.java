/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.mypoint.banner.dao;

import java.util.List;

import org.springframework.ui.ModelMap;

import com.wiz.clip.mypoint.banner.bean.BannerBean;

public interface BannerDAO {

	List<BannerBean> getBannerList(ModelMap map);

	int getBannerListCnt(ModelMap map);

	int addBanner(BannerBean bannerBean);

	BannerBean bannerView(int idx);

	int bannerUpdate(BannerBean bannerBean);

	int bannerDelete(int idx);
	

}
