/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.mypoint.banner.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.wiz.clip.common.paging.BoardPaging;
import com.wiz.clip.mypoint.banner.bean.BannerBean;
import com.wiz.clip.mypoint.banner.service.BannerService;

@Controller
@RequestMapping("/mypoint/banner")
public class BannerController {
	Logger logger = Logger.getLogger(BannerController.class.getClass());
	
	@Value("#{config['PAGE_SIZE']}") int PAGE_SIZE;
	
	@Autowired
	private BannerService bannerService;
	
	/*
	 * 리스트
	 */
	@RequestMapping(value = "/bannerList.do")
	public ModelAndView getBannerList(@RequestParam(value = "pg", required = false) String page,
			@RequestParam(required = false) String status, ModelMap map) {

		String pg = page;
		// 페이징 관련
		if (pg == null || pg == ""){
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - ( PAGE_SIZE - 1 ) ;
		map.put("startNum", startNum);
		map.put("endNum", endNum);
		
		// 검색어 관련
		if (status != null && !status.equals("")) {
			map.put("status", status);
		} else {
			map.put("status", "");
		}

		List<BannerBean> list = bannerService.getBannerList(map);
		
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));
		// 토탈 카운트
		paging.makePagingHtml(bannerService.getBannerListCnt(map));

		map.clear();
		map.put("pg", pg);
		map.put("list", list);
		map.put("status", status);
		map.put("boardPaging", paging);

		ModelAndView mav = new ModelAndView();
		mav.addObject("menu1", "MY포인트관리");
		mav.addObject("menu2", "배너관리");
		mav.addObject("display", "../clip/mypoint/banner/bannerList.jsp");
		mav.setViewName("/main/index");
		return mav;
	}
	
	/*
	 * 입력폼
	 */
	@RequestMapping(value = "addBannerForm.do")
	public ModelAndView addBannerForm(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("addBannerForm Page start!!!");
		ModelAndView mav = new ModelAndView();

		mav.addObject("menu1", "MY포인트관리");
		mav.addObject("menu2", "배너관리");
		mav.addObject("display", "../clip/mypoint/banner/addBannerForm.jsp");
		mav.setViewName("/main/index");
		return mav;
	}
	
	
	/*
	 * 배너 추가
	 */
	@RequestMapping(value = "/addBanner.do")
	public ModelAndView addBanner(HttpServletRequest request, HttpServletResponse response, ModelMap map, BannerBean bannerBean) {

		logger.debug("addBanner start!!!");
		
		ModelAndView mav = new ModelAndView();
		bannerService.addBanner(bannerBean);
		
		map.addAttribute("msg", "작성하신 내용이 정상적으로 등록되었습니다.");
		mav.addObject("url", "/mypoint/banner/bannerList.do");
		mav.addObject("display", "../go-between.jsp");
		mav.setViewName("/main/index");
		return mav;
	}

	/*
	 * 상세보기
	 */
	@RequestMapping(value = "/bannerView.do")
	public ModelAndView bannerView(@RequestParam int idx, @RequestParam(required = false) String page,
			ModelMap map) {
		logger.debug("channelView Page start!!!");
		
		String pg = page;
		if (pg == null || pg == ""){
			pg = "1";
		}

		BannerBean bannerBean = bannerService.bannerView(idx);

		map.put("pg", pg);
		map.put("bannerBean", bannerBean);

		ModelAndView mav = new ModelAndView();
		mav.addObject("menu1", "MY포인트관리");
		mav.addObject("menu2", "배너관리");
		mav.addObject("display", "../clip/mypoint/banner/bannerView.jsp");
		mav.setViewName("/main/index");
		return mav;
	}
	
	/*
	 * 수정폼
	 */
	@RequestMapping(value = "/bannerUpdateForm.do")
	public ModelAndView bannerUpdateForm(@RequestParam int idx, @RequestParam(required = false) String page,
			ModelMap map) {
		logger.debug("bannerUpdateForm Page start!!!");
		String pg = page;
		if (pg == null || pg == ""){
			pg = "1";
		}

		BannerBean bannerBean = bannerService.bannerView(idx);

		map.put("pg", pg);
		map.put("bannerBean", bannerBean);

		ModelAndView mav = new ModelAndView();
		mav.addObject("menu1", "MY포인트관리");
		mav.addObject("menu2", "배너관리");
		mav.addObject("display", "../clip/mypoint/banner/bannerUpdateForm.jsp");
		mav.setViewName("/main/index");
		return mav;
	}	
	
	/*
	 * 수정처리
	 */
	@RequestMapping(value = "/bannerUpdate.do")
	public ModelAndView bannerUpdate(
			@RequestParam(required = false) int idx,
			@RequestParam(required = false) String page,
			HttpServletRequest request, HttpServletResponse response, ModelMap map,
			BannerBean bannerBean
			) throws java.lang.Exception {

		logger.debug("bannerUpdate Page start!!!");
		ModelAndView mav = new ModelAndView();

		String pg = page;

		if (pg == null || pg == ""){
			pg = "1";
		}

		bannerService.bannerUpdate(bannerBean);

		map.addAttribute("msg", "작성하신 내용이 정상적으로 수정되었습니다.");
		map.addAttribute("url", "/mypoint/banner/bannerView.do?idx=" + idx + "&pg=" + pg);

		mav.addObject("display", "../go-between.jsp");
		mav.setViewName("/main/index");
		return mav;
	}	
	
	
	/*
	 * 삭제처리
	 */
	@RequestMapping(value = "/bannerDelete.do")
	public ModelAndView bannerDelete(@RequestParam int idx,
			@RequestParam(value = "pg", required = false) String page, ModelMap map) {
		logger.debug("bannerDelete Page start!!!");

		String pg = page;
		if (pg == null || pg == ""){
			pg = "1";
		}

		bannerService.bannerDelete(idx);

		map.put("pg", pg);

		ModelAndView mav = new ModelAndView();
		map.addAttribute("msg", "정상적으로 삭제 되었습니다.");
		map.addAttribute("url", "/mypoint/banner/bannerList.do");
		mav.addObject("display", "../go-between.jsp");
		mav.setViewName("/main/index");
		return mav;
	}	
}
