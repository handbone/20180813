/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.user.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.wiz.clip.security.bean.CmsUserInfo;
import com.wiz.clip.user.bean.CmsLoginLog;
import com.wiz.clip.user.bean.CmsUseLog;
import com.wiz.clip.user.bean.SearchBean;


@Repository
public class UserDAOImpl implements UserDAO  {

	@Autowired
	private SqlSession postgreSession;

	Logger logger = Logger.getLogger(UserDAOImpl.class.getName());

	@Override
	public List<CmsUserInfo> getUserList(ModelMap map) {
		return postgreSession.selectList("USERINFO.getUserList", map);
	}

	@Override
	public int getUserListCnt(ModelMap map) {
		return postgreSession.selectOne("USERINFO.getUserListCnt", map);
	}

	@Override
	public int userInsert(CmsUserInfo cmsUserInfo) {
		return postgreSession.insert("USERINFO.userInsert", cmsUserInfo);
	}

	@Override
	public CmsUserInfo getUser(String userid) {
		return postgreSession.selectOne("USERINFO.getUser", userid);
	}

	@Override
	public int userUpdate(CmsUserInfo cmsUserInfo) {
		return postgreSession.update("USERINFO.userUpdate", cmsUserInfo);
	}

	@Override
	public int userDelete(String userid) {
		return postgreSession.delete("USERINFO.userDelete", userid);
	}

	@Override
	public int userIdCnt(String userid) {
		return postgreSession.selectOne("USERINFO.userIdCnt", userid);
	}

	@Override
	public List<CmsLoginLog> viewLogLoginOut(SearchBean searchBean) {
		return postgreSession.selectList("USERINFO.viewLogLoginOut", searchBean);
	}

	@Override
	public List<CmsUseLog> viewLogMenu(SearchBean searchBean) {
		return postgreSession.selectList("USERINFO.viewLogMenu", searchBean);
	}

	@Override
	public int viewLogLoginOutCnt(SearchBean searchBean) {
		return postgreSession.selectOne("USERINFO.viewLogLoginOutCnt", searchBean);
	}

	@Override
	public int viewLogMenuCnt(SearchBean searchBean) {
		return postgreSession.selectOne("USERINFO.viewLogMenuCnt", searchBean);
	}
	
	
	
	

}
