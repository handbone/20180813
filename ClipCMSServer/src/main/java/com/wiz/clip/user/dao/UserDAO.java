/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.user.dao;

import java.util.List;

import org.springframework.ui.ModelMap;

import com.wiz.clip.security.bean.CmsUserInfo;
import com.wiz.clip.user.bean.CmsLoginLog;
import com.wiz.clip.user.bean.CmsUseLog;
import com.wiz.clip.user.bean.SearchBean;



public interface UserDAO  {

	List<CmsUserInfo> getUserList(ModelMap map);

	int getUserListCnt(ModelMap map);

	int userInsert(CmsUserInfo cmsUserInfo);

	CmsUserInfo getUser(String userid);

	int userUpdate(CmsUserInfo cmsUserInfo);

	int userDelete(String userid);

	int userIdCnt(String userid);

	List<CmsLoginLog> viewLogLoginOut(SearchBean searchBean);

	List<CmsUseLog> viewLogMenu(SearchBean searchBean);

	int viewLogLoginOutCnt(SearchBean searchBean);

	int viewLogMenuCnt(SearchBean searchBean);

	

}
