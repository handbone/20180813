/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.onm.service;

import java.util.List;

import com.wiz.clip.onm.bean.BZADPostLog;
import com.wiz.clip.onm.bean.BZScreenPostLog;
import com.wiz.clip.onm.bean.SMSSend;
import com.wiz.clip.onm.bean.SearchBean;
import com.wiz.clip.onm.bean.StatConnect;

public interface ONMService {
	
	// bzadPostLog
	List<BZADPostLog> bzadPostLog(SearchBean searchBean);
	int bzadPostLogCnt(SearchBean searchBean);
	
	// bzScreenPostLog
	List<BZScreenPostLog> bzScreenPostLog(SearchBean searchBean);
	int bzScreenPostLogCnt(SearchBean searchBean);
	
	// smsSend
	List<SMSSend> smsSend(SearchBean searchBean);
	int smsSendCnt(SearchBean searchBean);
	
	// statConnect
	List<StatConnect> statConnect(SearchBean searchBean);
	int statConnectCnt(SearchBean searchBean);
	
}
