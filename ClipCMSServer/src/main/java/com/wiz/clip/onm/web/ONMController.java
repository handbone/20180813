/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.onm.web;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.wiz.clip.common.paging.BoardPaging;
import com.wiz.clip.onm.bean.BZADPostLog;
import com.wiz.clip.onm.bean.BZScreenPostLog;
import com.wiz.clip.onm.bean.SMSSend;
import com.wiz.clip.onm.bean.SearchBean;
import com.wiz.clip.onm.bean.StatConnect;
import com.wiz.clip.onm.service.ONMService;

@Controller
public class ONMController {

	Logger logger = Logger.getLogger(ONMController.class.getName());

	@Value("#{config['PAGE_SIZE']}")
	int PAGE_SIZE;

	@Autowired
	private ONMService onmService;
	
	
	
	/*
	 *		BZAD
	 */
	@RequestMapping(value = "/onm/bzadPostLog.do")
	public ModelAndView bzadPostLog(@RequestParam(value = "pg", required = false) String page, SearchBean searchBean,
			ModelMap map) {

		String pg = page;
		// 페이징 관련
		if (pg == null || pg == "") {
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - (PAGE_SIZE - 1);
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));

		// 카운트 값을 페이징에 사용
		int totalCnt = onmService.bzadPostLogCnt(searchBean);

		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);

		// 페이지에 뿌려질 목록을 list에 넣는다.
		List<BZADPostLog> list = onmService.bzadPostLog(searchBean);

		ModelAndView mav = new ModelAndView();
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("menu1", "ONM");
		mav.addObject("menu2", "[로그] BZAD");
		mav.addObject("display", "../clip/onm/bzadPost.jsp");

		if ("Y".equals(searchBean.getExcelDownload())) {
			mav.addObject("pageNm", "BZAD");
			mav.addObject("divString", "bzadPostLog");
			mav.setViewName("excelView");
		} else {
			mav.setViewName("/main/index");
		}

		return mav;
	}
	
	

	/*
	 *		BZSCREEN
	 */
	@RequestMapping(value = "/onm/bzScreenPostLog.do")
	public ModelAndView bzScreenPostLog(@RequestParam(value = "pg", required = false) String page, SearchBean searchBean,
			ModelMap map) { 

		String pg = page;
		// 페이징 관련
		if (pg == null || pg == "") {
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - (PAGE_SIZE - 1);
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));

		// 카운트 값을 페이징에 사용
		int totalCnt = onmService.bzScreenPostLogCnt(searchBean);

		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);

		// 페이지에 뿌려질 목록을 list에 넣는다.
		List<BZScreenPostLog> list = onmService.bzScreenPostLog(searchBean);

		ModelAndView mav = new ModelAndView();
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("menu1", "ONM");
		mav.addObject("menu2", "[로그] BZSCREEN");
		mav.addObject("display", "../clip/onm/bzScreenPost.jsp");

		if ("Y".equals(searchBean.getExcelDownload())) {
			mav.addObject("pageNm", "BZSCREEN");
			mav.addObject("divString", "bzScreenPostLog");
			mav.setViewName("excelView");
		} else {
			mav.setViewName("/main/index");
		}

		return mav;
	}
	
	
	
	/*
	 *		SMS 발송기록
	 */
	@RequestMapping(value = "/onm/smsSend.do")
	public ModelAndView smsSend(@RequestParam(value = "pg", required = false) String page, SearchBean searchBean,
			ModelMap map) {

		String pg = page;
		// 페이징 관련
		if (pg == null || pg == "") {
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - (PAGE_SIZE - 1);
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));

		// 카운트 값을 페이징에 사용
		int totalCnt = onmService.smsSendCnt(searchBean);

		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);

		// 페이지에 뿌려질 목록을 list에 넣는다.
		List<SMSSend> list = onmService.smsSend(searchBean);

		ModelAndView mav = new ModelAndView();
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("menu1", "ONM");
		mav.addObject("menu2", "[로그] SMS 발송기록");
		mav.addObject("display", "../clip/onm/smsSend.jsp");

		if ("Y".equals(searchBean.getExcelDownload())) {
			mav.addObject("pageNm", "SMS 발송기록");
			mav.addObject("divString", "smsSend");
			mav.setViewName("excelView");
		} else {
			mav.setViewName("/main/index");
		}

		return mav;
	}

	
	
	/*
	 *		엑세스로그 통계
	 */
	@RequestMapping(value = "/onm/statConnect.do")
	public ModelAndView statConnect(@RequestParam(value = "pg", required = false) String page, SearchBean searchBean,
			ModelMap map) {

		String pg = page;
		// 페이징 관련
		if (pg == null || pg == "") {
			pg = "1";
		}
		int endNum = Integer.parseInt(pg) * PAGE_SIZE;
		int startNum = endNum - (PAGE_SIZE - 1);
		map.put("endNum", endNum);
		map.put("startNum", startNum);
		searchBean.setStartNum(startNum);
		searchBean.setEndNum(endNum);
		// 페이징
		BoardPaging paging = new BoardPaging(10, PAGE_SIZE);
		paging.setCurrentPage(Integer.parseInt(pg));

		// 카운트 값을 페이징에 사용
		int totalCnt = onmService.statConnectCnt(searchBean);

		paging.makePagingHtml(totalCnt);
		map.clear();
		map.put("pg", pg);
		map.put("boardPaging", paging);

		// 페이지에 뿌려질 목록을 list에 넣는다.
		List<StatConnect> list = onmService.statConnect(searchBean);

		ModelAndView mav = new ModelAndView();
		mav.addObject("searchBean", searchBean);
		mav.addObject("list", list);
		mav.addObject("menu1", "ONM");
		mav.addObject("menu2", "엑세스로그");
		mav.addObject("display", "../clip/onm/statConnect.jsp");

		if ("Y".equals(searchBean.getExcelDownload())) {
			mav.addObject("pageNm", "엑세스로그");
			mav.addObject("divString", "statConnect");
			mav.setViewName("excelView");
		} else {
			mav.setViewName("/main/index");
		}

		return mav;
	}
	
	
}
