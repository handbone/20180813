/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.onm.bean;

import com.wiz.clip.common.page.PageBean;

public class SearchBean extends PageBean {
	
	// 검색[공통]
	
	private String status;
	private String keywd;
	private String dateType;
	private String startdate; 
	private String enddate;
	
	private String excelDownload;

	// bzadPostLog
	// bzScreenPostLog
	private String user_id;
	
	
	// smsSend
	private String send_userid;
	private String username;
	private String call_ctn;
	private String rcv_ctn;
	
	// statConnect
	private String stat_code;
	
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getKeywd() {
		return keywd;
	}
	public void setKeywd(String keywd) {
		this.keywd = keywd;
	}
	public String getDateType() {
		return dateType;
	}
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getExcelDownload() {
		return excelDownload;
	}
	public void setExcelDownload(String excelDownload) {
		this.excelDownload = excelDownload;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getSend_userid() {
		return send_userid;
	}
	public void setSend_userid(String send_userid) {
		this.send_userid = send_userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getCall_ctn() {
		return call_ctn;
	}
	public void setCall_ctn(String call_ctn) {
		this.call_ctn = call_ctn;
	}
	public String getRcv_ctn() {
		return rcv_ctn;
	}
	public void setRcv_ctn(String rcv_ctn) {
		this.rcv_ctn = rcv_ctn;
	}
	public String getStat_code() {
		return stat_code;
	}
	public void setStat_code(String stat_code) {
		this.stat_code = stat_code;
	}

	
	
	
}
