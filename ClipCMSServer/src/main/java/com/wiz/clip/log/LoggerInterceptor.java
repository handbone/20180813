/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.log;

import java.net.InetAddress;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.wiz.clip.log.service.PvLogService;
import com.wiz.clip.security.bean.CmsUserInfo;

public class LoggerInterceptor extends HandlerInterceptorAdapter {
	protected Log log = LogFactory.getLog(LoggerInterceptor.class);

	@Value("#{config['LOCAL_IP_6']}") String LOCAL_IP_6;
	
	@Autowired
	private PvLogService pvLogService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws java.lang.Exception {
		if (log.isDebugEnabled()) {
			log.debug("======================================          START         ======================================");
			log.debug(" Request URI \t:  " + request.getRequestURI());
			log.debug(" Request Params \t:");
			Enumeration e = request.getParameterNames();			
			String name = null;
			while(e.hasMoreElements()) {
				name=e.nextElement().toString();
				log.debug(name+":"+request.getParameter(name));
			}
		}
		return super.preHandle(request, response, handler);
	}
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws java.lang.Exception {

		try {

			if (null != request.getSession().getAttribute("AgentUserId")) {
				CmsUserInfo user = (CmsUserInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

				String userId = user.getUserid();
				String menu = modelAndView.getModel().get("menu1").toString();
				StringBuffer sb = new StringBuffer(menu);
				if (null != modelAndView.getModel().get("menu2")) {
					sb.append(" > ").append(modelAndView.getModel().get("menu2").toString());
				}
				menu = sb.toString();
				String action = request.getRequestURI();

		 		String USER_IP = request.getRemoteAddr();
				if (USER_IP.equalsIgnoreCase(LOCAL_IP_6)) {
				    InetAddress inetAddress = InetAddress.getLocalHost();
				    String ipAddress = inetAddress.getHostAddress();
				    USER_IP = ipAddress;
				}
				
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("userId", userId);
				map.put("menu", menu);
				map.put("action", action);
				map.put("userIp", USER_IP);

				pvLogService.insertPvLog(map);

			}
		} catch (Exception e) {
			log.debug(" e  " + e.getMessage());
		}
		if (log.isDebugEnabled()) {
			log.debug(
					"======================================          END         ======================================");
		}
	}
}
