/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.wiz.clip.log.service;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.wiz.clip.common.dblog.dao.DbLogDAO;
import com.wiz.clip.log.dao.PvLogDao;

@Service
public class PvLogServiceImpl implements PvLogService {

	@Autowired
	private PvLogDao pvLogDao;
	
	@Override
	public void insertPvLog(Map<String, String> map) {
		pvLogDao.insertPvLog(map);
		
	}

	@Override
	public void insertSMSLog(ModelMap model) {
		pvLogDao.insertSMSLog(model);
		
	}
	
}