/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */
package com.crypto;

import org.apache.commons.dbcp.BasicDataSource;

public class CryptoDataSources extends BasicDataSource{
	 
	@Override
	public void setUrl(String url){
	}
	@Override
	public void setUsername(String username){
	}
	@Override
	public void setPassword(String password){
	}
	
}
